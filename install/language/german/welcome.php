<?php
$content .=
"<p>
<b>vtCMS</b> ist ein Dynamisches OO (Objekt Orientiertes) open source Portal script geschrieben in PHP. Unterstüzt wird es mit einer Anzahl von Datenbanken (aktuell nur mysql), vtCMS ist somit ein ideales CMS für den Aufbau von kleineren und größeren Communities.
</p>
<p>
vtCMS ist freigegeben unter den Bedingungen der <a href='http://www.gnu.org/copyleft/gpl.html' target='_blank'>GNU General Public License (GPL)</a> und ist frei zu verwenden und zu ändern.
Es ist frei, so lange Änderungen, wie Sie durch die Bestimungen des GPL bleiben.
</p>
<u><b>Anforderungen</b></u>
<p>
<ul>
<li>WWW Server (<a href='http://www.apache.org/' target='_blank'>Apache</a>, IIS, Roxen, etc)</li>
<li><a href='http://www.php.net/' target='_blank'>PHP</a> 5.5+</li>
<li><a href='http://www.mysql.com/' target='_blank'>mysql</a> Database 5.0+</li>
</ul>
</p>
<u><b>Vorbereitungen</b></u>
<ul>
<li>Setup WWW Server, PHP und Datenbankrechte.</li>
<li>Erstellen Sie eine Datenbank für Ihre vtCMS Seite.</li>
<li>Bereiten Sie Benutzerkonto vor und vergeben Sie dem Benutzer die rechte zu Datenbank.</li>
<li>Die Verzeichnisse uploads/, cache/ und templates_c/ sowie die Datei mainfile.php müssen beschreibbar sein.</li>
<li>Schalten Sie Cookie und JavaScript in Ihrem Browser auf an.</li>
</ul>
<u><b>Installation</b></u>
<p>
Folgen Sie dem Installations Wizard
</p>
"
?>
