<?php
// $Id: install.php,v 1.4 2003/03/21 13:12:18 w4z004 Exp $
define('_INSTALL_L0','Willkommen im Installations Wizard für XOOPS 2.0');
define('_INSTALL_L70','Bitte setzen Sie die Rechte der mainfile.php so, dass sie beschrieben werden kann ( chmod 777 mainfile.php auf einem UNIX/LINUX Server, auf einem Windows Server -read-only- ). Laden Sie die Seite neu, wenn Sie die Einstellungen geändert haben.');
//define('_INSTALL_L71','Klicken Sie auf den Button, um mit der Installation zu beginnen.');
define('_INSTALL_L1','Öffnen Sie die mainfile.php mit einem Texteditor und suchen Sie folgenden Code in Zeile 31');
define('_INSTALL_L2','Jetzt ändern Sie diese in:');
define('_INSTALL_L3','jetzt, in Zeile 35, Ändern %s in %s');
define('_INSTALL_L4','OK, nach den geänderten Einstellungen versuchen Sie es bitte noch einmal!');
define('_INSTALL_L5','Warnung!');
define('_INSTALL_L6','Es wurde ein Fehler in Ihrer XOOPS_ROOT_PATH Konfiguration in der Zeile 31 der mainfile.php gefunden.');
define('_INSTALL_L7','Ihre Einstellungen: ');
define('_INSTALL_L8','Wir haben gefunden: ');
define('_INSTALL_L9','(Auf MS Plattformen können diese Fehler auftreten, selbst wenn Ihre Einstellungen korrekt sind, wenn das der Fall ist, bestätigen Sie mit dem unteren Button um fortzufahren.)');
define('_INSTALL_L10','Bitte klicken Sie auf den Button um fortzufahren wenn dies OK ist.');
define('_INSTALL_L11','Der Server-Pfad zu Ihrem XOOPS Root Verzeichnis: ');
define('_INSTALL_L12','URL zu Ihrem XOOPS Root Verzeichnis: ');
define('_INSTALL_L13','Wenn die oben genannten Einstellungen korrekt sind, klicken Sie auf den Button, um fortzufahren. <br />(Es werden noch keine Tabellen erstellt)');
define('_INSTALL_L14','Weiter');
define('_INSTALL_L15','Öffnen Sie bitte mainfile.php und geben Sie angeforderte Datenbank Daten ein ');
define('_INSTALL_L16','%s ist der Hostname Ihres Datenbank-Servers.');
define('_INSTALL_L17','%s ist der Username Ihres Datenbank-Accounts.');
define('_INSTALL_L18','%s ist das Passwort welches Verlangt wird, um auf Ihre Datenbank zuzugreifen.');
define('_INSTALL_L19','%s ist der Name Ihrer Datenbank in der die XOOPS Tabellen erstellt werden.');
define('_INSTALL_L20','%s ist der prefix für ihre Tabellen, das wird während der Installation gemacht.');
define('_INSTALL_L21','Es konnte keine Datenbank mit folgendem Namen auf Ihrem Server gefunden werden:');
define('_INSTALL_L22','Wollen Sie diese Datenbank erstellen?');
define('_INSTALL_L23','Ja');
define('_INSTALL_L24','Nein');
define('_INSTALL_L25','Wir haben die folgenden Datenbankinformationen aus Ihrer Konfiguration in der mainfile.php ermittelt. Verbessern Sie diese jetzt, wenn die Einstellungen nicht korrekt sind.');
define('_INSTALL_L26','Datenbank Konfiguration');
define('_INSTALL_L51','<b>Datenbank</b>');
define('_INSTALL_L66','Wählen Sie die zu verwendende Datenbank');
define('_INSTALL_L27','<b>Datenbank Hostname</b>');
define('_INSTALL_L67','Hostname der Datenbank. Wenn Sie unsicher sind, " localhost " arbeitet in den meisten Fällen.');
define('_INSTALL_L28','<b>Datenbank Username</b>');
define('_INSTALL_L65','Ihr Datenbank Useraccount');
define('_INSTALL_L29','<b>Datenbank Name</b>');
define('_INSTALL_L64','Der Name Ihrer Datenbank. Der Wizard wird versuchen diese zu erstellen, falls sie noch nicht existiert.');
define('_INSTALL_L52','<b>Datenbank Passwort</b>');
define('_INSTALL_L68','Das Passwort für Ihre Datenbank');
define('_INSTALL_L30','<b>Tabellen Prefix</b>');
define('_INSTALL_L63','Dieses Prefix wird allen neuen Tabellen vorangestellt, um Namenskonflikte in der Datenbank zu vermeiden. Wenn Sie unsicher sind behalten Sie das Prefix " xoops " bei.');
define('_INSTALL_L54','<b>Brauchen Sie eine permanente Verbindung?</b>');
define('_INSTALL_L69','Standardmässig "Nein". Wählen Sie "Nein" wenn Sie unsicher sind.');
define('_INSTALL_L55','<b>XOOPS Physikalischer Pfad</b>');
define('_INSTALL_L59','Physikalischer Pfad zu Ihrem XOOPS Verzeichnis ohne abschliessenden Slash.');
define('_INSTALL_L56','<b>XOOPS Virtueller Pfad (URL)</b>');
define('_INSTALL_L58','Virtueller Pfad zu Ihrem XOOPS Verzeichnis ohne abschliessenden Slash.');

define('_INSTALL_L31','Datenbank konnte nicht erstellt werden. Sprechen Sie mit ihrem Server Administrator wenn Sie Details brauchen.');
define('_INSTALL_L32','Installation abgeschlossen!');
define('_INSTALL_L33','Drücken Sie <a href="../index.php">hier</a>, um zur Startseite Ihrer neuen Website zu gelangen.');
define('_INSTALL_L35','Wenn Sie irgendwelche Fehler hatten, setzen Sie sich bitte mit dem deutschen Support Team auf <a href="http://www.myxoops.org/">myXOOPS.org</a> in Verbindung.');
define('_INSTALL_L36','Wählen Sie bitte Ihren Admin Namen und Passwort und klicken Sie unten auf den Button, um die Erstellung der Tabellen in der Datenbank zu starten.');
define('_INSTALL_L37','Admin Name');
define('_INSTALL_L38','Admin Email');
define('_INSTALL_L39','Admin Passwort');
define('_INSTALL_L74','Admin Passwort wiederholen');
define('_INSTALL_L40','Erstelle Tabellen');
define('_INSTALL_L41','Bitte gehen Sie zurück und füllen Sie alle erforderlichen Felder aus.');
define('_INSTALL_L42','zurück');
define('_INSTALL_L57','Bitte tragen Sie ein: %s');

// %s is database name
define('_INSTALL_L43','Datenbank %s wurde erstellt!');

// %s is table name
define('_INSTALL_L44','Konnte %s nicht erstellen.');
define('_INSTALL_L45','Tabelle %s erstellt.');

define('_INSTALL_L46','Im Ordner für die Module müßen die folgenden Dateien beschreibbar sein um richtig zu arbeiten. Bitte ändern Sie die Zugriffsrechte. (chmod 666 auf einem UNIX/LINUX Server, oder überprüfe Sie die Einstellungen für die Dateien und überzeugen Sie sich, dass die nur lesen rechte auf einem Windows-Server nicht gesetzt sind)');
define('_INSTALL_L47','weiter');

define('_INSTALL_L53','Bitte bestätigen Sie die folgenden eingetragenen Daten:');

define('_INSTALL_L60','Konnte die mainfile.php nicht öffnen. Bitte überprüfen Sie die Zugriffsrechte der Datei und versuchen Sie es noch mal.');
define('_INSTALL_L61','Konnte nicht in die mainfile.php schreiben. Kontaktiere den Server Administrator für Details.');
define('_INSTALL_L62','Konfigurationsdaten wurden erfolgreich gespeichert. Klicken Sie auf den Button um fortzufahren.');
define('_INSTALL_L72','Die folgenden Verzeichnise müßen mit Schreibrechten vom Server erstellt werden. (zB "chmod 777 Verzeichnis_name" auf einem UNIX/LINUX Server)');
define('_INSTALL_L73','Falsche Email');

// add by haruki
define('_INSTALL_L80','Anleitung');
define('_INSTALL_L81','check Datei Berechtigung');
define('_INSTALL_L82','Checking Datei und Verzeichniss Berechtigung.');
define('_INSTALL_L83','Datei %s ist Nicht beschreibbar.');
define('_INSTALL_L84','Datei %s ist beschreibbar.');
define('_INSTALL_L85','Verzeichnis %s ist NICHT beschreibbar.');
define('_INSTALL_L86','Verzeichnis %s ist beschreibbar.');
define('_INSTALL_L87','Keine Fehler gefunden.');
define('_INSTALL_L89','Allgemeine Einstellungen');
define('_INSTALL_L90','Algemeine Konfiguration');
define('_INSTALL_L91','wiederholen');
define('_INSTALL_L92','speichere Einstellungen');
define('_INSTALL_L93','bearbeite Einstellungen');
define('_INSTALL_L88','Speichere Configurationsdaten.');
define('_INSTALL_L94','Überprüfe Pfad & URL');
define('_INSTALL_L127','Überprüfe Dateipfad & URL Einstelungem..');
define('_INSTALL_L95','Kann physikalischen Pfad zu Ihrem XOOPS Verzeichnis nicht finden.');
define('_INSTALL_L96','Es gibt einen Konflikt zwischen dem ermittelten physikalischen Pfad (%s) und Ihren Eingaben');
define('_INSTALL_L97','<b>Physikalischer Pfad</b> ist in Odnung.');

define('_INSTALL_L99','<b>Physikalischer Pfad</b> muß ein Verzeichnis sein.');
define('_INSTALL_L100','<b>Virtueller Pfad</b> Ihre Eingabe ist die korrekte URL.');
define('_INSTALL_L101','<b>Virtueller Pfad</b> Ihre Eingabe ist keine korrekte URL.');
define('_INSTALL_L102','Überprüfe Datenbank Einstellungen');
define('_INSTALL_L103','Neustart');
define('_INSTALL_L104','Überprüfe Datenbank');
define('_INSTALL_L105','Versuche Datenbank zu erstellen');
define('_INSTALL_L106','Kann nicht zum Datenbankserver verbinden.');
define('_INSTALL_L107','Überprüfen Sie bitte den Datenbakserver und seine Konfiguration.');
define('_INSTALL_L108','Verbindung zum Datenbankserver ist OK.');
define('_INSTALL_L109','Datenbank %s existiert nicht.');
define('_INSTALL_L110','Datenbank %s existiert und Verfügbar.');
define('_INSTALL_L111','Datenbank-Verbindung ist OK.<br />Klicke auf den button um die Tabelen in der Datenbank zu erstellen.');
define('_INSTALL_L112','Administrator Einstellungen');
define('_INSTALL_L113','Tabelle %s gelöscht.');
define('_INSTALL_L114','kann Tabellen nicht erstellen.');
define('_INSTALL_L115','Tabellen in der Datenbank erstellt.');
define('_INSTALL_L116','füge Daten ein');
define('_INSTALL_L117','Fertig');

define('_INSTALL_L118','Fehler beim erstellen der Tabelle %s.');
define('_INSTALL_L119','%d eingetragen in die Tabele %s.');
define('_INSTALL_L120','Fehler beim Eintargen von %d in die Tabelle %s.');

define('_INSTALL_L121','Konstante %s gespeichert in %s.');
define('_INSTALL_L122','Fehler beim schreiben der Konstante %s.');

define('_INSTALL_L123','Datei %s im cache/ Verzeichniss gespeichert.');
define('_INSTALL_L124','Fehler beim speichern von Datei %s ins cache/ Verzeichniss.');

define('_INSTALL_L125','Datei %s überschrieben von %s.');
define('_INSTALL_L126','konnte Datei nicht schreiben %s.');

define('_INSTALL_L130','Der Installateur hat Tabellen für XOOPS 1.3.x in Ihrer Datenbank ermittelt.<br />Der Installateur versucht jetzt, Ihre Datenbank zu XOOPS2 upzudaten .');
define('_INSTALL_L131','Tabellen für XOOPS2 bestehen bereits in Ihrer Datenbank .');
define('_INSTALL_L132','update Tabellen');
define('_INSTALL_L133','Tabellen upgedated.');
define('_INSTALL_L134','Updatefehler in Tabelle %s.');
define('_INSTALL_L135','Tabellenupdate fehlgeschlagen.');
define('_INSTALL_L136','Die Tabellen der Datenbank Upgedated.');
define('_INSTALL_L137','Update Modules');
define('_INSTALL_L138','Update Comments');
define('_INSTALL_L139','Update Avatars');
define('_INSTALL_L140','Update Smilies');
define('_INSTALL_L141','Der Installateur aktualisiert jetzt jedes Modul, um mit XOOPS2 zu arbeiten .<br />Überprüfen Sie alles Module in ihrem XOOPS2 Ordner, Gegebenenfalz uplopaden Sie diese.<br />Das Update kann einige Zeit in Anspruch nehmen');
define('_INSTALL_L142','Updating Modules..');
define('_INSTALL_L143','Die Konfigurations Daten werden jetzt aktualisiert.');
define('_INSTALL_L144','Update Konfiguration');
define('_INSTALL_L145','Kommentare werden geschrieben.');
define('_INSTALL_L146','Kommentare konnten nicht geschrieben werden.');
define('_INSTALL_L147','Updating Kommentare..');
define('_INSTALL_L148','Update komplett.');
define('_INSTALL_L149','Die Installation versucht jetzt ihre Kommentare von der Vers. 1.3.x in die Vers. 2.0 zu konvertieren, dieses kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L150','Die Installation versucht jetzt Ihre Smilys und Rang Images zu XOOPS2 konvertieren, dieses kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L151','Die Installation versucht jetzt die User Avatare in XOOPS2 zu konvertieren,<br /> dieses kann einige Zeit in Anspruch nehmen.');
define('_INSTALL_L155','Updating Smiley/Rang Images..');
define('_INSTALL_L156','Updating User Avatar Images..');
define('_INSTALL_L157','Benutzergruppen zurückstellen');
define('_INSTALL_L158','Gruppen in 1.3.x');
define('_INSTALL_L159','Webmasters');
define('_INSTALL_L160','Register Users');
define('_INSTALL_L161','Anonymous Users');
define('_INSTALL_L162','Sie müssen die Default Gruppe für jede auswählen.');
define('_INSTALL_L163','Tabelle %s markiert.');
define('_INSTALL_L164','Fehler beim löschen der Tabelle %s.');
define('_INSTALL_L165','Diese Seite ist wegen Wartungs/Update Arbeiten geschlossen. Bitte besuchen Sie uns zu einem späterem Zeitpunkt nocheinmal.');

// %s is filename
define('_INSTALL_L152','Kann nicht öffnen %s.');
define('_INSTALL_L153','Konnte nicht updaten %s.');
define('_INSTALL_L154','%s Updated.');

// ====| Getting Ready For Unicode (UTF-8) conversion - hyperclock |==== \\
#define('_INSTALL_CHARSET', 'ISO-8859-1');
define('_INSTALL_CHARSET', 'UTF-8');

?>
