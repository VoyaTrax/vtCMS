<?php
// $Id: install2.php,v 1.4 2003/03/21 13:12:19 w4z004 Exp $
define('_INSTALL_WEBMASTER','Webmaster');
define('_INSTALL_WEBMASTERD','Webmaster dieser Seite');
define('_INSTALL_REGUSERS','Registrierte User');
define('_INSTALL_REGUSERSD','Registrierte Usergruppe');
define('_INSTALL_ANONUSERS','Anonymous User');
define('_INSTALL_ANONUSERSD','Anonymous Usergruppe');

define('_INSTALL_ANON','Anonymous');

define('_INSTALL_DISCLMR', 'Diese Seite übernimmt keinerlei Haftung für Schäden, die durch das System (die Internetseite) oder die angeboten Dateien entstehen. Alle Dateien sind auf Viren geprüft.
Der Benutzer wird hiermit darauf hingewiesen, selber die Dateien auf Viren u.ä. zu prüfen.
Eine Garantie für die Sicherheit der Dateien kann nicht gegeben gegeben werden, da diese Dateien meisst nicht aus unserer Produktion stammen.
Auch kann keine Garantie übernommen werden für die Erreichbarkeit unseres Services und von Dateien.
Das Herunterladen, die Installation und die Verwendung der Programme die unter dieser Seite aufgeführt werden, erfolgt auf eigene Gefahr!
Der Betreiber übernimmt keine Gewährleistung oder Haftung für etwaige Schäden,
Folgeschäden oder Ausfälle, die entstehen können.
Erklärung: Bei unseren externen Links handelt es sich um eine subjektive Auswahl von Verweisen auf andere Internetseiten.
Für den Inhalt dieser Seiten sind die jeweiligen Betreiber / Verfasser selbst verantwortlich und haftbar.
Von etwaigen illegalen, persönlichkeitsverletzenden, moralisch oder ethisch anstößigen Inhalten distanzieren wir uns in aller Deutlichkeit.
Bitte Informieren Sie uns, wenn wir auf ein solches Angebot linken sollten.
Diese Seite ist als Inhaltsanbieter für die eigenen Inhalte, die er zur Nutzung bereithält, nach den allgemeinen Gesetzen verantwortlich.
Von diesen eigenen Inhalten sind Querverweise (Links) auf die von anderen Anbietern bereitgehaltenen Inhalte zu unterscheiden.
Für diese fremden Inhalte ist diese Seite nur dann verantwortlich,
wenn von ihnen (d.h. auch von einem rechtswidrigen bzw. strafbaren Inhalt) positive Kenntnis vorliegt und es technisch möglich und zumutbar ist,
deren Nutzung zu verhindern (§5 Abs.2 TDG).
Bei Links handelt es sich allerdings stets um lebende (dynamische) Verweisungen.
Diese Seite hat bei der erstmaligen Verknüpfung zwar den fremden Inhalt daraufhin überprüft,
ob durch ihn eine mögliche zivilrechtliche oder strafrechtliche Verantwortlichkeit ausgelöst wird.
Der Inhaltsanbieter ist aber nach dem TDG nicht dazu verpflichtet, die Inhalte, auf die er in seinem Angebot verweist,
ständig auf Veränderungen zu überprüfen, die eine Verantwortlichkeit neu begründen könnten.
Erst wenn er feststellt oder von anderen darauf hingewiesen wird, daß ein konkretes Angebot, zu dem er einen Link bereitgestellt hat,
eine zivil- oder strafrechtliche Verantwortlichkeit auslöst, wird er den Verweis auf dieses Angebot aufheben,
soweit ihm dies technisch möglich und zumutbar ist.
Die technische Möglichkeit und Zumutbarkeit wird nicht dadurch beeinflußt, daß auch nach Unterbindung des Zugriffs von dieser Seite,
von anderen Servern aus auf das rechtswidrige oder strafbare Angebot zugegriffen werden kann.
Salvatorische Klausel:
Sollte aus irgendwelchen Gründen eine der vorstehenden Bedingungen ungültig sein, so wird die Wirksamkeit der anderen Bestimmungen davon nicht berührt.
Der Download von Programmen von oben genannter Seite erfolgt auf eigene Gefahr.
Sämtliche hier angebotenen Downloads werden keiner Virenprüfung unterzogen.
Eine Haftung seitens dieser Seite für Schäden und Beeinträchtigungen durch Computerviren ist ausgeschlossen. Schadenersatzansprüche sind ausgeschlossen.
Dies gilt auch für Ansprüche auf Ersatz von Folgeschäden wie Datenverlust.');

// Begin TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock
define("_INSTALL_IMPRINT","Impressum");
define("_INSTALL_PRIVACY","Datenschutzerklärung");
define("_INSTALL_TERMS","Nutzungsbedingungen");
define("_INSTALL_ABOUT","Über Uns");
define("_INSTALL_DISCLAIMER","Haftungsausschluss");
// End TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock
?>
