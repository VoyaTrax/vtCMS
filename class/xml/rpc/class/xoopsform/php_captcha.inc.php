<?php
/***************************************************************/
/* Adaptation system and fusion by Kern Christian (PinMaster)  */
/* Last Updated: 18-03-06									   */
/***************************************************************/
/* PhpCaptcha                                                  */
/* Copyright � 2005-2006 E.J.Eliot - http://www.ejeliot.com/   */
/* Last Updated:  12th March 2006                              */
/* Disclaimer: The author accepts no responsibility for        */
/* problems arising from the use of this class. The CAPTCHA    */
/* generated is not guaranteed to be unbreakable               */
/***************************************************************/
/* Class : SecurityImage 1.2								   */
/* Author: DuGris aka L. Jen <http://www.comite-citoyen.org>   */
/* Email : DuGris@wanadoo.fr								   */
/***************************************************************/


class PhpCaptcha extends XoopsFormElement {
	var $oImage;
	var $aFonts;
	var $iWidth;
	var $iHeight;
	var $iNumChars;
	var $iNumLines;
	var $iNumCircles;
	var $iSpacing;
	var $bCharShadow;
	var $sOwnerText;
	var $aCharSet;
	var $bCaseInsensitive;
	var $sBackgroundType;
	var $vBackgroundImages;
	var $iMinFontSize;
	var $iMaxFontSize;
	var $bUseColour;
	var $sFileType;
	var $iCacheSize;
	var $sCode = '';
	var $sNameFile = '';

	function __construct($caption, $ConfigCaptcha) {
		if (opendir( XOOPS_ROOT_PATH.'/cache/captcha' ) == FALSE)	// create cache, if not exist
			mkdir (XOOPS_ROOT_PATH.'/cache/captcha', 0755);

		if (!extension_loaded('gd')) {
			$this->setCaption($caption."<br />"._US_CAPTCHA_GD2);
		}
		else {
			// get parameters
			$this->SetWidth($ConfigCaptcha['captcha_width_img']);
			$this->SetHeight($ConfigCaptcha['captcha_height_img']);
			$this->SetNumChars($ConfigCaptcha['captcha_num_chars']);
			$this->SetNumLines($ConfigCaptcha['captcha_num_lines']);
			$this->SetNumCircles($ConfigCaptcha['captcha_num_circles']);
			$this->DisplayShadow($ConfigCaptcha['captcha_char_shadow']);
			$this->SetOwnerText($ConfigCaptcha['captcha_owner_text']);
			$this->SetCharSet($ConfigCaptcha['captcha_char_set']);
			$this->CaseInsensitive($ConfigCaptcha['captcha_insensitive']);
			$this->SetBackgroundImagesList($ConfigCaptcha['captcha_bckgrd_path']);
			$this->SetFontsList($ConfigCaptcha['captcha_fonts_path']);
			$this->SetMinFontSize($ConfigCaptcha['captcha_min_font_size']);
			$this->SetMaxFontSize($ConfigCaptcha['captcha_max_font_size']);
			$this->UseColour($ConfigCaptcha['captcha_use_color']);
			$this->SetFileType($ConfigCaptcha['captcha_file_type_img']);
			$this->SetCacheSize($ConfigCaptcha['captcha_cache']);

			$this->sBackgroundType = $ConfigCaptcha['captcha_method'];
			$this->sNameFile = 'cache/captcha/secure'.substr(md5($_SERVER['HTTP_USER_AGENT'].mt_rand(0,255)), 0, $this->iNumChars).'.'.$ConfigCaptcha['captcha_file_type_img'];
			$this->Create();
			$this->setName('user_captcha');
			$this->setCaption($caption."<br /><img src='".XOOPS_URL."/".$this->sNameFile."' alt='verification' />");
		}
	}

	function CalculateSpacing() {
		$this->iSpacing = (int)($this->iWidth / $this->iNumChars);
	}

	function SetCacheSize($iCache) {
		$this->iCacheSize = iCacheSize;
		if ($this->iCacheSize < 512) $this->iCacheSize = 512;
	}

	function SetWidth($iWidth) {
		$this->iWidth = $iWidth;
		if ($this->iWidth > 500) $this->iWidth = 500; // to prevent perfomance impact
		$this->CalculateSpacing();
	}

	function SetHeight($iHeight) {
		$this->iHeight = $iHeight;
		if ($this->iHeight > 200) $this->iHeight = 200; // to prevent performance impact
	}

	function SetNumChars($iNumChars) {
		$this->iNumChars = intval($iNumChars);
		$this->CalculateSpacing();
	}

	function SetNumLines($iNumLines) {
		$this->iNumLines = $iNumLines;
	}

	function SetNumCircles($iNumCircles) {
		$this->iNumCircles = $iNumCircles;
	}

	function DisplayShadow($bCharShadow) {
		$this->bCharShadow = $bCharShadow;
	}

	function SetOwnerText($sOwnerText) {
		$this->sOwnerText = $sOwnerText;
	}

	function SetCharSet($vCharSet) {
		if ($vCharSet != '') {
			// split items on commas
			$aCharSet = explode('|', $vCharSet);

			// initialise array
			$this->aCharSet = array();

			// loop through items
			foreach ($aCharSet as $sCurrentItem) {
				// a range should have 3 characters, otherwise is normal character
				if (strlen($sCurrentItem) == 3) {
					// split on range character
					$aRange = explode('-', $sCurrentItem);

					// check for valid range
					if (count($aRange) == 2 && $aRange[0] < $aRange[1]) {
						// create array of characters from range
						$aRange = range($aRange[0], $aRange[1]);

						// add to charset array
						$this->aCharSet = array_merge($this->aCharSet, $aRange);
					}
				} else {
					$this->aCharSet[] = $sCurrentItem;
				}
			}
		}
	}

	function SetFontsList($FontPath) {
		$DirectoryFonts = XOOPS_ROOT_PATH.'/'.$FontPath;
		if (preg_match('@\.ttf$$@', $DirectoryFonts, $tmp)){
			$MyFonts[] = $DirectoryFonts;
		} else {
		    $handle = opendir( $DirectoryFonts );
			while (false !== ($file = readdir($handle))) {
				if ( preg_match('@\.ttf$$@', $file, $tmp) ) {
					$MyFonts[] = $DirectoryFonts . $file;
				}
		    }
		}
	    $this->aFonts = $MyFonts;
    }

	function SetBackgroundImagesList($BackgroundPath) {
	    $DirectoryBckgrd = XOOPS_ROOT_PATH.'/'.$BackgroundPath;
		if ((preg_match('@\.jpg$$@', $DirectoryBckgrd, $tmp)) || (preg_match('@\.jpeg$$@', $DirectoryBckgrd, $tmp))){
			$MyBackground[] = $DirectoryBckgrd;
		} else {
		    $handle = opendir( $DirectoryBckgrd );
			while (false !== ($file = readdir($handle))) {
				if ((preg_match('@\.jpg$$@', $file, $tmp)) || (preg_match('@\.jpeg$$@', $file, $tmp))){
					$MyBackground[] = $DirectoryBckgrd . $file;
				}
		    }
		}
        $this->vBackgroundImages = $MyBackground;
    }

	function CaseInsensitive($bCaseInsensitive) {
		$this->bCaseInsensitive = $bCaseInsensitive;
	}

	function SetMinFontSize($iMinFontSize) {
		$this->iMinFontSize = $iMinFontSize;
	}

	function SetMaxFontSize($iMaxFontSize) {
		$this->iMaxFontSize = $iMaxFontSize;
	}

	function UseColour($bUseColour) {
		$this->bUseColour = $bUseColour;
	}

	function SetFileType($sFileType) {
		// check for valid file type
		if (in_array($sFileType, array('gif', 'png', 'jpeg'))) {
			$this->sFileType = $sFileType;
		} else {
			$this->sFileType = 'jpeg';
		}
	}

	function DrawCircles() {
		$this->oImage = imagecreate($this->iWidth,$this->iHeight);
        imagecolorallocate ($this->oImage, 255, 255, 255);

		for ($i = 0; $i < $this->iNumCircles; $i++) {
			// allocate colour
			if ($this->bUseColour) {
				$iCircleColour = imagecolorallocate($this->oImage, mt_rand(190, 250), mt_rand(190, 250), mt_rand(190, 250));
			} else {
				$iRandColour = mt_rand(190, 250);
				$iCircleColour = imagecolorallocate($this->oImage, $iRandColour, $iRandColour, $iRandColour);
			}
			imagefilledellipse($this->oImage, mt_rand(0,$this->iWidth-10), mt_rand(0,$this->iHeight-3), mt_rand(10,50), mt_rand(20,70),$iCircleColour);
		}
		if ($this->sOwnerText != '') {
			$this->DrawOwnerText();
		}
    }

	function DrawLines() {
		$this->oImage = imagecreate($this->iWidth,$this->iHeight);
        imagecolorallocate ($this->oImage, 255, 255, 255);
		if ($this->sOwnerText != '') {
			$this->DrawOwnerText();
		}
		for ($i = 0; $i < $this->iNumLines; $i++) {
			// allocate colour
			if ($this->bUseColour) {
				$iLineColour = imagecolorallocate($this->oImage, mt_rand(190, 250), mt_rand(190, 250), mt_rand(190, 250));
			} else {
				$iRandColour = mt_rand(190, 250);
				$iLineColour = imagecolorallocate($this->oImage, $iRandColour, $iRandColour, $iRandColour);
			}
			// draw line
			imageline($this->oImage, mt_rand(0, $this->iWidth), mt_rand(0, $this->iHeight), mt_rand(0, $this->iWidth), mt_rand(0, $this->iHeight), $iLineColour);
		}
	}

	function DrawOwnerText() {
		// allocate owner text colour
		$iBlack = imagecolorallocate($this->oImage, 0, 0, 0);
		// get height of selected font
		$iOwnerTextHeight = imagefontheight(2);
		// calculate overall height
		$iLineHeight = $this->iHeight - $iOwnerTextHeight - 4;

        // draw line above text to separate from CAPTCHA
		imageline($this->oImage, 0, $iLineHeight, $this->iWidth, $iLineHeight, $iBlack);

		// write owner text
		imagestring($this->oImage, 2, 3, $this->iHeight - $iOwnerTextHeight - 3, $this->sOwnerText, $iBlack);

		// reduce available height for drawing CAPTCHA
		$this->iHeight = $this->iHeight - $iOwnerTextHeight - 5;
	}

	function GenerateCode() {
		// reset code
		$this->sCode = '';

		// loop through and generate the code letter by letter
		for ($i = 0; $i < $this->iNumChars; $i++) {
			if (count($this->aCharSet) > 0) {
				// select random character and add to code string
				$this->sCode .= $this->aCharSet[array_rand($this->aCharSet)];
			} else {
				// select random character and add to code string
				$this->sCode .= chr(mt_rand(65, 90));
			}
		}

		// save code in session variable
		if ($this->bCaseInsensitive) {
			$_SESSION[$ConfigCaptcha['captcha_session']] = md5(strtoupper($this->sCode));
		} else {
			$_SESSION[$ConfigCaptcha['captcha_session']] = md5($this->sCode);
		}
	}

	function DrawCharacters() {
		// loop through and write out selected number of characters
		for ($i = 0; $i < strlen($this->sCode); $i++) {
			// select random font
			$sCurrentFont = $this->aFonts[array_rand($this->aFonts)];
			// select random colour
			if ($this->bUseColour) {
				$iTextColour = imagecolorallocate($this->oImage, mt_rand(0, 100), mt_rand(0, 100), mt_rand(0, 100));

				if ($this->bCharShadow) {
					// shadow colour
					$iShadowColour = imagecolorallocate($this->oImage, mt_rand(0, 100), mt_rand(0, 100), mt_rand(0, 100));
				}
			} else {
				$iRandColour = mt_rand(0, 100);
				$iTextColour = imagecolorallocate($this->oImage, $iRandColour, $iRandColour, $iRandColour);

				if ($this->bCharShadow) {
			  		// shadow colour
			  		$iRandColour = mt_rand(0, 100);
			  		$iShadowColour = imagecolorallocate($this->oImage, $iRandColour, $iRandColour, $iRandColour);
				}
			}

			// select random font size
			$iFontSize = mt_rand($this->iMinFontSize, $this->iMaxFontSize);

			if ($sCurrentFont == "") {
				// calculate character starting coordinates
				$iX = $this->iSpacing / 4 + $i * $this->iSpacing;
				$iY = ($this->iHeight - imagefontHeight(5)) / 2;

				// write text to image
				imagestring($this->oImage, 5, $iX, $iY, $this->sCode[$i], $iTextColour);

				if ($this->bCharShadow) {
					$iRandOffsetX = mt_rand(-5, 5);
					$iRandOffsetY = mt_rand(-5, 5);

					imagestring($this->oImage, 5, $iX + $iRandOffsetX, $iY + $iRandOffsetY, $this->sCode[$i], $iShadowColour);;
				}
			} else {
				// select random angle
				$iAngle = mt_rand(-30, 30);

				// get dimensions of character in selected font and text size
				$aCharDetails = imageftbbox($iFontSize, $iAngle, $sCurrentFont, $this->sCode[$i], array());

				// calculate character starting coordinates
				$iX = $this->iSpacing / 4 + $i * $this->iSpacing;
				$iCharHeight = $aCharDetails[2] - $aCharDetails[5];
				$iY = $this->iHeight / 2 + $iCharHeight / 4;

				// write text to image
				imagefttext($this->oImage, $iFontSize, $iAngle, $iX, $iY, $iTextColour, $sCurrentFont, $this->sCode[$i], array());

				if ($this->bCharShadow) {
					$iOffsetAngle = mt_rand(-30, 30);

					$iRandOffsetX = mt_rand(-5, 5);
					$iRandOffsetY = mt_rand(-5, 5);

					imagefttext($this->oImage, $iFontSize, $iOffsetAngle, $iX + $iRandOffsetX, $iY + $iRandOffsetY, $iShadowColour, $sCurrentFont, $this->sCode[$i], array());
				}
			}
		}
	}

	function WriteFile() {
	    $directory_path = XOOPS_ROOT_PATH."/cache/captcha/";
	    $directory_size = 0;
		$handle = opendir( $directory_path );
		while (false !== ($filename = readdir($handle))) {
			$file = $directory_path.$filename;
			$directory_size += filesize($file);
			$file_list[] = $file;
	    }
		if ($directory_size/1024 > $this->iCacheSize)
		{
			$i = 2;
			while ($file_list[$i] != "") {unlink($file_list[$i]); $i++;}

		}

		switch ($this->sFileType) {
			case 'gif':
				imagegif($this->oImage, XOOPS_ROOT_PATH."/".$this->sNameFile);
			break;
			case 'png':
				imagepng($this->oImage, XOOPS_ROOT_PATH."/".$this->sNameFile);
			break;
			default:
				imagejpeg($this->oImage, XOOPS_ROOT_PATH."/".$this->sNameFile);
		}
	}

	function BackgroundFile() {
		if ( count($this->vBackgroundImages) != 0) {
			$this->oImage = imagecreatetruecolor($this->iWidth, $this->iHeight);

			// Le fond de l'image est en blanc
			$background = imagecolorallocate($this->oImage, 255, 255, 255);
            imagefilledrectangle($this->oImage, 0, 0, $this->iWidth, $this->iHeight, $background);

			// create background image
			if (is_array($this->vBackgroundImages) ) {
				$iRandImage = array_rand($this->vBackgroundImages);
				$oBackgroundImage = imagecreatefromjpeg($this->vBackgroundImages[$iRandImage]);
			} else {
				$oBackgroundImage = imagecreatefromjpeg($this->vBackgroundImages);
			}
			if ($this->sOwnerText != '') {
				$this->DrawOwnerText();
			}

			// copy background image
			imagecopyresized($this->oImage, $oBackgroundImage, 0, 0, 0, 0, $this->iWidth, $this->iHeight, ImageSX($oBackgroundImage), ImageSY($oBackgroundImage));

			// free memory used to create background image
			imagedestroy($oBackgroundImage);
        } else {
	        $this->DrawCircles();
        }
    }

    function Create() {
		if ($this->sBackgroundType == 'captcha_rand'){
			$sFond_Type =array_rand(array('captcha_line', 'captcha_img', 'captcha_circle'));
		} else {
			$sFond_Type = $this->sBackgroundType;
		}
    	switch ($sFond_Type) {
        	case 'captcha_ligne':
	        	$this->DrawLines();
            break;
        	case 'captcha_img':
		        $this->BackgroundFile();
            break;
        	case 'captcha_circle':
            default:
		        $this->DrawCircles();
            break;
        }
		$this->GenerateCode();
		$this->DrawCharacters();

		// write out image to file or browser
		$this->WriteFile($this->sNameFile);

		// free memory used in creating image
		imagedestroy($this->oImage);

		return true;
	}

	function render(){
		if (!extension_loaded('gd'))
		{
			$_SESSION[$ConfigCaptcha['captcha_session']] = '';
			return _US_CAPTCHA_GD2STP;
		}
		return "<input type='text' name='user_captcha' id='user_captcha' size='".$this->iNumChars."' maxlength='".$this->iNumChars."' value='' />";
	}
}
?>
