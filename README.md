vtCMS
=====

VoyaTrax CMS (vtCMS) is a hacked, modded, fixed....whatever Content Management
System based on XOOPS. We start with version  2.0.18.2
(released around 2008) and will add modules, themes and hacks to fit modern case usage.


Changes, Fixes, Hacks
--------

All changes, fixes and hacks will be documented as the project grows. They will
be in the changelogs.


Usage
-----

Will be document in the [wikis](https://gitlab.com/VoyaTrax/vtCMS//wikis/home) ~~as well as on the main [website](http://www.voyatrax.org)~~.


License
-------

The licensing will be upgraded from GPLv2 to GPLv3.


vtCMS Requirements
------------

This is being developed with the following configuration.

  * PHP ~~5.6.x~~ 7.2+
  * MySQL ~~5.0.x~~
    * mysql  Ver 15.1 Distrib 10.1.37-MariaDB
  * Apache2 Webserver


vtCMS Branches
----------

 * _Master Branch_ == The area where we put everything together until we get to a point of having an installable system. Then we will "_point_" branch that.
    * We are working on _PHP 7.2_ on this Branch

 * _5.6_ == The area where development further development stops. In this case it's early development for the 1.x version of vtCMS.
    * ***WORKS ON PHP 5.6 (maybe below-not tested) ONLY***



 Releases
 --------

 Releases will be done as the development matures to a (semi-)stable state. What
 this means is if everything installs and work well enough, we release.

 Releases are done in the _Semantic Versioning Method_, as can be followed @ http://semver.org/

   * **Semantic Versioning Summary

  Given a version number MAJOR.MINOR.PATCH, increment the:

  - MAJOR version when you make incompatible API changes,
  - MINOR version when you add functionality in a backwards-compatible manner, and
  - PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.
