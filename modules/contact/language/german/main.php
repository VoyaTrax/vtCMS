<?php
//%%%%%%		File Name contact.php 		%%%%%

define('_CT_CONTACTFORM','Setzen Sie sich mit uns in Verbindung.');
define('_CT_THANKYOU','Vielen Dank für Ihr Interesse an dieser Seite!');
define('_CT_NAME','Name');
define('_CT_EMAIL','Email');
define('_CT_URL','URL');
define('_CT_ICQ','ICQ');
define('_CT_COMPANY','Firma');
define('_CT_LOCATION','Land');
define('_CT_COMMENTS','Betreff');
define('_CT_SUBMIT','Absenden');
define('_CT_YOURMESSAGE','Ihre Nachricht:');
define('_CT_WEBMASTER','Webmaster');
define('_CT_HELLO','Hallo %s,');
define('_CT_THANKYOUCOMMENTS','Vielen Dank für Ihr Interesse an %s.');
define('_CT_SENTTOWEBMASTER','Ihre Nachricht ist zum Webmaster von %s gesendet worden.');
define('_CT_SUBMITTED','%s schickt Ihnen die folgende Information:');
define('_CT_MESSAGESENT','Nachricht an %s gesendet');
define('_CT_SENTASCONFIRM','Ihre Nachricht wurde dazu gesendet an: %s als eine Bestätigungs-Email.');

?>
