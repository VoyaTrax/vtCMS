<?php
//%%%%%%	File Name memberslist.php 	%%%%%

define('_MM_WEHAVESOFAR','Wir haben im Moment  <b>%s</b> registrierte Mitglieder.');
define('_MM_SEARCH','Mitglieder suchen');
define('_MM_AVATAR','Avatar');
define('_MM_REALNAME','Real Name');
define('_MM_REGDATE','Registrations Datum');
define('_MM_EMAIL','Email');
define('_MM_PM','PM');
define('_MM_URL','URL');
define('_MM_PREVIOUS','vorherige');
define('_MM_NEXT','nächste');
define('_MM_USERSFOUND','%s Mitglied(er) gefunden');

define('_MM_TOTALUSERS', 'Wir haben insgesammt: %s Mitglieder');
define('_MM_NOFOUND','Keine Mitglieder gefunden zu Ihrer Suchanfrage');
define('_MM_UNAME','Mitglieds Name');
define('_MM_ICQ','ICQ');
define('_MM_AIM','AIM');
define('_MM_YIM','YIM');
define('_MM_MSNM','MSNM');
define('_MM_LOCATION','Wohnort enthält');
define('_MM_OCCUPATION','Beruf enthält');
define('_MM_INTEREST','Interessen enthält');
define('_MM_URLC','URL enthält');
define('_MM_LASTLOGMORE','Letzter Besuch ist mehr als <span style="color:#ff0000;">X</span> Tage her');
define('_MM_LASTLOGLESS','Letzter Besuch ist kleiner als <span style="color:#ff0000;">X</span> Tage her');
define('_MM_REGMORE','Registrations Datum ist mehr als <span style="color:#ff0000;">X</span> Tage her');
define('_MM_REGLESS','Registrations Datum ist kleiner als<span style="color:#ff0000;">X</span> Tage her');
define('_MM_POSTSMORE','Anzahl der Beiträge ist größer als <span style="color:#ff0000;">X</span>');
define('_MM_POSTSLESS','Anzahl der Beiträge ist kleiner als <span style="color:#ff0000;">X</span>');
define('_MM_SORT','Sortieren nach');
define('_MM_ORDER','Sortieren');
define('_MM_LASTLOGIN','Letzter Besuch');
define('_MM_POSTS','Anzahl der Beiträge');
define('_MM_ASC','Aufsteigend sortieren');
define('_MM_DESC','Absteigend sortieren');
define('_MM_LIMIT','Anzahl der Mitglieder pro Seite');
define('_MM_RESULTS', 'Suchergebnisse');
?>
