<?php
// Module Info

// The name of this module
define('_MI_MEMBERS_NAME','Mitglieder');

// A brief description of this module
define('_MI_MEMBERS_DESC','Zeigt eine Liste aller Mitglieder');

// Names of blocks for this module (Not all module has blocks)
define('_MI_MEMBERS_BNAME1','Top posters');
define('_MI_MEMBERS_BNAME2','Neustes Mitglied');
?>