਀䐀⸀䨀⸀⠀瀀栀瀀瀀瀀⤀ 栀琀琀瀀㨀⼀⼀砀漀漀瀀猀⸀漀爀最⸀挀渀 
===========================================================================================================਀ 
xlanguage, eXtensible Xoops Multilingual Content and Encoding Management ਀ 
਀䄀瀀瀀氀椀挀愀戀氀攀 
---------਀䄀渀礀 瘀攀爀猀椀漀渀 漀昀 堀伀伀倀匀 愀渀搀 愀渀礀 瘀攀爀猀椀漀渀 漀昀 愀渀礀 䴀伀䐀唀䰀䔀 眀椀琀栀 愀渀礀 吀䠀䔀䴀䔀⸀ 
NEW in 3.02 for XOOPS 2.4.0: no hacks of api.php needed anymore ਀ 
਀䔀愀猀礀 琀漀 甀猀攀 
-----------਀㄀ 䄀氀氀 礀漀甀 渀攀攀搀 搀漀 椀猀 琀漀 椀渀猀攀爀琀 伀一䰀夀 伀一䔀 䰀䤀一䔀 椀渀琀漀 挀漀洀洀漀渀⸀瀀栀瀀 愀渀搀 椀渀猀琀愀氀氀 ∀砀氀愀渀最甀愀最攀∀ 
2 Do NOT need to modify/hack any other XOOPS core files or any module਀ 
਀倀漀眀攀爀昀甀氀 攀渀漀甀最栀 琀漀 洀攀攀琀 礀漀甀爀 爀攀焀甀椀爀攀洀攀渀琀猀 
-----------------------------------------਀㄀ 䌀漀甀氀搀 栀愀渀搀氀攀 愀猀 洀愀渀礀 氀愀渀最甀愀最攀猀 漀昀 挀漀渀琀攀渀琀 愀猀 礀漀甀 眀愀渀琀 
2 Could handle different charset of a selected language਀㌀ 䌀漀甀氀搀 栀愀渀搀氀攀 洀甀氀琀椀氀椀渀最甀愀氀 挀漀渀琀攀渀琀 愀渀礀眀栀攀爀攀 漀渀 礀漀甀爀 猀椀琀攀Ⰰ 椀渀 愀 洀漀搀甀氀攀Ⰰ 愀 瀀栀瀀 昀椀氀攀Ⰰ 愀渀 栀琀洀氀 瀀愀最攀 漀爀 愀 琀栀攀洀攀✀猀 栀愀爀搀挀漀搀攀搀 挀漀渀琀攀渀琀 
4 Compatible with content cache਀㔀 䄀甀琀漀洀愀琀椀挀 搀攀琀攀挀琀椀漀渀 漀昀 甀猀攀爀 戀爀漀眀猀攀爀✀猀 氀愀渀最甀愀最攀 瀀爀攀昀攀爀攀渀挀攀 
਀ 
使用指南਀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ 
1 按正常模式和步驟安裝 "xlanguage"਀ 
2 在XOOPS/include/common.php中插入一行 (只有 XOOPS 2.4.0 之前版本需設定)਀ऀऀ椀渀挀氀甀搀攀开漀渀挀攀 堀伀伀倀匀开刀伀伀吀开倀䄀吀䠀⸀✀⼀洀漀搀甀氀攀猀⼀砀氀愀渀最甀愀最攀⼀愀瀀椀⸀瀀栀瀀✀㬀 
	位置在下列內容之前 ਀ऀ    ⼀⼀ ⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀ 䤀渀挀氀甀搀攀 猀椀琀攀ⴀ眀椀搀攀 氀愀渀最 昀椀氀攀 ⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀⌀ 
	    if ( file_exists(XOOPS_ROOT_PATH."/language/".$xoopsConfig['language']."/global.php") ) {਀ऀ        椀渀挀氀甀搀攀开漀渀挀攀 堀伀伀倀匀开刀伀伀吀开倀䄀吀䠀⸀∀⼀氀愀渀最甀愀最攀⼀∀⸀␀砀漀漀瀀猀䌀漀渀昀椀最嬀✀氀愀渀最甀愀最攀✀崀⸀∀⼀最氀漀戀愀氀⸀瀀栀瀀∀㬀 
	    } else {਀ऀ        椀渀挀氀甀搀攀开漀渀挀攀 堀伀伀倀匀开刀伀伀吀开倀䄀吀䠀⸀∀⼀氀愀渀最甀愀最攀⼀攀渀最氀椀猀栀⼀最氀漀戀愀氀⸀瀀栀瀀∀㬀 
	    }਀  
 3 修改 language/schinese/global.php (如果是正體中文，請쥜㥏汥愀渀最甀愀最攀⼀琀挀栀椀渀攀猀攀⼀最氀漀戀愀氀⸀瀀栀瀀⤀ 
	//%%%%%		LANGUAGE SPECIFIC SETTINGS   %%%%%਀ऀ⼀⼀搀攀昀椀渀攀⠀✀开䌀䠀䄀刀匀䔀吀✀Ⰰ ✀䜀䈀㈀㌀㄀㈀✀⤀㬀 
	//define('_LANGCODE', 'zh-CN');਀ऀ搀攀昀椀渀攀⠀✀开䌀䠀䄀刀匀䔀吀✀Ⰰ 攀洀瀀琀礀⠀␀砀氀愀渀最甀愀最攀嬀∀挀栀愀爀猀攀琀∀崀⤀㼀✀䜀䈀㈀㌀㄀㈀✀㨀␀砀氀愀渀最甀愀最攀嬀∀挀栀愀爀猀攀琀∀崀⤀㬀 
	define('_LANGCODE', empty($xlanguage["code"])?'zh-CN':$xlanguage["code"]);਀ऀ␀砀氀愀渀最甀愀最攀嬀✀挀栀愀爀猀攀琀开戀愀猀攀✀崀 㴀 ∀最戀㈀㌀㄀㈀∀㬀 
    ਀㐀 砀骐者ⱗ鹧ﮊ⁼⠀鸀硓֐깮ⵕ硎자⥤☀鹎ꁘ㡞鹏ﮊ⁼⠀舀鱙逸ⱗ鹧ﮊ멼捰푫⶚蝎౥쯿ފଡ଼ᝎ杒륑ⵛ葎ⅶ푼⾚挀푫ƚ猰挀栀椀渀攀猀攀⼀琀挀栀椀渀攀猀攀Ā朰戀㈀㌀㄀㈀⼀戀椀最㔀Ā稰栀ⴀ䌀一⼀稀栀ⴀ吀圀尀調)਀ऀ퐀艫ౙ苿鱙腧⢉୎繎⹞鹺ﮊ⡼ᘀ뱽⥸䬀鍎ޕ�㩣 麂Ⲋ ℀푼⶚蝎⡥最戀㈀㌀㄀㈀⤀Ⰰ 挀푫⶚蝎⡥戀椀最㔀⤀ 谀⁔唀吀䘀ⴀ㠀 ⴀ蝎e
	則需要選定基本語系(需要確定你的XOOPS已經有english和schinese兩個語系包):਀ऀ㄀㨀 ऀ吀稱: english; 		描述(可選): 英語; 			編碼: iso-8859-1; 	語系代碼: en (或其他任何字母比如 "xen", 並⽎ὦ捷葫鹶ﮊ뱎ⱸ ⡓虵᥏ᡪ螂ڐ葒杶륑⥛ 
	2: 	ㅔ㩺 猀挀栀椀渀攀猀攀㬀 ऀ케⢏硓⦐㨀 ℀푼⶚蝎㭥 ऀऀ뱽㩸 最戀㈀㌀㄀㈀㬀 ऀऀ鸀ﮊ뱎㩸 稀栀 ⠀ᘀ癢홑פֿ啎坏쵛푫艫⁙∀猀挀∀Ⰰ ☀乎是真正的語系代碼, 只用來標記中文部分的內容)਀ऀ㘀豱鹟ꁘ禍뱗Ⅵ푼⶚蝎葥㡞鹏ﮊ⡼܀͜�䭱貐束륑͛햁鹒⅟푼⶚蝎䥥�⥣㨀 
	1: 	ㅔ㩺 琀挀栀椀渀攀猀攀㬀 ऀ케⢏硓⦐㨀 挀푫⶚蝎㭥 ऀऀ뱽㩸 戀椀最㔀㬀 ऀऀ鸀ﮊ뱎㩸 稀栀ⴀ吀圀 ⠀挀푫⶚蝎葥ὶ捷葫鹶ﮊ뱎⥸ 
	2: 	ㅔ㩺 甀琀昀㠀㬀 ऀऀ케⢏硓⦐㨀 唀吀䘀㠀ⴀ蝎㭥 ऀऀ뱽㩸 甀琀昀ⴀ㠀 㬀 ऀऀ鸀ﮊ뱎㩸 稀栀ⴀ䌀一 ⠀℀푼⶚蝎葥ὶ捷葫鹶ﮊ뱎⥸ 
਀㔀 ⠀䁗䩓ꅘٻ杴ݑ≜鸀ﮊ硼자≤䀀䩓ⵘ骊멛譓
਀㘀 ⠀恗葏ⅶ䑪杽륑ⵛᙎ⽢Ⅶ罪⽧䠀潏ⵦ鹎ꁘᩒ鹙ﮊ杼륑౛翿⡏敵彫㒚ⴀ驎꥛葿鹶ﮊ뱎ݸ쥶条륑՛睓蚍⁏嬀舀鱙恧乏使用多語系內容切換，而是只用於正體簡體自動轉換，則跳過這一步]: ਀ऀ嬀氀愀渀最挀漀搀攀㄀崀䌀漀渀琀攀渀琀 漀昀 琀栀攀 氀愀渀最甀愀最攀㄀嬀⼀氀愀渀最挀漀搀攀㄀崀 嬀氀愀渀最挀漀搀攀㈀崀䌀漀渀琀攀渀琀 漀昀 琀栀攀 氀愀渀最甀愀最攀㈀嬀⼀氀愀渀最挀漀搀攀㈀崀 嬀氀愀渀最挀漀搀攀㌀崀䌀漀渀琀攀渀琀 漀昀 琀栀攀 氀愀渀最甀愀最攀㌀嬀⼀氀愀渀最挀漀搀攀㌀崀 ⸀⸀⸀ 
	如果某些內容為兩種以上語系共有, 你可以使用分隔符"|"來定義共享的內容:	਀ऀ嬀氀愀渀最挀漀搀攀㄀簀氀愀渀最挀漀搀攀㈀崀䌀漀渀琀攀渀琀 猀栀愀爀攀搀 戀礀 氀愀渀最甀愀最攀㄀☀㈀嬀⼀氀愀渀最挀漀搀攀㄀簀氀愀渀最挀漀搀攀㈀崀 嬀氀愀渀最挀漀搀攀㌀崀䌀漀渀琀攀渀琀 漀昀 琀栀攀 氀愀渀最甀愀最攀㌀嬀⼀氀愀渀最挀漀搀攀㌀崀 ⸀⸀⸀ 
	਀ऀ魛讖偏⁛⠀䜀驐敛彫㒚ⴀⵎ骊葛鹶ﮊ뱎ٸ╒⽒㩦 麂ⶊ攀渀㬀 픀鹬ⶊ昀爀㬀 ℀푼⶚蝎ⵥ猀挀⤀㨀 
	[en]My XOOPS[/en][fr]Moi XOOPS[/fr][sc]我的XOOPS[/sc]਀ऀᘀ㩢 
	[english|french]This is my content in English and French[/english|french][schinese]中文內容[/schinese]਀ 
7 xlanguage將自動將內容在各延伸語系之間轉換 [實際上在這一步你N膗ﮉ啎쵏層嵏 
਀㠀 搀뮖鹓ﮊ硼자Ⅴ䑪䭽ᙎౙ苿鱙恧⡠䡗潏ᙦ⽢Ⅶ罪ⵧ鹎ꁘ鹒ﮊݼ�董ݶ㩎 
	1) 修改 /modules/xlanguage/api.php "$xlanguage_theme_enable = true;"਀ऀ㈀⤀ ⴀ骊썛硓⁥∀␀漀瀀琀椀漀渀猀 㴀 愀爀爀愀礀⠀∀椀洀愀最攀猀∀Ⰰ ∀ ∀Ⰰ 㔀⤀㬀 ⼀⼀ 漀㪘ⅹཪⱟ ؀鑒⚖ⱻ 케k䱎碈≶㬀 
	3) 將 "<{$smarty.const.XLANGUAGE_SWITCH_CODE}>" 插入到你的佈景或是模板中需要顯示的地方。਀ 
	਀砀氀愀渀最愀甀最攀 搀攀猀挀爀椀瀀琀椀漀渀 
-------------------------਀䄀渀 攀堀琀攀渀猀椀戀氀攀 䴀甀氀琀椀ⴀ氀愀渀最甀愀最攀 挀漀渀琀攀渀琀 愀渀搀 挀栀愀爀愀挀琀攀爀 攀渀挀漀搀椀渀最 䴀愀渀愀最攀洀攀渀琀 瀀氀甀最椀渀 
Multilanguage management handles displaying contents of different languages, like English, French and Chinese਀䌀栀愀爀愀挀琀攀爀 攀渀挀漀搀椀渀最 洀愀渀愀最攀洀攀渀琀 栀愀渀搀氀攀猀 挀漀渀琀攀渀琀猀 漀昀 搀椀昀昀攀爀攀渀琀 攀渀挀漀搀椀渀最 猀攀琀猀 昀漀爀 漀渀攀 氀愀渀最甀愀最攀Ⰰ 氀椀欀攀 䜀䈀㈀㌀㄀㈀ ⠀䌀栀椀渀攀猀攀 匀椀洀瀀氀椀昀椀攀搀⤀ 愀渀搀 䈀䤀䜀㔀 ⠀䌀栀椀渀攀猀攀 吀爀愀搀椀琀椀漀渀愀氀⤀ 昀漀爀 䌀栀椀渀攀猀攀⸀  
਀ 
What xlanguage CAN do਀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ 
1 displaying content of specified language based on user's dynamic choice਀㈀ 挀漀渀瘀攀爀琀椀渀最 挀漀渀琀攀渀琀 昀爀漀洀 漀渀攀 挀栀愀爀愀挀琀攀爀 攀渀挀漀搀椀渀最 猀攀琀 琀漀 愀渀漀琀栀攀爀 
਀ 
What xlanguage canNOT do਀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ 
1 xlanguage does NOT have the ability of translating content from one language to another one. You have to input contents of various languages by yourself਀㈀ 砀氀愀渀最甀愀最攀 搀漀攀猀 一伀吀 眀漀爀欀 眀椀琀栀漀甀琀 愀搀搀椀渀最 漀渀攀 氀椀渀攀 琀漀 堀伀伀倀匀⼀椀渀挀氀甀搀攀⼀挀漀洀洀漀渀⸀瀀栀瀀 ⠀猀攀攀 最甀椀搀攀 戀攀氀漀眀⤀ 
3 xlanguage does NOT have the ability of converting content from one character encoding to another if none of "iconv", "mb_string" or "xconv" is available. ਀ 
਀䘀攀愀琀甀爀攀猀 
--------਀㄀ 愀甀琀漀ⴀ搀攀琀攀挀琀椀漀渀 漀昀 瘀椀猀椀琀漀爀✀猀 氀愀渀最甀愀最攀 漀渀 栀椀猀 昀椀爀猀琀 瘀椀猀椀琀漀爀 
2 memorizing users' langauge preferences਀㌀ 猀眀椀琀挀栀椀渀最 挀漀渀琀攀渀琀猀 漀昀 搀椀昀昀攀爀攀渀琀 氀愀渀最甀最攀猀⼀攀渀挀漀搀椀渀最 猀攀琀猀 漀渀ⴀ昀氀礀 
4 supporting M-S-M mode for character encoding handler਀ 
Note:਀䴀ⴀ匀ⴀ䴀㨀 䴀甀氀琀椀瀀氀攀 攀渀挀漀搀椀渀最 椀渀瀀甀琀Ⰰ 匀椀渀最氀攀 攀渀挀漀搀椀渀最 猀琀漀爀愀最攀Ⰰ 䴀甀氀琀椀瀀氀攀 攀渀挀漀搀椀渀最 漀甀琀瀀甀琀⸀ 
M-S-M allows one site to fit various users with different language character encoding usages. For example, a site having xlanguage implemented porperly allows users to input content either with GB2312, with BIG5 or UTF-8 encoding and to store the content into DB with specified encoding, for say GB2312, and to display the content either with GB2312, with BIG5 or with UTF-8 encoding.਀ 
਀䌀栀愀渀最攀氀漀最 
---------਀砀氀愀渀最甀愀最攀 ㌀⸀　㈀ 挀栀愀渀最攀氀漀最㨀 
1 adjusted for Xoops 2.4.0 using Preloads, no hacks of Core files required anymore in 2.4.0 and above (trabis)਀ 
xlanguage 3.0 changelog:਀㄀ 挀漀洀瀀愀琀愀戀氀攀 昀漀爀 愀氀氀 堀漀漀瀀猀 愀挀琀椀瘀攀 瘀攀爀猀椀漀渀猀 
2 added smarty template for block਀㌀ 愀搀搀攀搀 椀渀氀椀渀攀 猀挀爀椀瀀琀猀 昀漀爀 搀椀猀瀀氀愀礀椀渀最 氀愀渀最甀愀最攀 猀眀椀琀挀栀 洀愀渀渀攀爀 愀渀礀眀栀攀爀攀 瀀爀攀昀攀爀攀搀 
਀砀氀愀渀最甀愀最攀 ㈀⸀　㐀 挀栀愀渀最攀氀漀最㨀 
capable for different language cache, reported by suico @ xoops.org਀ 
xlanguage 2.03 changelog:਀∀椀渀瀀甀琀∀ 瀀愀爀猀攀 椀洀瀀爀漀瘀攀洀攀渀琀Ⰰ 爀攀瀀漀爀琀攀搀 戀礀 椀爀洀琀昀愀渀 䀀 砀漀漀瀀猀⸀漀爀最 
਀砀氀愀渀最甀愀最攀 ㈀⸀　㈀ 戀甀最昀椀砀 昀漀爀 堀匀匀 瘀甀氀渀攀爀愀戀椀氀椀琀礀 
Thanks domifara @ dev.xoops.org਀ 
xlanguage 2.01 bugfix for nonexisting language਀ 
਀ 
Credits਀ⴀⴀⴀⴀⴀⴀⴀ 
1 Adi Chiributa - webmaster@artistic.ro, language handler਀㈀ 眀樀甀攀 ⴀ 栀琀琀瀀㨀⼀⼀眀眀眀⸀眀樀甀攀⸀漀爀最Ⰰ 稀椀氀椀渀最 䈀䤀䜀㔀ⴀ䜀䈀㈀㌀㄀㈀ 挀漀渀瘀攀爀猀椀漀渀 
3 GIJOE - http://www.peak.ne.jp, easiest multilanguage hack਀ 
Author਀ⴀⴀⴀⴀⴀⴀ 
D.J. (phppp)਀栀琀琀瀀㨀⼀⼀砀漀漀瀀猀⸀漀爀最⸀挀渀 
http://xoopsforge.com