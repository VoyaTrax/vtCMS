<?php
define('_XD_DOCS','FAQ');
define('_XD_CATEGORY','Kategorie');
define('_XD_MAIN','Übersicht');
define('_XD_TOC','Inhaltsverzeichnis');
define('_XD_TITLE','Titel');
define('_XD_ANSWER','Antwort');
define('_XD_QUESTION','Frage');
define('_XD_CONTENTS','Frage & Antwort');
define('_XD_BACKTOTOP','Zurück zum Seitenanfang');
define('_XD_BACKTOINDEX','Zurück zum FAQ Index');
define('_XD_CATEGORIES','Kategorien');
define('_XD_ORDER','Position');
define('_XD_DISPLAY','Anzeigen');
define('_XD_DOCSCONFIG','FAQ Einstellungen');
define('_XD_ADDCAT','Hinzufügen einer Kategorie');
define('_XD_ADDCONTENTS','Hinzufügen FAQ');
define('_XD_NOHTML','Deaktiviern HTML tags');
define('_XD_NOSMILEY','Deaktiviern Smiley icons');
define('_XD_NOXCODE','Deaktiviern XOOPS codes');
define('_XD_EDITCAT','Kategorie ändern');
define('_XD_SAVECHANGES','Einstellungen Speichern');
define('_XD_EDITCONTENTS','Inhalt ändern');
define('_XD_RUSURECAT','Sind Sie sicher, dass Sie dieses Faq und ihren ganzen Fragen löschen wollen?');
define('_XD_RUSURECONT','Sind Sie sich sicher, daß diese Q&A löschen wollen?');
define('_XD_DBSUCCESS','Datenbank erfolgreich Aktualisiert!');
?>
