<?php

// Module Info

// The name of this module
define('_MI_MYLINKS_NAME','Web Links');

// A brief description of this module
define('_MI_MYLINKS_DESC','Erstellt einen Web Links Bereich wo Mitglieder suchen/einsenden/bewerten verschiedene Web Seiten.');

// Names of blocks for this module (Not all module has blocks)
define('_MI_MYLINKS_BNAME1','Neue Links');
define('_MI_MYLINKS_BNAME2','Top Links');

// Sub menu titles
define('_MI_MYLINKS_SMNAME1','Einsenden');
define('_MI_MYLINKS_SMNAME2','Populär');
define('_MI_MYLINKS_SMNAME3','Top bewertete');

// Names of admin menu items
define('_MI_MYLINKS_ADMENU2','Hinzufügen/Ändern Links');
define('_MI_MYLINKS_ADMENU3','Eingesendete Links');
define('_MI_MYLINKS_ADMENU4','Defekte Links');
define('_MI_MYLINKS_ADMENU5','Geänderte Links');

// Title of config items
define('_MI_MYLINKS_POPULAR', 'Klicks um Populär zu sein');
define('_MI_MYLINKS_NEWLINKS', 'Neue Links auf der Seite');
define('_MI_MYLINKS_PERPAGE', 'Angezeigte Links pro Seite');
define('_MI_MYLINKS_USESHOTS', 'Screenshots?');
define('_MI_MYLINKS_USEFRAMES', 'Frames?');
define('_MI_MYLINKS_SHOTWIDTH', 'Screenshot Größe');
define('_MI_MYLINKS_ANONPOST','Kann Anonymous Links posten?');
define('_MI_MYLINKS_AUTOAPPROVE','Automatische Linkeintragung aktivieren?');

// Description of each config items
define('_MI_MYLINKS_POPULARDSC', 'Anzahl der Klicks um Populär zu sein');
define('_MI_MYLINKS_NEWLINKSDSC', 'Anzahl der neuen Links');
define('_MI_MYLINKS_PERPAGEDSC', 'Anzeige der max. Links auf der Seite');
define('_MI_MYLINKS_USEFRAMEDSC', 'Soll der Link im Frame angezeigt werden ?');
define('_MI_MYLINKS_USESHOTSDSC', 'Soll ein Screenshot angezeigt werden ?');
define('_MI_MYLINKS_SHOTWIDTHDSC', 'Die Breite vom Screenshot ');
define('_MI_MYLINKS_AUTOAPPROVEDSC','');
?>
