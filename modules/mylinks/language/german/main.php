<?php
// $Id: main.php,v 1.7 2003/03/21 13:12:22 w4z004 Exp $
//%%%%%%		Module Name 'MyLinks'		%%%%%

define('_MD_THANKSFORINFO','Danke für Ihren Link, wir werden diesen nach redaktioneller Prüfung freischalten!');
define('_MD_THANKSFORHELP','Danke, daß Sie uns dabei unterstützen, das Verzeichnis aktuell zu halten.');
define('_MD_FORSECURITY','Aus Sicherheitsgründen wird Ihr Benutzername und Ihre IP temporär gespeichert.');

define('_MD_SEARCHFOR','Suchen nach');
define('_MD_ANY','Einer der Begriffe');
define('_MD_SEARCH','Suchen');

define('_MD_MAIN','Übersicht');
define('_MD_SUBMITLINK','Link eintragen');
define('_MD_SUBMITLINKHEAD','Weblink übermitteln');
define('_MD_POPULAR','Populär');
define('_MD_TOPRATED','Top bewertet');

define('_MD_NEWTHISWEEK','Neu in dieser Woche');
define('_MD_UPTHISWEEK','Aktualisiert in dieser Woche');

define('_MD_POPULARITYLTOM','Popularität (wenigste bis meiste Aufrufe)');
define('_MD_POPULARITYMTOL','Popularität (meiste bis wenigste Aufrufe)');
define('_MD_TITLEATOZ','Title (A bis Z)');
define('_MD_TITLEZTOA','Title (Z bis A)');
define('_MD_DATEOLD','Datum (Ältere Links zuerst)');
define('_MD_DATENEW','Datum (Neuere Links zuerst)');
define('_MD_RATINGLTOH','Bewertung (niedrigste bis höchste Bewertung)');
define('_MD_RATINGHTOL','Bewertung (höchste bis niedrigste Bewertung)');

define('_MD_NOSHOTS','Keine Vorschau vorhanden');
define('_MD_EDITTHISLINK','Ändere diesen Link');

define('_MD_DESCRIPTIONC','Beschreibung: ');
define('_MD_EMAILC','Email: ');
define('_MD_CATEGORYC','Kategorie: ');
define('_MD_LASTUPDATEC','Letztes Aktualisierung: ');
define('_MD_HITSC','Aufrufe: ');
define('_MD_RATINGC','Bewertung: ');
define('_MD_ONEVOTE','1 Bewertung');
define('_MD_NUMVOTES','%s Bewertungen');
define('_MD_RATETHISSITE','Bewerte diese Seite');
define('_MD_MODIFY','Ändern');
define('_MD_REPORTBROKEN','Defekten Link melden');
define('_MD_TELLAFRIEND','Freund benachrichtigen');

define('_MD_THEREARE','Es befinden sich %s Links in der Datenbank');
define('_MD_LATESTLIST','Aktuelle Links');

define('_MD_REQUESTMOD','Anfrage zur Link-Änderung');
define('_MD_LINKID','Link ID: ');
define('_MD_SITETITLE','Webseiten Titel: ');
define('_MD_SITEURL','Website URL: ');
define('_MD_CONTACTEMAIL','Kontakt E-Mail Adresse: ');
define('_MD_SHOTIMAGE','Vorschau Bild: ');
define('_MD_SENDREQUEST','Anfrage senden');

define('_MD_VOTEAPPRE','Ihre Stimme wurde hinzugefügt,vielen Dank für Ihre Bewertung.');
define('_MD_THANKURATE','Danke, daß Sie sich die Zeit genommen haben, bei %s abzustimmen.');
define('_MD_VOTEFROMYOU','Eingaben von Benutzern wie Ihnen helfen anderen Benutzern, die besten Links schneller zu finden.');
define('_MD_VOTEONCE','Bitte Stimmen Sie für einen Link nur einmal ab!');
define('_MD_RATINGSCALE','Die Bewertungsskala reicht von 1 - 10, wobei 1 die niedrigste und 10 die höchste Bewertung ist.');
define('_MD_BEOBJECTIVE','Seien Sie  objektiv, denn wenn jeder mit 1 oder 10 abstimmt, sind die Ergebnisse wenig aussagefähig.');
define('_MD_DONOTVOTE','Stimmen Sie bitte nicht für Ihren eigenen Link!');
define('_MD_RATEIT','Bewerten');

define('_MD_INTRESTLINK','Interessanter Link auf %s');  // %s is your site name
define('_MD_INTLINKFOUND','Ich habe einen interessanten Link auf %s gefunden');  // %s is your site name

define('_MD_RECEIVED','Wir haben Ihre Weblink Information erhalten. Danke!');
define('_MD_WHENAPPROVED','Sie erhalten eine Nachricht, wenn ihr Weblink veröffentlicht wird.');
define('_MD_SUBMITONCE','Senden Sie uns Ihren Weblink nur einmal.');
define('_MD_ALLPENDING','Alle eingesendeten Weblinks werden zuerst von uns überprüft.');
define('_MD_DONTABUSE','Benutzername und IP werden gespeichert.');
define('_MD_TAKESHOT','Es kann einige Tage dauern, bis Ihr Link in unsere Datenbank eingetragen wird.');

define('_MD_RANK','Rang');
define('_MD_CATEGORY','Kategorie');
define('_MD_HITS','Hits');
define('_MD_RATING','Bewertung');
define('_MD_VOTE','Abstimmung');
define('_MD_TOP10','%s Top 10'); // %s is a link category title

define('_MD_SEARCHRESULTS','Suchergebnisse für <b>%s</b>:'); // %s is search keywords
define('_MD_SORTBY','Sortiert nach:');
define('_MD_TITLE','Titel');
define('_MD_DATE','Datum');
define('_MD_POPULARITY','Popularität');
define('_MD_CURSORTEDBY','Seiten sortiert nach: %s');
define('_MD_PREVIOUS','vorherige');
define('_MD_NEXT','nächste');
define('_MD_NOMATCH','Keine Treffer auf Deine Suchanfrage gefunden');

define('_MD_SUBMIT','Senden');
define('_MD_CANCEL','Abbrechen');

define('_MD_ALREADYREPORTED','Sie haben den Weblink schon als defekt gemeldet.');
define('_MD_MUSTREGFIRST','Sie haben nicht die notwendigen Zugriffsrechte.<br>Bitte registrieren Sie sich!');
define('_MD_NORATING','Keine Bewertung ausgewählt.');
define('_MD_CANTVOTEOWN','Sie können nicht für Ihren eigenen Weblink eine Stimme abgeben.<br>Alle Stimmen werden geloggt und überprüft');
define('_MD_VOTEONCE2','Sie können nicht für Ihren eigenen Weblink eine Stimme abgeben.<br>Alle Stimmen werden geloggt und überprüft');

//%%%%%%	Module Name 'MyLinks' (Admin)	  %%%%%

define('_MD_WEBLINKSCONF','Web Links Einstellungen');
define('_MD_GENERALSET','My Links Einstellungen');
define('_MD_ADDMODDELETE','Hinzufügen/Löschen oder Ändern von Kategorien/Links');
define('_MD_LINKSWAITING','Links die auf Bestätigung warten');
define('_MD_BROKENREPORTS','Gemeldete Defekte Link');
define('_MD_MODREQUESTS','Anfragen zur Link-Änderung');
define('_MD_SUBMITTER','Übermittler: ');
define('_MD_VISIT','Besuchen');
define('_MD_SHOTMUST','Bildvorschau muß ein im %s Ordner vorhandenes Bild sein (z.B.: shot.gif). Möchten Sie keine Bildvorschau, lassen Sie einfach das Feld leer.');
define('_MD_APPROVE','hinzufügen');
define('_MD_DELETE','Löschen');
define('_MD_NOSUBMITTED','Keine neuen Link-Einsendungen.');
define('_MD_ADDMAIN','Hauptkategorie hinzufügen');
define('_MD_TITLEC','Titel: ');
define('_MD_IMGURL','Bild URL (OPTIONAL max 50px groß): ');
define('_MD_ADD','Hinzufügen');
define('_MD_ADDSUB','Neue Unter-Kategorie');
define('_MD_IN','in');
define('_MD_ADDNEWLINK','Neuen Link hinzufügen');
define('_MD_MODCAT','Kategorie modifizieren');
define('_MD_MODLINK','Link Information ändern');
define('_MD_TOTALVOTES','Link Bewertung (alle Stimmen: %s)');
define('_MD_USERTOTALVOTES','Mitglieder-Stimmen (Summe der Bewertungen: %s)');
define('_MD_ANONTOTALVOTES','Gast-Stimmen (Summe der Bewertungen: %s)');
define('_MD_USER','Mitglieder');
define('_MD_IP','IP Addresse');
define('_MD_USERAVG','Durchschnittliche Mitglieder-Bewertung');
define('_MD_TOTALRATE','Insgesamte Bewertung');
define('_MD_NOREGVOTES','Gast-Bewertung');
define('_MD_NOUNREGVOTES','Mitglieder-Bewertung');
define('_MD_VOTEDELETED','Bewertunsergebnis gelöscht.');
define('_MD_NOBROKEN','Keine defekten links gemeldet.');
define('_MD_IGNOREDESC','Ignorieren (Ignoriert die Nachrichten über defekte Link)');
define('_MD_DELETEDESC','Löschen (Löscht <b>die gemeldeten Websiten </b> und <b>defekten Link meldungen</b> von diesem Link.)');
define('_MD_REPORTER','Report Sender');
define('_MD_LINKSUBMITTER','Mitteilungs Verfasser');
define('_MD_IGNORE','Ignorieren');
define('_MD_LINKDELETED','Link gelöscht.');
define('_MD_BROKENDELETED','Defekter Link. Mitteilung gelöscht.');
define('_MD_USERMODREQ','Benutzer Link Änderungs-Anfrage');
define('_MD_ORIGINAL','Original');
define('_MD_PROPOSED','Vorschlag');
define('_MD_OWNER','Urheber: ');
define('_MD_NOMODREQ','Keine Link Änderungs-Anfragen vorhanden.');
define('_MD_DBUPDATED','Datenbank erfolgreich Aktualisiert!');
define('_MD_MODREQDELETED','Änderungsanfrage gelöscht.');
define('_MD_IMGURLMAIN','Bild URL (OPTIONEL und nur für die Hauptkategorie. Bild Höhe max 50px): ');
define('_MD_PARENT','Übergeordnete Kategorie:');
define('_MD_SAVE','Speichern');
define('_MD_CATDELETED','Kategorie gelöscht.');
define('_MD_WARNING','WARNUNG: Sind Sie sicher, daß Sie diese Kategorie und alle Inhalte löschen möchten?');
define('_MD_YES','Ja');
define('_MD_NO','Nein');
define('_MD_NEWCATADDED','Neue Kategorie hinzugefügt!');
define('_MD_ERROREXIST','FEHLER: Dieser Link befindet sich schon in unserer Datenbank!');
define('_MD_ERRORTITLE','FEHLER: Sie müßen einen Titel eingeben!');
define('_MD_ERRORDESC','FEHLER: Sie müßen eine Beschreibung eingeben!');
define('_MD_NEWLINKADDED','Neuer Link in die Datenbank eingefügt!');
define('_MD_YOURLINK','Ihr Webseiten Link auf %s');
define('_MD_YOUCANBROWSE','Sie können ihn sich ansehen auf %s');
define('_MD_HELLO','Hallo %s');
define('_MD_WEAPPROVED','Ihr Link wurde in die Datenbank eingetragen.');
define('_MD_THANKSSUBMIT','Danke für Ihren Eingetrag!');
define('_MD_ISAPPROVED','Wir haben Ihren Link überprüft');
?>
