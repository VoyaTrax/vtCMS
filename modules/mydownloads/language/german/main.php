<?php
// $Id: main.php,v 1.7 2003/03/21 13:12:21 w4z004 Exp $
//%%%%%%		Module Name 'MyDownloads'		%%%%%

define('_MD_THANKSFORINFO','Danke für Ihren Link, wir werden diesen nach redaktioneller Prüfung freischalten!');
define('_MD_THANKSFORHELP','Danke, daß Sie uns dabei unterstützen, das Verzeichnis aktuell zu halten.');
define('_MD_FORSECURITY','Aus Sicherheitsgründen wird Ihr Benutzername und Ihre IP temporär gespeichert.');
define('_MD_NOPERMISETOLINK', 'Dieser Download, gehört nicht zu dieser Seite <br /><br />Bitte senden Sie dem Webmaster eine E-Mail:   <br /><b>NOT TO LEECH OTHER SITES LINKS!!</b> <br /><br /><b>Definition of a Leecher:</b> One who is to lazy to link from his own server or steals other peoples hard work and makes it look like his own <br /><br />  Your IP address <b>has been logged</b>.');
define('_MD_ALL','alle');
define('_MD_DESCRIPTION','Beschreibung');
define('_MD_SEARCH','Suche');
define('_MD_SUBMITCATHEAD','Download übermitteln');

define('_MD_MAIN','Übersicht');
define('_MD_POPULAR','Populär');
define('_MD_NEWTHISWEEK','Neu in dieser Woche');
define('_MD_UPTHISWEEK','Aktualisiert in dieser Woche');

define('_MD_POPULARITYLTOM','Popularität (wenigste bis meiste Aufrufe)');
define('_MD_POPULARITYMTOL','Popularität (meiste bis wenigste Aufrufe)');
define('_MD_TITLEATOZ','Title (A bis Z)');
define('_MD_TITLEZTOA','Title (Z bis A)');
define('_MD_DATEOLD','Datum (Ältere Downloads zuerst)');
define('_MD_DATENEW','Datum (Neuere Downloads zuerst)');
define('_MD_RATINGLTOH','Bewertung (niedrigste bis höchste Bewertung)');
define('_MD_RATINGHTOL','Bewertung (höchste bis niedrigste Bewertung)');

define('_MD_NOSHOTS','Keine Vorschau vorhanden');
define('_MD_EDITTHISDL','Ändere diesen Download');

define('_MD_DESCRIPTIONC','Beschreibung: ');
define('_MD_EMAILC','Email: ');
define('_MD_CATEGORYC','Kategorie: ');
define('_MD_LASTUPDATEC','Letztes Aktualisierung: ');
define('_MD_DLNOW','Download');
define('_MD_VERSION','Version');
define('_MD_SUBMITDATE','Eingetragen am');
define('_MD_DLTIMES','diese Datei wurde %s mal heruntergeladen');
define('_MD_FILESIZE','Datei Größe');
define('_MD_SUPPORTEDPLAT','Unterstützte Betriebssyteme');
define('_MD_HOMEPAGE','Homepage');
define('_MD_HITSC','Aufrufe: ');
define('_MD_RATINGC','Bewertung: ');
define('_MD_ONEVOTE','1 Bewertung');
define('_MD_NUMVOTES','%s Bewertungen');
define('_MD_RATETHISFILE','Bewerte diesen Download');
define('_MD_MODIFY','Ändern');
define('_MD_REPORTBROKEN','Defekten Download melden');
define('_MD_TELLAFRIEND','Freund benachrichtigen');
define('_MD_EDIT','Ändern');

define('_MD_THEREARE','Es befinden sich %s Downloads in der Datenbank');
define('_MD_LATESTLIST','Aktuelle Downloads');

define('_MD_REQUESTMOD','Anfrage zur Download-Änderung');
define('_MD_FILEID','Download ID: ');
define('_MD_FILETITLE','Download Titel: ');
define('_MD_DLURL','Download URL: ');
define('_MD_HOMEPAGEC','Homepage: ');
define('_MD_VERSIONC','Version: ');
define('_MD_FILESIZEC','Datei Größe: ');
define('_MD_NUMBYTES','%s bytes');
define('_MD_PLATFORMC','Beriebssystem: ');
define('_MD_CONTACTEMAIL','Kontakt E-Mail Adresse: ');
define('_MD_SHOTIMAGE','Vorschau Bild: ');
define('_MD_SENDREQUEST','Anfrage senden');


define('_MD_VOTEAPPRE','Ihre Stimme wurde hinzugefügt,vielen Dank für Ihre Bewertung.');
define('_MD_THANKYOU','Danke, daß Sie sich die Zeit genommen haben, bei %s abzustimmen.'); // %s is your site name
define('_MD_VOTEONCE','Bitte Stimmen Sie für einen Download nur einmal ab!');
define('_MD_RATINGSCALE','Die Bewertungsskala reicht von 1 - 10, wobei 1 die niedrigste und 10 die höchste Bewertung ist.');
define('_MD_BEOBJECTIVE','Seien Sie objektiv, denn wenn jeder mit 1 oder 10 abstimmt, sind die Ergebnisse wenig aussagefähig.');
define('_MD_DONOTVOTE','Stimmen Sie bitte nicht für Ihren eigenen Download!');
define('_MD_RATEIT','Bewerten');

define('_MD_INTFILEFOUND','Ich habe einen interessanten Download auf %s gefunden'); // %s is your site name

define('_MD_RECEIVED','Wir haben Ihre Download Information erhalten. Danke!');
define('_MD_WHENAPPROVED','Sie erhalten eine Nachricht, wenn Ihr Download veröffentlicht wird.');
define('_MD_SUBMITONCE','Senden Sie uns Ihre Downloads nur einmal.');
define('_MD_ALLPENDING','Alle eingesendeten Downloads werden zuerst von uns überprüft.');
define('_MD_DONTABUSE','Benutzername und IP werden gespeichert.');
define('_MD_TAKEDAYS','Es kann einige Tage dauern, bis sich der Download in unserer Datenbank befindet.');

define('_MD_RANK','Rang');
define('_MD_CATEGORY','Kategorie');
define('_MD_HITS','Hits');
define('_MD_RATING','Bewertung');
define('_MD_VOTE','Abstimmung');

define('_MD_SORTBY','Sortiert nach:');
define('_MD_TITLE','Titel');
define('_MD_DATE','Datum');
define('_MD_POPULARITY','Popularität');
define('_MD_CURSORTBY','Downloads sortiert nach: %s');
define('_MD_PREVIOUS','vorherige');
define('_MD_NEXT','nächste');
define('_MD_NOMATCH','Keine Treffer auf Ihre Suchanfrage gefunden');

define('_MD_TOP10','%s Top 10'); // %s is a downloads category name

define('_MD_SUBMIT','Senden');
define('_MD_CANCEL','Abbrechen');

define('_MD_BYTES','Bytes');
define('_MD_ALREADYREPORTED','Sie haben den Download schon als defekt gemeldet.');
define('_MD_MUSTREGFIRST','Sie haben nicht die notwendigen Zugriffsrechte.<br>Bitte registrieren Sie sich!');
define('_MD_NORATING','Keine Bewertung ausgewählt.');
define('_MD_CANTVOTEOWN','Sie können nicht für Ihren eigenen Download eine Stimme abgeben.<br>Alle Stimmen werden geloggt und überprüft.');

//%%%%%%	Module Name 'MyDownloads' (Admin)	  %%%%%

define('_MD_DLCONF','Downloads Einstellungen');
define('_MD_GENERALSET','Downloads Einstellungen');
define('_MD_ADDMODDELETE','Hinzufügen/Löschen oder Ändern von Dateien/Kategorien');
define('_MD_DLSWAITING','Downloads die auf Bestätigung warten');
define('_MD_BROKENREPORTS','Gemeldete Defekte Downloads');
define('_MD_MODREQUESTS','Anfragen zur Downloadänderung');
define('_MD_SUBMITTER','Übermittler: ');
define('_MD_DOWNLOAD','Download');
define('_MD_APPROVE','hinzufügen');
define('_MD_DELETE','Löschen');
define('_MD_NOSUBMITTED','Keine neuen Download-Einsendungen.');
define('_MD_ADDMAIN','Hauptkategorie hinzufügen');
define('_MD_TITLEC','Titel: ');
define('_MD_IMGURL','Bild URL (OPTIONAL max 50px groß): ');
define('_MD_ADD','Hinzufügen');
define('_MD_ADDSUB','Neue Unter-Kategorie');
define('_MD_IN','in');
define('_MD_ADDNEWFILE','Neuen Download hinzufügen');
define('_MD_MODCAT','Kategorie modifizieren');
define('_MD_MODDL','Download Information ändern');
define('_MD_USER','Benutzer');
define('_MD_IP','IP Addresse');
define('_MD_USERAVG','Durchschnittliche Mitglieder-Bewertung');
define('_MD_TOTALRATE','Insgesamte Bewertung');
define('_MD_NOREGVOTES','Gast-Bewertung');
define('_MD_NOUNREGVOTES','Benutzer-Bewertung');
define('_MD_VOTEDELETED','Bewertunsergebnis gelöscht.');
define('_MD_NOBROKEN','Keine defekten Downloads im Eingang.');
define('_MD_IGNOREDESC','Ignorieren (Ignoriert die Nachrichten über defekte Downloads)');
define('_MD_DELETEDESC','Löschen (<b>Löscht</b> die defekten Downloads und die Nachrichten.)');
define('_MD_REPORTER','Mitteilungs Verfasser:');
define('_MD_FILESUBMITTER','Download Absender:');
define('_MD_IGNORE','Ignorieren');
define('_MD_FILEDELETED','Download gelöscht.');
define('_MD_BROKENDELETED','Defekter Download. Mitteilung gelöscht!');
define('_MD_USERMODREQ','Benutzer Download Änderungs-Anfrage');
define('_MD_ORIGINAL','Original');
define('_MD_PROPOSED','Vorschlag');
define('_MD_OWNER','Urheber: ');
define('_MD_NOMODREQ','Keine Download Änderungs-Anfragen vorhanden.');
define('_MD_DBUPDATED','Datenbank erfolgreich aktualisiert!');
define('_MD_MODREQDELETED','Änderungsanfrage gelöscht.');
define('_MD_IMGURLMAIN','Bild URL (OPTIONEL und nur für die Hauptkategorie. Bild Höhe max 50px): ');
define('_MD_PARENT','Übergeordnete Kategorie:');
define('_MD_SAVE','Speichern');
define('_MD_CATDELETED','Kategorie gelöscht.');
define('_MD_WARNING','WARNUNG: Sind Sie sicher, daß Sie diese Kategorie und alle Inhalte löschen möchten?');
define('_MD_YES','Ja');
define('_MD_NO','Nein');
define('_MD_NEWCATADDED','Neue Kategorie hinzugefügt!');
define('_MD_ERROREXIST','FEHLER: Dieser Download befindet sich schon in unserer Datenbank!');
define('_MD_ERRORTITLE','FEHLER: Sie müßen einen Titel eingeben!');
define('_MD_ERRORDESC','FEHLER: Sie müßen eine Beschreibung eingeben!');
define('_MD_NEWDLADDED','Neuer Download in die Datenbank eingefügt!');
define('_MD_HELLO','Hallo %s');
define('_MD_WEAPPROVED','Wir haben Ihren Download in unsere Datenbank aufgenommen!');
define('_MD_THANKSSUBMIT','Danke für Ihren Download!');

define('_MD_MUSTBEVALID','Bildvorschau muß ein im %s Ordner vorhandenes Bild sein (z.B.: shot.gif). Möchten Sie keine Bildvorschau, lassen Sie einfach das Feld leer.');

define('_MD_REGUSERVOTES','Mitglieder-Stimmen (Summe der Bewertungen: %s)');
define('_MD_ANONUSERVOTES','Gast-Stimmen (Summe der Bewertungen: %s)');

define('_MD_YOURFILEAT','Ihr Download wurde Eingetragen in %s'); // this is an approved mail subject. %s is your site name

define('_MD_VISITAT','Besuche unseren Download - Bereich auf %s');

define('_MD_DLRATINGS','Download Bewertung (alle Stimmen: %s)');
define('_MD_ISAPPROVED','Ihren Download Vorschlag haben wir aufgenommen.');

?>
