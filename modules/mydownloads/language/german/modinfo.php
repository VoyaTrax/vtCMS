<?php
// $Id: modinfo.php,v 1.7 2003/03/21 13:12:22 w4z004 Exp $
// Module Info

// The name of this module
define('_MI_MYDOWNLOADS_NAME','Downloads');

// A brief description of this module
define('_MI_MYDOWNLOADS_DESC','Erstellt einen Download Bereich wo Mitglieder downloaden/einsenden/bewerten verschiedene Dateien.');

// Names of blocks for this module (Not all module has blocks)
define('_MI_MYDOWNLOADS_BNAME1','Neue Downloads');
define('_MI_MYDOWNLOADS_BNAME2','Top Downloads');

// Sub menu titles
define('_MI_MYDOWNLOADS_SMNAME1','Einsenden');
define('_MI_MYDOWNLOADS_SMNAME2','Populär');
define('_MI_MYDOWNLOADS_SMNAME3','Top bewertete');

// Names of admin menu items
define('_MI_MYDOWNLOADS_ADMENU2','Hinzufügen/Ändern Downloads');
define('_MI_MYDOWNLOADS_ADMENU3','Eingesendete Downloads');
define('_MI_MYDOWNLOADS_ADMENU4','Defekte Downloads');
define('_MI_MYDOWNLOADS_ADMENU5','Geänderte Downloads');

// Title of config items
define('_MI_MYDOWNLOADS_POPULAR','Hits die Populär sind<br>Wähle die Anzahl der der downloadbaren dateien die als Populär gekenzeichnet werden sollen.');
define('_MI_MYDOWNLOADS_NEWDLS','Anzahl der Downloads die auf der Startseite angezeigt werden sollen<br>Wähle die maximale Anzahl der Downloads die auf der Startseite angezeigt werden sollen.');
define('_MI_MYDOWNLOADS_PERPAGE','Angezeigte downloads je Seite<br>Wähle die maximale Anzahl der angezeigten Downloads je Seite');
define('_MI_MYDOWNLOADS_ANONPOST','Erlauben Sie anonymen Usern downloads einzusenden');
define('_MI_MYDOWNLOADS_USESHOTS','Verwenden Sie Screenshots<br>wählen sie ja, wenn sie bilder für jeden download verwenden möchten');
define('_MI_MYDOWNLOADS_SHOTWIDTH','Bildgröße<br>Geben Sie bitte die maximale Bildbreite an');
define('_MI_MYDOWNLOADS_CHECKHOST','Nicht erlauben von das jeder runterladen tut ohne zu geben');
define('_MI_MYDOWNLOADS_REFERERS','Verlinkte Seiten nach dem Download<br>Tragen Sie hier die Seiten ein die nach dem Download verlinkt werden<br>separate each one with | jeden Link mit | trennen');
define('_MI_MYDOWNLOADS_AUTOAPPROVE','Downloadvorschläge Automatisch einschreiben?');

// Description of each config items
define('_MI_MYDOWNLOADS_POPULARDSC', '');
define('_MI_MYDOWNLOADS_NEWDLSDSC', '');
define('_MI_MYDOWNLOADS_PERPAGEDSC', '');
define('_MI_MYDOWNLOADS_USESHOTSDSC', '');
define('_MI_MYDOWNLOADS_SHOTWIDTHDSC', '');
define('_MI_MYDOWNLOADS_REFERERSDSC', '');
define('_MI_MYDOWNLOADS_AUTOAPPROVEDSC', '');
?>
