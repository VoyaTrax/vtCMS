<?php
/**
* uninstall.php - cleanup on module uninstall
*
* This file is part of gwiki - geekwright wiki
*
* @copyright  Copyright © 2013 geekwright, LLC. All rights reserved. 
* @license    gwiki/docs/license.txt  GNU General Public License (GPL)
* @since      1.0
* @author     Richard Griffith <richard@geekwright.com>
* @package    gwiki
* @version    $Id$
*/

if (!defined("XOOPS_ROOT_PATH"))  die("Root path not defined");

function xoops_module_uninstall_gwiki(&$module) {
// global $xoopsDB,$xoopsConfig;

	// nothing to do yet
	return true;
}

?>
