gwiki
=====

gwiki, the geekwright wiki, is a XOOPS module which implements a wiki 
based on the [WikiCreole 1.0](http://wikicreole.org/) specification. 
In addition to the basics as defined in the WikiCreole spec, the wiki 
features several powerful extensions, giving it great flexibility as 
a content authoring and presentation tool. It also features an equally 
flexible permission capability, making a single instance suitable for 
multiple collaborative efforts, each with their own policies.

Documentation is supplied as wiki pages which can be optionally loaded 
into the wiki by the module administrator.

Although nearing release, this is still a work in progress.

This module has been tested in Xoops version 2.5.6 and ImpressCMS 1.2.7.


