<?php
// Module Info

// The name of this module
define('_MI_POLLS_NAME','Umfragen');

// A brief description of this module
define('_MI_POLLS_DESC','Zeigt Umfragen in einem Block an');

// Names of blocks for this module (Not all module has blocks)
define('_MI_POLLS_BNAME1','Umfragen Block');

// Names of admin menu items
define('_MI_POLLS_ADMENU1','Umfragen Anzeigen');
define('_MI_POLLS_ADMENU2','Umfragen Hinzufügen');
?>
