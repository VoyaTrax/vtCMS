<?php
//%%%%%%	Admin Module Name  Polls 	%%%%%
define('_AM_DBUPDATED','Datenbank erfolgreich aktualisiert!');
define('_AM_POLLCONF','Poll Konfiguration');
define('_AM_POLLSLIST', 'Umfragen');
define('_AM_AUTHOR', 'Autor dieser Umfrage');
define('_AM_DISPLAYBLOCK', 'Im Block anzeigen?');
define('_AM_POLLQUESTION', 'Umfrage Titel');
define('_AM_VOTERS', 'Stimmen');
define('_AM_VOTES', 'Abgestimmt werden kann');
define('_AM_EXPIRATION', 'Umfrage endet am');
define('_AM_EXPIRED', 'ABGELAUFEN');
define('_AM_VIEWLOG','Logs anzeigen');
define('_AM_CREATNEWPOLL', 'Neue Umfrage starten');
define('_AM_POLLDESC', 'Umfrage Beschreibung');
define('_AM_DISPLAYORDER', 'Position');
define('_AM_ALLOWMULTI', 'Kann mehr als 1 eine Stimme abgeben werden?<br>(multi auswahl)');
define('_AM_NOTIFY', 'Den Umfrage Autor benachrichtigen, wenn diese Umfrage abgelaufen ist?');
define('_AM_POLLOPTIONS', 'Optionen (Fragen)');
define('_AM_EDITPOLL', 'Umfrage ändern');
define('_AM_FORMAT', 'Format: yyyy-mm-dd hh:mm:ss');
define('_AM_CURRENTTIME', 'Aktuelle Zeit ist %s');
define('_AM_EXPIREDAT', 'Abgelaufen am %s');
define('_AM_RESTART', 'Diese Umfrage neu starten');
define('_AM_ADDMORE', 'Mehr Fragen hinzufügen zu den bereits vorhandenen');
define('_AM_POLLRESULTS', 'Umfrage Ergebniss');
define('_AM_RUSUREDEL', 'Sind Sie sicher Sie möchten diese Abstimmung und alle seine Kommentare löschen?');
define('_AM_RESTARTPOLL', 'Umfrage neu starten');
define('_AM_RESET', 'Neustarten der Logs für diese Umfrage?');
define('_AM_ADDPOLL','Umfrage hinzufügen');
?>
