Hallo {X_UNAME},

Ihre Umfrage '{POLL_QUESTION}' ist benendet.
Ihre Umfrage startete am {POLL_START} und endete am {POLL_END}.
Es waren {POLL_VOTERS} Stimmen und {POLL_VOTES} Fragen insgesammt.

Ansehen können Sie ihn sich unter der folgenden URL:
{SITEURL}modules/polls/pollresults.php?poll_id={POLL_ID}

-----------
{SITENAME} ({SITEURL}) 
Webmaster
{ADMINMAIL}
