<?php
// $Id: main.php,v 1.6 2003/03/20 12:39:41 w4z004 Exp $
//%%%%%%	File Name pollresults.php 	%%%%%
define('_PL_TOTALVOTES','Abgestimmt werden kann: %s');
define('_PL_TOTALVOTERS','Stimmen: %s');

//%%%%%%	File Name index.php 	%%%%%
define('_PL_POLLSLIST','Umfragen');
define('_PL_ALREADYVOTED', 'Sorry, aber Sie haben schon abgestimmt.');
define('_PL_THANKSFORVOTE','Danke für Ihre Stimme!');
define('_PL_SORRYEXPIRED', 'Sorry, aber die Umfrage wude schon beendet.');
define('_PL_YOURPOLLAT', '%s, Ihre Stimme bei %s'); // 1st %s is user name, 2nd %s is site name
define('_PL_PREV', 'vorherige');
define('_PL_NEXT', 'nächste');
define('_PL_POLLQUESTION', 'Umfrage Titel');
define('_PL_VOTERS', 'Stimmen');
define('_PL_VOTES', 'Abgestimmt werden kann');
define('_PL_EXPIRATION', 'Umfrage endet am');
define('_PL_EXPIRED', 'ABGELAUFEN');

//%%%%%%	File Name xoopspollrenderer.php 	%%%%%
// %s represents date
define('_PL_ENDSAT','Endet am %s');
define('_PL_ENDEDAT','Beendet am %s');
define('_PL_VOTE','Wähle');
define('_PL_RESULTS','Ergebnis');

?>
