<?php
require_once( '../../../include/cp_header.php' ) ;
require_once( '../include/gtickets.php' ) ;


// COPY TABLES
if( ! empty( $_POST['copy'] ) && ! empty( $_POST['old_prefix'] ) ) {

	if( preg_match( '/[^0-9A-Za-z_-]/' , $_POST['new_prefix'] ) ) die( 'wrong prefix' ) ;

	// Ticket check
	if ( ! $xoopsGTicket->check() ) {
		redirect_header(XOOPS_URL.'/',3,$xoopsGTicket->getErrors());
	}

	$new_prefix = empty( $_POST['new_prefix'] ) ? 'x' . substr( md5( time() ) , -5 ) : $_POST['new_prefix'] ;
	$old_prefix = $_POST['old_prefix'] ;

	$srs = $xoopsDB->queryF( 'SHOW TABLE STATUS FROM `'.XOOPS_DB_NAME.'`' ) ;

	if( ! $xoopsDB->getRowsNum( $srs ) ) die( "You are not allowed to copy tables" ) ;

	$count = 0;
	while( $row_table = $xoopsDB->fetchArray( $srs ) ) {
		$count ++ ;
		$old_table = $row_table['Name'] ;
		if( substr( $old_table , 0 , strlen( $old_prefix ) + 1 ) !== $old_prefix . '_' ) continue ;

		$new_table = $new_prefix . substr( $old_table , strlen( $old_prefix ) ) ;

		$crs = $xoopsDB->queryF( 'SHOW CREATE TABLE '.$old_table ) ;
		if( ! $xoopsDB->getRowsNum( $crs ) ) {
			echo "error: SHOW CREATE TABLE ($old_table)<br />\n" ;
			continue ;
		}
		$row_create = $xoopsDB->fetchArray( $crs ) ;
		$create_sql = preg_replace( "/^CREATE TABLE `$old_table`/" , "CREATE TABLE `$new_table`" , $row_create['Create Table'] , 1 ) ;

		$crs = $xoopsDB->queryF( $create_sql ) ;
		if( ! $crs ) {
			echo "error: CREATE TABLE ($new_table)<br />\n" ;
			continue ;
		}

		$irs = $xoopsDB->queryF( "INSERT INTO `$new_table` SELECT * FROM `$old_table`" ) ;
		if( ! $irs ) {
			echo "error: INSERT INTO ($new_table)<br />\n" ;
			continue ;
		}

	}

	$_SESSION['protector_logger'] = $xoopsLogger->dumpQueries() ;

	redirect_header( 'prefix_manager.php' , 1 , _AM_MSG_DBUPDATED ) ;
	exit ;

// DUMP INTO A LOCAL FILE
} else if( ! empty( $_POST['backup'] ) && ! empty( $_POST['prefix'] ) ) {

	if( preg_match( '/[^0-9A-Za-z_-]/' , $_POST['prefix'] ) ) die( 'wrong prefix' ) ;

	// Ticket check
	if ( ! $xoopsGTicket->check() ) {
		redirect_header(XOOPS_URL.'/',3,$xoopsGTicket->getErrors());
	}

	$prefix = $_POST['prefix'] ;

	// get table list
	$srs = $xoopsDB->queryF( 'SHOW TABLE STATUS FROM `'.XOOPS_DB_NAME.'`' ) ;
	if( ! $xoopsDB->getRowsNum( $srs ) ) die( "You are not allowed to delete tables" ) ;

	$export_string = '' ;

	while( $row_table = $xoopsDB->fetchArray( $srs ) ) {
		$table = $row_table['Name'] ;
		if( substr( $table , 0 , strlen( $prefix ) + 1 ) !== $prefix . '_' ) continue ;
		$drs = $xoopsDB->queryF( "SHOW CREATE TABLE `$table`" ) ;
		$export_string .= "\nDROP TABLE IF EXISTS `$table`;\n".mysqli_result($drs, 0, 1).";\n\n" ;
		$result = mysqli_query($GLOBALS["___mysqli_ston"],  "SELECT * FROM `$table`" ) ;
		$fields_cnt = (($___mysqli_tmp = mysqli_num_fields( $result )) ? $___mysqli_tmp : false) ;
		$fields_meta = (((($___mysqli_tmp = mysqli_fetch_field_direct( $result , mysqli_field_tell( $result ))) && is_object($___mysqli_tmp)) ? ( (!is_null($___mysqli_tmp->primary_key = ($___mysqli_tmp->flags & MYSQLI_PRI_KEY_FLAG) ? 1 : 0)) && (!is_null($___mysqli_tmp->multiple_key = ($___mysqli_tmp->flags & MYSQLI_MULTIPLE_KEY_FLAG) ? 1 : 0)) && (!is_null($___mysqli_tmp->unique_key = ($___mysqli_tmp->flags & MYSQLI_UNIQUE_KEY_FLAG) ? 1 : 0)) && (!is_null($___mysqli_tmp->numeric = (int)(($___mysqli_tmp->type <= MYSQLI_TYPE_INT24) || ($___mysqli_tmp->type == MYSQLI_TYPE_YEAR) || ((defined("MYSQLI_TYPE_NEWDECIMAL")) ? ($___mysqli_tmp->type == MYSQLI_TYPE_NEWDECIMAL) : 0)))) && (!is_null($___mysqli_tmp->blob = (int)in_array($___mysqli_tmp->type, array(MYSQLI_TYPE_TINY_BLOB, MYSQLI_TYPE_BLOB, MYSQLI_TYPE_MEDIUM_BLOB, MYSQLI_TYPE_LONG_BLOB)))) && (!is_null($___mysqli_tmp->unsigned = ($___mysqli_tmp->flags & MYSQLI_UNSIGNED_FLAG) ? 1 : 0)) && (!is_null($___mysqli_tmp->zerofill = ($___mysqli_tmp->flags & MYSQLI_ZEROFILL_FLAG) ? 1 : 0)) && (!is_null($___mysqli_type = $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = (($___mysqli_type == MYSQLI_TYPE_STRING) || ($___mysqli_type == MYSQLI_TYPE_VAR_STRING)) ? "type" : "")) &&(!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && in_array($___mysqli_type, array(MYSQLI_TYPE_TINY, MYSQLI_TYPE_SHORT, MYSQLI_TYPE_LONG, MYSQLI_TYPE_LONGLONG, MYSQLI_TYPE_INT24))) ? "int" : $___mysqli_tmp->type)) &&(!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && in_array($___mysqli_type, array(MYSQLI_TYPE_FLOAT, MYSQLI_TYPE_DOUBLE, MYSQLI_TYPE_DECIMAL, ((defined("MYSQLI_TYPE_NEWDECIMAL")) ? constant("MYSQLI_TYPE_NEWDECIMAL") : -1)))) ? "real" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_TIMESTAMP) ? "timestamp" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_YEAR) ? "year" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && (($___mysqli_type == MYSQLI_TYPE_DATE) || ($___mysqli_type == MYSQLI_TYPE_NEWDATE))) ? "date " : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_TIME) ? "time" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_SET) ? "set" : $___mysqli_tmp->type)) &&(!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_ENUM) ? "enum" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_GEOMETRY) ? "geometry" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_DATETIME) ? "datetime" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && (in_array($___mysqli_type, array(MYSQLI_TYPE_TINY_BLOB, MYSQLI_TYPE_BLOB, MYSQLI_TYPE_MEDIUM_BLOB, MYSQLI_TYPE_LONG_BLOB)))) ? "blob" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type && $___mysqli_type == MYSQLI_TYPE_NULL) ? "null" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->type = ("" == $___mysqli_tmp->type) ? "unknown" : $___mysqli_tmp->type)) && (!is_null($___mysqli_tmp->not_null = ($___mysqli_tmp->flags & MYSQLI_NOT_NULL_FLAG) ? 1 : 0)) ) : false ) ? $___mysqli_tmp : false) ;
		$field_flags = array();
		for ($j = 0; $j < $fields_cnt; $j++) {
			$field_flags[$j] = (($___mysqli_tmp = mysqli_fetch_field_direct( $result ,  $j )->flags) ? (string)(substr((($___mysqli_tmp & MYSQLI_NOT_NULL_FLAG)       ? "not_null "       : "") . (($___mysqli_tmp & MYSQLI_PRI_KEY_FLAG)        ? "primary_key "    : "") . (($___mysqli_tmp & MYSQLI_UNIQUE_KEY_FLAG)     ? "unique_key "     : "") . (($___mysqli_tmp & MYSQLI_MULTIPLE_KEY_FLAG)   ? "unique_key "     : "") . (($___mysqli_tmp & MYSQLI_BLOB_FLAG)           ? "blob "           : "") . (($___mysqli_tmp & MYSQLI_UNSIGNED_FLAG)       ? "unsigned "       : "") . (($___mysqli_tmp & MYSQLI_ZEROFILL_FLAG)       ? "zerofill "       : "") . (($___mysqli_tmp & 128)                        ? "binary "         : "") . (($___mysqli_tmp & 256)                        ? "enum "           : "") . (($___mysqli_tmp & MYSQLI_AUTO_INCREMENT_FLAG) ? "auto_increment " : "") . (($___mysqli_tmp & MYSQLI_TIMESTAMP_FLAG)      ? "timestamp "      : "") . (($___mysqli_tmp & MYSQLI_SET_FLAG)            ? "set "            : ""), 0, -1)) : false) ;
		}
		$search = array("\x00", "\x0a", "\x0d", "\x1a");
		$replace = array('\0', '\n', '\r', '\Z');
		$current_row = 0;
		while( $row = mysqli_fetch_row($result) ) {
			$current_row ++ ;
			for( $j = 0 ; $j < $fields_cnt ; $j ++ ) {
				// NULL
				if (!isset($row[$j]) || is_null($row[$j])) {
					$values[] = 'NULL';
				// a number
				// timestamp is numeric on some mysql 4.1
				} elseif ($fields_meta[$j]->numeric && $fields_meta[$j]->type != 'timestamp') {
					$values[] = $row[$j];
				// a binary field
				// Note: with mysqli, under mysql 4.1.3, we get the flag
				// "binary" for those field types (I don't know why)
				} else if (stristr($field_flags[$j], 'BINARY')
						&& $fields_meta[$j]->type != 'datetime'
						&& $fields_meta[$j]->type != 'date'
						&& $fields_meta[$j]->type != 'time'
						&& $fields_meta[$j]->type != 'timestamp'
					   ) {
					// empty blobs need to be different, but '0' is also empty :-(
					if (empty($row[$j]) && $row[$j] != '0') {
						$values[] = '\'\'';
					} else {
						$values[] = '0x' . bin2hex($row[$j]);
					}
				// something else -> treat as a string
				} else {
					$values[] = '\'' . str_replace($search, $replace, addslashes($row[$j])) . '\'';
				} // end if
			} // end for

			$export_string .= "INSERT INTO `$table` VALUES (" . implode(', ', $values) . ");\n" ;
			unset($values);

		} // end while
		((mysqli_free_result( $result ) || (is_object( $result ) && (get_class( $result ) == "mysqli_result"))) ? true : false) ;

	}

	header('Content-Type: Application/octet-stream') ;
	header('Content-Disposition: attachment; filename="'.$prefix.'_'.date('YmdHis').'.sql"') ;
	header('Content-Length: '.strlen($export_string)) ;
	set_time_limit( 0 ) ;
	echo $export_string ;
	exit ;

// DROP TABLES
} else if( ! empty( $_POST['delete'] ) && ! empty( $_POST['prefix'] ) ) {

	if( preg_match( '/[^0-9A-Za-z_-]/' , $_POST['prefix'] ) ) die( 'wrong prefix' ) ;

	// Ticket check
	if ( ! $xoopsGTicket->check() ) {
		redirect_header(XOOPS_URL.'/',3,$xoopsGTicket->getErrors());
	}

	$prefix = $_POST['prefix'] ;

	// check if prefix is working
	if( $prefix == XOOPS_DB_PREFIX ) die( "You can't drop working tables" ) ;

	// check if prefix_xoopscomments exists
	$check_rs = $xoopsDB->queryF( "SELECT * FROM {$prefix}_xoopscomments LIMIT 1" ) ;
	if( ! $check_rs ) die( "This is not a prefix for XOOPS" ) ;

	// get table list
	$srs = $xoopsDB->queryF( 'SHOW TABLE STATUS FROM `'.XOOPS_DB_NAME.'`' ) ;
	if( ! $xoopsDB->getRowsNum( $srs ) ) die( "You are not allowed to delete tables" ) ;

	while( $row_table = $xoopsDB->fetchArray( $srs ) ) {
		$table = $row_table['Name'] ;
		if( substr( $table , 0 , strlen( $prefix ) + 1 ) !== $prefix . '_' ) continue ;
		$drs = $xoopsDB->queryF( "DROP TABLE `$table`" ) ;
	}

	$_SESSION['protector_logger'] = $xoopsLogger->dumpQueries() ;

	redirect_header( 'prefix_manager.php' , 1 , _AM_MSG_DBUPDATED ) ;
	exit ;
}


// beggining of Output
xoops_cp_header();
include( './mymenu.php' ) ;

// query
$srs = $xoopsDB->queryF( "SHOW TABLE STATUS FROM `".XOOPS_DB_NAME.'`' ) ;
if( ! $xoopsDB->getRowsNum( $srs ) ) {
	die( "You are not allowed to copy tables" ) ;
	xoops_cp_footer() ;
	exit ;
}

// search prefixes
$tables = array() ;
$prefixes = array() ;
while( $row_table = $xoopsDB->fetchArray( $srs ) ) {
	if( substr( $row_table["Name"] , -6 ) === '_users' ) {
		$prefixes[] = array(
				'name' => substr( $row_table["Name"] , 0 , -6 ) ,
				'updated' => $row_table["Update_time"]
			) ;
	}
	$tables[] = $row_table["Name"] ;
}


// table
echo "
<h3>"._AM_H3_PREFIXMAN."</h3>
<table class='outer' width='95%'>
	<tr>
		<th>PREFIX</th>
		<th>TABLES</th>
		<th>UPDATED</th>
		<th>COPY</th>
		<th>ACTIONS</th>
	</tr>
" ;

foreach( $prefixes as $prefix ) {

	// count the number of tables with the prefix
	$table_count = 0 ;
	$has_xoopscomments = false ;
	foreach( $tables as $table ) {
		if( $table == $prefix['name'] . '_xoopscomments' ) $has_xoopscomments = true ;
		if( substr( $table , 0 , strlen( $prefix['name'] ) + 1 ) === $prefix['name'] . '_' ) $table_count ++ ;
	}

	// check if prefix_xoopscomments exists
	if( ! $has_xoopscomments ) continue ;

	$prefix4disp = htmlspecialchars( $prefix['name'] , ENT_QUOTES ) ;
	$ticket_input = $xoopsGTicket->getTicketHtml( __LINE__ ) ;

	if( $prefix['name'] == XOOPS_DB_PREFIX ) {
		$del_button = '' ;
		$style_append = 'background-color:#FFFFFF' ;
	} else {
		$del_button = "<input type='submit' name='delete' value='delete' onclick='return confirm(\""._AM_CONFIRM_DELETE."\")' />" ;
		$style_append = '' ;
	}

	echo "
	<tr>
		<td class='odd' style='$style_append;'>$prefix4disp</td>
		<td class='odd' style='text-align:right;$style_append;'>$table_count</td>
		<td class='odd' style='text-align:right;$style_append;'>{$prefix['updated']}</td>
		<td class='odd' style='text-align:center;$style_append;' nowrap='nowrap'>
			<form action='' method='POST' style='margin:0px;'>
				$ticket_input
				<input type='hidden' name='old_prefix' value='$prefix4disp' />
				<input type='text' name='new_prefix' size='8' maxlength='16' />
				<input type='submit' name='copy' value='copy' />
			</form>
		</td>
		<td class='odd' style='text-align:center;$style_append;'>
			<form action='' method='POST' style='margin:0px;'>
				$ticket_input
				<input type='hidden' name='prefix' value='$prefix4disp' />
				$del_button
				<input type='submit' name='backup' value='backup' onclick='this.form.target=\"_blank\"' />
			</form>
		</td>
	</tr>\n" ;

}

echo "
</table>
<p>".sprintf(_AM_TXT_HOWTOCHANGEDB,XOOPS_ROOT_PATH,XOOPS_DB_PREFIX)."</p>

" ;

// Display Log if exists
if( ! empty( $_SESSION['protector_logger'] ) ) {
	echo $_SESSION['protector_logger'] ;
	$_SESSION['protector_logger'] = '' ;
	unset( $_SESSION['protector_logger'] ) ;
}

xoops_cp_footer();
?>
