<?php
// $Id: findusers.php,v 1.7 2003/03/21 13:12:25 w4z004 Exp $
//%%%%%%	File Name findusers.php 	%%%%%

define('_AM_FINDUS','Benutzer Finden');
define('_AM_AVATAR','Avatar');
define('_AM_REALNAME','Realer Name');
define('_AM_REGDATE','Anmeldedatum');
define('_AM_EMAIL','E-Mail');
define('_AM_PM','PM');
define('_AM_URL','URL');
define('_AM_PREVIOUS','Zurück');
define('_AM_NEXT','Vor');
define('_AM_USERSFOUND','%s Benutzer gefunden');

define('_AM_ACTUS', 'Aktiver Benutzer: %s');
define('_AM_INACTUS', 'Inaktive Benutzer: %s');
define('_AM_NOFOUND','Keinen Benutzer gefunden');
define('_AM_UNAME','User Name');
define('_AM_ICQ','ICQ');
define('_AM_AIM','AIM');
define('_AM_YIM','YIM');
define('_AM_MSNM','MSNM');
define('_AM_LOCATION','Wohnort');
define('_AM_OCCUPATION','Beruf');
define('_AM_INTEREST','Interessen');
define('_AM_URLC','URL');
define('_AM_LASTLOGMORE','Letzter Login vor mehr als<span style="color:#ff0000;">X</span> Tagen');
define('_AM_LASTLOGLESS','Letzter Login vor weniger als <span style="color:#ff0000;">X</span> Tagen');
define('_AM_REGMORE','Anmeldedatum ist vor mehr als <span style="color:#ff0000;">X</span> Tagen');
define('_AM_REGLESS','Anmeldedatum ist weniger als vor <span style="color:#ff0000;">X</span> Tagen');
define('_AM_POSTSMORE','Anzahl der Posts ist größer als <span style="color:#ff0000;">X</span>');
define('_AM_POSTSLESS','Anzahl der Posts ist weniger als<span style="color:#ff0000;">X</span>');
define('_AM_SORT','Sortieren nach');
define('_AM_ORDER','Order');
define('_AM_LASTLOGIN','Letzter Login');
define('_AM_POSTS','Posts gesamt');
define('_AM_ASC','Aufsteigende Reihenfolge');
define('_AM_DESC','Absteigende Reihenfolge');
define('_AM_LIMIT','Anzahl der Benutzer pro Seite');
define('_AM_RESULTS', 'Suchergebnisse');
define('_AM_SHOWMAILOK', 'Benutzer Typ nach Mail anzeigen');
define('_AM_MAILOK','Benutzer die einer Mail zugestimmt haben');
define('_AM_MAILNG','Benutzer die einer Mail nicht zugestimmt haben');
define('_AM_SHOWTYPE', 'Benutzer Typ anzeigen');
define('_AM_ACTIVE','Nur aktive Benutzer');
define('_AM_INACTIVE','Nur inaktive Benutzer');
define('_AM_BOTH', 'Alle Benutzer');
define('_AM_SENDMAIL', 'E-Mail senden');
define('_AM_ADD2GROUP', 'Beuzter zur %s Gruppe hinzufügen');
?>
