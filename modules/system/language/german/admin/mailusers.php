<?php
// $Id: mailusers.php,v 1.7 2003/03/21 13:12:26 w4z004 Exp $
//%%%%%%	Admin Module Name  MailUsers	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

//%%%%%%	mailusers.php 	%%%%%
define('_AM_SENDTOUSERS','Massage an Benutzer senden:');
define('_AM_SENDTOUSERS2','Senden an:');
define('_AM_GROUPIS','Gruppe ist (optional)');
define('_AM_TIMEFORMAT', '(Format JJJJ-MM-TT, optional)');
define('_AM_LASTLOGMIN','Letzer Login nach');
define('_AM_LASTLOGMAX','Letzter Login vor');
define('_AM_REGDMIN','Anmeldedatum ist nach');
define('_AM_REGDMAX','Anmeldedatum ist vor');
define('_AM_IDLEMORE','Letzter Login ist länger als X Tage her (optional)');
define('_AM_IDLELESS','Letzter Login ist weniger als X Tage her (optional)');
define('_AM_MAILOK','Nachricht nur an Benutzer versenden die dem eingewilligt hatten (optional)');
define('_AM_INACTIVE','Nur inaktive Benutzer benachrichtigen (optional)');
define('_AMIFCHECKD', 'Wenn Sie das anclicken wird das auch als private nachricht übermittelt');
define('_AM_MAILFNAME','Name (nur mail)');
define('_AM_MAILFMAIL','E-Mail (nur mail)');
define('_AM_MAILSUBJECT','Betraff');
define('_AM_MAILBODY','Nachricht');
define('_AM_MAILTAGS','nützliche Tags:');
define('_AM_MAILTAGS1','{X_UID}  Zeigt Benutzer ID');
define('_AM_MAILTAGS2','{X_UNAME} Zeigt Benutzernamen');
define('_AM_MAILTAGS3','{X_UEMAIL} Zeigt Benutzer E-Mail');
define('_AM_MAILTAGS4','{X_UACTLINK} Zeigt Benutzer Activation-link');
define('_AM_SENDTO','Senden an');
define('_AM_EMAIL','E-Mail');
define('_AM_PM','Private Message');
define('_AM_SENDMTOUSERS', 'Massage an Benutzer senden');
define('_AM_SENT', 'An Benutzer senden');
define('_AM_SENTNUM', '%s - %s (total: %s Benutzer)');
define('_AM_SENDNEXT', 'Nächste');
define('_AM_NOUSERMATCH', 'Kein Benutzer gefunden');
define('_AM_SENDCOMP', 'Nachrichten wurden komplett versendet.');

// class/xoopsmailer.php
define('_AM_MSGBODY', 'Message body wurde nicht gesetzt.');
define('_AM_FAILOPTPL', 'Temaplate Datei konnte nicht geöffnet werden.');
define('_AM_FNAMENG', 'Von Name wurde nciht ausgefüllt.');
define('_AM_FEMAILNG', 'Von Email wurde nicht ausgefüllt.');
define('_AM_SENDMAILNG', 'Konnte keine Mail an %s senden.');
define('_AM_MAILGOOD', 'Mail senden an %s.');
define('_AM_SENDPMNG', 'Konnte keine Privat Message an %s senden.');
define('_AM_PMGOOD', 'Private Message an %s senden.');

?>
