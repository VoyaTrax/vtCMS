<?php
// $Id: smilies.php,v 1.5 2003/02/22 19:39:20 w4z004 Exp $
//%%%%%%	Admin Module Name  Smilies 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_SMILESCONTROL','Smilie Verwaltung');
define('_AM_CODE','Code');
define('_AM_SMILIE','Smilie');
define('_AM_ACTION','Bearbeiten');
define('_AM_EDIT','Editieren');
define('_AM_DEL','Löschen');
define('_AM_CNRFTSD','Konnte nicht aus der Datenbank gelöscht werden.');
define('_AM_ADDSMILE','Smilie Hinzufügen');
define('_AM_EDITSMILE','Smilie Editieren');
define('_AM_SMILECODE','Code:');
define('_AM_SMILEURL','URL:');
define('_AM_SMILEEMOTION','Beschreibung:');
define('_AM_ADD','Hinzufügen');
define('_AM_SAVE','Speichern');
define('_AM_WAYSYWTDTS','Wollen sie den Smilie wirklich löschen?');define('_AM_DISPLAYF', 'Anzeigen?');
?>
