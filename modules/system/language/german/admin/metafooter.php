<?php
// $Id: metafooter.php,v 1.6 2003/03/21 13:12:26 w4z004 Exp $
//%%%%%%	Admin Module Name  MetaFooter 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_ERRDELOLD','Fehler beim löschen des alten Meta-Footers');
define('_AM_ERRINSERT','Fehler beim aktualisieren des Meta-Footers');
define('_AM_EDITMKF','bearbeite die Meta-Schlüsselwörter und Footer');
define('_AM_METAKEY','Meta-Schlüsselwörter');
define('_AM_TYPEBYCOM','Gib die Schlüsselwörter durch ein Komma getrennt ein. (myxoops, PHP, mysql, portal system)');
define('_AM_FOOTER','Footer');
define('_AM_BESURELINK','Gib Links mit den vollen Pfadangaben ein z.B. http://www.ihrName.de/link.gif, da sonst der Link nicht korrekt dargestellt wird.');
define('_AM_ADDCODE','Aktualisieren');
define('_AM_CLEAR','Löschen');
?>
