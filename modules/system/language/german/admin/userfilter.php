<?php
//%%%%%%	Admin Module Name  UserFilter 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_MODIFY','Ändern');
define('_AM_BWORDSSET','BadWords Filter Einstellungen');
define('_AM_IPFILTERSET','Mitglieder/IP Filter Einstellungen');
define('_AM_ERRORINSERT','FEHLER! Kann nicht in die Datenbank schreiben');
define('_AM_UNEED2ENTER','Du musst die erforderlichen Felder ausfüllen!');
define('_AM_ERRORDELETE','FEHLER! Kann nicht aus der Datenbank löschen');
define('_AM_ERRORUPDATE','FEHLER! Kann die Datenbank nicht aktualisieren');
define('_AM_CURRENTWCENSOR','Aktuell zensierte Wörter');
define('_AM_TOMODIFYWORD','Um ein Wort zu bearbeiten und/oder das Ersatzwort zu ändern, klicke einfach auf den Button ><b>Ändern</b><.');
define('_AM_TOREMOVEWORD','Um ein zensiertes Wort zu löschen, klicke auf den Button ><b>Löschen</b><.');
define('_AM_WORD','Wort');
define('_AM_REPLACEMENT','Ersatzwort');
define('_AM_EDIT','Ändern');
define('_AM_DELETE','Löschen');
define('_AM_ERRORQUERY','FEHLER! Kann keine Verbindung zur Datenbank herstellen');
define('_AM_NOWORDSINDB','Keine zensierten Wörter in der Datenbank. Du kannst ein zensiertes Wort in dem unteren Formular eingeben');
define('_AM_ADDAWORD','Wort hinzufügen');
define('_AM_FORM2ADDWORD','Nutze dieses Formular um ein neues Wort hinzuzufügen.');
define('_AM_ACTION','Funktionen');
define('_AM_ERRORNOUSER','FEHLER! Kein passender Mitglieder-Eintrag!');
define('_AM_DURATION','Dauer');
define('_AM_IPADDRESS','IP Addresse');
define('_AM_SECONDS','Sekunden');
define('_AM_MINUTES','Minuten');
define('_AM_HOURS','Stunden');
define('_AM_DAYS','Tage');
define('_AM_YEARS','Jahre');
define('_AM_CURBANIP','Im moment verbannte IPs');
define('_AM_CURBANUSER','Im moment verbannte Mitgliedernamen');
define('_AM_USERNAME','Mitglieder-Name');
define('_AM_ADDABAN','Hinzufügen');
define('_AM_FORM2ADDBAN','Nutze das folgende Formular um Mitglieder oder IP´s zur Bannliste hinzuzufügen.');
define('_AM_TOBANIPS','Um eine aufeinanderfolgende Reihe von IP´s zu bannen, gib nicht die kompletten IP an, sondern: 192.168.1. Es werden gebannt 192.168.1.0-255<br>
     Verbannungen werden nach Ablauf der Zeit automatisch aufgehoben, um jemanden auf Dauer zu verbannen, lasse das Feld der Zeitangabe leer.');
define('_AM_IPUNAME','IP/Mitgliedername');

define('_AM_FILTERSETTINGS','Filtereinstellungen');
define('_AM_BADIPS','Ban IP-Addresse');
define('_AM_BADWORDS','Böse Wörter Liste');
define('_AM_BADUNAMES','Böse Mitglieder Liste');
define('_AM_ENTERIPS','Tragen Sie die IP-Addresse ein die von der Seite verbannt werden soll.<br>Tragen Sie immer nur eine IP-Addresse pro Zeile ein.');
define('_AM_BADIPSTART','^aaa.bbb.ccc wird Besucher zurückweisen, deren IP anfängt mit. aaa.bbb.ccc');
define('_AM_BADIPEND','aaa.bbb.ccc$ wird Besucher zurückweisen, deren IP endet mit aaa.bbb.ccc');
define('_AM_BADIPCONTAIN','aaa.bbb.ccc wird Besucher zurückweisen, deren IP beinhaltet aaa.bbb.ccc');
define('_AM_ENTERUNAMES','Tragen Sie die ein, die (als Mitgliedsnamen) nicht ausgewählt werden sollen.<br>Tragen Sie immer nur einen Namen pro Zeile ein.');
define('_AM_ENTERWORDS','Tragen Sie die Wörter hier ein, die in Beitragen ersetzt werden sollen. Das Wort wird dann ersetzt durch ####.<br>Tragen Sie immer nur ein Wort pro Zeile ein.');
?>
