<?php
// $Id: groups.php,v 1.7 2003/03/21 13:12:26 w4z004 Exp $
//%%%%%%	Admin Module Name  AdminGroup 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_EDITADG','Gruppe Administration');
define('_AM_MODIFY','Editieren');
define('_AM_DELETE','Löschen');
define('_AM_CREATENEWADG','Neue Gruppe Anlegen');
define('_AM_NAME','Name');
define('_AM_DESCRIPTION','Beschreibung');
define('_AM_INDICATES','* Pflichtfelder');
define('_AM_SYSTEMRIGHTS','System Adminrechte');
define('_AM_ACTIVERIGHTS','Modul Adminrechte');
define('_AM_IFADMIN','Für Administratoren werden die Blöcke automatisch angezeigt');
define('_AM_ACCESSRIGHTS','Modul Gruppenrechte');
define('_AM_UPDATEADG','Gruppe bearbeiten');
define('_AM_MODIFYADG','Gruppe editieren');
define('_AM_DELETEADG','Gruppe löschen');
define('_AM_AREUSUREDEL','Soll die %s Gruppe wirklich gelöscht werden?');
define('_AM_YES','Ja');
define('_AM_NO','Nein');
define('_AM_EDITMEMBER','Editiere Benutzer dieser Gruppe');
define('_AM_MEMBERS','Benutzer');
define('_AM_NONMEMBERS','keine Benutzer');
define('_AM_ADDBUTTON',' einfügen --> ');
define('_AM_DELBUTTON','<--löschen');
define('_AM_UNEED2ENTER','Füllen Sie die Pflichtfelder aus!');

// Added in RC3
define('_AM_BLOCKRIGHTS','Block Gruppen Rechte');

define('_AM_FINDU4GROUP', 'Mitglieder für diese Gruppe suchen');
define('_AM_GROUPSMAIN', 'Gruppen Menü');
?>
