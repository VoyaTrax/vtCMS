<?php
// $Id: banners.php,v 1.7 2003/03/21 13:12:25 w4z004 Exp $
//%%%%%%        Admin Module Name  Banners         %%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_CURACTBNR','Aktive Banner');
define('_AM_BANNERID','Banner ID');
define('_AM_IMPRESION','Eindrücke');
define('_AM_IMPLEFT','Imp. Links');
define('_AM_CLICKS','Aufrufe');
define('_AM_NCLICKS','% aufgerufen');
define('_AM_CLINAME','Kunden Name');
define('_AM_FUNCTION','Funktionen');
define('_AM_UNLIMIT','unbegrenzt');
define('_AM_EDIT','Editieren');
define('_AM_DELETE','Löschen');
define('_AM_FINISHBNR','Fertige Banner');
define('_AM_IMPD','Imp.');
define('_AM_STARTDATE','Datum Start');
define('_AM_ENDDATE','Datum Ende');
define('_AM_ADVCLI','Werbe Kunden');
define('_AM_ACTIVEBNR','Aktive Banner');
define('_AM_CONTNAME','Kontakt Name');
define('_AM_CONTMAIL','Kontakt E-Mail');
define('_AM_CLINAMET','Kunden Name:');
define('_AM_ADDNWBNR','Neuen Banner einfügen');
define('_AM_IMPPURCHT','gekaufte Einblendungen:');
define('_AM_IMGURLT','Banner URL:');
define('_AM_CLICKURLT','Link URL:');
define('_AM_ADDBNR','Banner hinzufügen');
define('_AM_ADDNWCLI','Neuen Kunden eintragen');
define('_AM_CONTNAMET','Kontakt Name:');
define('_AM_CONTMAILT','Kontakt E-Mail:');
define('_AM_CLILOGINT','Kunden Login:');
define('_AM_CLIPASST','Kunden Password:');
define('_AM_ADDCLI','Kunde hinzufügen');
define('_AM_DELEBNR','Banner Löschen');
define('_AM_SUREDELE','Wollen Sie das Banner wirklich löschen?');
define('_AM_NO','Nein');
define('_AM_YES','Ja');
define('_AM_EDITBNR','Banner editieren');
define('_AM_ADDIMPT','Mehr Eindrücke zuweisen:');
define('_AM_PURCHT','gekaufte:');
define('_AM_MADET','erhaltene:');
define('_AM_CHGBNR','Banner bearbeiten');
define('_AM_DELEADC','Kunden Löschen');
define('_AM_SUREDELCLI','Sind Sie sich sicher das sie <b>%s</b> und all seine Banner löschen wollen???');
define('_AM_NOBNRRUN','Dieser Kunde hat keine aktiven Banner.');
define('_AM_WARNING','WARRNUNG!!!');
define('_AM_ACTBNRRUN','Dieser Kunde hat folgende aktive Banner auf unserer Seite:');
define('_AM_SUREDELBNR','Sind Sie sich sicher, das sie den Kunden und all seine Banner löschen wollen??');
define('_AM_EDITADVCLI','Kunden editieren');
define('_AM_EXTINFO','Extra Info:');
define('_AM_CHGCLI','Kunden ändern');
define('_AM_USEHTML','Html benutzen ?');
define('_AM_CODEHTML','Beschreibung:');

?>
