<?php
// $Id: tplsets.php,v 1.2 2003/03/21 13:12:27 w4z004 Exp $
//%%%%%% Template Manager %%%%%
define('_MD_TPLMAIN','Templates Manager');
define('_MD_INSTALL','Installieren');
define('_MD_EDITTEMPLATE','Templatedatei editieren');
define('_MD_FILENAME','Datei Name');
define('_MD_FILEDESC','Beschreibung');
define('_MD_LASTMOD','zuletzt geändert');
define('_MD_FILEMOD','zuletzt geändert (Datei)');
define('_MD_FILECSS','CSS');
define('_MD_FILEHTML','HTML');
define('_MD_AM_BTOTADMIN', 'Zurück zum Template Manager');
define('_MD_RUSUREDELTH', 'Soll das Template und alle seine Daten wirklich gelöscht werden?');
define('_MD_RUSUREDELTPL', 'Soll das Template und seine Daten wirklich gelöscht werden?');
define('_MD_PLZINSTALL', 'Zum Starten der Installation hier drücken');
define('_MD_PLZGENERATE', 'Klicken Sie auf den unteren Button um die Daten zu generieren');
define('_MD_CLONETHEME','Kopiere ein Template-Set');
define('_MD_THEMENAME','Basis Template-Set');
define('_MD_NEWNAME','Geben Sie einen neuen Template-Set Namen ein');
define('_MD_IMPORT','Importieren');
define('_MD_RUSUREIMPT', 'Durch das Importieren vom Template-Daten aus dem Template Verzeichnis werden Ihre Änderungen in der Datenbank überschrieben.<br />Klicken Sie "Import" zum starten.');
define('_MD_THMSETNAME','Name');
define('_MD_CREATED','Created');
define('_MD_SKIN','Skin');
define('_MD_TEMPLATES','Templates');
define('_MD_EDITSKIN','Skin editieren');
define('_MD_NOFILE','Keine Datei');
define('_MD_VIEW','anschauen');
define('_MD_COPYDEFAULT','Kopiere Standard Datei');
define('_MD_DLDEFAULT','Download Standard Datei');
define('_MD_VIEWDEFAULT','Zeige Standard-Template');
define('_MD_DOWNLOAD','Download');
define('_MD_UPLOAD','Upload');
define('_MD_GENERATE','Generieren');
define('_MD_CHOOSEFILE', 'Datei zum Upload auswählen');
define('_MD_UPWILLREPLACE', 'Durch das Hochladen dieser Datei werden Ihre Daten in Ihre Datenbank gecshrieben!');
define('_MD_UPLOADTAR', 'Upload ein Template-Set');
define('_MD_CHOOSETAR', 'Wähle ein Template-Set-Package zum Upload');
define('_MD_ONLYTAR', 'Muss eine tar.gz/.tar Datei mit einer gültigen  XOOPS Template-Set Struktur sein');
define('_MD_NTHEMENAME', 'Neuer Template-Set name');
define('_MD_ENTERTH', 'Neuer Template-Set Name für dieses Paket. Lassen Sie es frei für eine automatische Erkennung.');
define('_MD_TITLE','Titel');
define('_MD_CONTENT','Block');
define('_MD_ACTION','Ändern/Editieren');
define('_MD_DEFAULTTHEME','Ihre Seite verwendet dieses Template-Set als Standart');
define('_MD_AM_ERRTHEME', 'Dieses Template enthält kein gültiges Skin File. Bitte drücken Sie Löschen um die restlichen Daten die zu diesem Template gehören zu entfernen!');
define('_MD_SKINIMGS','Skin image files');
define('_MD_EDITSKINIMG','Editiere skin image files');
define('_MD_IMGFILE','Datei Name');
define('_MD_IMGNEWFILE','Neue Datei hochladen');
define('_MD_IMGDELETE','Löschen');
define('_MD_ADDSKINIMG','Add skin image file');
define('_MD_BLOCKHTML', 'Block HTML');
define('_MD_IMAGES', 'Bilder');
define('_MD_NOZLIB', 'Zlib support muß auf dem Server eingeschaltet sein');
define('_MD_LASTIMP', 'Zuletzt importiert');
define('_MD_FILENEWER', 'Eine neuere Datei, die noch nicht importiert wurde, existiert im <b>templates</b> Ordner.');
define('_MD_FILEIMPORT', 'Eine ältere Datei, die noch nicht importiert wurde, existiert im <b>templates</b> Ordner.');
define('_MD_FILEGENER', 'Template existiert nicht, kann aber generiert (Kopie vom <b>default</b>-Template), hochgeladen, oder aus dem b>templates</b> Ordner importiert werden.');

?>
