<?php
// $Id: users.php,v 1.7 2003/03/21 13:12:27 w4z004 Exp $
//%%%%%%	Admin Module Name  Users 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_AM_AYSYWTDU','Soll das Mitglied %s wirkich gelöscht werden?');
define('_AM_BYTHIS','Es werden alle Daten von dem Benutzer gelöscht, wenn Sie fortfahren.');
define('_AM_YES','Ja');
define('_AM_NO','Nein');
define('_AM_YMCACF','Es müßen alle Felder ausgefüllt werden');
define('_AM_CNRNU','Neue Benutzer konnte nicht registriert werden.');
define('_AM_EDEUSER','Mitglied editieren/löschen');
define('_AM_NICKNAME','Nickname');
define('_AM_MODIFYUSER','Mitglied editieren');
define('_AM_DELUSER','Mitglied löschen');
define('_AM_GO','Los!');
define('_AM_ADDUSER','Mitglied hinzufügen');
define('_AM_NAME','Name');
define('_AM_EMAIL','E-Mail');
define('_AM_OPTION','Option');
define('_AM_AVATAR','Avatar');
define('_AM_THEME','Theme');
define('_AM_AOUTVTEAD','E-Mail Adresse für alle sichtbar');
define('_AM_URL','URL');
define('_AM_ICQ','ICQ');
define('_AM_AIM','AIM');
define('_AM_YIM','YIM');
define('_AM_MSNM','MSNM');
define('_AM_LOCATION','Wohnort');
define('_AM_OCCUPATION','Beruf');
define('_AM_INTEREST','Interessen');
define('_AM_RANK','Rang');
define('_AM_NSRA','Kein Spezial Rang vorhanden');
define('_AM_NSRID','Keine Spezialränge in der Datenbank vorhanden');
define('_AM_ACCESSLEV','Gruppen Level');
define('_AM_SIGNATURE','Signatur');
define('_AM_PASSWORD','Password');
define('_AM_INDICATECOF','* Pflichtfelder die ausgefüllt werden müßen');
define('_AM_NOTACTIVE','Dieser User wurde noch nicht aktiviert. Soll der User aktiviert werden?');
define('_AM_UPDATEUSER','User ändern');
define('_AM_USERINFO','User Info');
define('_AM_USERID','User ID');
define('_AM_RETYPEPD','Password wiederholen');
define('_AM_CHANGEONLY','(für nur Änderungen)');
define('_AM_USERPOST','Benutzer Beiträge');
define('_AM_STORIES','Stories');
define('_AM_COMMENTS','Comments');
define('_AM_PTBBTSDIYT','Zum Synchronisieren der Beiträge den Button drücken und die Daten werden aktualisiert');
define('_AM_SYNCHRONIZE','Synchronisieren');
define('_AM_USERDONEXIT','Benutzer existiert nicht!');
define('_AM_STNPDNM','Falsches Password eingegeben, gehen Sie zurück und probieren Sie es nocheinmal');
define('_AM_CNGTCOM','Konnte nicht die Gesamtanzahl Kommentare ermitteln');
define('_AM_CNGTST','Konnte nicht die Gesamtanzahl Nachrichten ermitteln');
define('_AM_CNUUSER','Benutzer kann nicht aktualisiert werden');
define('_AM_CNGUSERID','Konnte User IDS nicht finden');
define('_AM_LIST','List');
define('_AM_NOUSERS', 'Kein User ausgewählt');
?>
