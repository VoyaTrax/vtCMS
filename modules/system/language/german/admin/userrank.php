<?php
// $Id: userrank.php,v 1.7 2003/03/21 13:12:27 w4z004 Exp $
//%%%%%%	Admin Module Name  UserRank 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);
define('_AM_RANKSSETTINGS','User Rang Einstellungen');
define('_AM_TITLE','Titel');
define('_AM_MINPOST','Min. Posts');
define('_AM_MAXPOST','Max. Posts');
define('_AM_IMAGE','Bild');
define('_AM_SPERANK','Spezial Rang');
define('_AM_ON','An');
define('_AM_OFF','Aus');
define('_AM_EDIT','Editieren');
define('_AM_DEL','Löschen');
define('_AM_ADDNEWRANK','Neuen Rang einfügen');
define('_AM_RANKTITLE','Rang Titel');
define('_AM_SPECIAL','Spezial');
define('_AM_ADD','Einfügen');
define('_AM_EDITRANK','Rang editieren');
define('_AM_ACTIVE','aktivieren');
define('_AM_SAVECHANGE','Einstellungen speichern');
define('_AM_WAYSYWTDTR','Wollen Sie den Rang wirklich löschen?');
define('_AM_YES','Ja');
define('_AM_NO','Nein');
define('_AM_VALIDUNDER','(Ein gültiges image file unter dem <b>%s</b> Verzeichniss)');
define('_AM_SPECIALCAN','(Spezialränge können auch nach Anzahl der Beiträge ausgegeben werden.)');
define('_AM_ACTION','Ändern/Löschen');
?>
