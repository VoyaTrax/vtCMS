<?php
// $Id: blocksadmin.php,v 1.7 2003/03/21 13:12:25 w4z004 Exp $
//%%%%%%	Admin Module Name  Blocks 	%%%%%
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

//%%%%%%	blocks.php 	%%%%%
define('_AM_BADMIN', 'Blocks Administration');
define('_AM_ADDBLOCK', 'Neuen Block Erstellen');
define('_AM_LISTBLOCK', 'Alle Blöcke');
define('_AM_SIDE', 'Seite');
define('_AM_BLKDESC', 'Block Beschreibung');
define('_AM_TITLE', 'Titel');
define('_AM_WEIGHT','Position');
define('_AM_ACTION','Aktion');
define('_AM_BLKTYPE','Block Typ');
define('_AM_LEFT','Links');
define('_AM_RIGHT','Rechts');
define('_AM_CENTER','Mitte');
define('_AM_VISIBLE','Sichtbar');
define('_AM_POSCONTT','Position des eingefügten Blockes');
define('_AM_ABOVEORG','Über den vorhandenen Block');
define('_AM_AFTERORG','Unter den vorhandenen Block');
define('_AM_EDIT','Editieren');
define('_AM_DELETE','Löschen');
define('_AM_SBLEFT','Seiten Block - Links');
define('_AM_SBRIGHT','Seiten Block - Rechts');
define('_AM_CBLEFT','Center Block - Links');
define('_AM_CBRIGHT','Center Block - Rechts');
define('_AM_CBCENTER','Center Block - Mitte');
define('_AM_CONTENT','Block');
define('_AM_OPTIONS','Optionen');
define('_AM_CTYPE','Block Typ');
define('_AM_HTML','HTML');
define('_AM_PHP','PHP Script');
define('_AM_AFWSMILE','Auto Format (Smilies an)');
define('_AM_AFNOSMILE','Auto Format (Smilies aus)');
define('_AM_SUBMIT','senden');
define('_AM_CUSTOMHTML','Eigener Block (HTML)');
define('_AM_CUSTOMPHP','Eigener Block (PHP)');
define('_AM_CUSTOMSMILE','Eigener Block (Auto Format + Smilies)');
define('_AM_CUSTOMNOSMILE','Eigener Block (Auto Format)');
define('_AM_DISPRIGHT','Im rechten Block anzeigen');
define('_AM_SAVECHANGES','Einstellungen speichern');
define('_AM_EDITBLOCK','Block editieren');
define('_AM_SYSTEMCANT','Der Block wurde gelöscht!');
define('_AM_MODULECANT','Der Block kann nicht gelöscht werden! Es muß erst das dazugehörige Modul deaktiviert werden.');
define('_AM_RUSUREDEL','Wollen Sie diesen <b>%s</b> Block wirklich löschen?');
define('_AM_NAME','Name');
define('_AM_USEFULTAGS','N&uuml;tzliche Tags:');
define('_AM_BLOCKTAG1','%s will print %s');
define('_AM_SVISIBLEIN', 'Block sichtbar in %s');
define('_AM_TOPPAGE', 'Startseite');
define('_AM_VISIBLEIN', 'Sichtbar in');
define('_AM_ALLPAGES', 'Auf allen Seiten');
define('_AM_TOPONLY', 'Top Page Only');
define('_AM_ADVANCED', 'Erweiterte Einstellungen');
define('_AM_BCACHETIME', 'Cache Zeit');
define('_AM_BALIAS', 'Alias Name');
define('_AM_CLONE', 'Block duplizieren');  // clone a block
define('_AM_CLONEBLK', 'duplizierte Blöcke'); // cloned block
define('_AM_CLONEBLOCK', 'Duplizierten Block erstellen');
define('_AM_NOTSELNG', '"%s" ist nicht ausgewählt!'); // error message
define('_AM_EDITTPL', 'Template editieren');
define('_AM_MODULE', 'Module');
?>
