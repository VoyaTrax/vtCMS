<?php
// $Id: images.php,v 1.5 2003/02/22 19:39:20 w4z004 Exp $
//%%%%%% Image Manager %%%%%


define('_MD_IMGMAIN','Bild Manager Menü');

define('_MD_ADDIMGCAT','Kategorie hinzufügen:');
define('_MD_EDITIMGCAT','Kategorie editieren:');
define('_MD_IMGCATNAME','Kategorie Name:');
define('_MD_IMGCATRGRP','Gruppe auswählen die den Bildermanager benutzen dürfen:<br /><br /><span style="font-weight: normal;">Ein Uplaodrecht ist darin nicht enthalten.</span>');
define('_MD_IMGCATWGRP','Gruppe auswählen, den ein Bilderupload gestattet wird:<br /><br /><span style="font-weight: normal;">Voreingestellt für Moderatoren und Administratoren.</span>');
define('_MD_IMGCATWEIGHT','Anzeige im Bildmanager:');
define('_MD_IMGCATDISPLAY','Kategorie sichtbar?');
define('_MD_IMGCATSTRTYPE','Bilder werden geuppt nach:');
define('_MD_STRTYOPENG','Dies kann später nicht mehr geändert werden!');
define('_MD_INDB',' Bild speichern in Datenbank(as binary "blob" data)');
define('_MD_ASFILE',' Bild speichern (in Upload Ordner)<br />');
define('_MD_RUDELIMGCAT','Sind Sie sicher, das diese Kategorie und alle ihre Bilder gelöscht werden sollen?');
define('_MD_RUDELIMG','Soll das Bild wirklich gelöscht werden?');

define('_MD_FAILDEL', '%s Konnte nicht gelöscht werden');
define('_MD_FAILDELCAT', '%s Kategorie, konnte nicht aus der Datenbank gelöscht werden');
define('_MD_FAILUNLINK', '%s konnte nicht vom Server gelöscht werden');
?>
