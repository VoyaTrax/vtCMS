<?php
// $Id: preferences.php,v 1.7 2003/03/21 13:12:26 w4z004 Exp $
//%%%%%%	Admin Module Name  AdminGroup 	%%%%%
// dont change
define('_AM_DBUPDATED',_MD_AM_DBUPDATED);

define('_MD_AM_SITEPREF','Seiten Einstellungen');
define('_MD_AM_SITENAME','Name der Seite');
define('_MD_AM_SLOGAN','Slogan der Seite');
define('_MD_AM_ADMINML','Admin E-Mail Adresse');
define('_MD_AM_LANGUAGE','Sprachauswahl');
define('_MD_AM_STARTPAGE','Modul der Startseite');
define('_MD_AM_NONE','keins');
define('_MD_AM_SERVERTZ','Server Zeitzone');
define('_MD_AM_DEFAULTTZ','Zeitzone');
define('_MD_AM_DTHEME','Theme der Seite');
define('_MD_AM_THEMESET','Start Theme');
define('_MD_AM_ANONNAME','Benutzername für Gäste');
define('_MD_AM_MINPASS','Min. Zeichenlänge des Passwordes');
define('_MD_AM_NEWUNOTIFY','E-Mail Nachricht, wenn neuer Benutzer sich anmeldet?');
define('_MD_AM_SELFDELETE','Darf ein Benutzer sein Account löschen?');
define('_MD_AM_LOADINGIMG','Seite wird geladen... Anzeige aktivieren?');
define('_MD_AM_USEGZIP','gzip compression aktivieren?');
define('_MD_AM_UNAMELVL','Was für Zeichen dürfen für Benutzernamen verwendet werden?');
define('_MD_AM_STRICT','Genaue Reihenfolge (Nur Zahlen und Buchstaben)');
define('_MD_AM_MEDIUM','Mittel');
define('_MD_AM_LIGHT','Leicht (Alle Zeichen erlaubt)');
define('_MD_AM_USERCOOKIE','Name für Benutzer Cookies.');
define('_MD_AM_USERCOOKIEDSC','Legt einen Cookie für 1 Jahr an und der Benutzername steht immer im Login.');
define('_MD_AM_USEMYSESS','Verwenden Sie User bezogene Sessions');
define('_MD_AM_USEMYSESSDSC','Wählen Sie Ja um User bezogene Sessions zu verwenden.');
define('_MD_AM_SESSNAME','Session Name');
define('_MD_AM_SESSNAMEDSC','Der Name der Session (Nur gültig, wenn "Userspezifische Session" eingeschaltet ist)');
define('_MD_AM_SESSEXPIRE','Max. Sitzungslänge in Sekunden');
define('_MD_AM_SESSEXPIREDSC','Dies Cookie ermöglicht dem Mitglied automatisch innerhalbe einer bestimmten Zeitspanne angemeldet zu sein.');
define('_MD_AM_BANNERS','Banneranzeige aktivieren?');
//define('_MD_AM_ADMINGRAPHIC','Grafikmenü für Admin aktivieren?');
define('_MD_AM_MYIP','Ihre IP Adresse');
define('_MD_AM_MYIPDSC','Diese IP wird nicht für Impressionen der Banner gewertet');
define('_MD_AM_ALWDHTML','HTML tags in allen Beiträgen zulassen.');
define('_MD_AM_INVLDMINPASS','Min. Zeichenlänge des Passwordes.');
define('_MD_AM_INVLDUCOOK','Unzulässiger Wert für usercookie name.');
define('_MD_AM_INVLDSCOOK','Unzulässiger Wert für sessioncookie name.');
define('_MD_AM_INVLDSEXP','Unzulässiger Wert für die session Verfallzeit.');
define('_MD_AM_ADMNOTSET','Admin E-Mail ist nicht gesetzt.');
define('_MD_AM_YES','Ja');
define('_MD_AM_NO','Nein');
define('_MD_AM_DONTCHNG','nicht verändern!');
define('_MD_AM_REMEMBER','Ändern Sie den chmod auf 666 dieser Datei, um das System Einträge vornehmen zu lassen.');
define('_MD_AM_IFUCANT','Sollten Sie die Einstellung nicht automatisch vornehmen können, ändern Sie deise bitte von Hand.');


define('_MD_AM_COMMODE','Standartmodus für Komentaranzeige');
define('_MD_AM_COMORDER','Standart Ordnung für Komentare');
//define('_MD_AM_ALLOWSIG','Signaturen immer anhängen?');
define('_MD_AM_ALLOWHTML','HTML tags immer aktivieren?');
define('_MD_AM_DEBUGMODE','Debug Modus');
define('_MD_AM_DEBUGMODEDSC','Verschiedene Debug Optionen. Bei funktionierenden Webseiten ausschalten.');
define('_MD_AM_AVATARALLOW','Dürfen Benutzer Avatare hochladen?');
define('_MD_AM_AVATARMP','nötige Mindestbeiträge');
define('_MD_AM_AVATARMPDSC','Eingabe der Mindestbeiträge bevor der Benutzer ein Avatar hochladen darf');
define('_MD_AM_AVATARW','Avatar max Breite (pixel)');
define('_MD_AM_AVATARH','Avatar max Höhe (pixel)');
define('_MD_AM_AVATARMAX','Avatar max. Größe (in byte)');
define('_MD_AM_AVATARCONF','Einstellungen für Benutzeravatare');
define('_MD_AM_CHNGUTHEME','Theme aller Benutzer ändern');
define('_MD_AM_NOTIFYTO','Gruppe auswählen die eine Nachricht erhalten auf Antworten ihrer Beiträge');
define('_MD_AM_ALLOWTHEME','Benutzer darf sein Theme selber wählen?');
define('_MD_AM_ALLOWIMAGE','Benutzer darf in Beiträgen Bilder verwenden?');

define('_MD_AM_USERACTV','Aktivierung über den Benutzer (E-Mail)');
define('_MD_AM_AUTOACTV','automatische Aktivierung');
define('_MD_AM_ADMINACTV','Aktivierung durch den Admin');
define('_MD_AM_ACTVTYPE','Auswahl der Registrierungsmethode bei Anmeldung eines neuen Benutzers');
define('_MD_AM_ACTVGROUP','Gruppe auswählen die ein Benachrichtigung bekommt, wenn ein neuer Benutzer sich anmeldet');
define('_MD_AM_ACTVGROUPDSC','Grundeinstellung ist die Administratorbenachrichtigung');
define('_MD_AM_USESSL', 'SSL Modus für Login?');
define('_MD_AM_SSLPOST', 'SSL Post variabler Name ');
define('_MD_AM_SSLPOSTDSC', 'Der verwendete Name der Variable für den Sessions Transfer via POST. Wenn Sie unsicher sind, belassen Sie die Voreinstellung.');
define('_MD_AM_DEBUGMODE0','Aus');
define('_MD_AM_DEBUGMODE1','PHP Debug');
define('_MD_AM_DEBUGMODE2','mysql/Blocks Debug');
define('_MD_AM_DEBUGMODE3','Smarty Templates Debug');
define('_MD_AM_MINUNAME', 'Min Länge des Benutzernamens');
define('_MD_AM_MAXUNAME', 'Max Länge des Benutzernamens');
define('_MD_AM_GENERAL', 'Haupteinstellungen');
define('_MD_AM_USERSETTINGS', 'Einstellungen Benutzer');
define('_MD_AM_ALLWCHGMAIL', 'Darf der Benutzer seine E-Mail Adresse ändern?');
define('_MD_AM_ALLWCHGMAILDSC', '');
define('_MD_AM_IPBAN', 'IP Bannen');
define('_MD_AM_BADEMAILS', 'E-Mail Adressen, die nicht zugelassen werden');
define('_MD_AM_BADEMAILSDSC', 'Einträge mit einem <b>|</b>, trennen.');
define('_MD_AM_BADUNAMES', 'Zensierte Benutzernamen, die nicht zugelassen werden');
define('_MD_AM_BADUNAMESDSC', 'Einträge mit einem <b>|</b>, trennen.');
define('_MD_AM_DOBADIPS', 'IP Bann aktivieren?');
define('_MD_AM_DOBADIPSDSC', 'Benutzer mit diesen IP dürfen die Seite nicht betreten');
define('_MD_AM_BADIPS', 'Eingabe der IP Adresse, die diese Seite nicht besuchen dürfen.<br />Einträge mit einem <b>|</b>, trennen.');
define('_MD_AM_BADIPSDSC', '^aaa.bbb.ccc Wird Besucher abweisen die mit einer aaa.bbb.ccc beginnen<br />aaa.bbb.ccc$ will Wird Besucher abweisen die mit einer aaa.bbb.ccc enden<br />aaa.bbb.ccc wird Besucher abweisen die eine aaa.bbb.ccc in ihrer IP haben');
define('_MD_AM_PREFMAIN', 'Einstellungs Menü');
define('_MD_AM_METAKEY', 'Meta Keywörter');
define('_MD_AM_METAKEYDSC', 'Meta Keywörter dienen den Suchmaschinen um Inhalte nach Stichpunkten zu finden. Mehrere Meta Tags mit einem komma und anschließenden Space trennen. z.B (Ex. XOOPS, PHP, mysql, portal system)');
define('_MD_AM_METARATING', 'Meta Einstellung');
define('_MD_AM_METARATINGDSC', 'Diese Angaben stellen die Grundinformationen ihrer Seite dar.');
define('_MD_AM_METAOGEN', 'General');
define('_MD_AM_METAO14YRS', '14 Jahre');
define('_MD_AM_METAOREST', 'Beschränken');
define('_MD_AM_METAOMAT', 'ausgereift (?!?!?)');
define('_MD_AM_METAROBOTS', 'Meta Robots');
define('_MD_AM_METAROBOTSDSC', 'Die Robo Tags "erklären" den Suchmaschinen, wo sie den Inhalt auf einer Seite finden.');
define('_MD_AM_INDEXFOLLOW', 'Index, folgen');
define('_MD_AM_NOINDEXFOLLOW', 'Nicht im Index, folgen');
define('_MD_AM_INDEXNOFOLLOW', 'Index, nicht folgen');
define('_MD_AM_NOINDEXNOFOLLOW', 'Nicht im Index, nicht folgen');
define('_MD_AM_METAAUTHOR', 'Meta Author');
define('_MD_AM_METAAUTHORDSC', 'Hier können Daten über den Author einer Seite eingegeben werden, diese dann auch ein Copyright darstellen.');
define('_MD_AM_METACOPYR', 'Meta Copyright');
define('_MD_AM_METACOPYRDSC', 'Das Copyright und die Metatags zeichnen die Homepage auch mit einer Echtheit aus.');
define('_MD_AM_METADESC', 'Meta Beschreibung');
define('_MD_AM_METADESCDSC', 'Die angegebenen Meta Tags gelten für alle Seiten der Homepage');
define('_MD_AM_METAFOOTER', 'Meta Tags und Footer');
define('_MD_AM_FOOTER', 'Footer');
define('_MD_AM_FOOTERDSC', 'Immer den vollen Pfad angeben ab: http://, damit dieser auch korrekt dargestellt werden kann');
define('_MD_AM_CENSOR', 'Optionen Wortzensur');
define('_MD_AM_DOCENSOR', 'Zensieren von unerwünschten Wörter ermöglichen?');
define('_MD_AM_DOCENSORDSC', 'Wörter werden zensiert. Das einschalten kann die Geschwindigkeit der Seite beeinflußen');
define('_MD_AM_CENSORWRD', 'Zensierte Wörter');
define('_MD_AM_CENSORWRDDSC', 'Wörter die Zensiert werden sollen.<br />Wörter mit einem <b>|</b>, trennen.');
define('_MD_AM_CENSORRPLC', 'Bad Wörter werden ersetzt durch:');
define('_MD_AM_CENSORRPLCDSC', 'Zensierte Wörter werden durch diese Einträge in den Beiträgen ersetzt');

define('_MD_AM_SEARCH', 'Such Optionen');
define('_MD_AM_DOSEARCH', 'Globale Suche aktivieren?');
define('_MD_AM_DOSEARCHDSC', 'Allg. SUche auf diesem Portal.');
define('_MD_AM_MINSEARCH', 'Min. Schlüßelwortlänge');
define('_MD_AM_MINSEARCHDSC', 'Min. Länge des Suchwortes um einen Treffer zu erzielen');
define('_MD_AM_MODCONFIG', 'Modul Konfiguration');
define('_MD_AM_DSPDSCLMR', 'Disclaimer anzeigen?');
define('_MD_AM_DSPDSCLMRDSC', 'Ja wählen um den Disclaimer auf der Startseite anzuzeigen');
define('_MD_AM_REGDSCLMR', 'Registrations Disclaimer');
define('_MD_AM_REGDSCLMRDSC', 'Text eingeben, der bei der Registration dann angezeigt wird.');
define('_MD_AM_ALLOWREG', 'Dürfen sich neue Benutzer anmelden?');
define('_MD_AM_ALLOWREGDSC', 'Ja auswählen um neue Benutzerregistrierungen zu gestatten');
define('_MD_AM_THEMEFILE', 'Sollen die Update Modul-Template .html Dateien durch die des theme/templates Ordners aktualisiert werden?');
define('_MD_AM_THEMEFILEDSC', 'Wenn diese Option eingeschaltet ist, werden Templates automatisch upgedatet wenn sich eine neuere Datei im /theme/templates Verziehnis des aktuellen Themas befindet. Dies sollten sie abschalten sobald die Seite public (offen für jedermann) geht.');
define('_MD_AM_CLOSESITE', 'Die Seite nur für bestimmte Gruppen zugänglich machen?');
define('_MD_AM_CLOSESITEDSC', 'Wählen Sie ja, um ihre Seite nur bestimmten Gruppen zugänglich zu machen. ');
define('_MD_AM_CLOSESITEOK', 'Wählen Sie die Gruppe aus, für die Sie die Seite zugänglich machen wollen.');
define('_MD_AM_CLOSESITEOKDSC', 'User der Webmaster- Gruppe, werden immer Zugang zur Seite haben.');
define('_MD_AM_CLOSESITETXT', 'Der Grund, dass nur bestimmte Gruppen Zugang zur Seite haben.');
define('_MD_AM_CLOSESITETXTDSC', 'Der angezeigte Text, warum ihre Seite geschlossen ist.');
define('_MD_AM_SITECACHE', 'Site-wide Cache');
define('_MD_AM_SITECACHEDSC', 'Caches whole contents of the site for a specified amount of time to enhance performance. Setting site-wide cache will override module-level cache, block-level cache, and module item level cache if any.');
define('_MD_AM_MODCACHE', 'Module-wide Cache');
define('_MD_AM_MODCACHEDSC', 'Caches module contents for a specified amount of time to enhance performance. Setting module-wide cache will override module item level cache if any.');
define('_MD_AM_NOMODULE', 'There is no module that can be cached.');
define('_MD_AM_DTPLSET', 'Standard-Template-Set');
define('_MD_AM_SSLLINK', 'URL wo SSL login Seite liegt');

// added for mailer
define('_MD_AM_MAILER','Mail Setup');
define('_MD_AM_MAILER_MAIL','');
define('_MD_AM_MAILER_SENDMAIL','');
define('_MD_AM_MAILER_','');
define('_MD_AM_MAILFROM','FROM Adresse');
define('_MD_AM_MAILFROMDESC','');
define('_MD_AM_MAILFROMNAME','FROM Name');
define('_MD_AM_MAILFROMNAMEDESC','');
// RMV-NOTIFY
define('_MD_AM_MAILFROMUID','FROM User');
define('_MD_AM_MAILFROMUIDDESC','Welcher Name soll beim Versender erscheinen, wenn das System eine Benachrichtigung verschickt?');
define('_MD_AM_MAILERMETHOD','Mail Versand-Methode');
define('_MD_AM_MAILERMETHODDESC','Methode, die benutzt wird, um Mails zu versenden. Standard ist \'mail\'. Nur andere benutzen, wenn dies Probleme bereitet.');
define('_MD_AM_SMTPHOST','SMTP Host(s)');
define('_MD_AM_SMTPHOSTDESC','Liste von SMTP-Servern, wo versucht werden soll, zu verbinden.');
define('_MD_AM_SMTPUSER','SMTPAuth Username');
define('_MD_AM_SMTPUSERDESC','Username, um zu einem SMTP host mit SMTPAuth zu verbinden.');
define('_MD_AM_SMTPPASS','SMTPAuth Password');
define('_MD_AM_SMTPPASSDESC','Passwort, um zu einem SMTP host mit SMTPAuth zu verbinden.');
define('_MD_AM_SENDMAILPATH','Pfad zu sendmail');
define('_MD_AM_SENDMAILPATHDESC','Pfad zum sendmail-programm (oder Ersatz) am Server.');
define('_MD_AM_THEMEOK','Selektierbare Themen');
define('_MD_AM_THEMEOKDSC','Wählen Sie Themen aus, die User dann als Standard-Thema einstellen können');

// Xoops Authentication Konstanten
define ("_MD_AM_AUTH_CONFOPTION_XOOPS", "XOOPS-Datenbank");
define ("_MD_AM_AUTH_CONFOPTION_LDAP", "Standard LDAP-Verzeichnis");
define ("_MD_AM_AUTH_CONFOPTION_AD", "Microsoft Active Directory-Kopie");
define ("_MD_AM_AUTHENTICATION", "Authentication Options");
define ("_MD_AM_AUTHMETHOD", "Authentifizierungsmethode");
define ("_MD_AM_AUTHMETHODDESC", "Welche Authentifizierungsmethode möchten Sie für Ihre Anmeldung für Benutzer verwenden möchte.");
define ("_MD_AM_LDAP_MAIL_ATTR", "LDAP - Mail-Feldname");
define ("_MD_AM_LDAP_MAIL_ATTR_DESC", "Der Name des Attributs E-Mail in Ihrem LDAP-Verzeichnisbaum.");
define ("_MD_AM_LDAP_NAME_ATTR", "LDAP - Common Name Feldname");
define ("_MD_AM_LDAP_NAME_ATTR_DESC", "Der Name des gemeinsamen Namensattribut in Ihrem LDAP-Verzeichnis.");
define ("_MD_AM_LDAP_SURNAME_ATTR", "LDAP - Name Feldname");
define ("_MD_AM_LDAP_SURNAME_ATTR_DESC", "Der Name der Name-Attribut im LDAP-Verzeichnis.");
define ("_MD_AM_LDAP_GIVENNAME_ATTR", "LDAP - Vorname Feldname");
define ("_MD_AM_LDAP_GIVENNAME_ATTR_DSC", "Der Name der Vorname Attribut im LDAP-Verzeichnis.");
define ("_MD_AM_LDAP_BASE_DN", "LDAP - Basis-DN");
define ("_MD_AM_LDAP_BASE_DN_DESC", "die Basis-DN (Distinguished Name) des LDAP-Verzeichnisbaum.");
define ("_MD_AM_LDAP_PORT", "LDAP - Port Number");
define ("_MD_AM_LDAP_PORT_DESC", "Die Port-Nummer benötigt, um Ihre LDAP-Verzeichnis-Server zuzugreifen.");
define ("_MD_AM_LDAP_SERVER", "LDAP - Server Name");
define ("_MD_AM_LDAP_SERVER_DESC", "Der Name Ihrer LDAP-Verzeichnisserver.");

define ("_MD_AM_LDAP_MANAGER_DN", "DN des LDAP-Manager");
define ("_MD_AM_LDAP_MANAGER_DN_DESC", "Der DN des Benutzers zu ermöglichen, Suche (zB Manager) machen");
define ("_MD_AM_LDAP_MANAGER_PASS", "Passwort des LDAP-Manager");
define ("_MD_AM_LDAP_MANAGER_PASS_DESC", "Das Passwort des Benutzers zu ermöglichen, um die Suche zu machen");
define ("_MD_AM_LDAP_VERSION", "LDAP-Protokoll Version");
define ("_MD_AM_LDAP_VERSION_DESC", "die LDAP-Protokoll Version: 2 oder 3");
define ("_MD_AM_LDAP_USERS_BYPASS", "erlaubt Benutzer, um die LDAP-Authentifizierung zu umgehen");
define ("_MD_AM_LDAP_USERS_BYPASS_DESC", "Benutzer mit nativen XOOPS Methode authentifiziert werden");

define ("_MD_AM_LDAP_USETLS", "Use TLS-Verbindung");
define ("_MD_AM_LDAP_USETLS_DESC", "Verwenden Sie eine TLS (Transport Layer Security) Verbindung. TLS verwenden Standard-389-Port-Nummer <BR>".
"Und die LDAP-Version muss auf 3 gesetzt werden");

define ("_MD_AM_LDAP_LOGINLDAP_ATTR", "LDAP-Attribut zu verwenden, um den Benutzer zu suchen");
define ("_MD_AM_LDAP_LOGINLDAP_ATTR_D", "Wenn Loginname den Einsatz in der DN-Option auf yes gesetzt ist, müssen mit den Anmeldenamen XOOPS entsprechen");
define ("_MD_AM_LDAP_LOGINNAME_ASDN", "Login-Name den Einsatz in der DN");
define ("_MD_AM_LDAP_LOGINNAME_ASDN_D", "Die XOOPS Login-Name wird in der LDAP-DN verwendet werden (zB: uid = <Login-Name>, dc = xoops, dc = org) szmtag Der Eintrag wird direkt in der LDAP-Server, ohne Such lesen") ;

define ("_MD_AM_LDAP_FILTER_PERSON", "den Suchfilter LDAP-Abfrage, um Benutzer zu finden");
define ("_MD_AM_LDAP_FILTER_PERSON_DESC", "Spezielle LDAP-Filter, um Benutzer zu finden.@@@@Loginnamen zu ersetzen durch die Benutzer-Login Name <br> MUSS leer, wenn Sie nicht wissen, was SIE TUN '!".
"<br> Ex: (& (objectclass = person) (sAMAccountName =@@@@Loginnamen)) für AD".
"<br> Ex: (& (objectclass = inetOrgPerson) (uid =@@@@Loginnamen)) für LDAP");

define ("_MD_AM_LDAP_DOMAIN_NAME", "Der Domain-Name");
define ("_MD_AM_LDAP_DOMAIN_NAME_DESC", "Windows-Domänennamen für ADS und NT Server nur.");

define ("_MD_AM_LDAP_PROVIS", "Automatic xoops Konto provisionning");
define ("_MD_AM_LDAP_PROVIS_DESC", "Erstellen xoops Benutzerdatenbank, falls nicht vorhanden");

define ("_MD_AM_LDAP_PROVIS_GROUP", "Standard auswirken Gruppe");
define ("_MD_AM_LDAP_PROVIS_GROUP_DSC", "Der neue Benutzer zuweisen, um diesen Gruppen");

define ("_MD_AM_LDAP_FIELD_MAPPING_ATTR", "Xoops-Auth-Server Felder-Mapping");
define ("_MD_AM_LDAP_FIELD_MAPPING_DESC", "Beschreiben Sie hier die Zuordnung zwischen dem Datenbankfeld und das Feld LDAP-Authentifizierung System Xoops.".
"<br> Format [Xoops Datenbank-Feld] = [Auth-System LDAP-Attribut]".
"<br> zB: email = E-Mail".
"<br> jeweils mit einer separaten |" .
"<br> !! Für fortgeschrittene Benutzer !!");

define ("_MD_AM_LDAP_PROVIS_UPD", "Pflegen xoops Konto provisionning");
define ("_MD_AM_LDAP_PROVIS_UPD_DESC", "Die Xoops Benutzerkonto wird immer mit dem Authentication Server synchronisiert");


// Begin TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock
define("_MD_AM_PRIVACY","Datenschutz erklärung");
define("_MD_AM_PRIVACYDSC","Um Mehrsprachig Einstellungen zu verwenden, müssen Sie  wie folgt hinzufügen:" .
		"<br><br><code>[en]YOUR TEXT[/en]</code>" .
		"<br><code>[de]IHREN TEXT[/de]</code>" .
		"<br><br>!! Viel Spass !!");
define("_MD_AM_TERMS","Nutzungs bedingungen");
define("_MD_AM_TERMSDSC","Um Mehrsprachig Einstellungen zu verwenden, müssen Sie  wie folgt hinzufügen:" .
		"<br><br><code>[en]YOUR TEXT[/en]</code>" .
		"<br><code>[de]IHREN TEXT[/de]</code>" .
		"<br><br>!! Viel Spass !!");
define("_MD_AM_IMPRINT","Impressum");
define("_MD_AM_IMPRINTDSC","Um Mehrsprachig Einstellungen zu verwenden, müssen Sie  wie folgt hinzufügen:" .
		"<br><br><code>[en]YOUR TEXT[/en]</code>" .
		"<br><code>[de]IHREN TEXT[/de]</code>" .
		"<br><br>!! Viel Spass !!");
define("_MD_AM_ABOUT","Über Uns");
define("_MD_AM_ABOUTDSC","Um Mehrsprachig Einstellungen zu verwenden, müssen Sie  wie folgt hinzufügen:" .
		"<br><br><code>[en]YOUR TEXT[/en]</code>" .
		"<br><code>[de]IHREN TEXT[/de]</code>" .
		"<br><br>!! Viel Spass !!");
define("_MD_AM_DISCLAIMER","Haftungsausschluss");
define("_MD_AM_DISCLAIMERDSC","To use Multilanguage settings, you need to add them like this:" .
		"<br><br><code>[en]YOUR TEXT[/en]</code>" .
		"<br><code>[de]IHREN TEXT[/de]</code>" .
		"<br><br>!! Have Fun !!");
// End TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock


// Auth CAPTCHA add by PinMaster
define('_MD_AM_CAPTCHA',"CAPTCHA Einstellungen.");
define('_MD_AM_CAPTCHA_ACTIVE',"CAPTCHA-Aktivierung.");
define('_MD_AM_CAPTCHA_SESSION_ID', "CAPTCHA session name");
define('_MD_AM_CAPTCHA_TYPE_BCKGRD', "Wählen Sie die Art der Hintergrund, der verwendet wird.");
define('_MD_AM_CAPTCHA_LIGNE',"Linien Erzeugen");
define('_MD_AM_CAPTCHA_CIRCLE',"Erzeugenden Kreise");
define('_MD_AM_CAPTCHA_IMG',"Image(s) Hintergrund");
define('_MD_AM_CAPTCHA_RAND',"Zufallsmodus");
define('_MD_AM_CAPTCHA_WIDTH', "Breite des erzeugten Bildes (max 500px)");
define('_MD_AM_CAPTCHA_HEIGHT', "Höhe des erzeugten Bildes (max 200px)");
define('_MD_AM_CAPTCHA_NUM_CHARS', "Anzahl der Zeichen ERSTELLT");
define('_MD_AM_CAPTCHA_NUM_LINES', "Anzahl der erzeugten Linien (Fashion Line-Generator)");
define('_MD_AM_CAPTCHA_NUM_CIRCLES', "Generierte Zahl der Kreise (Kreise Generierungsmodus)");
define('_MD_AM_CAPTCHA_CHAR_SHADOW', "Erzeugung der Schatten der Figuren");
define('_MD_AM_CAPTCHA_OWNER_TEXT', "Anzeigen von Text Eigentümer.");
define('_MD_AM_CAPTCHA_OWNER_TEXT_DESC', "Leer lassen um etwas zu zeigen");
define('_MD_AM_CAPTCHA_CHAR_SET', "Geben Sie einen Zeichensatz für die Code-Generierung."); 
define('_MD_AM_CAPTCHA_CHAR_SET_DESC', "Wenn das Feld leer ist die folgenden Zeichensatz AZ genommen wird. Sie können Ihre eigenen Zeichensatz für die Codegenerierung zu erstellen. Hier ist eine kleine Regel: <br /> <ul><li>das Zeichen '-' können Sie eine Liste </li><li>Der char machen '|' kann eine andere Liste zu machen (keine Leerzeichen, um Listen</li></ul> Die folgenden Beispiele :. 'AZ | AZ | 1-9', um alle Zeichen und Zahlen zu verwenden,' trennen ag | 3 | 6 ', dann werden wir einen reduzierten Satz von Kleinbuchstaben \"a\" bis \"g\" mit den Nummern 3 und 6."); 
define('_MD_AM_CAPTCHA_INSENSITIVE', "Deaktivierung der Überprüfung");
define('_MD_AM_CAPTCHA_BCKGRD_PATH', "Wählen Sie ein Hintergrundbild oder einen Ordner mit mehreren Bildern.");
define('_MD_AM_CAPTCHA_BCKGRD_DESC', "Unterstützte Formate: jpg, jpeg.");
define('_MD_AM_CAPTCHA_FONTS_PATH', "Wählen Sie eine Schriftart oder einen Ordner mit mehreren Schriftart.");
define('_MD_AM_CAPTCHA_FONTS_DESC', "Format unterstützt: True Type Font (.ttf)");
define('_MD_AM_CAPTCHA_MIN_FONT_SIZE', "Minimale Größe der Schrift, um Zeichen anzuzeigen");
define('_MD_AM_CAPTCHA_MAX_FONT_SIZE', "Maximale Größe der Schrift, um Zeichen anzuzeigen");
define('_MD_AM_CAPTCHA_USE_COLOUR', "Um die Änderung der Farbe der zufälligen Zeichen und das Hintergrundrauschen");
define('_MD_AM_CAPTCHA_FILE_TYPE', "Geben Sie den Typ des erzeugten Bildes");
define('_MD_AM_CAPTCHA_FILE_TYPE_DESC', "Unterstützte Formate: jpeg (jpg), png, gif.");
define('_MD_AM_CAPTCHA_CACHE', "Maximale Cachegröße (ko, min 512ko)");
define('_MD_AM_CAPTCHA_CACHE_DESC', "Ermöglicht die automatische Säuberung des Cache zu unnötigen Platzverlust zu vermeiden.");
define('_MD_AM_CAPTCHA_GRP', "Wählen Sie die Gruppen, um die Sicherheit CAPTCHA verwenden");
define('_MD_AM_CAPTCHA_GRPDESC', "Diese Sicherheit ist für jedes anonyme Besucher freigegeben");
define('_MD_PREVIEW_CAPTCHA', "Testgenerator");

?>
