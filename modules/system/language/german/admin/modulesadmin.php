<?php
// $Id: modulesadmin.php,v 1.7 2003/03/21 13:12:26 w4z004 Exp $
//%%%%%%	File Name  modulesadmin.php 	%%%%%
define('_MD_AM_MODADMIN','Modul Administration');
define('_MD_AM_MODULE','Module');
define('_MD_AM_VERSION','Version');
define('_MD_AM_LASTUP','Letztes Update');
define('_MD_AM_DEACTIVATED','Deaktiviert');
define('_MD_AM_ACTION','Aktiviert');
define('_MD_AM_DEACTIVATE','Deaktiviert');
define('_MD_AM_ACTIVATE','Activiert');
define('_MD_AM_UPDATE','Update');
define('_MD_AM_DUPEN','Doppelter Eintrag in der Modultabelle!');
define('_MD_AM_DEACTED','Das gewählte Modul wurde deaktiviert. Sie können das Modul jetzt sicher entfernen');
define('_MD_AM_ACTED','Das gewählte Modul wurde aktiviert!');
define('_MD_AM_UPDTED','Das gewählte Modul wurde aktualisiert!');
define('_MD_AM_SYSNO','System Modul kann nicht deaktiviert wrden.');
define('_MD_AM_STRTNO','Dieses Modul wird auf der Startseite sichtbar sein. Bitte stellen sie es Ihren Wünschen entsprechend ein.');

// added in RC2
define('_MD_AM_PCMFM','Bitte bestätigen Sie:');

// added in RC3
define('_MD_AM_ORDER','Einstellung');
define('_MD_AM_ORDER0','(0 = versteckt)');
define('_MD_AM_ACTIVE','Aktiv');
define('_MD_AM_INACTIVE','Inaktiv');
define('_MD_AM_NOTINSTALLED','Nicht installiert');
define('_MD_AM_NOCHANGE','nicht möglich');
define('_MD_AM_INSTALL','Installieren');
define('_MD_AM_UNINSTALL','Deinstallieren');
define('_MD_AM_SUBMIT','Ändern');
define('_MD_AM_CANCEL','Abbrechen');
define('_MD_AM_DBUPDATE','Datenbank erfolgreich aktualisiert!');
define('_MD_AM_BTOMADMIN','Zurück zur Modul Administration');

// %s represents module name
define('_MD_AM_FAILINS','%s kann nicht installiert werden.');
define('_MD_AM_FAILACT','%s kann nicht aktiviert werden.');
define('_MD_AM_FAILDEACT','%s kann nicht deaktiviert werden.');
define('_MD_AM_FAILUPD','%s kann nicht aktualisiert werden.');
define('_MD_AM_FAILUNINS','%s kann nicht deinstalliert werden.');
define('_MD_AM_FAILORDER','%s kann nicht verändert werden.');
define('_MD_AM_FAILWRITE','Kann nicht im Hauptmenü aufgeführt werden.');
define('_MD_AM_ALEXISTS','Modul %s ist bereits vorhanden.');
define('_MD_AM_ERRORSC', 'Fehler:');
define('_MD_AM_OKINS','Modul %s wurde erfolgreich installiert.');
define('_MD_AM_OKACT','Modul %s wurde erfolgreich aktiviert.');
define('_MD_AM_OKDEACT','Modul %s wurde erfolgreich deaktiviert.');
define('_MD_AM_OKUPD','Modul %s wurde erfolgreich aktualisiert.');
define('_MD_AM_OKUNINS','Modul %s wurde erfolgreich deinstalliert.');
define('_MD_AM_OKORDER','Modul %s wurde erfolgreich geändert.');

define('_MD_AM_RUSUREINS', 'Zur Modulinstallation bestätigen Sie bitte');
define('_MD_AM_RUSUREUPD', 'Um das Modul zu aktualisieren bestätigen Sie bitte');
define('_MD_AM_RUSUREUNINS', 'Soll das Modul wirklich deinstalliert werden?');
define('_MD_AM_LISTUPBLKS', 'Der folgende Block wird aktualisiert.<br />Wählen Sie den Block zum ändern aus(template und optionen).<br />');
define('_MD_AM_NEWBLKS', 'Neuer Block');
define('_MD_AM_DEPREBLKS', 'abgelehnte Blöcke');
?>
