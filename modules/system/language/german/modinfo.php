<?php
// $Id: modinfo.php,v 1.7 2003/03/21 13:12:25 w4z004 Exp $
// Module Info

// The name of this module
define('_MI_SYSTEM_NAME','System');

// A brief description of this module
define('_MI_SYSTEM_DESC','For administration of core settings of the site.');

// Names of blocks for this module (Not all module has blocks)
define('_MI_SYSTEM_BNAME2','User Menu');
define('_MI_SYSTEM_BNAME3','Mitglieder Login');
define('_MI_SYSTEM_BNAME4','Suche');
define('_MI_SYSTEM_BNAME5','Wartende Übermittlungen');
define('_MI_SYSTEM_BNAME6','Hauptmenu');
define('_MI_SYSTEM_BNAME7','Seiten Info´s');
define('_MI_SYSTEM_BNAME8', 'Wer ist Online');
define('_MI_SYSTEM_BNAME9', 'Aktivste Mitglieder');
define('_MI_SYSTEM_BNAME10', 'Neue Mitglieder');
define('_MI_SYSTEM_BNAME11', 'Die letzten Kommentare');
// RMV-NOTIFY
define('_MI_SYSTEM_BNAME12', 'Benachrichtigungen');
define('_MI_SYSTEM_BNAME13', 'Themen');


// Names of admin menu items
define('_MI_SYSTEM_ADMENU1','Banner');
define('_MI_SYSTEM_ADMENU2','Blöcke');
define('_MI_SYSTEM_ADMENU3','Gruppen');
define('_MI_SYSTEM_ADMENU5','Module');
define('_MI_SYSTEM_ADMENU6','Einstellungen');
define('_MI_SYSTEM_ADMENU7','Smilies');
define('_MI_SYSTEM_ADMENU9','Mitglieder Ränge');
define('_MI_SYSTEM_ADMENU10','Mitglied editieren');
define('_MI_SYSTEM_ADMENU11','Mail an Mitglieder');
define('_MI_SYSTEM_ADMENU12', 'Mitglied finden');
define('_MI_SYSTEM_ADMENU13', 'Bilder');
define('_MI_SYSTEM_ADMENU14', 'Avatare');
define('_MI_SYSTEM_ADMENU15', 'Themen');
define('_MI_SYSTEM_ADMENU16', 'Kommentare');
?>
