<?php
// $Id: admin.php,v 1.7 2003/03/21 13:12:24 w4z004 Exp $
//%%%%%%	File Name  admin.php 	%%%%%
define('_MD_AM_DBUPDATED','Datenbank wurde erfolreich aktualisiert!');


// Admin Module Names
define('_MD_AM_ADGS','Gruppen');
define('_MD_AM_BANS','Banner');
define('_MD_AM_BKAD','Blöcke');
define('_MD_AM_MDAD','Module');
define('_MD_AM_SMLS','Smilies');
define('_MD_AM_RANK','Mitglieder Ränge');
define('_MD_AM_USER','Mitglieder editieren');
define('_MD_AM_FINDUSER', 'Mitglieder finden');
define('_MD_AM_PREF','Einstellungen');
define('_MD_AM_VRSN','Version');
define('_MD_AM_MLUS', 'Mail an Mitglieder');
define('_MD_AM_IMAGES', 'Bildmanager');
define('_MD_AM_AVATARS', 'Avatare');
define('_MD_AM_TPLSETS', 'Templates');
define('_MD_AM_COMMENTS', 'Kommentar Manager');

// Group permission phrases
define('_MD_AM_PERMADDNG','Konnte Gruppenberechtigung nicht hinzufügen (Berechtigung: %s Gruppe: %s)');
define('_MD_AM_PERMADDOK','Gruppenberechtigung erfolgreich hinzugefügt (Berechtigung: %s Gruppe: %s)');
define('_MD_AM_PERMRESETNG','Konnte Gruppenberechtigung für das Modul %s nicht zurücksetzen');
?>
