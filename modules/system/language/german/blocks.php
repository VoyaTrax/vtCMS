<?php
// $Id: blocks.php,v 1.7 2003/03/21 13:12:25 w4z004 Exp $
// Blocks
define('_MB_SYSTEM_ADMENU','Administration');
define('_MB_SYSTEM_RNOW','Registrierung!');
define('_MB_SYSTEM_LPASS','Password vergessen?');
define('_MB_SYSTEM_SEARCH','Suche');
define('_MB_SYSTEM_ADVS','erweiterte Suche');
define('_MB_SYSTEM_VACNT','Profil');
define('_MB_SYSTEM_EACNT','Profil bearbeiten');
// RMV-NOTIFY
define('_MB_SYSTEM_NOTIF', 'Benachrichtigungen');
define('_MB_SYSTEM_LOUT','Ausloggen');
define('_MB_SYSTEM_INBOX','Private Nachrichten');
define('_MB_SYSTEM_SUBMS','übermittelte Artikel');
define('_MB_SYSTEM_WLNKS','übermittelte Links');
define('_MB_SYSTEM_BLNK','defekte Links');
define('_MB_SYSTEM_MLNKS','editierte Links');
define('_MB_SYSTEM_WDLS','übermittelte Downloads');
define('_MB_SYSTEM_BFLS','defekte Downloads');
define('_MB_SYSTEM_MFLS','editierte Downloads');
define('_MB_SYSTEM_HOME','Startseite'); // link to home page in main menu block
define('_MB_SYSTEM_RECO','Empfehlen Sie uns.');
define('_MB_SYSTEM_PWWIDTH','Pop-Up Fenster - Breite');
define('_MB_SYSTEM_PWHEIGHT','Pop-Up Fenster - Höhe');
define('_MB_SYSTEM_LOGO','Bild URL für die Datei %s');  // %s is your root image directory name

define('_MB_SYSTEM_SADMIN','Admin Gruppe einsehen');
define('_MB_SYSTEM_SPMTO','Private Message an %s senden');
define('_MB_SYSTEM_SEMTO','E-Mail an %s senden');

define('_MB_SYSTEM_DISPLAY','Anzeige %s Benutzer');
define('_MB_SYSTEM_DISPLAYA','Anzeige Mitgliederavatars');
define('_MB_SYSTEM_NODISPGR','Zeige keine Benutzer wo Rang ist:');

define('_MB_SYSTEM_DISPLAYC','Display %s comments');
define('_MB_SYSTEM_SECURE', 'Sicherer Login');

define('_MB_SYSTEM_NUMTHEME', '%s Themen');
define('_MB_SYSTEM_THSHOW', 'Zeige Screenshot');
define('_MB_SYSTEM_THWIDTH', 'Screenshot Breite');

?>
