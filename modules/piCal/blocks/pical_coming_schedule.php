<?php

// piCal xoops用ブロックモジュール
// pical_coming_schedule.php
// 今日以降のスケジュールをブロックに表示
// by GIJ=CHECKMATE (PEAK Corp. http://www.peak.ne.jp/)

// older (not in use)
function pical_coming_schedule_show()
{
	global $xoopsConfig , $xoopsDB ;

	// 各種パスの設定
	$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
	$mod_url = XOOPS_URL."/modules/piCal" ;

	// piCalクラスの定義
	require_once( "$mod_path/piCal.php" ) ;
	require_once( "$mod_path/piCal_xoops.php" ) ;

	// オブジェクトの生成
	$cal = new piCal_xoops( date( 'Y-n-j' ) , $xoopsConfig['language'] ) ;
	$cal->use_server_TZ = true ;

	// 各プロパティの設定
	$cal->conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
	$cal->table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;
	include( "$mod_path/read_configs.php" ) ;
	$cal->images_url = "$mod_url/images/$skin_folder" ;
	$cal->images_path = "$mod_path/images/$skin_folder" ;

	$block['content'] = $cal->get_coming_schedule( "$mod_url/index.php" , $coming_numrows ) ;
	return $block ;
}



// newer (with template)
function pical_coming_schedule_show_tpl()
{
	global $xoopsConfig , $xoopsDB ;

	// 各種パスの設定
	$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
	$mod_url = XOOPS_URL."/modules/piCal" ;

	// piCalクラスの定義
	require_once( "$mod_path/piCal.php" ) ;
	require_once( "$mod_path/piCal_xoops.php" ) ;

	// オブジェクトの生成
	$cal = new piCal_xoops( date( 'Y-n-j' ) , $xoopsConfig['language'] ) ;
	$cal->use_server_TZ = true ;

	// 各プロパティの設定
	$cal->conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
	$cal->table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;
	include( "$mod_path/read_configs.php" ) ;
	$cal->images_url = "$mod_url/images/$skin_folder" ;
	$cal->images_path = "$mod_path/images/$skin_folder" ;

	$block =& $cal->get_blockarray_coming_event( "$mod_url/index.php" , $coming_numrows , true ) ;
	return $block ;
}

?>