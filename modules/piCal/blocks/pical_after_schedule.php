<?php

// piCal xoops用ブロックモジュール
// pical_after_schedule.php
// カレンダーで指定された日以降のスケジュールをブロックに表示
// by GIJ=CHECKMATE (PEAK Corp. http://www.peak.ne.jp/)

// older (not in use)
function pical_after_schedule_show()
{
	global $xoopsConfig , $xoopsDB ;

	// 各種パスの設定
	$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
	$mod_url = XOOPS_URL."/modules/piCal" ;

	// piCalクラスの定義
	require_once( "$mod_path/piCal.php" ) ;
	require_once( "$mod_path/piCal_xoops.php" ) ;

	// オブジェクトの生成
	$cal = new piCal_xoops( "" , $xoopsConfig['language'] ) ;

	// 各プロパティの設定
	$cal->conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
	$cal->table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;
	include( "$mod_path/read_configs.php" ) ;
	$cal->images_url = "$mod_url/images/$skin_folder" ;
	$cal->images_path = "$mod_path/images/$skin_folder" ;

	// タイトルの書き換え（かなりまずい解決法）
	global $block_arr , $i ;
	$title = sprintf( _MB_PICAL_EVENTS_AFTER , sprintf( _PICAL_FMT_MD , $cal->month_short_names[ date( 'n' , $cal->unixtime ) ] , $cal->date_short_names[ date( 'j' , $cal->unixtime ) ] ) ) ;
//	$title = sprintf( _MB_PICAL_EVENTS_THEDAY , date( _PICAL_DTFMT_MD , $cal->unixtime ) ) ;
	// お行儀の悪いモジュールへの対応（まあ、こっちもお行儀悪いんだけど）
	if( is_object( $block_arr ) ) $block_arr[$i]->setVar( 'title' , $title ) ;

	$block['content'] = $cal->get_coming_schedule( "$mod_url/index.php" , $coming_numrows ) ;
	return $block ;
}



// newer (with template)
function pical_after_schedule_show_tpl()
{
	global $xoopsConfig , $xoopsDB ;

	// 各種パスの設定
	$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
	$mod_url = XOOPS_URL."/modules/piCal" ;

	// piCalクラスの定義
	require_once( "$mod_path/piCal.php" ) ;
	require_once( "$mod_path/piCal_xoops.php" ) ;

	// オブジェクトの生成
	$cal = new piCal_xoops( "" , $xoopsConfig['language'] ) ;

	// 各プロパティの設定
	$cal->conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
	$cal->table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;
	include( "$mod_path/read_configs.php" ) ;
	$cal->images_url = "$mod_url/images/$skin_folder" ;
	$cal->images_path = "$mod_path/images/$skin_folder" ;

	// タイトルの書き換え（かなりまずい解決法）
	global $block_arr , $i ;
	$title = sprintf( _MB_PICAL_EVENTS_AFTER , sprintf( _PICAL_FMT_MD , $cal->month_short_names[ date( 'n' , $cal->unixtime ) ] , $cal->date_short_names[ date( 'j' , $cal->unixtime ) ] ) ) ;
//	$title = sprintf( _MB_PICAL_EVENTS_THEDAY , date( _PICAL_DTFMT_MD , $cal->unixtime ) ) ;
	// お行儀の悪いモジュールへの対応（まあ、こっちもお行儀悪いんだけど）
	if( is_object( $block_arr[$i] ) ) $block_arr[$i]->setVar( 'title' , $title ) ;
	$block =& $cal->get_blockarray_coming_event( "$mod_url/index.php" , $coming_numrows , false ) ;
	return $block ;
}

?>