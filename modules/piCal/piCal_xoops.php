<?php

// 簡易スケジューラ付カレンダークラス piCal のXOOPS用サブクラス
// piCal_xoops.php
// by GIJ=CHECKMATE (PEAK Corp. http://www.peak.ne.jp/)
// 実際には、ほとんどがRyuji(http://joetsu.info/ryuji/)さんのコードです


class piCal_xoops extends piCal {

function textarea_sanitizer_for_sql( $data )
{
//	二重にaddslashesがかかるのを防ぐため、コメントアウト
//	$myts =& MyTextSanitizer::getInstance();
//	return $myts->makeTareaData4Save($data);
	return $data ;
}

function textarea_sanitizer_for_show( $data )
{
	$myts =& MyTextSanitizer::getInstance();
	return $myts->makeTareaData4Show($data);
}

function textarea_sanitizer_for_edit( $data )
{
	$myts =& MyTextSanitizer::getInstance();
	return $myts->makeTareaData4Edit($data);
}

function textarea_sanitizer_for_export_ics( $data )
{
	$myts =& MyTextSanitizer::getInstance();
	return $myts->makeTareaData4Show($data);
}


function get_formtextdateselect( $name , $value )
{
	$jstime = formatTimestamp( $this->unixtime , 'F j Y, H:i:s' ) ;

	if( $this->week_start ) $js_cal_week_start = 'true' ;	// Monday
	else $js_cal_week_start = 'false' ;						// Sunday

	return "
		<input type='text' name='$name' id='$name' size='15' maxlength='15' value='$value' />
		<input type='reset' value='...' onclick='

  var el = xoopsGetElementById(\"$name\");
  if (calendar != null) {
    calendar.hide();
    calendar.parseDate(el.value);
  } else {
    var cal = new Calendar($js_cal_week_start, new Date(\"$jstime\"), selected, closeHandler);
    calendar = cal;
    cal.setRange(2000, 2015); // GIJ TODO
    calendar.create();
    calendar.parseDate(el.value);
  }
  calendar.sel = el;
  calendar.showAtElement(el);
  Calendar.addEvent(document, \"mousedown\", checkCalendar);
  return false;

		' />
	" ;
}

function get_submitter_info( $uid )
{
	$poster = new XoopsUser( $uid ) ;

	// check if invalid uid
	if( $poster->uname() == '' ) return '' ;

	if( $this->nameoruname == 'uname' ) {
		$name = $poster->uname() ;
	} else {
		$name = trim( $poster->name() ) ;
		if( $name == "" ) $name = $poster->uname() ;
	}

	return "<a href='".XOOPS_URL."/userinfo.php?uid=$uid'>$name</a>" ;
}

// Notifications
function notify_new_event( $event_id )
{
	$rs = mysqli_query( $this->conn ,  "SELECT summary,admission FROM $this->table WHERE id='$event_id'" ) ;
	$event = mysqli_fetch_object( $rs ) ;
	if( $event->admission ) {
		$notification_handler =& xoops_gethandler('notification');
		$notification_handler->triggerEvent('global', 0, 'new_event', array('EVENT_SUMMARY' => $event->summary , 'EVENT_URI' => "$this->base_url/?action=View&event_id=$event_id" ) ) ;
		return true ;
	} else {
		return false ;
	}
}


// $this->caldate日の予定ブロック配列を返す
function get_blockarray_date_event( $get_target = '' )
{
	if( $get_target == '' ) $get_target = $_SERVER[ 'PHP_SELF' ] ;

	// 時差を計算しつつ、WHERE節の期間に関する条件生成
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	$toptime_of_day = $this->unixtime + $this->day_start ;
	if( $tzoffset == 0 ) {
		// 時差がない場合 （mysqlに負荷をかけさせないため、ここで条件分けしとく)
		$whr_term = "start<'".( $toptime_of_day + 86400 ) ."' AND end>'$toptime_of_day'" ;
	} else {
		// 時差がある場合は、alldayによって場合分け
		$whr_term = "(allday AND start<='$this->unixtime' AND end>'$this->unixtime') OR ( ! allday AND start<='".( $toptime_of_day + 86400 - $tzoffset )."' AND end>'".( $toptime_of_day - $tzoffset )."')" ;
	}

	// CLASS関連のWHERE条件取得
	$whr_class = $this->get_where_about_class() ;

	// 当日のスケジュール取得
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start,end" ) ;
	$num_rows = mysqli_num_rows( $yrs ) ;

	$block = array(
		'insertable' => $this->insertable ,
		'num_rows' => $num_rows ,
		'get_target' => $get_target ,
		'images_url' => $this->images_url ,
		'caldate' => $this->caldate ,
		'locale_time' => _PICAL_STRFFMT_TIME ,
		'locale_date' => _PICAL_STRFFMT_DATE_FOR_BLOCK ,
		'lang_PICAL_MB_CONTINUING' => _PICAL_MB_CONTINUING ,
		'lang_PICAL_MB_NOEVENT' => _PICAL_MB_NOEVENT ,
		'lang_PICAL_MB_ADDEVENT' => _PICAL_MB_ADDEVENT
	) ;

	while( $event = mysqli_fetch_object( $yrs ) ) {

		$is_start_date = date( "Y-n-j" , $event->start ) == $this->caldate ;
		$is_end_date = date( "Y-n-j" , $event->end ) == $this->caldate ;

		if( ! $event->allday ) {
			// 通常イベント
			// $event->start,end はサーバ時間  $start,$end はユーザ時間
			$start = $event->start + $tzoffset ;
			$end = $event->end + $tzoffset ;

			// 当日に開始や終了するかでドットGIFを替える
			if( $is_start_date ) $dot = "dot_startday.gif" ;
			else if( $is_end_date ) $dot = "dot_endday.gif" ;
			else $dot = "dot_interimday.gif" ;

			if( $is_end_date ) {
				$end_desc = date( "H:i" , $end ) ;
			} else if( date( "z" , $end - $this->day_start - 1 ) == date( "z" , $end ) - 1 ) {
				// 深夜0:00〜3:59までは、当日扱いで24:00〜27:59と表示
				$is_end_date = true ;
				$end_desc = ( date( 'H' , $end ) + 24 ) . ':' . date( 'i' , $end ) ;
			} else {
				$end_desc = strftime( _PICAL_STRFFMT_DATE_FOR_BLOCK , $end ) ;
			}

			// 通常イベントの配列セット
			$block['events'][] = array( 
				'summary' => $this->text_sanitizer_for_show( $event->summary ) ,
				'allday' => $event->allday ,
				'start' => $start ,
				'start_desc' => date( 'H:i' , $start ) ,
				'end' => $end ,
				'end_desc' => $end_desc ,
				'id' => $event->id ,
				'dot_gif' => $dot ,
				'is_start_date' => $is_start_date ,
				'is_end_date' => $is_end_date
			) ;
		} else {
			// 全日イベントの配列セット
			$block['events'][] = array( 
				'summary' => $this->text_sanitizer_for_show( $event->summary ) ,
				'allday' => $event->allday ,
				'start' => $event->start ,
				'end' => $event->end ,
				'id' => $event->id ,
				'dot_gif' => "dot_allday.gif" ,
				'is_start_date' => $is_start_date ,
				'is_end_date' => $is_end_date
			) ;
		}
	}

	return $block ;
}



// $this->caldate以降の予定ブロック配列を返す
function get_blockarray_coming_event( $get_target = '' , $num = 5 , $alt_disp = false )
{
	if( $get_target == '' ) $get_target = $_SERVER[ 'PHP_SELF' ] ;
	$now = $alt_disp ? time() : $this->unixtime ;

	// 時差を計算しつつ、WHERE節の期間に関する条件生成
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	if( $tzoffset == 0 ) {
		// 時差がない場合 （mysqlに負荷をかけさせないため、ここで条件分けしとく)
		$whr_term = "end>'$now'" ;
	} else {
		// 時差がある場合は、alldayによって場合分け
		$whr_term = "(allday AND end>'$now') OR ( ! allday AND end>'".($now - $tzoffset )."')" ;
	}

	// CLASS関連のWHERE条件取得
	$whr_class = $this->get_where_about_class() ;

	// 当日以降のスケジュール取得
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start" ) ;

	$num_rows = mysqli_num_rows( $yrs ) ;

	$block = array(
		'insertable' => $this->insertable ,
		'num_rows' => $num_rows ,
		'get_target' => $get_target ,
		'images_url' => $this->images_url ,
		'caldate' => $this->caldate ,
		'locale_time' => _PICAL_STRFFMT_TIME ,
		'locale_date' => _PICAL_STRFFMT_DATE_FOR_BLOCK ,
		'lang_PICAL_MB_CONTINUING' => _PICAL_MB_CONTINUING ,
		'lang_PICAL_MB_NOEVENT' => _PICAL_MB_NOEVENT ,
		'lang_PICAL_MB_ADDEVENT' => _PICAL_MB_ADDEVENT ,
		'lang_PICAL_MB_RESTEVENT_PRE' => _PICAL_MB_RESTEVENT_PRE ,
		'lang_PICAL_MB_RESTEVENT_SUF' => _PICAL_MB_RESTEVENT_SUF
	) ;

	$count = 0 ;
	while( $event = mysqli_fetch_object( $yrs ) ) {

		if( ++ $count > $num ) break ;

		// $event->start,end はサーバ時間  $start,$end はユーザ時間
		if( $event->allday ) {
			$can_time_disp = false ;
			$start_for_time = $start_for_date = $event->start ;
			$end_for_time = $end_for_date = $event->end ;
		} else {
			$can_time_disp = $alt_disp ;
			$start_for_time = $event->start + $tzoffset ;
			$start_for_date = $event->start + $tzoffset - $this->day_start ;
			$end_for_time = $event->end + $tzoffset ;
			$end_for_date = $event->end + $tzoffset - $this->day_start ;
		}

		if( $event->start < $now ) {
			// already started
			$distance = 0 ;
			$dot = "dot_started.gif" ;
			$start_desc = '' ;
			if( $event->end - $now < 86400 && $can_time_disp ) {
				$end_desc = date( "H:i" , $end_for_time ) ;
			} else {
				$end_desc = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $end_for_date ) ] , $this->date_long_names[ date( 'j' , $end_for_date ) ] ) ;
			}
		} else if( $event->start - $now < 86400 ) {
			// near event (24hour)
			$dot = "dot_today.gif" ;
			if( $can_time_disp ) {
				$start_desc = date( "H:i" , $start_for_time ) ;
			} else {
				$start_desc = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $start_for_date ) ] , $this->date_long_names[ date( 'j' , $start_for_date ) ] ) ;
			}
			if( $event->end - $now < 86400 && $can_time_disp ) {
				$end_desc = date( "H:i" , $end_for_time ) ;
				$distance = 1 ;
			} else {
				$end_desc = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $end_for_date ) ] , $this->date_long_names[ date( 'j' , $end_for_date ) ] ) ;
				$distance = 2 ;
			}
		} else {
			// far event (>1day)
			$distance = 3 ;
			$dot = "dot_future.gif" ;
			$start_desc = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $start_for_date ) ] , $this->date_long_names[ date( 'j' , $start_for_date ) ] ) ;
			$end_desc = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $end_for_date ) ] , $this->date_long_names[ date( 'j' , $end_for_date ) ] ) ;
		}

		$block['events'][] = array( 
			'summary' => $this->text_sanitizer_for_show( $event->summary ) ,
			'allday' => $event->allday ,
			'start' => $start_for_time ,
			'start_desc' => $start_desc ,
			'end' => $end_for_time ,
			'end_desc' => $end_desc ,
			'id' => $event->id ,
			'dot_gif' => $dot ,
			'distance' => $distance
		) ;
	}

	$block['num_rows_rest'] = $num_rows - $count ;

	return $block ;
}





// The End of Class
}
?>