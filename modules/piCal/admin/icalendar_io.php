<?php

require_once( '../../../include/cp_header.php' ) ;
require_once( '../piCal.php' ) ;
require_once( XOOPS_ROOT_PATH . "/include/xoopscodes.php" ) ;
require_once( '../piCal_xoops.php' ) ;

// SERVER, GET 変数の取得
$HTTP_HOST = $_SERVER[ 'HTTP_HOST' ] ;
$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
$tz = isset( $_GET[ 'tz' ] ) ? $_GET[ 'tz' ] : "y" ;
$pos = isset( $_GET[ 'pos' ] ) ? intval( $_GET[ 'pos' ] ) : 0 ;
$num = isset( $_GET[ 'num' ] ) ? intval( $_GET[ 'num' ] ) : 20 ;
$txt = isset( $_GET[ 'txt' ] ) ? trim( $_GET[ 'txt' ] ) : '' ;


// mysqlへの接続 (面倒なのであえて$xoopsDBは使わない)
// $conn = mysql_connect( XOOPS_DB_HOST , XOOPS_DB_USER , XOOPS_DB_PASS ) or die( "Could not connect." ) ;
// mysql_select_db( XOOPS_DB_NAME , $conn ) ;
$conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
$table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;

// Timezone の処理
$serverTZ = date( 'Z' ) / 3600 ;
// $serverTZ = $xoopsConfig['server_TZ'] ;
$userTZ = $xoopsUser->timezone() ;
$tzoptions = "
	<option value='s'>Sever Timezone</option>
	<option value='g'>GMT</option>
	<option value='y'>Your Timezone</option>\n" ;
switch( $tz ) {
	case 's' :
		$tzoffset = 0 ;
		$tzdisp = ( $serverTZ >= 0 ? "+" : "-" ) . sprintf( "%02d:%02d" , abs( $serverTZ ) , abs( $serverTZ ) * 60 % 60 ) ;
		$tzoptions = str_replace( "'s'>" , "'s' selected='selected'>" , $tzoptions ) ;
		break ;
	case 'g' :
		$tzoffset = - $serverTZ * 3600 ;
		$tzdisp = "GMT" ;
		$tzoptions = str_replace( "'g'>" , "'g' selected='selected'>" , $tzoptions ) ;
		break ;
	default :
	case 'y' :
		$tzoffset = ( $userTZ - $serverTZ ) * 3600 ;
		$tzdisp = ( $userTZ >= 0 ? "+" : "-" ) . sprintf( "%02d:%02d" , abs( $userTZ ) , abs( $userTZ ) * 60 % 60 ) ;
		$tzoptions = str_replace( "'y'>" , "'y' selected='selected'>" , $tzoptions ) ;
		break ;
}


// 各種パスの設定
$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
$mod_url = XOOPS_URL."/modules/piCal" ;


// オブジェクトの生成
$cal = new piCal_xoops( "" , $xoopsConfig['language'] ) ;

// 各プロパティの設定
$cal->conn = $conn ;
$cal->table = $table ;
include( '../read_configs.php' ) ;
$cal->base_url = $mod_url ;
$cal->images_url = "$mod_url/images/$skin_folder" ;
$cal->images_path = "$mod_path/images/$skin_folder" ;



// データベース更新などがからむ処理
if( isset( $_POST[ 'http_import' ] ) && isset( $_POST[ 'import_uri' ] ) ) {

	// httpコネクション経由でのiCalendarインポート
	$cal->import_ics_via_http( $_POST[ 'import_uri' ] , $xoopsUser->uid() ) ;

} else if( isset( $_POST[ 'local_import' ] ) && isset( $_FILES[ 'user_ics' ][ 'tmp_name' ] ) && is_readable( $_FILES[ 'user_ics' ][ 'tmp_name' ] ) ) {

	// ファイルアップロードによるiCalendarインポート
	$cal->import_ics_from_local( 'user_ics' , $xoopsUser->uid() ) ;

} else if( isset( $_POST[ 'delete' ] ) ) {
	// レコードの削除
	if( isset( $_POST[ 'ids' ] ) && is_array( $_POST[ 'ids' ] ) ) {
		$whr = "" ;
		foreach( $_POST[ 'ids' ] as $id ) {
			$whr .= "id=$id OR rrule_pid=$id OR " ;
			xoops_comment_delete( $xoopsModule->mid() , $id ) ;
		}
		$sql = "DELETE FROM $table WHERE $whr 0" ;
		if( ! mysqli_query( $conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		else $mes = urlencode( _AM_MES_DELETED ) ;
	} else {
		$mes = "" ;
	}
	Header( "Location: $cal->connection://$HTTP_HOST$PHP_SELF?mes=$mes" ) ;
	exit ;
} else if( isset( $_POST[ 'output_ics' ] ) && isset( $_POST[ 'ids' ] ) && is_array( $_POST[ 'ids' ] ) ) {
	// ics出力の確認メッセージ
	$hiddens = "" ;
	foreach( $_POST[ 'ids' ] as $id ) {
		$hiddens .= "<input type='hidden' name='event_ids[]' value='$id'>\n" ;
	}
	// webcalリンク生成
	$webcal_url = str_replace( 'http://' , 'webcal://' , $mod_url ) ;
	// 確認フォーム生成部
	$confirm_html = "
	<p style='margin:0px;color:blue;'><b>"._AM_LABEL_IO_SELECTPLATFORM."</b></p>
	<table border='0' cellpadding='2' cellspacing='0'>
	<tr>
	<td>
	<form action='$mod_url/' method='post' target='_blank'>
		$hiddens
		<input type='submit' name='output_ics' value='iCalendar(Win)'
	</form>
	</td>
	<td>
	<form action='$webcal_url/' method='post' target='_blank'>
		$hiddens
		<input type='submit' name='output_ics' value='iCal(Mac)'
	</form>
	</td>
	</tr>
	</table><br /><br />\n" ;
}


// フリーワード検索
$whr = "WHERE (rrule_pid=0 OR rrule_pid=id) " ;
if( $txt != "" ) {
	if( get_magic_quotes_gpc() ) $txt = stripslashes( $txt ) ;
	$keywords = explode( " " , $cal->mb_convert_kana( $txt , "s" ) ) ;
	foreach( $keywords as $keyword ) {
		$whr .= "AND (CONCAT( summary , description , location , contact ) LIKE '%" . addslashes( $keyword ) . "%') " ;
	}
}

// クエリ
$rs = mysqli_query( $conn ,  "SELECT COUNT(id) FROM $table $whr" ) ;
$numrows = mysqli_result( $rs ,  0 ,  0 ) ;
$rs = mysqli_query( $conn ,  "SELECT * FROM $table $whr ORDER BY start,end LIMIT $pos,$num" ) ;

// ページ分割処理
include XOOPS_ROOT_PATH.'/class/pagenav.php';
$nav = new XoopsPageNav( $numrows , $num , $pos , 'pos' , "tz=$tz&amp;num=$num&amp;txt=" . urlencode($txt) ) ;
$nav_html = $nav->renderNav( 10 ) ;


// メイン出力部
xoops_cp_header();

echo "
<h4>"._AM_ICALENDAR_IMPORT."</h4>
<p><font color='blue'>".(isset($_GET['mes'])?htmlspecialchars($_GET['mes'],ENT_QUOTES):"")."</font></p>\n"
. ( isset( $confirm_html ) ? $confirm_html : "" ) .
"<form action='$PHP_SELF?tz=$tz' method='post'>
  "._AM_LABEL_IMPORTFROMWEB."<br />
  <input type='text' name='import_uri' size='80'>
  <input type='submit' name='http_import' value='"._AM_BUTTON_IMPORT."'>
</form>
<form action='$PHP_SELF?tz=$tz' method='post' enctype='multipart/form-data'>
  "._AM_LABEL_UPLOADFROMFILE."<br />
  <input type='hidden' name='MAX_FILE_SIZE' value='16384'>
  <input type='file' name='user_ics' size='72'>
  <input type='submit' name='local_import' value='"._AM_BUTTON_UPLOAD."'>
</form>
<h4>"._AM_ICALENDAR_EXPORT."</h4>
<form action='$PHP_SELF?tz=$tz&amp;num=$num' method='get' style='margin-bottom:0px;text-align:right'>
  <input type='text' name='txt' value='".htmlspecialchars($txt,ENT_QUOTES)."'>
  <input type='submit' value='"._AM_BUTTON_EXTRACT."'> &nbsp; 
  $nav_html &nbsp; 
  <select name='tz' onChange='submit();'>$tzoptions</select>
</form>
<form action='$PHP_SELF?tz=$tz' method='post' style='margin-top:0px;'>
<table width='100%' class='outer' cellpadding='4' cellspacing='1'>
  <tr valign='middle'>
    <th>"._AM_IO_TH0."</th>
    <th>"._AM_IO_TH1."<br />($tzdisp)</th>
    <th>"._AM_IO_TH2."<br />($tzdisp)</th>
    <th>"._AM_IO_TH3."</th>
    <th>"._AM_IO_TH4."</th>
    <th>"._AM_IO_TH5."</th>
    <th></th>
    <th></th>
  </tr>
" ;

// リスト出力部
$oddeven = 'odd' ;
while( $event = mysqli_fetch_object( $rs ) ) {
	$oddeven = ( $oddeven == 'odd' ? 'even' : 'odd' ) ;
	if( $event->allday ) {
		$start_desc = date( _AM_DTFMT_LIST_ALLDAY , $event->start ) . '<br />(allday)' ;
		$end_desc = date( _AM_DTFMT_LIST_ALLDAY , $event->end - 300 ) . '<br />(allday)' ;
	} else {
		$start_desc = date( _AM_DTFMT_LIST_NORMAL , $event->start + $tzoffset ) ;
		$end_desc = date( _AM_DTFMT_LIST_NORMAL , $event->end + $tzoffset ) ;
	}
	echo "
  <tr>
    <td class='$oddeven'>".$xoopsUser->getUnameFromId($event->uid)."</td>
    <td class='$oddeven' nowrap='nowrap'>$start_desc</td>
    <td class='$oddeven' nowrap='nowrap'>$end_desc</td>
    <td class='$oddeven'><a href='$mod_url/?action=View&amp;event_id=$event->id'>".htmlspecialchars($event->summary,ENT_QUOTES)."</a></td>
    <td class='$oddeven'>".$cal->rrule_to_human_language($event->rrule)."</td>
    <td class='$oddeven'>".($event->admission?_YES:_NO)."</td>
    <td class='$oddeven' align='right'><a href='$mod_url/?action=Edit&amp;event_id=$event->id' target='_blank'><img src='$cal->images_url/addevent.gif' border='0' width='14' height='12' /></a></td>
    <td class='$oddeven' align='right'><input type='checkbox' name='ids[]' value='$event->id'></td>
  </tr>\n" ;
}

echo "
  <tr>
    <td colspan='8' align='right'>"._AM_LABEL_IO_CHECKEDITEMS." &nbsp; "._AM_LABEL_IO_OUTPUT."<input type='submit' name='output_ics' value='"._SUBMIT."'> &nbsp; "._AM_LABEL_IO_DELETE."<input type='submit' name='delete' value='"._DELETE."' onclick='return confirm(\""._AM_CONFIRM_DELETE."\")'></td>
  </tr>
  <!-- Copyright of piCal (Don't remove this) -->
  <tr>
    <td colspan='8' align='right' valign='bottom' height='50'>".PICAL_COPYRIGHT."</td>
  </tr>
</table>
</form>
" ;


xoops_cp_footer();
?>
