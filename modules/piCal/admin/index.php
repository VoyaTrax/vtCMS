<?php

require_once( '../../../include/cp_header.php' ) ;
require_once( '../piCal.php' ) ;
require_once( XOOPS_ROOT_PATH . "/include/xoopscodes.php" ) ;
require_once( '../piCal_xoops.php' ) ;

// mysqlへの接続 (面倒なのであえて$xoopsDBは使わない)
// $conn = mysql_connect( XOOPS_DB_HOST , XOOPS_DB_USER , XOOPS_DB_PASS ) or die( "Could not connect." ) ;
// mysql_select_db( XOOPS_DB_NAME , $conn ) ;
$conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ
$table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;

// number of events needs admission
$rs = mysqli_query( $conn ,  "SELECT COUNT(id) FROM $table WHERE admission<1 AND (rrule_pid=0 OR rrule_pid=id)" ) ;
$notadmitted = mysqli_result( $rs ,  0 ,  0 ) ;
if( $notadmitted > 0 ) {
	$notadmitted = "<font color='#ff0000'><b>$notadmitted</b></font>";
}

xoops_cp_header();

echo "
<h4>"._AM_CONFIG."</h4>
<table width='100%' border='0' cellspacing='1' class='outer'>
  <tr>
    <td class='odd'>
      <b><a href='admission.php'>"._AM_ADMISSION." ($notadmitted)</a></b>
      <br />
      <br />
      <b><a href='icalendar_io.php'>"._AM_ICALENDAR_IO."</a></b>
      <br />
      <br />
      <b><a href='groupperm.php'>"._AM_GROUPPERM."</a></b>
      <br />
      <br />
      <b><a href='myblocksadmin.php'>"._AM_MYBLOCKSADMIN."</a></b>
      <br />
      <br />
      <b><a href='".XOOPS_URL.'/modules/system/admin.php?fct=preferences&amp;op=showmod&amp;mod='.$xoopsModule->getVar('mid')."'>"._AM_GENERALCONF."</a></b>
      <br />
      <br />
      <br />
      <br />
      <!-- Copyright of piCal (Don't remove this) -->
      <table width='100%'><tr><td class='odd' width='100%' align='right'>".PICAL_COPYRIGHT."</td></tr></table>
    </td>
  </tr>
</table>
" ;

xoops_cp_footer();
?>
