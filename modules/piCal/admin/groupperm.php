<?php

include_once( '../../../include/cp_header.php' ) ;
include_once( 'mygrouppermform.php' ) ;


// language files
$language = $xoopsConfig['language'] ;
if( ! file_exists( XOOPS_ROOT_PATH . "/modules/system/language/$language/admin/blocksadmin.php") ) $language = 'english' ;
include_once( XOOPS_ROOT_PATH . "/modules/system/language/$language/admin.php" ) ;

if( ! empty( $_POST['submit'] ) ) {
	include( "mygroupperm.php" ) ;
	redirect_header( XOOPS_URL."/modules/piCal/admin/groupperm.php" , 1 , _MD_AM_DBUPDATED );
	exit ;
}



/*if( ! file_exists( XOOPS_ROOT_PATH.'/class/xoopsform/grouppermform.php' ) ) {
	redirect_header( "index.php" , 5 , "The feature of group's permission needs XOOPS >= 2.0.4" ) ;
}*/
// include_once XOOPS_ROOT_PATH.'/class/xoopsform/grouppermform.php';


// include_once '../piCal.php';
// require_once( XOOPS_ROOT_PATH . "/include/xoopscodes.php" ) ;
// require_once( '../piCal_xoops.php' ) ;


$item_list = array(
	'1' => _AM_GPERM_G_INSERTABLE ,
	'2' => _AM_GPERM_G_SUPERINSERT ,
	'4' => _AM_GPERM_G_EDITABLE ,
	'8' => _AM_GPERM_G_SUPEREDIT ,
//	'16' => _AM_GPERM_G_DELETABLE ,
	'32' => _AM_GPERM_G_SUPERDELETE
//	'64' => _AM_GPERM_G_TOUCHOTHERS
	) ;

$form = new MyXoopsGroupPermForm( _AM_GROUPPERM , $xoopsModule->mid() , 'pical_global' , _AM_GROUPPERMDESC ) ;
foreach( $item_list as $item_id => $item_name) {
	$form->addItem( $item_id , $item_name ) ;
}

xoops_cp_header();
echo $form->render() ;
xoops_cp_footer();

?>
