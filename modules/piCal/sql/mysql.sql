#
# Table structure for table 'pical_event'
#
# uid:¥æ¡¼¥¶ID  (iCalender¤Ë¤ª¤±¤ë UniqueID ¤È¤Ï°Û¤Ê¤ë)


CREATE TABLE pical_event (
  id int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  uid mediumint(8) unsigned zerofill NOT NULL,
  groupid smallint(5) unsigned zerofill NOT NULL,
  summary VARCHAR(255) NOT NULL DEFAULT '' ,
  location VARCHAR(255) NOT NULL DEFAULT '' ,
  organizer VARCHAR(255) NOT NULL DEFAULT '',
  sequence VARCHAR(255) NOT NULL DEFAULT '',
  contact VARCHAR(255) NOT NULL DEFAULT '',
  tzid VARCHAR(255) NOT NULL DEFAULT 'Japan',
  description text NOT NULL DEFAULT '',
  dtstamp TIMESTAMP NOT NULL,
  categories VARCHAR(255) NOT NULL DEFAULT '',
  transp TINYINT NOT NULL DEFAULT 1,
  priority TINYINT NOT NULL DEFAULT 0,
  admission TINYINT NOT NULL DEFAULT 0,
  class VARCHAR(255) NOT NULL DEFAULT 'PUBLIC',
  rrule VARCHAR(255) NOT NULL DEFAULT '',
  rrule_pid int(8) unsigned zerofill NOT NULL DEFAULT 0,
  unique_id VARCHAR(255) NOT NULL DEFAULT '',
  allday TINYINT NOT NULL DEFAULT 0,
  start INT(10) unsigned NOT NULL DEFAULT 0,
  end INT(10) unsigned NOT NULL DEFAULT 0,
  extkey0 INT(10) unsigned zerofill NOT NULL DEFAULT 0,
  extkey1 INT(10) unsigned zerofill NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
) ENGINE=MyISAM;

