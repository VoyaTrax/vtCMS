<?php
// for monthly calendar block
define('_MB_PICAL_PREV_MONTH','Prev');
define('_MB_PICAL_NEXT_MONTH','Next');
define('_MB_PICAL_YEAR','');
define('_MB_PICAL_MONTH','');
define('_MB_PICAL_JUMP','Jump');

// for after the day's events block
// %s means the indicated day
define('_MB_PICAL_EVENTS_AFTER','Events after %s');

// for the day's events block
// %s means the indicated day
define('_MB_PICAL_EVENTS_THEDAY','Events on %s');


?>