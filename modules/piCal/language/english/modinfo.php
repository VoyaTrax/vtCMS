<?php
// Module Info

// The name of this module
define("_MI_PICAL_NAME","piCal");

// A brief description of this module
define("_MI_PICAL_DESC","Calendar module with Scheduler");

// Names of blocks for this module (Not all module has blocks)
define("_MI_PICAL_BNAME_MINICAL","MiniCalendar");
define("_MI_PICAL_BNAME_MINICAL_DESC","Display mini calendar block");
define("_MI_PICAL_BNAME_MONTHCAL","monthly calendar");
define("_MI_PICAL_BNAME_MONTHCAL_DESC","Display full size of monthly calendar");
define("_MI_PICAL_BNAME_TODAYS","Today's events");
define("_MI_PICAL_BNAME_TODAYS_DESC","Display events of today");
define("_MI_PICAL_BNAME_THEDAYS","This day's events");
define("_MI_PICAL_BNAME_THEDAYS_DESC","Display events of the day indicated");
define("_MI_PICAL_BNAME_COMING","Coming events");
define("_MI_PICAL_BNAME_COMING_DESC","Display coming events");
define("_MI_PICAL_BNAME_AFTER","Events after this day");
define("_MI_PICAL_BNAME_AFTER_DESC","Display events after the day indicated");

// Names of submenu
// define("_MI_PICAL_SMNAME1","");

//define("_MI_PICAL_ADMENU1","");

// Title of config items
define("_MI_USERS_AUTHORITY", "Authorities of users");
define("_MI_GUESTS_AUTHORITY", "Authorities of guests");
define("_MI_MINICAL_TARGET", "Page displaying in center when the date of minicalendar is clicked");
define("_MI_COMING_NUMROWS", "The number of displaying events in coming events block");
define("_MI_SKINFOLDER", "Name of skin folder");
define("_MI_SUNDAYCOLOR", "Color of Sunday");
define("_MI_WEEKDAYCOLOR", "Color of weekday");
define("_MI_SATURDAYCOLOR", "Color of Saturday");
define("_MI_HOLIDAYCOLOR", "Color of holiday");
define("_MI_TARGETDAYCOLOR", "Color of targeted day");
define("_MI_SUNDAYBGCOLOR", "Bgcolor of Sunday");
define("_MI_WEEKDAYBGCOLOR", "Bgcolor of weekday");
define("_MI_SATURDAYBGCOLOR", "Bgcolor of Saturday");
define("_MI_HOLIDAYBGCOLOR", "Bgcolor of holiday");
define("_MI_TARGETDAYBGCOLOR", "Bgcolor of targeted day");
define("_MI_CALHEADCOLOR", "Color of header part of calendar");
define("_MI_CALHEADBGCOLOR", "Bgcolor of header part of calendar");
define("_MI_CALFRAMECSS", "Style for the flame of calendar");
define("_MI_CANOUTPUTICS", "Permission of outputting ics files");
define("_MI_MAXRRULEEXTRACT", "Upper limit of events extracted by rrule.(COUNT)");
define("_MI_WEEKSTARTFROM", "Beginning day of the week");
define("_MI_DAYSTARTFROM", "Borderline to separate days");
define("_MI_NAMEORUNAME" , "Poster name displayed" ) ;
define("_MI_DESCNAMEORUNAME" , "Select which 'name' is displayed" ) ;

// Description of each config items
define("_MI_EDITBYGUESTDSC", "Permission of adding events by guet");

// Options of each config items
define("_MI_OPT_AUTH_NONE", "cannot add");
define("_MI_OPT_AUTH_WAIT", "can add but need admission");
define("_MI_OPT_AUTH_POST", "can add without admission");
define("_MI_OPT_AUTH_BYGROUP", "Specified in Group's permissions");
define("_MI_OPT_MINI_PHPSELF", "current page");
define("_MI_OPT_MINI_MONTHLY", "monthly calendar");
define("_MI_OPT_MINI_WEEKLY", "weekly calendar");
define("_MI_OPT_MINI_DAILY", "daily calendar");
define("_MI_OPT_CANOUTPUTICS", "can output");
define("_MI_OPT_CANNOTOUTPUTICS", "cannot output");
define("_MI_OPT_STARTFROMSUN", "Sunday");
define("_MI_OPT_STARTFROMMON", "Monday");
define("_MI_OPT_USENAME" , "Handle Name" ) ;
define("_MI_OPT_USEUNAME" , "Login Name" ) ;

// Admin Menus
define("_MI_PICAL_ADMENU0","Admitting Events");
define("_MI_PICAL_ADMENU1","iCalendar I/O");
define("_MI_PICAL_ADMENU2","Group's permissions");


// Text for notifications
define('_MI_PICAL_GLOBAL_NOTIFY', 'Global');
define('_MI_PICAL_GLOBAL_NOTIFYDSC', 'Global piCal notification options.');
define('_MI_PICAL_CATEGORY_NOTIFY', 'Category');
define('_MI_PICAL_CATEGORY_NOTIFYDSC', 'Notification options that apply to the current category.');
define('_MI_PICAL_EVENT_NOTIFY', 'Event');
define('_MI_PICAL_EVENT_NOTIFYDSC', 'Notification options that apply to the current event.');

define('_MI_PICAL_GLOBAL_NEWEVENT_NOTIFY', 'New Event');
define('_MI_PICAL_GLOBAL_NEWEVENT_NOTIFYCAP', 'Notify me when a new event is created.');
define('_MI_PICAL_GLOBAL_NEWEVENT_NOTIFYDSC', 'Receive notification when a new event is created.');
define('_MI_PICAL_GLOBAL_NEWEVENT_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New event');


?>