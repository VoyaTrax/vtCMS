<?php

// titles
define("_AM_CONFIG","Configuration Menu of piCal");
define("_AM_GENERALCONF","General Settings of piCal");
define("_AM_ADMISSION","Admitting Events");
define("_AM_ICALENDAR_IO","iCalendar I/O");
define("_AM_ICALENDAR_IMPORT","Import iCalendar");
define("_AM_ICALENDAR_EXPORT","Export iCalendar");
define("_AM_GROUPPERM","Group's Permissions");
define("_AM_MYBLOCKSADMIN","piCal's Block&Group admin");

// forms
define("_AM_BUTTON_EXTRACT","Extract");
define("_AM_BUTTON_ADMIT","Admit");
define("_AM_CONFIRM_DELETE","Delete OK?");

// format
define("_AM_DTFMT_LIST_ALLDAY",'y-m-d');
define("_AM_DTFMT_LIST_NORMAL",'y-m-d<\b\r />H:i');

// admission
define("_AM_LABEL_ADMIT","Checked events are: to be admitted");
define("_AM_MES_ADMITTED","Event(s) has be admitted");
define("_AM_ADMIT_TH0","User");
define("_AM_ADMIT_TH1","Start datetime");
define("_AM_ADMIT_TH2","Finish datetime");
define("_AM_ADMIT_TH3","Title");
define("_AM_ADMIT_TH4","Rrule");

// iCalendar I/O

define("_AM_LABEL_IMPORTFROMWEB","Import iCalendar data from web (Input URI started from 'http://' or 'webcal://')");
define("_AM_LABEL_UPLOADFROMFILE","Upload iCalendar data (Select a file from your local machine)");
define("_AM_BUTTON_IMPORT","Import!");
define("_AM_BUTTON_UPLOAD","Upload!");
define("_AM_LABEL_IO_CHECKEDITEMS","Checked events are:");
define("_AM_LABEL_IO_OUTPUT","to be exported in iCalendar");
define("_AM_LABEL_IO_SELECTPLATFORM","Select platform");
define("_AM_LABEL_IO_DELETE","to be deleted");
define("_AM_MES_DELETED","Event(s) has be deleted");
define("_AM_IO_TH0","User");
define("_AM_IO_TH1","Start datetime");
define("_AM_IO_TH2","Finish datetime");
define("_AM_IO_TH3","Title");
define("_AM_IO_TH4","Rrule");
define("_AM_IO_TH5","Admission");

// Group's Permissions
define( '_AM_GPERM_G_INSERTABLE' , "Can add" ) ;
define( '_AM_GPERM_G_SUPERINSERT' , "Super add" ) ;
define( '_AM_GPERM_G_EDITABLE' , "Can edit" ) ;
define( '_AM_GPERM_G_SUPEREDIT' , "Super edit" ) ;
define( '_AM_GPERM_G_DELETABLE' , "Can del" ) ;
define( '_AM_GPERM_G_SUPERDELETE' , "Super del" ) ;
define( '_AM_GPERM_G_TOUCHOTHERS' , "Can touch others" ) ;
define( '_AM_GROUPPERMDESC' , "Select permissions that each group is allowed to do<br />If you need this feature, set 'Authorities of users' to Specified in Group's permissions at first.<br />The settings of two groups of Administrator and Guest will be ignored." ) ;



?>