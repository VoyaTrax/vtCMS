<?php

// piCal xoops用メインモジュール
// index.php
// カレンダーの表示・編集・更新処理
// by GIJ=CHECKMATE (PEAK Corp. http://www.peak.ne.jp/)

	require( '../../mainfile.php' ) ;

	// 言語テスト用
	// $xoopsConfig[ 'language' ] = 'french' ;

	// mysqlへの接続
	// $conn = mysql_connect( XOOPS_DB_HOST , XOOPS_DB_USER , XOOPS_DB_PASS ) or die( "Could not connect." ) ;
	// mysql_select_db( XOOPS_DB_NAME , $conn ) ;
	$conn = $xoopsDB->conn ;	// 本来はprivateメンバなので将来的にはダメ

	// 各種パスの設定
	$mod_path = XOOPS_ROOT_PATH."/modules/piCal" ;
	$mod_url = XOOPS_URL."/modules/piCal" ;

	// クラス定義の読み込み
	require_once( "$mod_path/piCal.php" ) ;
	require_once( XOOPS_ROOT_PATH . "/include/xoopscodes.php" ) ;
	require_once( "$mod_path/piCal_xoops.php" ) ;

	// GET,POST変数の取得・前処理
	if( empty( $_GET['action'] ) && ! empty( $_GET['event_id'] ) ) $_GET['action'] = 'View' ;

	if( isset( $_GET[ 'smode' ] ) ) $smode = $_GET[ 'smode' ] ;
	else $smode = 'Monthly' ;

	if( isset( $_GET[ 'action' ] ) ) $action = $_GET[ 'action' ] ;
	else $action = '' ;

	// オブジェクトの生成
	$cal = new piCal_xoops( "" , $xoopsConfig['language'] ) ;

	// 各プロパティの設定
	$cal->conn = $conn ;
	$cal->table = XOOPS_DB_PREFIX . '_' . PICAL_EVENT_TABLE ;
	include( "$mod_path/read_configs.php" ) ;
	$cal->base_url = $mod_url ;
	$cal->images_url = "$mod_url/images/$skin_folder" ;
	$cal->images_path = "$mod_path/images/$skin_folder" ;

	// データベース更新関係の処理（いずれも、Locationで飛ばす）
	if( isset( $_POST[ 'update' ] ) ) {
		// 更新
		if( ! $editable ) die( _MB_PICAL_ERR_NOPERMTOUPDATE ) ;
		$cal->update_schedule( "$admission_update_sql" , $whr_sql_append ) ;
	} else if( isset( $_POST[ 'insert' ] ) || isset( $_POST[ 'saveas' ] ) ) {
		// saveas または 新規登録
		if( ! $insertable ) die( _MB_PICAL_ERR_NOPERMTOINSERT ) ;
		$_POST[ 'event_id' ] = "" ;
		$cal->update_schedule( ",uid='$user_id' $admission_insert_sql" , '' , 'notify_new_event' ) ;
	} else if( isset( $_POST[ 'delete' ] ) ) {
		// 削除
		if( ! $deletable ) die( _MB_PICAL_ERR_NOPERMTODELETE ) ;
		$cal->delete_schedule( $whr_sql_append , 'global $xoopsModule; xoops_comment_delete($xoopsModule->mid(),$id);' ) ;
	} else if( isset( $_POST[ 'output_ics' ] ) ) {
		// ics出力
		$cal->output_ics( ) ;
	}

	// XOOPSヘッダ出力
	if( $action == 'View' ) $xoopsOption['template_main'] = 'pical_event_detail.html' ;
	include( XOOPS_ROOT_PATH.'/header.php' ) ;

	// embed style sheet の出力（Thx Ryuji !)
	$xoopsTpl->assign( "xoops_module_header" , "<style><!-- \n" . $cal->get_embed_css() . "\n--></style>\n" . $xoopsTpl->get_template_vars( "xoops_module_header" ) ) ;

	// 実行時間計測スタート
	// list( $usec , $sec ) = explode( " " , microtime() ) ;
	// $picalstarttime = $sec + $usec ;

	// ページ表示関連の処理分け
	if( $action == 'Edit' ) {
		include_once( XOOPS_ROOT_PATH.'/include/calendarjs.php' ) ;
		echo $cal->get_schedule_edit_html( ) ;
	} else if( $action == 'View' ) {
		// echo $cal->get_schedule_view_html( ) ;
		$xoopsTpl->assign( 'detail_body' , $cal->get_schedule_view_html( ) ) ;
		$HTTP_GET_VARS['event_id'] = $_GET['event_id'] = $cal->original_id ;
		include XOOPS_ROOT_PATH.'/include/comment_view.php' ;
	} else switch( $smode ) {
		case 'Yearly' :
			$cal->display_yearly( ) ;
			break ;
		case 'Weekly' :
			$cal->display_weekly( ) ;
			break ;
		case 'Daily' :
			$cal->display_daily( ) ;
			break ;
		case 'Monthly' :
		default :
			$cal->display_monthly( ) ;
			break ;
	}

	// 実行時間表示
	// list( $usec , $sec ) = explode( " " , microtime() ) ;
	// echo "<p>" . ( $sec + $usec - $picalstarttime ) . "sec.</p>" ;

//	var_dump( $xoopsTpl ) ;

	// XOOPSフッタ出力
	include( XOOPS_ROOT_PATH.'/footer.php' ) ;

?>
