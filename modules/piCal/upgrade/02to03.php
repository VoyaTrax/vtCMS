<?php

require( '../../../mainfile.php' ) ;

// すでにバージョンが上がっているかどうかを確認するためのクエリ
$sql4check = "SELECT start_date FROM ".$xoopsDB->prefix('schedule') ;

echo '<html><head><title>piCal 0.2 to 0.3 upgrade</title></head><body>';

if (isset($_POST['submit'])) {

	if( ! $xoopsDB->query( $sql4check ) ) {
		echo 'すでにpiCalは0.3にアップグレード済みです' ;
	} else {

		if (!$xoopsDB->queryFromFile('./02to03.sql')) {
			echo '02to03.sql というファイルがありません';
		}
		echo $xoopsLogger->dumpQueries();
		echo 'piCal 0.2 から 0.3 へのアップグレードが終了しました。<br />忘れずに、サーバから modules/piCal/upgrade/ フォルダを消して下さい。';
	}

} else {

	if( ! $xoopsDB->query( $sql4check ) ) {
		echo 'すでにpiCalは0.3にアップグレード済みです' ;
	} else {

		echo 'piCal 0.2 から 0.3 へのアップグレードを行うためには、下のボタンを押して下さい。<br />
<form action="02to03.php" method="post">
<input type="submit" name="submit" value="'._SUBMIT.'" />
</form>';
	}
}

echo '</body></html>';
?>