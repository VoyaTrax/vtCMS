<?php

require( '../../../mainfile.php' ) ;

// すでにバージョンが上がっているかどうかを確認するためのクエリ
$sql4check_020 = "SELECT start_date FROM ".$xoopsDB->prefix('schedule') ;
$sql4check_030 = "SELECT start FROM ".$xoopsDB->prefix('schedule') ;

echo '<html><head><title>piCal 0.3 to 0.4 upgrade</title></head><body>';

if (isset($_POST['submit'])) {

	if( $xoopsDB->query( $sql4check_020 ) ) {
		die( 'まず、piCalを0.2から0.3に上げてください' ) ;
	}

	if( ! $xoopsDB->query( $sql4check_030 ) ) {
		echo 'すでにpiCalは0.4にアップグレード済みです' ;
	} else {

		// テーブル名を、xoops_schedule から xoops_pical_event に変更
		$xoopsDB->query('DROP TABLE IF EXISTS ' . $xoopsDB->prefix('pical_event') ) ;
		$xoopsDB->query('ALTER TABLE ' . $xoopsDB->prefix('schedule') . ' RENAME ' . $xoopsDB->prefix('pical_event') ) ;
		$xoopsDB->query('ALTER TABLE ' . $xoopsDB->prefix('pical_event') . ' ADD rrule_pid int(8) unsigned zerofill NOT NULL DEFAULT 0') ;
		echo $xoopsLogger->dumpQueries();
		echo "piCal 0.3 から 0.4 へのアップグレードが終了しました。<br />( <font color='red'>SELECT start_date FROM *schedule Error number: 1054 Error message: Unknown column 'start_date' in 'field list'</font> というエラーが表示されているのは正常です)<br />忘れずに、サーバから modules/piCal/upgrade/ フォルダを消して下さい。" ;
	}

} else {

	if( $xoopsDB->query( $sql4check_020 ) ) {
		die( 'まず、piCalを0.2から0.3に上げてください' ) ;
	}

	if( ! $xoopsDB->query( $sql4check_030 ) ) {
		echo 'すでにpiCalは0.4にアップグレード済みです' ;
	} else {

		echo 'piCal 0.3 から 0.4 へのアップグレードを行うためには、下のボタンを押して下さい。<br />
<form action="03to04.php" method="post">
<input type="submit" name="submit" value="'._SUBMIT.'" />
</form>';
	}
}

echo '</body></html>';
?>