ALTER TABLE schedule ADD unique_id VARCHAR(255) NOT NULL DEFAULT '', ADD allday TINYINT NOT NULL DEFAULT 0, ADD start INT(10) unsigned NOT NULL DEFAULT 0, ADD end INT(10) unsigned NOT NULL DEFAULT 0, ADD extkey0 INT(10) unsigned zerofill NOT NULL DEFAULT 0, ADD extkey1 INT(10) unsigned zerofill NOT NULL DEFAULT 0;
UPDATE schedule SET end=UNIX_TIMESTAMP(CONCAT(end_date,end_time)),start=UNIX_TIMESTAMP(CONCAT(start_date,start_time)),unique_id=CONCAT('pical030-',PASSWORD(id+RAND())),dtstamp=dtstamp;
ALTER TABLE schedule DROP start_date, DROP start_time, DROP end_date, DROP end_time;
