<?php

// �������塼���ե������������饹 piCal
// piCal.php
// by GIJ=CHECKMATE (PEAK Corp. http://www.peak.ne.jp/)


define( 'PICAL_COPYRIGHT' , "piCal-0.51 &nbsp; <a href='http://www.peak.ne.jp/' target='_blank'>&copy;PEAK Corp.</a>" ) ;
define( 'PICAL_EVENT_TABLE' , 'pical_event' ) ;


class piCal
{
	// �����ե������ˤ����ꤵ�����٤��ѿ�
	var $holidays = array() ;
	var $date_short_names = array() ;
	var $date_long_names = array() ;
	var $week_numbers = array() ;
	var $week_short_names = array() ;
	var $week_middle_names = array() ;
	var $week_long_names = array() ;
	var $month_short_names = array() ;
	var $month_middle_names = array() ;
	var $month_long_names = array() ;
	var $byday2langday_w = array() ;
	var $byday2langday_m = array() ;

	// ����������ɽ������ public
	var $holiday_color = '#CC0000' ;
	var $holiday_bgcolor = '#FFEEEE' ;
	var $sunday_color = '#CC0000' ;
	var $sunday_bgcolor = '#FFEEEE' ;
	var $saturday_color = '#0000FF' ;
	var $saturday_bgcolor = '#EEF7FF' ;
	var $weekday_color = '#000099' ;
	var $weekday_bgcolor = '#FFFFFF' ;
	var $targetday_bgcolor = '#CCFF99' ;
	var $calhead_color = '#009900' ;
	var $calhead_bgcolor = '#CCFFCC' ;
	var $frame_css = 'border:solid 1px green;' ;

	// TimeZone�ط�
	var $server_TZ = 9 ;			// �����Ф�Timezone Offset (ñ��:����)
	var $user_TZ = 9 ;				// �桼����Timezone Offset (ñ��:����)
	var $use_server_TZ = false ;	// �����Х������륿������caldate������������
	var $server_tzid = 'Japan' ;	// �����Ф�Timezone ID
	var $user_tzid = 'Japan' ;		// �桼����Timezone ID

	// ���´ط�
	var $insertable = true ;		// �������٥��Ȥ���Ͽ����
	var $editable = true ;			// ��¸���٥��Ȥ��ѹ�����
	var $deletable = true ;			// ��¸���٥��Ȥκ�������
	var $user_id = -1 ;				// �桼��ID
	var $isadmin = false ;			// �����Ը��¤��ɤ���

	// ����¾ public�ץ��ѥƥ�
	var $conn ;					// mysql�Ȥ���³�ϥ��ɥ� (ͽ�������򤹤������å�)
	var $table = 'pical_event' ;	// ���٥��ȥơ��֥�̾
	var $base_url = '' ;
	var $images_url = '/include/piCal/images' ;	// ���Υե������� spacer.gif, arrow*.gif �����֤��Ƥ���
	var $images_path = 'include/piCal/images' ;
	var $can_output_ics = true ;	// ics�ե��������Ϥ����Ĥ��뤫�ɤ���
	var $connection = 'http' ;		// http �� https ��
	var $max_rrule_extract = 100 ;	// rrule ��Ÿ���ξ��¿�(COUNT)
	var $week_start = 0 ;			// ���γ������� 0������ 1������
	var $day_start = 0 ;			// ���դζ���������ñ�̡�
	var $groups = array() ;			// PRIVATE����������ǽ�ʥ��롼�פ�Ϣ������
	var $nameoruname = 'name' ;		// ���ƼԤ�ɽ���ʥ�������̾���ϥ��ɥ�̾����

	// private������
	var $year ;
	var $month ;
	var $date ;
	var $day ;			// 0:���� �� 6:����
	var $daytype ;		// 0:ʿ�� 1:���� 2:���� 3:����
	var $caldate ;		// ����Y-n-j�����ˤ��Ƥ���
	var $unixtime ;
	var $mydir ;
	var $long_event_legends = array() ;
	var $language = "japanese" ;

	// �����դ������ѥ�����
	var $original_id ;	// $_GET['event_id']����������ľ���˻��Ȳ�ǽ


/*******************************************************************/
/*        ���󥹥ȥ饯����                                         */
/*******************************************************************/

// ���󥹥ȥ饯��
function __construct( $target_date = "" , $language = "japanese" )
{
	// ���դΥ��å�
	if( $target_date ) {
		$this->set_date( $target_date ) ;
	} else if( isset( $_GET[ 'caldate' ] ) ) {
		$this->set_date( $_GET[ 'caldate' ] ) ;
	} else if( isset( $_POST[ 'pical_jumpcaldate' ] ) && isset( $_POST[ 'pical_year' ] ) ) {
		if( empty( $_POST[ 'pical_month' ] ) ) {
			// ǯ�Τߤ�POST���줿����
			$month = 1 ;
			$date = 1 ;
		} else if( empty( $_POST[ 'pical_date' ] ) ) {
			// ǯ���POST���줿����
			$month = intval( $_POST[ 'pical_month' ] ) ;
			$date = 1 ;
		} else {
			// ǯ�������POST���줿����
			$month = intval( $_POST[ 'pical_month' ] ) ;
			$date = intval( $_POST[ 'pical_date' ] ) ;
		}
		$year = intval( $_POST[ 'pical_year' ] ) ;
		$this->set_date( "$year-$month-$date" ) ;
	} else {
		$this->set_date( date( 'Y-n-j' ) ) ;
		$this->use_server_TZ = true ;
	}

	// SSL��̵ͭ����$_SERVER['HTTPS'] �ˤ�Ƚ��
	if( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == "on" ) {
		$this->connection = 'https' ;
	} else {
		$this->connection = 'http' ;
	}

	// piCal���饹��¸�ߤ����ǥ��쥯�ȥ���Ĵ�٤Ƥ���
	$__file__ = str_replace( DIRECTORY_SEPARATOR , '/' , __FILE__ ) ;
	$this->mydir = substr( $__file__ , 0 , strrpos( $__file__ , '/' ) ) ;

	// �����ե��������ɤ߹���
	if ( file_exists( "$this->mydir/language/$language/pical_vars.phtml" ) ) {
		include "$this->mydir/language/$language/pical_vars.phtml" ;
		include_once "$this->mydir/language/$language/pical_constants.php" ;
		$this->language = $language ;
	} else if( file_exists( "$this->mydir/language/english/pical_vars.phtml") ) {
		include "$this->mydir/language/english/pical_vars.phtml" ;
		include_once "$this->mydir/language/english/pical_constants.php" ;
		$this->language = "english" ;
	}
}


// year,month,day,caldate,unixtime �򥻥åȤ���
function set_date( $setdate )
{
	if( ! ( ereg( "^([0-9][0-9]+)[-./]?([0-1]?[0-9])[-./]?([0-3]?[0-9])" , $setdate , $regs ) && checkdate( $regs[2] , $regs[3] , $regs[1] ) ) ) {
		ereg( "^([0-9]{4})-([0-9]{2})-([0-9]{2})" , date( 'Y-m-d' ) , $regs ) ;
		$this->use_server_TZ = true ;
	}
	$this->year = $year = intval( $regs[1] ) ;
	$this->month = $month = intval( $regs[2] ) ;
	$this->date = $date = intval( $regs[3] ) ;
	$this->caldate = "$year-$month-$date" ;
	$this->unixtime = mktime(0,0,0,$month,$date,$year) ;

	// ���������ե����פΥ��å�
	// �ĥ��顼�θ���
	if( $month <= 2 ) {
		$year -- ;
		$month += 12 ;
	}
	$day = ( $year + floor( $year / 4 ) - floor( $year / 100 ) + floor( $year / 400 ) + floor( 2.6 * $month + 1.6 ) + $date ) % 7 ;

	$this->day = $day ;
	if( $day == 0 ) $this->daytype = 2 ;
	else if( $day == 6 ) $this->daytype = 1 ;
	else $this->daytype = 0 ;

	if( isset( $this->holidays[ $this->caldate ] ) ) $this->daytype = 3 ;
}



// �����������μ��फ���طʿ���ʸ����������
function daytype_to_colors( $daytype )
{
	switch( $daytype ) {
		case 3 :
			//	Holiday
			return array( $this->holiday_bgcolor , $this->holiday_color ) ;
		case 2 :
			//	Sunday
			return array( $this->sunday_bgcolor , $this->sunday_color ) ;
		case 1 :
			//	Saturday
			return array( $this->saturday_bgcolor , $this->saturday_color ) ;
		case 0 :
		default :
			// Weekday
			return array( $this->weekday_bgcolor , $this->weekday_color ) ;
	}
}



// SQL���������դ��顢�����������μ����������륯�饹�ؿ�
function get_daytype( $date )
{
	ereg( "^([0-9][0-9]+)[-./]?([0-1]?[0-9])[-./]?([0-3]?[0-9])" , $date , $regs ) ;
	$year = intval( $regs[1] ) ;
	$month = intval( $regs[2] ) ;
	$date = intval( $regs[3] ) ;

	// ������3
	if( isset( $this->holidays[ "$year-$month-$date" ] ) ) return 3 ;

	// �ĥ��顼�θ���
	if ($month <= 2) {
		$year -- ;
		$month += 12;
	}
	$day = ( $year + floor( $year / 4 ) - floor( $year / 100 ) + floor( $year / 400 )+ floor( 2.6 * $month + 1.6 ) + $date ) % 7 ;

	if( $day == 0 ) return 2 ;
	else if( $day == 6 ) return 1 ;
	else return 0 ;
}



/*******************************************************************/
/*        �֥��å���ɽ���ؿ�                                       */
/*******************************************************************/

// $this->caldate����ͽ�� ���֤�
function get_date_schedule( $get_target = '' )
{
	if( $get_target == '' ) $get_target = $_SERVER[ 'PHP_SELF' ] ;

	$ret = '' ;

	// �������׻����Ĥġ�WHERE���δ��֤˴ؤ�����������
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	if( $tzoffset == 0 ) {
		// �������ʤ����� ��mysql�����٤򤫤������ʤ����ᡢ�����Ǿ���ʬ�����Ȥ�)
		$whr_term = "start<'".($this->unixtime + 86400)."' AND end>'$this->unixtime'" ;
	} else {
		// ���������������ϡ�allday�ˤ��äƾ���ʬ��
		$whr_term = "( allday AND start<='$this->unixtime' AND end>'$this->unixtime') OR ( ! allday AND start<'".($this->unixtime + 86400 - $tzoffset )."' AND end>'".($this->unixtime - $tzoffset )."')" ;
	}

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// �����Υ������塼������
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start,end" ) ;
	$num_rows = mysqli_num_rows( $yrs ) ;

	if( $num_rows == 0 ) $ret .= _PICAL_MB_NOEVENT."\n" ;
	else while( $event = mysqli_fetch_object( $yrs ) ) {

		$summary = $this->text_sanitizer_for_show( $event->summary ) ;

		if( $event->allday ) {
			// �������٥���
			$ret .= "
	       <table border='0' cellpadding='0' cellspacing='0' width='100%'>
	         <tr>
	           <td><img border='0' src='$this->images_url/dot_allday.gif' /> &nbsp; </td>
	           <td><font size='2'><a href='$get_target?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='calsummary_allday'>$summary</a></font></td>
	         </tr>
	       </table>\n" ;
		} else {
			// �̾磻�٥���
			$event->start += $tzoffset ;
			$event->end += $tzoffset ;
			$ret .= "
	       <dl>
	         <dt>
	           <font size='2'>".$this->get_todays_time_description( $event->start , $event->end , $this->caldate , false , true )."</font>
	         </dt>
	         <dd>
	           <font size='2'><a href='$get_target?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='calsummary'>$summary</a></font>
	         </dd>
	       </dl>\n" ;
		}
	}

	// ͽ�����ɲáʱ�ɮ����������
	if( $this->insertable ) $ret .= "
	       <dl>
	         <dt>
	           &nbsp; <font size='2'><a href='$get_target?smode=Daily&amp;action=Edit&amp;caldate=$this->caldate'><img src='$this->images_url/addevent.gif' border='0' width='14' height='12' />"._PICAL_MB_ADDEVENT."</a></font>
	         </dt>
	       </dl>\n" ;

	return $ret ;
}



// $this->caldate�ʹߤ�ͽ�� ������ $num ���֤�
function get_coming_schedule( $get_target = '' , $num = 5 )
{
	if( $get_target == '' ) $get_target = $_SERVER[ 'PHP_SELF' ] ;

	$ret = '' ;

	// �������׻����Ĥġ�WHERE���δ��֤˴ؤ�����������
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	if( $tzoffset == 0 ) {
		// �������ʤ����� ��mysql�����٤򤫤������ʤ����ᡢ�����Ǿ���ʬ�����Ȥ�)
		$whr_term = "end>'$this->unixtime'" ;
	} else {
		// ���������������ϡ�allday�ˤ��äƾ���ʬ��
		$whr_term = "(allday AND end>'$this->unixtime') OR ( ! allday AND end>'".($this->unixtime - $tzoffset )."')" ;
	}

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// �����ʹߤΥ������塼������
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start" ) ;
	$num_rows = mysqli_num_rows( $yrs ) ;

	if( $num_rows == 0 ) $ret .= _PICAL_MB_NOEVENT."\n" ;
	else for( $i = 0 ; $i < $num ; $i ++ ) {
		$event = mysqli_fetch_object( $yrs ) ;
		if( $event == false ) break ;
		$summary = $this->text_sanitizer_for_show( $event->summary ) ;

		if( $event->allday ) {
			// �������٥���
			$ret .= "
	       <dl>
	         <dt>
	           <font size='2'><img border='0' src='$this->images_url/dot_allday.gif' /> ".sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $event->start ) ] , $this->date_long_names[ date( 'j' , $event->start ) ] )."</font>
	         </dt>
	         <dd>
	           <font size='2'><a href='$get_target?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='calsummary_allday'>$summary</a></font>
	         </dd>
	       </dl>\n" ;
		} else {
			// �̾磻�٥���
			$event->start += $tzoffset ;
			$event->end += $tzoffset ;
			$ret .= "
	       <dl>
	         <dt>
	           <font size='2'>".$this->get_coming_time_description( $event->start , $this->unixtime )."</font>
	         </dt>
	         <dd>
	           <font size='2'><a href='$get_target?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='calsummary'>$summary</a></font>
	         </dd>
	       </dl>\n" ;
		}
	}

	// �Ĥ�������ɽ��
	if( $num_rows > $num ) $ret .= "
           <table border='0' cellspacing='0' cellpadding='0' width='100%'>
            <tr>
             <td align='right'><small>"._PICAL_MB_RESTEVENT_PRE.($num_rows-$num)._PICAL_MB_RESTEVENT_SUF."</small></td>
            </tr>
           </table>\n" ;

	// ͽ�����ɲáʱ�ɮ����������
	if( $this->insertable ) $ret .= "
	       <dl>
	         <dt>
	           &nbsp; <font size='2'><a href='$get_target?smode=Daily&amp;action=Edit&amp;caldate=$this->caldate'><img src='$this->images_url/addevent.gif' border='0' width='14' height='12' />"._PICAL_MB_ADDEVENT."</a></font>
	         </dt>
	       </dl>\n" ;

	return $ret ;
}



// �ߥ˥����������ѥ��٥��ȼ����ؿ�
function get_flags_date_has_events( $start , $end )
{
	// ���餫�����������������Ƥ���
	for( $time = $start ; $time < $end ; $time += 86400 ) {
		$ret[ date( 'j' , $time ) ] = 0 ;
	}

	// �����׻�
	$tzoffset = intval( ( $this->user_TZ - $this->server_TZ ) * 3600 ) ;
	$gmtoffset = intval( $this->server_TZ * 3600 ) ;

	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,allday FROM $this->table WHERE admission > 0 AND start < ".($end + 86400)." AND end > ".($start - 86400) ) ;
	while( $event = mysqli_fetch_object( $yrs ) ) {
		$time = $event->start > $start ? $event->start : $start ;
		if( ! $event->allday ) {
			$time += $tzoffset ;
			$event->end += $tzoffset ;
		}
		$time -= ( $time + $gmtoffset ) % 86400 ;
		while( $time < $end && $time < $event->end ) {
			$ret[ date( 'j' , $time ) ] = 1 ;
			$time += 86400 ;
		}
	}

	return $ret ;
}



// �ߥ˥���������ɽ����ʸ�������֤�
function get_mini_calendar_html( $get_target = '' , $query_string = '' , $mode = '' )
{
	// �¹Ի��ַ�¬��������
	// list( $usec , $sec ) = explode( " " , microtime() ) ;
	// $picalstarttime = $sec + $usec ;

	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	$original_level = error_reporting( E_ALL ^ E_NOTICE ) ;
	require_once( "$this->mydir/include/patTemplate.php" ) ;
	$tmpl = new PatTemplate() ;
	$tmpl->setBasedir( "$this->images_path" ) ;

	// ɽ���⡼�ɤ˱����ơ��ƥ��ץ졼�ȥե������򿶤�ʬ��
	switch( $mode ) {
		case 'NO_YEAR' :
			// ǯ��ɽ����
			$tmpl->readTemplatesFromFile( "minical_for_yearly.tmpl.html" ) ;
			$target_highlight_flag = false ;
			break ;
		case 'NO_NAVIGATE' :
			// ���֤β���������
			$tmpl->readTemplatesFromFile( "minical_for_monthly.tmpl.html" ) ;
			$target_highlight_flag = false ;
			break ;
		default :
			// �̾��Υߥ˥����������֥��å���
			$tmpl->readTemplatesFromFile( "minical.tmpl.html" ) ;
			$target_highlight_flag = true ;
			break ;
	}

	// �����γ��������٥��Ȥ����äƤ��뤫�ɤ���������
	$event_dates = $this->get_flags_date_has_events( mktime(0,0,0,$this->month,1,$this->year) , mktime(0,0,0,$this->month+1,1,$this->year) ) ;

	// �����Ϸ����������Ϸ����Ȥ���
	$prev_month = date("Y-n-j", mktime(0,0,0,$this->month,0,$this->year));
	$next_month = date("Y-n-j", mktime(0,0,0,$this->month+1,1,$this->year));

	$tmpl->addVar( "WholeBoard" , "PHP_SELF" , $PHP_SELF ) ;
	$tmpl->addVar( "WholeBoard" , "GET_TARGET" , $get_target ) ;
	$tmpl->addVar( "WholeBoard" , "QUERY_STRING" , $query_string ) ;

	$tmpl->addVar( "WholeBoard" , "MB_PREV_MONTH" , _PICAL_MB_PREV_MONTH ) ;
	$tmpl->addVar( "WholeBoard" , "MB_NEXT_MONTH" , _PICAL_MB_NEXT_MONTH ) ;
	$tmpl->addVar( "WholeBoard" , "MB_LINKTODAY" , _PICAL_MB_LINKTODAY ) ;

	$tmpl->addVar( "WholeBoard" , "SKINPATH" , $this->images_url ) ;
	$tmpl->addVar( "WholeBoard" , "FRAME_CSS" , $this->frame_css ) ;
//	$tmpl->addVar( "WholeBoard" , "YEAR" , $this->year ) ;
//	$tmpl->addVar( "WholeBoard" , "MONTH" , $this->month ) ;
	$tmpl->addVar( "WholeBoard" , "MONTH_NAME" , $this->month_middle_names[ $this->month ] ) ;
	$tmpl->addVar( "WholeBoard" , "YEAR_MONTH_TITLE" , sprintf( _PICAL_FMT_YEAR_MONTH , $this->year , $this->month_middle_names[ $this->month ] ) ) ;
	$tmpl->addVar( "WholeBoard" , "PREV_MONTH" , $prev_month ) ;
	$tmpl->addVar( "WholeBoard" , "NEXT_MONTH" , $next_month ) ;

	$tmpl->addVar( "WholeBoard" , "CALHEAD_BGCOLOR" , $this->calhead_bgcolor ) ;
	$tmpl->addVar( "WholeBoard" , "CALHEAD_COLOR" , $this->calhead_color ) ;


	$first_date = getdate(mktime(0,0,0,$this->month,1,$this->year));
	$date = ( - $first_date['wday'] + $this->week_start - 7 ) % 7 ;
	$wday_end = 7 + $this->week_start ;

	// ����̾�롼��
	$rows = array() ;
	for( $wday = $this->week_start ; $wday < $wday_end ; $wday ++ ) {
		if( $wday % 7 == 0 ) {
			//	Sunday
			$bgcolor = $this->sunday_bgcolor ;
			$color = $this->sunday_color ;
		} elseif( $wday == 6 ) {
			//	Saturday
			$bgcolor = $this->saturday_bgcolor ;
			$color = $this->saturday_color ;
		} else {
			// Weekday
			$bgcolor = $this->weekday_bgcolor ;
			$color = $this->weekday_color ;
		}

		// �ƥ��ץ졼���������ؤΥǡ������å�
		array_push( $rows , array(
			"BGCOLOR" => $bgcolor ,
			"COLOR" => $color ,
			"DAYNAME" => $this->week_short_names[ $wday % 7 ] ,
		) ) ;
	}

	// �ƥ��ץ졼�Ȥ˥ǡ�������������
	$tmpl->addRows( "DayNameLoop" , $rows ) ;
	$tmpl->parseTemplate( "DayNameLoop" , 'w' ) ;

	// �� (row) �롼��
	for( $week = 0 ; $week < 6 ; $week ++ ) {

		$rows = array() ;

		// �� (col) �롼��
		for( $wday = $this->week_start ; $wday < $wday_end ; $wday ++ ) {
			$date ++ ;
			if( ! checkdate($this->month,$date,$this->year) ) {
				// �����ϰϳ�
				array_push( $rows , array(
					"PHP_SELF" => $PHP_SELF ,
					"GET_TARGET" => $get_target ,
					"QUERY_STRING" => $query_string ,
					"SKINPATH" => $this->images_url ,
					"DATE" => date( 'j' , mktime( 0 , 0 , 0 , $this->month , $date , $this->year ) ) ,
					"DATE_TYPE" => 0
				) ) ;
				continue ;
			}

			$link = "$this->year-$this->month-$date" ;

			// ���������פˤ������迧����ʬ��
			if( isset( $this->holidays[$link] ) ) {
				//	Holiday
				$bgcolor = $this->holiday_bgcolor ;
				$color = $this->holiday_color ;
			} elseif( $wday % 7 == 0 ) {
				//	Sunday
				$bgcolor = $this->sunday_bgcolor ;
				$color = $this->sunday_color ;
			} elseif( $wday == 6 ) {
				//	Saturday
				$bgcolor = $this->saturday_bgcolor ;
				$color = $this->saturday_color ;
			} else {
				// Weekday
				$bgcolor = $this->weekday_bgcolor ;
				$color = $this->weekday_color ;
			}

			// ���������طʿ��ϥ��饤�Ƚ���
			if( $date == $this->date && $target_highlight_flag ) $bgcolor = $this->targetday_bgcolor ;

			// �ƥ��ץ졼���������ؤΥǡ������å�
			array_push( $rows , array(
				"PHP_SELF" => $PHP_SELF ,
				"GET_TARGET" => $get_target ,
				"QUERY_STRING" => $query_string ,

				"BGCOLOR" => $bgcolor ,
				"COLOR" => $color ,
				"LINK" => $link ,
				"DATE" => $date ,
				"DATE_TYPE" => $event_dates[ $date ] + 1
			) ) ;
		}
		// �ƥ��ץ졼�Ȥ˥ǡ�������������
		$tmpl->addRows( "DailyLoop" , $rows ) ;
		$tmpl->parseTemplate( "DailyLoop" , 'w' ) ;
		$tmpl->parseTemplate( "WeekLoop" , 'a' ) ;
	}

	$ret = $tmpl->getParsedTemplate() ;

	error_reporting( $original_level ) ;

	// �¹Ի��ֵ�Ͽ
	// list( $usec , $sec ) = explode( " " , microtime() ) ;
	// error_log( "MiniCalendar " . ( $sec + $usec - $picalstarttime ) . "sec." , 0 ) ;

	return $ret ;
}



/*******************************************************************/
/*        �ᥤ����ɽ���ؿ�                                         */
/*******************************************************************/

// ǯ�֥������������Τ�ɽ����patTemplate����)
function display_yearly( $get_target = '' , $query_string = '' )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	$original_level = error_reporting( E_ALL ^ E_NOTICE ) ;
	require_once( "$this->mydir/include/patTemplate.php" ) ;
	$tmpl = new PatTemplate() ;
	$tmpl->readTemplatesFromFile( "$this->images_path/yearly.tmpl.html" ) ;

	// �������ե������Υ��å�
	$tmpl->addVar( "WholeBoard" , "SKINPATH" , $this->images_url ) ;

	// �����ȥ����顦�ܥ����ʤɤν�����
	$tmpl->addVar( "WholeBoard" , "GET_TARGET" , $get_target ) ;
	$tmpl->addVar( "WholeBoard" , "QUERY_STRING" , $query_string ) ;

	// �����������إå�������ɬ�פʾ�����Ϣ���������֤�
	$tmpl->addVars( "WholeBoard" , $this->get_calendar_information( 'Y' ) ) ;

	$tmpl->addVar( "WholeBoard" , "LANG_JUMP" , _PICAL_BTN_JUMP ) ;

	// �Ʒ��Υߥ˥���������
	// $this->caldate �ΥХå����å�
	$backuped_caldate = $this->caldate ;

	// 12����ʬ�Υߥ˥��������������롼��
	for( $m = 1 ; $m <= 12 ; $m ++ ) {
		$this->set_date( date("Y-n-j", mktime(0,0,0,$m,1,$this->year)) ) ;
		$tmpl->addVar( "WholeBoard" , "MINICAL$m" , $this->get_mini_calendar_html( $get_target , $query_string , "NO_YEAR" ) ) ;
	}

	// $this->caldate �Υꥹ�ȥ�
	$this->set_date( $backuped_caldate ) ;

	// �Ǹ���patTemplate�ǥѡ�������ɽ��
	$tmpl->displayParsedTemplate( "WholeBoard" ) ;

	error_reporting( $original_level ) ;
}



// ���֥������������Τ�ɽ����patTemplate����)
function display_monthly( $get_target = '' , $query_string = '' )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	$original_level = error_reporting( E_ALL ^ E_NOTICE ) ;
	require_once( "$this->mydir/include/patTemplate.php" ) ;
	$tmpl = new PatTemplate() ;
	$tmpl->readTemplatesFromFile( "$this->images_path/monthly.tmpl.html" ) ;

	// �������ե������Υ��å�
	$tmpl->addVar( "WholeBoard" , "SKINPATH" , $this->images_url ) ;

	// �����ȥ����顦�ܥ����ʤɤν�����
	$tmpl->addVar( "WholeBoard" , "GET_TARGET" , $get_target ) ;
	$tmpl->addVar( "WholeBoard" , "QUERY_STRING" , $query_string ) ;
	$tmpl->addVar( "WholeBoard" , "YEAR_MONTH_TITLE" , sprintf( _PICAL_FMT_YEAR_MONTH , $this->year , $this->month_middle_names[ $this->month ] ) ) ;

	// �����������إå�������ɬ�פʾ�����Ϣ���������֤�
	$tmpl->addVars( "WholeBoard" , $this->get_calendar_information( 'M' ) ) ;

	$tmpl->addVar( "WholeBoard" , "LANG_JUMP" , _PICAL_BTN_JUMP ) ;

	// ��������������
	$tmpl->addVar( "WholeBoard" , "CALENDAR_BODY" , $this->get_monthly_html( $get_target , $query_string ) ) ;

	// Ĺ�����٥��Ȥ�����
	foreach( $this->long_event_legends as $bit => $legend ) {
		$tmpl->addVar( "LongEventLegends" , "BIT_MASK" , 1 << ( $bit - 1 ) ) ;
		$tmpl->addVar( "LongEventLegends" , "LEGEND_ALT" , _PICAL_MB_ALLDAY_EVENT . " $bit" ) ;
		$tmpl->addVar( "LongEventLegends" , "LEGEND" , $legend ) ;
		$tmpl->addVar( "LongEventLegends" , "SKINPATH" , $this->images_url ) ;
		$tmpl->parseTemplate( "LongEventLegends" , "a" ) ;
	}

	// ��������Υߥ˥���������
	// $this->caldate �ΥХå����å�
	$backuped_caldate = $this->caldate ;
	// �����������դ򥻥åȤ��������Υߥ˥����������򥻥å�
	$this->set_date( date("Y-n-j", mktime(0,0,0,$this->month,0,$this->year)) ) ;
	$tmpl->addVar( "WholeBoard" , "PREV_MINICAL" , $this->get_mini_calendar_html( $get_target , $query_string , "NO_NAVIGATE" ) ) ;
	// �����Ϥ����դ򥻥åȤ����ߥ˥�����������ɽ��
	$this->set_date( date("Y-n-j", mktime(0,0,0,$this->month+2,1,$this->year)) ) ;
	$tmpl->addVar( "WholeBoard" , "NEXT_MINICAL" , $this->get_mini_calendar_html( $get_target , $query_string , "NO_NAVIGATE" ) ) ;
	// $this->caldate �Υꥹ�ȥ�
	$this->set_date( $backuped_caldate ) ;

	// �Ǹ���patTemplate�ǥѡ�������ɽ��
	$tmpl->displayParsedTemplate( "WholeBoard" ) ;

	error_reporting( $original_level ) ;
}



// ���֥������������Τ�ɽ����patTemplate����)
function display_weekly( $get_target = '' , $query_string = '' )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	$original_level = error_reporting( E_ALL ^ E_NOTICE ) ;
	require_once( "$this->mydir/include/patTemplate.php" ) ;
	$tmpl = new PatTemplate() ;
	$tmpl->readTemplatesFromFile( "$this->images_path/weekly.tmpl.html" ) ;

	// �������ե������Υ��å�
	$tmpl->addVar( "WholeBoard" , "SKINPATH" , $this->images_url ) ;

	// �����ȥ����顦�ܥ����ʤɤν�����
	$tmpl->addVar( "WholeBoard" , "GET_TARGET" , $get_target ) ;
	$tmpl->addVar( "WholeBoard" , "QUERY_STRING" , $query_string ) ;

	// �����������إå�������ɬ�פʾ�����Ϣ���������֤�
	$tmpl->addVars( "WholeBoard" , $this->get_calendar_information( 'W' ) ) ;

	$tmpl->addVar( "WholeBoard" , "LANG_JUMP" , _PICAL_BTN_JUMP ) ;

	// ��������������
	$tmpl->addVar( "WholeBoard" , "CALENDAR_BODY" , $this->get_weekly_html( $get_target , $query_string ) ) ;

	// �Ǹ���patTemplate�ǥѡ�������ɽ��
	$tmpl->displayParsedTemplate( "WholeBoard" ) ;

	error_reporting( $original_level ) ;
}



// �����������������Τ�ɽ����patTemplate����)
function display_daily( $get_target = '' , $query_string = '' )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	$original_level = error_reporting( E_ALL ^ E_NOTICE ) ;
	require_once( "$this->mydir/include/patTemplate.php" ) ;
	$tmpl = new PatTemplate() ;
	$tmpl->readTemplatesFromFile( "$this->images_path/daily.tmpl.html" ) ;

	// �������ե������Υ��å�
	$tmpl->addVar( "WholeBoard" , "SKINPATH" , $this->images_url ) ;

	// �����ȥ����顦�ܥ����ʤɤν�����
	$tmpl->addVar( "WholeBoard" , "GET_TARGET" , $get_target ) ;
	$tmpl->addVar( "WholeBoard" , "QUERY_STRING" , $query_string ) ;

	// �����������إå�������ɬ�פʾ�����Ϣ���������֤�
	$tmpl->addVars( "WholeBoard" , $this->get_calendar_information( 'D' ) ) ;

	$tmpl->addVar( "WholeBoard" , "LANG_JUMP" , _PICAL_BTN_JUMP ) ;

	// ��������������
	$tmpl->addVar( "WholeBoard" , "CALENDAR_BODY" , $this->get_daily_html( $get_target , $query_string ) ) ;

	// �Ǹ���patTemplate�ǥѡ�������ɽ��
	$tmpl->displayParsedTemplate( "WholeBoard" ) ;

	error_reporting( $original_level ) ;
}



// �����������Υإå�������ɬ�פʾ�����Ϣ���������֤��ʷ��֡����֡��������̡�
function get_calendar_information( $mode = 'M' )
{
	$ret = array() ;

	// ���ܾ���
	$ret[ 'TODAY' ] = date( "Y-n-j" ) ;		// GIJ TODO �׼�ľ���ʻȤ��ʤ�����
	$ret[ 'CALDATE' ] = $this->caldate ;
	$ret[ 'DISP_YEAR' ] = sprintf( _PICAL_FMT_YEAR , $this->year ) ;
	$ret[ 'DISP_MONTH' ] = $this->month_middle_names[ $this->month ] ;
	$ret[ 'DISP_DATE' ] = $this->date_long_names[ $this->date ] ;
	$ret[ 'DISP_DAY' ] = "({$this->week_middle_names[ $this->day ]})" ;
	list( $bgcolor , $color ) =  $this->daytype_to_colors( $this->daytype ) ;
	$ret[ 'DISP_DAY_COLOR' ] = $color ;
	$ret[ 'COPYRIGHT' ] = PICAL_COPYRIGHT ;

	// �إå������Υ��顼
	$ret[ 'CALHEAD_BGCOLOR' ]  =  $this->calhead_bgcolor ;
	$ret[ 'CALHEAD_COLOR' ] = $this->calhead_color ;

	// ����������alt
	$ret[ 'ICON_DAILY' ] = _PICAL_ICON_DAILY ;
	$ret[ 'ICON_WEEKLY' ] = _PICAL_ICON_WEEKLY ;
	$ret[ 'ICON_MONTHLY' ] = _PICAL_ICON_MONTHLY ;
	$ret[ 'ICON_YEARLY' ] = _PICAL_ICON_YEARLY ;

	// ���å������֥��å�
	$ret[ 'MB_PREV_YEAR' ] = _PICAL_MB_PREV_YEAR ;
	$ret[ 'MB_NEXT_YEAR' ] = _PICAL_MB_NEXT_YEAR ;
	$ret[ 'MB_PREV_MONTH' ] = _PICAL_MB_PREV_MONTH ;
	$ret[ 'MB_NEXT_MONTH' ] = _PICAL_MB_NEXT_MONTH ;
	$ret[ 'MB_PREV_WEEK' ] = _PICAL_MB_PREV_WEEK ;
	$ret[ 'MB_NEXT_WEEK' ] = _PICAL_MB_NEXT_WEEK ;
	$ret[ 'MB_PREV_DATE' ] = _PICAL_MB_PREV_DATE ;
	$ret[ 'MB_NEXT_DATE' ] = _PICAL_MB_NEXT_DATE ;
	$ret[ 'MB_LINKTODAY' ] = _PICAL_MB_LINKTODAY ;

	// �������ؤΥ�����
	$ret[ 'PREV_YEAR' ] = date("Y-n-j", mktime(0,0,0,$this->month,$this->date,$this->year-1));
	$ret[ 'NEXT_YEAR' ] = date("Y-n-j", mktime(0,0,0,$this->month,$this->date,$this->year+1));
	$ret[ 'PREV_MONTH' ] = date("Y-n-j", mktime(0,0,0,$this->month-1,$this->date,$this->year));
	$ret[ 'NEXT_MONTH' ] = date("Y-n-j", mktime(0,0,0,$this->month+1,$this->date,$this->year));
	$ret[ 'PREV_WEEK' ] = date( "Y-n-j" , $this->unixtime - 86400*7 ) ;
	$ret[ 'NEXT_WEEK' ] = date( "Y-n-j" , $this->unixtime + 86400*7 ) ;
	$ret[ 'PREV_DATE' ] = date("Y-n-j", $this->unixtime - 86400 ) ;
	$ret[ 'NEXT_DATE' ] = date("Y-n-j", $this->unixtime + 86400 ) ;

	// ���ե��������ѥե������γƥ����ȥ�����
	// ǯ���������ν�����
	if( empty( $_POST[ 'pical_year' ] ) ) $year = $this->year ;
	else  $year = intval( $_POST[ 'pical_year' ] ) ;
	if( empty( $_POST[ 'pical_month' ] ) ) $month = $this->month ;
	else $month = intval( $_POST[ 'pical_month' ] ) ;
	if( empty( $_POST[ 'pical_date' ] ) ) $date = $this->date ;
	else $date = intval( $_POST[ 'pical_date' ] ) ;

	// ǯ��������(2001��2010 �Ȥ���)
	$year_options = "" ;
	for( $y = 2001 ; $y <= 2010 ; $y ++ ) {
		if( $y == $year ) {
			$year_options .= "\t\t\t<option value='$y' selected='selected'>".sprintf(strip_tags(_PICAL_FMT_YEAR),$y)."</option>\n" ;
		} else {
			$year_options .= "\t\t\t<option value='$y'>".sprintf(strip_tags(_PICAL_FMT_YEAR),$y)."</option>\n" ;
		}
	}
	$ret[ 'YEAR_OPTIONS' ] = $year_options ;

	// ����������
	$month_options = "" ;
	for( $m = 1 ; $m <= 12 ; $m ++ ) {
		if( $m == $month ) {
			$month_options .= "\t\t\t<option value='$m' selected='selected'>{$this->month_short_names[$m]}</option>\n" ;
		} else {
			$month_options .= "\t\t\t<option value='$m'>{$this->month_short_names[$m]}</option>\n" ;
		}
	}
	$ret[ 'MONTH_OPTIONS' ] = $month_options ;

	// ����������
	if( $mode == 'W' || $mode == 'D' ) {
		$date_options = "" ;
		for( $d = 1 ; $d <= 31 ; $d ++ ) {
			if( $d == $date ) {
				$date_options .= "\t\t\t<option value='$d' selected='selected'>{$this->date_short_names[$d]}</option>\n" ;
			} else {
				$date_options .= "\t\t\t<option value='$d'>{$this->date_short_names[$d]}</option>\n" ;
			}
		}

		$ret[ 'YMD_SELECTS' ] = sprintf( _PICAL_FMT_YMD , "<select name='pical_year'>{$ret['YEAR_OPTIONS']}</select> &nbsp; " , "<select name='pical_month'>{$ret['MONTH_OPTIONS']}</select> &nbsp; " , "<select name='pical_date'>$date_options</select> &nbsp; " ) ;
		$week_number = floor( ( $this->date - ( $this->day - $this->week_start + 7 ) % 7 + 12 ) / 7 ) ;
		$ret[ 'YMW_TITLE' ] = sprintf( _PICAL_FMT_YMW , $this->year , $this->month_middle_names[ $this->month ] , $this->week_numbers[ $week_number ] ) ;
		$ret[ 'YMD_TITLE' ] = sprintf( _PICAL_FMT_YMD , $this->year , $this->month_middle_names[ $this->month ] , $this->date_long_names[$date] ) ;
	}

	return $ret ;
}



// ���������������Τ��֤��ʣ�����ʬ��
function get_monthly_html( $get_target = '' , $query_string = '' )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	if( $get_target == '' ) $get_target = $PHP_SELF ;

	// �����������������Τ����ν����ʤʤ��Ȥ���������Ū������
	$sunday_th = "
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->sunday_color'><span class='calweekname'>{$this->week_middle_names[0]}</span></font></td>\n" ;
	if( $this->week_start ) {
		$week_top_th = "" ;
		$week_end_th = $sunday_th ;
	} else {
		$week_top_th = $sunday_th ;
		$week_end_th = "" ;
	}

	$ret = "
	 <table border='0' cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse;'>
	 <tr>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='10' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	 </tr>
	 <!-- week names -->
	 <tr>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='10' height='20' /></td>
	   $week_top_th
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->weekday_color'><span class='calweekname'>{$this->week_middle_names[1]}</span></font></td>
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->weekday_color'><span class='calweekname'>{$this->week_middle_names[2]}</span></font></td>
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->weekday_color'><span class='calweekname'>{$this->week_middle_names[3]}</span></font></td>
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->weekday_color'><span class='calweekname'>{$this->week_middle_names[4]}</span></font></td>
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->weekday_color'><span class='calweekname'>{$this->week_middle_names[5]}</span></font></td>
	   <td align='center' style='vertical-align:middle;$this->frame_css'><font size='3' color='$this->saturday_color'><span class='calweekname'>{$this->week_middle_names[6]}</span></font></td>
	   $week_end_th
	 </tr>\n";

	$mtop_unixtime = mktime(0,0,0,$this->month,1,$this->year) ;
	$first_date = getdate( $mtop_unixtime ) ;
	$date = ( - $first_date['wday'] + $this->week_start - 7 ) % 7 ;
	$wday_end = 7 + $this->week_start ;
	$last_date = date( 't' , $this->unixtime ) ;
	$mlast_unixtime = $mtop_unixtime + 86400 * $last_date ;

	// �������׻����Ĥġ�WHERE���δ��֤˴ؤ�����������
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	if( $tzoffset == 0 ) {
		// �������ʤ����� ��mysql�����٤򤫤������ʤ����ᡢ�����Ǿ���ʬ�����Ȥ�)
		$whr_term = "start<='$mlast_unixtime' AND end>'$mtop_unixtime'" ;
	} else {
		// ���������������ϡ�allday�ˤ��äƾ���ʬ��
		$whr_term = "(allday AND start<='$mlast_unixtime' AND end>'$mtop_unixtime') OR ( ! allday AND start<='".( $mlast_unixtime - $tzoffset )."' AND end>'".( $mtop_unixtime - $tzoffset )."')" ;
	}

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// Ĺ�����٥��Ȥ�Unique-ID������4��������Ƥ���
	$rs = mysqli_query( $this->conn ,  "SELECT DISTINCT unique_id FROM $this->table WHERE ($whr_term) AND ($whr_class) AND (allday & 2) LIMIT 4" ) ;
	$long_event_ids = array() ;
	$bit = 1 ;
	while( $event = mysqli_fetch_object( $rs ) ) {
		$long_event_ids[ $bit ] = $event->unique_id ;
		$bit ++ ;
	}

	// ������ʬ�Υ������塼�����ޤȤ��Ƽ������Ƥ���
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday,admission,uid,unique_id FROM $this->table WHERE ($whr_term) AND ($whr_class) ORDER BY start" ) ;
	$numrows_yrs = mysqli_num_rows( $yrs ) ;

	// ����������BODY��ɽ��
	for( $week = 0 ; $week < 6 ; $week ++ ) {

		// ��ɽ���Υ����ǥå���
		if( $date < $last_date ) {
			$ret .= "<tr>\n<td><a href='$get_target?smode=Weekly&amp;caldate=".date('Y-n-j',mktime(0,0,0,$this->month,$date+1,$this->year))."'><img src='$this->images_url/week_index.gif' width='10' height='70' border='0' alt='".$this->week_numbers[$week+1]."' /></a></td>\n" ;
		} else {
			break ;
		}

		for( $wday = $this->week_start ; $wday < $wday_end ; $wday ++ ) {
			$date ++;

			// �оݷ����ϰϳ��ˤ������ν���
			if( ! checkdate($this->month,$date,$this->year) ) {
				if( $date < 28 ) $ret .= "<td bgcolor='#EEEEEE' style='$this->frame_css'><span class='calbody'><img src='$this->images_url/spacer.gif' alt='' width='80' height='70' /></span></td>\n" ;
				else $ret .= "<td><span class='calbody'><img src='$this->images_url/spacer.gif' alt='' width='80' height='70' /></span></td>\n" ;
				continue ;
			}

			$now_unixtime = $mtop_unixtime + ( $date - 1 ) * 86400 ;
			$link = "$this->year-$this->month-$date" ;

			// �������塼���ǡ�����ɽ���롼��
			$waitings = 0 ;
			$event_str = "" ;
			$long_event = 0 ;
			if( $numrows_yrs > 0 ) mysqli_data_seek( $yrs ,  0 ) ;
			while( $event = mysqli_fetch_object( $yrs ) ) {
				if( $event->allday ) {
					// �������٥��ȡʻ����׻��ʤ���
					// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
					if( $event->start >= $now_unixtime + 86400 || $event->end <= $now_unixtime ) continue ;
				} else {
					// �̾磻�٥��ȡʻ����׻�������
					$event->start += $tzoffset ;
					$event->end += $tzoffset ;
					// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
					if( $event->start >= $now_unixtime + $this->day_start + 86400 || $event->end <= $now_unixtime + $this->day_start ) continue ;
				}

				if( $event->admission ) {

					// ���˥�����
					$event->summary = $this->text_sanitizer_for_show( $event->summary ) ;
					// �Ȥꤢ����Ⱦ��33�������¤Ȥ��Ƥ���
					$summary = mb_strcut( $event->summary , 0 , 33 ) ;
					if( $summary != $event->summary ) $summary .= ".." ;
					$event_str_tmp = "<a href='$get_target?smode=Monthly&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' style='font-size:10px;font-weight:normal;text-decoration:none;'>$summary</a>" ;

					$bit = array_search( $event->unique_id , $long_event_ids ) ;
					// ������ !== false �Ȥ��٤��������ɤ���1��4���������ʤ��Τ�
					if( $bit > 0 && $bit <= 4 ) {
						// Ĺ�����٥��������ˤ����г����ӥåȤ�Ω�ơ�legends��������Ͽ
						$long_event |= 1 << ( $bit - 1 ) ;
						$this->long_event_legends[ $bit ] = $event_str_tmp ;
					} else if( $event->allday & 4 ) {
						// ��ǰ���ե饰��Ω�äƤ����顢$holiday_color�ˤ��ơ����־��˻��äƤ���
						$event_str_tmp = str_replace( " style='" , " style='color:$this->holiday_color;" , $event_str_tmp ) ;
						$event_str = "$event_str_tmp<br />\n$event_str" ;
					} else {
						// �ʤ����С����եޥ���������
						$event_str .= $event_str_tmp . "<br />\n" ;
					}
				} else {
					// ̤��ǧ�������塼���Υ������ȥ��å�
					if( $this->isadmin || ( $this->user_id > 0 AND $this->user_id == $event->uid ) ) $waitings ++ ;
				}
			}

			// ̤��ǧ�������塼������������ɽ��
			if( $waitings > 0 ) $event_str .= "<span style='color:#00FF00;font-size:10px;font-weight:normal;'>".sprintf( _PICAL_NTC_NUMBEROFNEEDADMIT , $waitings )."</span><br />\n" ;

			// ���������פˤ������迧����ʬ��
			if( isset( $this->holidays[$link] ) ) {
				//	Holiday
				$bgcolor = $this->holiday_bgcolor ;
				$color = $this->holiday_color ;
			} elseif( $wday % 7 == 0 ) {
				//	Sunday
				$bgcolor = $this->sunday_bgcolor ;
				$color = $this->sunday_color ;
			} elseif( $wday == 6 ) {
				//	Saturday
				$bgcolor = $this->saturday_bgcolor ;
				$color = $this->saturday_color ;
			} else {
				// Weekday
				$bgcolor = $this->weekday_bgcolor ;
				$color = $this->weekday_color ;
			}

			// ���������طʿ��ϥ��饤�Ƚ���
			if( $date == $this->date ) $bgcolor = $this->targetday_bgcolor ;

			// Ĺ�����٥��Ȥ��������طʡ�
			if( $long_event ) {
				$background = "background:url($this->images_url/monthbar_0".dechex($long_event).".gif) top repeat-x $bgcolor" ;
			} else $background = '' ;

			// ͽ�����ɲáʱ�ɮ����������
			if( $this->insertable ) $insert_link = "<a href='$get_target?smode=Monthly&amp;action=Edit&amp;caldate=$link'><img src='$this->images_url/addevent.gif' border='0' width='14' height='12' alt='"._PICAL_MB_ADDEVENT."' /></a>" ;
			else $insert_link = "<a href='$get_target?smode=Monthly&amp;caldate=$link'><img src='$this->images_url/spacer.gif' alt='' border='0' width='14' height='12' /></a>" ;

			$ret .= "<td valign='top' bgcolor='$bgcolor' style='$this->frame_css;$background'><table width='100%' cellspacing='0' cellpadding='0'><tr><td align='left'><a href='$get_target?smode=Daily&amp;caldate=$link' class='calbody'><font size='3' color='$color'><b><span class='calbody'>$date</span></b></font></a></td><td align='right'><a href='$get_target?smode=Monthly&amp;caldate=$link'><img src='$this->images_url/spacer.gif' alt='' border='0' width='32' height='12' /></a> $insert_link</td></tr></table>$event_str</td>\n" ;

		}
		$ret .= "</tr>\n";
	}

	$ret .= "</table>\n";

	return $ret ;
}



// ���������������Τ��֤��ʣ�����ʬ��
function get_weekly_html( )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;

	$ret = "
	 <table border='0' cellspacing='0' cellpadding='0' width='100%' style='border-collapse:collapse;'>
	 <tr>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='10' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='80' height='10' /></td>
	 </tr>\n" ;

	$wtop_unixtime = $this->unixtime - ( ( $this->day - $this->week_start + 7 ) % 7 ) * 86400 ;
	$wlast_unixtime = $wtop_unixtime + 86400 * 7 + $this->day_start ;

	// �������׻����Ĥġ�WHERE���δ��֤˴ؤ�����������
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	if( $tzoffset == 0 ) {
		// �������ʤ����� ��mysql�����٤򤫤������ʤ����ᡢ�����Ǿ���ʬ�����Ȥ�)
		$whr_term = "start<='$wlast_unixtime' AND end>'$wtop_unixtime'" ;
	} else {
		// ���������������ϡ�allday�ˤ��äƾ���ʬ��
		$whr_term = "(allday AND start<='$wlast_unixtime' AND end>'$wtop_unixtime') OR ( ! allday AND start<='".( $wlast_unixtime - $tzoffset )."' AND end>'".( $wtop_unixtime - $tzoffset )."')" ;
	}

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// �콵��ʬ�Υ������塼�����ޤȤ��Ƽ������Ƥ���
	$ars = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday,admission,uid FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start" ) ;
	$numrows_ars = mysqli_num_rows( $ars ) ;
	$wrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday,admission,uid FROM $this->table WHERE admission=0 AND ($whr_term) AND ($whr_class) ORDER BY start" ) ;
	$numrows_wrs = mysqli_num_rows( $wrs ) ;

	// ����������BODY��ɽ��
	$now_unixtime = $wtop_unixtime ;
	$wday_end = 7 + $this->week_start ;
	for( $wday = $this->week_start ; $wday < $wday_end ; $wday ++ , $now_unixtime += 86400 ) {

		$link = date( "Y-n-j" , $now_unixtime ) ;
		$disp = sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $now_unixtime ) ] , $this->date_long_names[ date( 'j' , $now_unixtime ) ] ) ;
		$disp .= "<br />({$this->week_middle_names[$wday]})" ;
		// $disp = date( _PICAL_DTFMT_MD , $now_unixtime ) . "<br />({$this->week_middle_names[$wday]})" ;
		$date_part_append = '' ;
		// �������塼��ɽ�����Υơ��֥볫��
		$event_str = "
				<table cellpadding='0' cellspacing='2'>
				  <tr>
				    <td><img src='$this->images_url/spacer.gif' alt='' border='0' width='120' height='4' /></td>
				    <td><img src='$this->images_url/spacer.gif' alt='' border='0' width='360' height='4' /></td>
				  </tr>
		\n" ;
/*
					} else if( $event->allday & 4 ) {
						// ��ǰ���ե饰��Ω�äƤ����顢$holiday_color�ˤ��ơ����־��˻��äƤ���
						$event_str_tmp = str_replace( " style='" , " style='color:$this->holiday_color;" , $event_str_tmp ) ;
						$event_str = "$event_str_tmp<br />\n$event_str" ;
*/


		// ��ǧ�Ѥߥ������塼���ǡ�����ɽ���롼��
		if( $numrows_ars > 0 ) mysqli_data_seek( $ars ,  0 ) ;
		while( $event = mysqli_fetch_object( $ars ) ) {

			if( $event->allday ) {
				// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
				if( $event->start >= $now_unixtime + 86400 || $event->end <= $now_unixtime ) continue ;
			} else {
				// �����׻�����
				$event->start += $tzoffset ;
				$event->end += $tzoffset ;
				// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
				if( $event->start >= $now_unixtime + $this->day_start + 86400 || $event->end <= $now_unixtime + $this->day_start ) continue ;
			}

			// ���˥�����
			$summary = $this->text_sanitizer_for_show( $event->summary ) ;

			if( $event->allday ) {
				if( $event->allday & 4 ) {
					// ��ǰ���ե饰��Ω�äƤ�������
					$date_part_append .= "<br /><font size='2'><a href='$PHP_SELF?smode=Weekly&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='cal_summary_specialday'><font color='$this->holiday_color'>$summary</font></a></font>\n" ;
					continue ;
				} else {
					// �̾����������٥���
					$time_part = "             <img border='0' src='$this->images_url/dot_allday.gif' />" ;
					$summary_class = "calsummary_allday" ;
				}
			} else {
				// �̾磻�٥���
				$time_part = $this->get_todays_time_description( $event->start , $event->end , $link , true , true ) ;
				$summary_class = "calsummary" ;
			}

			$event_str .= "
				  <tr>
				    <td valign='top' align='center'>
				      <pre style='margin:0px;'><font size='2'>$time_part</font></pre>
				    </td>
				    <td valign='top'>
				      <font size='2'><a href='$PHP_SELF?smode=Weekly&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='$summary_class'>$summary</a></font>
				    </td>
				  </tr>
			\n" ;
		}

		// ̤��ǧ�������塼����ɽ���롼�ס�uid�����פ��륲���Ȱʳ��Υ쥳���ɤΤߡ�
		if( $this->isadmin || $this->user_id > 0 ) {

			if( $numrows_wrs > 0 ) mysqli_data_seek( $wrs ,  0 ) ;
			while( $event = mysqli_fetch_object( $wrs ) ) {

				if( $event->allday ) {
					// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
					if( $event->start >= $now_unixtime + 86400 || $event->end <= $now_unixtime ) continue ;
				} else {
					// �����׻�����
					$event->start += $tzoffset ;
					$event->end += $tzoffset ;
					// �оݥ��٥��Ȥ��������ˤ����äƤ��뤫�ɤ����Υ����å�
					if( $event->start >= $now_unixtime + $this->day_start + 86400 || $event->end <= $now_unixtime + $this->day_start ) continue ;
				}

				// ���˥�����
				$summary = $this->text_sanitizer_for_show( $event->summary ) ;

				if( $event->allday ) {
					// �������٥��ȡ������ե饰���Ĥ��Ƥ��Ƥ⡢�̾ﰷ����
					$time_part = "             <img border='0' src='$this->images_url/dot_notadmit.gif' />" ;
					$summary_class = "calsummary_allday" ;
				} else {
					// �̾磻�٥���
					$event->start += $tzoffset ;
					$event->end += $tzoffset ;
					$time_part = $this->get_todays_time_description( $event->start , $event->end , $link , true , false ) ;
					$summary_class = "calsummary" ;
				}

				$event_str .= "
					  <tr>
					    <td valign='top' align='center'>
					      <pre style='margin:0px;'><font size='2'>$time_part</font></pre>
					    </td>
					    <td valign='top'>
					      <font size='2'><a href='$PHP_SELF?smode=Weekly&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='$summary_class'><font color='#00FF00'>$summary ("._PICAL_MB_EVENT_NEEDADMIT.")</font></a></font>
					    </td>
					  </tr>
				\n" ;
			}
		}

		// ͽ�����ɲáʱ�ɮ����������
		if( $this->insertable ) $event_str .= "
				  <tr>
				    <td valign='bottom' colspan='2'>
				      &nbsp; <font size='2'><a href='$PHP_SELF?smode=Daily&amp;action=Edit&amp;caldate=$link'><img src='$this->images_url/addevent.gif' border='0' width='14' height='12' />"._PICAL_MB_ADDEVENT."</a></font>
				    </td>
				  </tr>
		\n" ;

		// �������塼��ɽ�����Υơ��֥뽪λ
		$event_str .= "\t\t\t\t</table>\n" ;

		// ���������פˤ������迧����ʬ��
		if( isset( $this->holidays[ $link ] ) ) {
			//	Holiday
			$bgcolor = $this->holiday_bgcolor ;
			$color = $this->holiday_color ;
		} elseif( $wday % 7 == 0 ) {
			//	Sunday
			$bgcolor = $this->sunday_bgcolor ;
			$color = $this->sunday_color ;
		} elseif( $wday == 6 ) {
			//	Saturday
			$bgcolor = $this->saturday_bgcolor ;
			$color = $this->saturday_color ;
		} else {
			// Weekday
			$bgcolor = $this->weekday_bgcolor ;
			$color = $this->weekday_color ;
		}

		// ���������طʿ��ϥ��饤�Ƚ���
		if( $link == $this->caldate ) $body_bgcolor = $this->targetday_bgcolor ;
		else $body_bgcolor = $bgcolor ;

		$ret .= "
	 <tr>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='10' height='80' /></td>
	   <td bgcolor='$bgcolor' align='center' valign='middle' style='vertical-align:middle;text-align:center;$this->frame_css'>
	     <a href='$PHP_SELF?smode=Daily&amp;caldate=$link' class='calbody'><font size='3' color='$color'><b><span class='calbody'>$disp</span></b></font></a><br />
	     $date_part_append
	   </td>
	   <td valign='top' colspan='6' bgcolor='$body_bgcolor' style='$this->frame_css'>
	     $event_str
	   </td>
	 </tr>\n" ;
	}

	$ret .= "\t </table>\n";

	return $ret ;
}



// ���������������Τ��֤��ʣ���ʬ��
function get_daily_html( )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;

	list( $bgcolor , $color ) =  $this->daytype_to_colors( $this->daytype ) ;

	$ret = "
	<table border='0' cellspacing='0' cellpadding='0' width='570'>
	 <tr>
	 <td width='570' class='calframe'>
	 <table border='0' cellspacing='0' cellpadding='0' width='100%'>
	 <tr>
	   <td colspan='8'><img src='$this->images_url/spacer.gif' alt='' width='570' height='10' /></td>
	 </tr>
	 <tr>
	   <td><img src='$this->images_url/spacer.gif' alt='' width='10' height='350' /></td>
	   <td colospan='7' valign='top' bgcolor='$bgcolor' style='$this->frame_css'>
	     <table border='0' cellpadding='0' cellspacing='0'>
	       <tr>
	         <td><img src='$this->images_url/spacer.gif' alt='' width='120' height='10' /></td>
	         <td><img src='$this->images_url/spacer.gif' alt='' width='440' height='10' /></td>
	       </tr>
	\n" ;

	// �������׻����Ĥġ�WHERE���δ��֤˴ؤ�����������
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	$toptime_of_day = $this->unixtime + $this->day_start ;
	if( $tzoffset == 0 ) {
		// �������ʤ����� ��mysql�����٤򤫤������ʤ����ᡢ�����Ǿ���ʬ�����Ȥ�)
		$whr_term = "start<'".( $toptime_of_day + 86400 ) ."' AND end>'$toptime_of_day'" ;
	} else {
		// ���������������ϡ�allday�ˤ��äƾ���ʬ��
		$whr_term = "(allday AND start<='$this->unixtime' AND end>'$this->unixtime') OR ( ! allday AND start<='".( $toptime_of_day + 86400 - $tzoffset )."' AND end>'".( $toptime_of_day - $tzoffset )."')" ;
	}

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// �����Υ������塼��������ɽ��
	$yrs = mysqli_query( $this->conn ,  "SELECT start,end,summary,id,allday,admission,uid,description FROM $this->table WHERE admission>0 AND ($whr_term) AND ($whr_class) ORDER BY start,end" ) ;
	$num_rows = mysqli_num_rows( $yrs ) ;

	if( $num_rows == 0 ) $ret .= "<tr><td></td><td>"._PICAL_MB_NOEVENT."</td></tr>\n" ;
	else while( $event = mysqli_fetch_object( $yrs ) ) {

		if( $event->allday ) {
			// �������٥��ȡʻ����׻��ʤ���
			$time_part = "             <img border='0' src='$this->images_url/dot_allday.gif' />" ;
		} else {
			// �̾磻�٥��ȡʻ����׻�������
			$event->start += $tzoffset ;
			$event->end += $tzoffset ;
			$time_part = $this->get_todays_time_description( $event->start , $event->end , $this->caldate , true , true ) ;
		}

		// ���˥�����
		$description = $this->textarea_sanitizer_for_show( $event->description ) ;
		$summary = $this->text_sanitizer_for_show( $event->summary ) ;

		$summary_class = $event->allday ? "calsummary_allday" : "calsummary" ;

		$ret .= "
	       <tr>
	         <td valign='top' align='center'>
	           <pre style='margin:0px;'><font size='3'>$time_part</font></pre>
	         </td>
	         <td vlalign='top'>
	           <font size='3'><a href='$PHP_SELF?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='$summary_class'>$summary</a></font><br />
	           <font size='2'>$description</font><br />
	           &nbsp;
	         </td>
	       </tr>\n" ;
	}

	// ̤��ǧ�������塼��������ɽ����uid�����פ��륲���Ȱʳ��Υ쥳���ɤΤߡ�
	if( $this->isadmin || $this->user_id > 0 ) {
	  $whr_uid = $this->isadmin ? "1" : "uid=$this->user_id " ;
	  $yrs = mysqli_query( $this->conn ,  "SELECT * FROM $this->table WHERE admission=0 AND $whr_uid AND ($whr_term) AND ($whr_class) ORDER BY start,end" ) ;

	  while( $event = mysqli_fetch_object( $yrs ) ) {

		if( $event->allday ) {
			// �������٥��ȡʻ����׻��ʤ���
			$time_part = "             <img border='0' src='$this->images_url/dot_notadmit.gif' />" ;
		} else {
			// �̾磻�٥��ȡʻ����׻�������
			$event->start += $tzoffset ;
			$event->end += $tzoffset ;
			$time_part = $this->get_todays_time_description( $event->start , $event->end , $this->caldate , true , false ) ;
		}

		// ���˥�����
		$summary = $this->text_sanitizer_for_show( $event->summary ) ;

		$summary_class = $event->allday ? "calsummary_allday" : "calsummary" ;

		$ret .= "
	       <tr>
	         <td valign='top' align='center'>
	           <pre style='margin:0px;'><font size='3'>$time_part</font></pre>
	         </td>
	         <td vlalign='top'>
	           <font size='3'><a href='$PHP_SELF?smode=Daily&amp;action=View&amp;event_id=$event->id&amp;caldate=$this->caldate' class='$summary_class'><font color='#00FF00'>$summary ("._PICAL_MB_EVENT_NEEDADMIT.")</font></a></font>
	         </td>
	       </tr>\n" ;
	  }
	}

	// ͽ�����ɲáʱ�ɮ����������
	if( $this->insertable ) $ret .= "
	       <tr>
	         <td valign='bottom' colspan='2'>
	           &nbsp; <font size='2'><a href='$PHP_SELF?smode=Daily&amp;action=Edit&amp;caldate=$this->caldate'><img src='$this->images_url/addevent.gif' border='0' width='14' height='12' />"._PICAL_MB_ADDEVENT."</a></font>
	         </td>
	       </tr>\n" ;

	$ret .= "
	     </table>
	   </td>
	 </tr>
	 </table>
	 </td>
	 </tr>
	</table>\n" ;

	return $ret ;
}



/*******************************************************************/
/*        �ᥤ���� �ʸ��̥ǡ���������                              */
/*******************************************************************/

// �������塼���ܺٲ���ɽ����ʸ�������֤�
function get_schedule_view_html()
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	$HTTP_HOST = $_SERVER[ 'HTTP_HOST' ] ;
	if( isset( $_GET[ 'smode' ] ) ) $smode = $_GET[ 'smode' ] ;
	else $smode = 'Monthly' ;
	$editable = $this->editable ;
	$deletable = $this->deletable ;

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// ͽ���ǡ����μ���
	$this->original_id = $event_id = $_GET[ 'event_id' ] ;
	$yrs = mysqli_query( $this->conn ,  "SELECT * FROM $this->table WHERE id='$event_id' AND ($whr_class)" ) ;
	if( mysqli_num_rows( $yrs ) < 1 ) die( _PICAL_ERR_INVALID_EVENT_ID ) ;
	$event = mysqli_fetch_object( $yrs ) ;

	// rrule�ˤ��ä�Ÿ�����줿�ǡ����Ǥ����С�����(��)�Υǡ���������
	if( trim( $event->rrule ) != '' ) {
		if( $event->rrule_pid != $event->id ) {
			$event->id = $event->rrule_pid ;
			$yrs = mysqli_query( $this->conn ,  "SELECT id,start FROM $this->table WHERE id='$event->rrule_pid' AND ($whr_class)" ) ;
			if( mysqli_num_rows( $yrs ) >= 1 ) {
				$event->id = $event->rrule_pid ;
				$parent_event = mysqli_fetch_object( $yrs ) ;
				$this->original_id = $parent_event->id ;
			} else {
				$parent_event =& $event ;
			}
		}
		$rrule = $this->rrule_to_human_language( $event->rrule ) ;
	} else {
		$rrule = '' ;
	}

	// ���Ȥ����Խ���ǽ�������Ǥ⡢��������uid�ȥ쥳���ɤ�uid��
	// ���פ��������ġ�Admin�⡼�ɤǤʤ����ϡ��Խ��������ԲĤȤ���
	if( $event->uid != $this->user_id && ! $this->isadmin ) {
		$editable = false ;
		$deletable = false ;
	}

	// ̤��ǧ�쥳���ɤϡ�$editable�Ǥʤ����С�ɽ�����ʤ�
	if( ! $event->admission && ! $editable ) die( _PICAL_ERR_NOPERM_TO_SHOW ) ;

	// �Խ��������ܥ���
	if( $editable ) {
		$edit_button = "
			<form method='get' action='$PHP_SELF' style='margin:0px;'>
				<input type='hidden' name='smode' value='$smode'>
				<input type='hidden' name='action' value='Edit'>
				<input type='hidden' name='event_id' value='$event->id'>
				<input type='hidden' name='caldate' value='$this->caldate'>
				<input type='submit' value='"._PICAL_BTN_EDITEVENT."'>
			</form>\n" ;
	} else if( $deletable ) {
		$edit_button = "
			<form method='post' action='$PHP_SELF' name='MainForm' style='margin:0px;'>
				<input type='hidden' name='smode' value='$smode'>
				<input type='hidden' name='last_smode' value='$smode'>
				<input type='hidden' name='event_id' value='$event->id'>
				<input type='hidden' name='caldate' value='$this->caldate'>
				<input type='hidden' name='last_caldate' value='$this->caldate'>
				<input type='submit' name='delete' value='"._PICAL_BTN_DELETE."' onclick='return confirm(\""._PICAL_CNFM_DELETE_YN."\")'>
			</form>\n" ;
	} else $edit_button = "" ;

	// iCalendar ���ϥܥ���
	if( $this->can_output_ics ) {
		$ics_output_button = "
			<table border='0' cellspacing='0' cellpadding='0'>
			<tr>
			<td>
			<form method='post' action='$PHP_SELF?fmt=single&amp;event_id=$event->id&amp;smode=$smode&amp;caldate=$this->caldate' target='_blank' style='margin:0px;'>
				<input type='submit' name='output_ics' value='"._PICAL_BTN_OUTPUTICS_WIN."'>
			</form>
			</td>
			<td>
			<form method='post' action='webcal://$HTTP_HOST$PHP_SELF?fmt=single&amp;event_id=$event->id&amp;smode=$smode&amp;caldate=$this->caldate' target='_blank' style='margin:0px;'>
				<input type='submit' name='output_ics' value='"._PICAL_BTN_OUTPUTICS_MAC."'>
			</form>
			</td>
			<td width='100%'></td>
			</tr>
			</table>\n" ;
	} else $ics_output_button = "" ;

	// ���ա�����ɽ���ν���
	if( $event->allday ) {
		// �������٥��ȡʻ����׻��ʤ���
		$event->end -= 300 ;
		$start_time_str = "("._PICAL_MB_ALLDAY_EVENT.")" ;
		$end_time_str = "" ;
	} else {
		// �̾磻�٥��ȡʻ����׻�������
		$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
		$event->start += $tzoffset ;
		$event->end += $tzoffset ;
		$disp_tz = ( $this->user_TZ >= 0 ? "+" : "-" ) . sprintf( "%02d:%02d" , abs( $this->user_TZ ) , abs( $this->user_TZ ) * 60 % 60 ) ;
		$start_time_str = date( _PICAL_DTFMT_TIME , $event->start ) . " &nbsp; $disp_tz" ;
		$end_time_str = date( _PICAL_DTFMT_TIME , $event->end ) . " &nbsp; $disp_tz" ;
	}

	$orig_locale = setlocale( LC_TIME , "0" ) ;
	setlocale( LC_TIME , _PICAL_LOCALE ) ;
	$start_date_str = strftime( _PICAL_STRFFMT_DATE , $event->start ) ;
	$end_date_str = strftime( _PICAL_STRFFMT_DATE , $event->end ) ;
	setlocale( LC_TIME , $orig_locale ) ;

	$start_datetime_str = "$start_date_str &nbsp; $start_time_str" ;
	$end_datetime_str = "$end_date_str &nbsp; $end_time_str" ;

	// �����֤��ǡ����ġ�����(��)�Ǥʤ��ǡ����ϡ��ƤؤΥ��󥯤�����
	if( trim( $event->rrule ) != '' ) {
		if( isset( $parent_event ) && $parent_event != $event ) {
			if( ! $event->allday ) $parent_event->start += $tzoffset ;
			$rrule .= "<br /><a href='$PHP_SELF?action=View&amp;event_id=$parent_event->id' target='_blank'>"._PICAL_MB_LINK_TO_RRULE1ST. date( 'Y-m-d' , $parent_event->start ) . "</a>" ;
		} else {
			$rrule .= '<br /> '._PICAL_MB_RRULE1ST ;
		}
	}

	// ���ƼԤ�ɽ��
	$submitter_info = $this->get_submitter_info( $event->uid ) ;

	// �����������������Ӥ����оݤ�������
	if( $event->class == 'PRIVATE' ) {
		$groupid = intval( $event->groupid ) ;
		if( $groupid == 0 ) $group = _PICAL_OPT_PRIVATEMYSELF ;
		else if( isset( $this->groups[ $groupid ] ) ) $group = sprintf( _PICAL_OPT_PRIVATEGROUP , $this->groups[ $groupid ] ) ;
		else $group = _PICAL_OPT_PRIVATEINVALID ;
		$class_status = _PICAL_MB_PRIVATE . sprintf( _PICAL_MB_PRIVATETARGET , $group ) ;
	} else {
		$class_status = _PICAL_MB_PUBLIC ;
	}

	// ����¾��ɽ����������
	$admission_status = $event->admission ? _PICAL_MB_EVENT_ADMITTED : _PICAL_MB_EVENT_NEEDADMIT ;
	$description = $this->textarea_sanitizer_for_show( $event->description ) ;
	$summary = $this->text_sanitizer_for_show( $event->summary ) ;
	$location = $this->text_sanitizer_for_show( $event->location ) ;
	$contact = $this->text_sanitizer_for_show( $event->contact ) ;

	// ɽ����
	$ret = "
<h2>"._PICAL_MB_TITLE_EVENTINFO." <small>-"._PICAL_MB_SUBTITLE_EVENTDETAIL."-</small></h2>
	<table border='0' cellpadding='0' cellspacing='2'>
	<tr>
		<td class='head'>"._PICAL_TH_SUMMARY."</td>
		<td class='even'>$summary</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_STARTDATETIME."</td>
		<td class='even'>$start_datetime_str</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_ENDDATETIME."</td>
		<td class='even'>$end_datetime_str</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_LOCATION."</td>
		<td class='even'>$location</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_CONTACT."</td>
		<td class='even'>$contact</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_DESCRIPTION."</td>
		<td class='even'>$description</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_SUBMITTER."</td>
		<td class='even'>$submitter_info</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_CLASS."</td>
		<td class='even'>$class_status</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_RRULE."</td>
		<td class='even'>$rrule</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_ADMISSIONSTATUS."</td>
		<td class='even'>$admission_status</td>
	</tr>
	<tr>
		<td align='right'>
			$edit_button
		</td>
		<td align='left'>
			$ics_output_button
		</td>
	</tr>
	<tr>
		<td><img src='$this->images_url/spacer.gif' alt='' width='150' height='4' /></td>		<td width='100%'></td>
	</tr>
	<!-- Copyright of piCal (Don't remove this) -->
	<tr>
		<td width='100%' align='right' colspan='2'>".PICAL_COPYRIGHT."</td>
	</tr>
	</table>\n" ;

	return $ret ;
}



// �������塼���Խ��Ѳ���ɽ����ʸ�������֤�
function get_schedule_edit_html( )
{
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	$editable = $this->editable ;
	$deletable = $this->deletable ;
	$smode = isset( $_GET['smode'] ) ? $_GET['smode'] : '' ;

	// Timezone �ν���
	$tzoffset = ( $this->user_TZ - $this->server_TZ ) * 3600 ;
	$disp_tz = ( $this->user_TZ >= 0 ? "+" : "-" ) . sprintf( "%02d:%02d" , abs( $this->user_TZ ) , abs( $this->user_TZ ) * 60 % 60 ) ;

	// �ѹ��ξ��硢��Ͽ�ѥ������塼����������
	if( isset( $_GET[ 'event_id' ] ) ) {

		$event_id = $_GET[ 'event_id' ] ;
		$yrs = mysqli_query( $this->conn ,  "SELECT * FROM $this->table WHERE id='$event_id'" ) ;
		if( mysqli_num_rows( $yrs ) < 1 ) die( _PICAL_ERR_INVALID_EVENT_ID ) ;
		$event = mysqli_fetch_object( $yrs ) ;

		// ���Ȥ����Խ���������ǽ�������Ǥ⡢��������uid�ȥ쥳���ɤ�uid��
		// ���פ��������ġ�Admin�⡼�ɤǤʤ����ϡ��Խ��������ԲĤȤ���
		if( $event->uid != $this->user_id && ! $this->isadmin ) {
			$editable = false ;
			$deletable = false ;
		}

		$description = $this->textarea_sanitizer_for_edit( $event->description ) ;
		$summary = $this->text_sanitizer_for_edit( $event->summary ) ;
		$location = $this->text_sanitizer_for_edit( $event->location ) ;
		$contact = $this->text_sanitizer_for_edit( $event->contact ) ;
		if( $event->class == 'PRIVATE' ) {
			$class_private = 'checked' ;
			$class_public = '' ;
			$select_private_disabled = '' ;
		} else {
			$class_private = '' ;
			$class_public = 'checked' ;
			$select_private_disabled = 'disabled' ;
		}
		$groupid = $event->groupid ;
		$rrule = $event->rrule ;
		$admission_status = $event->admission ? _PICAL_MB_EVENT_ADMITTED : _PICAL_MB_EVENT_NEEDADMIT ;
		$update_button = $editable ? "<input name='update' type='submit' value='"._PICAL_BTN_SUBMITCHANGES."'>" : "" ;
		$insert_button = "<input name='saveas' type='submit' value='"._PICAL_BTN_SAVEAS."' onclick='return confirm(\""._PICAL_CNFM_SAVEAS_YN."\")'>" ;
		$delete_button = $deletable ? "<input name='delete' type='submit' value='"._PICAL_BTN_DELETE."' onclick='return confirm(\""._PICAL_CNFM_DELETE_YN."\")'>" : "" ;

		// ���ա�����ɽ���ν���
		if( $event->allday ) {
			// �������٥��ȡʻ����׻��ʤ���
			$allday_checkbox = "checked" ;
			$allday_select = "disabled" ;
			$allday_bit1 = ( $event->allday & 2 ) ? "checked" : "" ;
			$allday_bit2 = ( $event->allday & 4 ) ? "checked" : "" ;
			$allday_bit3 = ( $event->allday & 8 ) ? "checked" : "" ;
			$allday_bit4 = ( $event->allday & 16 ) ? "checked" : "" ;
			$start_date = date( "Y-m-d" , $event->start ) ;
			$start_hour = 0 ;
			$start_min = 0 ;
			$end_date = date( "Y-m-d" , $event->end - 300 ) ;
			$end_hour = 23 ;
			$end_min = 55 ;
		} else {
			// �̾磻�٥��ȡʻ����׻�������
			$event->start += $tzoffset ;
			$event->end += $tzoffset ;
			$allday_checkbox = "" ;
			$allday_select = "" ;
			$allday_bit1 = $allday_bit2 = $allday_bit3 = $allday_bit4 = "" ;
			$start_date = date( "Y-m-d" , $event->start ) ;
			$start_hour = date( "H" , $event->start ) ;
			$start_min = date( "i" , $event->start ) ;
			$end_date = date( "Y-m-d" , $event->end ) ;
			$end_hour = date( "H" , $event->end ) ;
			$end_min = date( "i" , $event->end ) ;
		}

	// ������Ͽ�ξ���
	} else {
		$event_id = 0 ;

		$editable = true ;
		$summary = '' ;
		$location = '' ;
		$contact = '' ;
		$class_private = '' ;
		$class_public = 'checked' ;
		$select_private_disabled = 'disabled' ;
		$groupid = 0 ;
		$rrule = '' ;
		$description = '' ;
		$start_date = $this->caldate ;
		$end_date = $this->caldate ;
		$start_hour = 9 ;
		$start_min = 0 ;
		$end_hour = 17 ;
		$end_min = 0 ;
		$admission_status = _PICAL_MB_EVENT_NOTREGISTER ;
		$update_button = '' ;
		$insert_button = "<input name='insert' type='submit' value='"._PICAL_BTN_NEWINSERTED."'>" ;
		$delete_button = '' ;
		$allday_checkbox = $allday_select = "" ;
		$allday_bit1 = $allday_bit2 = $allday_bit3 = $allday_bit4 = "" ;
	}

	// ������
	$textbox_start_date = $this->get_formtextdateselect( 'StartDate' , $start_date ) ;

	// ���ϻ���
	$select_start_hour = "<select name='StartHour' $allday_select>\n" ;
	for( $h = 0 ; $h < 24 ; $h ++ ) {
		if( $h == $start_hour ) $select_start_hour .= "<option value='$h' selected='selected'>$h</option>\n" ;
		else $select_start_hour .= "<option value='$h'>$h</option>\n" ;
	}
	$select_start_hour .= "</select>" ;

	// ����ʬ
	$select_start_min = "<select name='StartMin' $allday_select>\n" ;
	for( $m = 0 ; $m < 60 ; $m += 5 ) {
		if( $m == $start_min ) $select_start_min .= "<option value='$m' selected='selected'>" . sprintf( "%02d" , $m ) . "</option>\n" ;
		else $select_start_min .= "<option value='$m'>" . sprintf( "%02d" , $m ) . "</option>\n" ;
	}
	$select_start_min .= "</select>" ;

	// ��λ��
	$textbox_end_date = $this->get_formtextdateselect( 'EndDate' , $end_date ) ;

	// ��λ����
	$select_end_hour = "<select name='EndHour' $allday_select>\n" ;
	for( $h = 0 ; $h < 24 ; $h ++ ) {
		if( $h == $end_hour ) $select_end_hour .= "<option value='$h' selected='selected'>$h</option>\n" ;
		else $select_end_hour .= "<option value='$h'>$h</option>\n" ;
	}
	$select_end_hour .= "</select>" ;

	// ��λʬ
	$select_end_min = "<select name='EndMin' $allday_select>\n" ;
	for( $m = 0 ; $m < 60 ; $m += 5 ) {
		if( $m == $end_min ) $select_end_min .= "<option value='$m' selected='selected'>" . sprintf( "%02d" , $m ) . "</option>\n" ;
		else $select_end_min .= "<option value='$m'>" . sprintf( "%02d" , $m ) . "</option>\n" ;
	}
	$select_end_min .= "</select>" ;

	// class PRIVATE�����о�����
	$select_private = "<select name='groupid' $select_private_disabled>\n<option value='0'>"._PICAL_OPT_PRIVATEMYSELF."</option>\n" ;
	foreach( $this->groups as $sys_gid => $gname ) {
		$option_desc = sprintf( _PICAL_OPT_PRIVATEGROUP , $gname ) ;
		if( $sys_gid == $groupid ) $select_private .= "<option value='$sys_gid' selected='selected'>$option_desc</option>\n" ;
		else $select_private .= "<option value='$sys_gid'>$option_desc</option>\n" ;
	}
	$select_private .= "</select>" ;

	$select_end_min = "<select name='EndMin' $allday_select>\n" ;
	for( $m = 0 ; $m < 60 ; $m += 5 ) {
		if( $m == $end_min ) $select_end_min .= "<option value='$m' selected='selected'>" . sprintf( "%02d" , $m ) . "</option>\n" ;
		else $select_end_min .= "<option value='$m'>" . sprintf( "%02d" , $m ) . "</option>" ;
	}
	$select_end_min .= "</select>" ;

	// DESCRIPTION �ν���ʬ��
	if( function_exists( 'xoopsCodeTarea' ) ) {
		// XOOPS��Textarea���ϥܥå��������ؿ��������Ф��������Ȥ�
		//Ryuji_edit(2003-05-04)
		ob_start();
		$GLOBALS["description_text"] = $description;
		xoopsCodeTarea("description_text",50,6);
		$description_textarea = ob_get_contents();
		// $xoops_codes_tarea = ob_get_contents();
		ob_end_clean();
		//Ryuji_edit End
	} else {
		// XOOPS�ʳ��ǻȤ����������ϡ�ñ�ʤ�textarea������
		$description_textarea = "<textarea name='description' cols='50' rows='6' wrap='soft'>$description</textarea>" ;
	}

	// ɽ����
	$ret = "
<h2>"._PICAL_MB_TITLE_EVENTINFO." <small>-"._PICAL_MB_SUBTITLE_EVENTEDIT."-</small></h2>
<form action='$PHP_SELF' method='post' name='MainForm'>
	<input type='hidden' name='caldate' value='$this->caldate'>
	<input type='hidden' name='event_id' value='$event_id'>
	<input type='hidden' name='last_smode' value='$smode'>
	<input type='hidden' name='last_caldate' value='$this->caldate'>
	<table border='0' cellpadding='0' cellspacing='2'>
	<tr>
		<td class='head'>"._PICAL_TH_SUMMARY."</td>
		<td class='even'><input type='text' name='summary' size='60' maxlength='250' value='$summary'></td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_STARTDATETIME."</td>
		<td class='even'>
			$textbox_start_date &nbsp;
			{$select_start_hour}"._PICAL_MB_HOUR_SUF." {$select_start_min}"._PICAL_MB_MINUTE_SUF." <small> &nbsp; (GMT$disp_tz)</small>
		</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_ENDDATETIME."</td>
		<td class='even'>
			$textbox_end_date &nbsp;
			{$select_end_hour}"._PICAL_MB_HOUR_SUF." {$select_end_min}"._PICAL_MB_MINUTE_SUF." <small> &nbsp; (GMT$disp_tz)</small>
		</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_ALLDAYOPTIONS."</td>
		<td class='even'>
			<fieldset>
				<legend class='blockTitle'><input type='checkbox' name='allday' value='1' $allday_checkbox onClick='document.MainForm.StartHour.disabled=document.MainForm.StartMin.disabled=document.MainForm.EndHour.disabled=document.MainForm.EndMin.disabled=this.checked'>"._PICAL_MB_ALLDAY_EVENT."</legend>
				<input type='checkbox' name='allday_bits[]' value='1' {$allday_bit1}>"._PICAL_MB_LONG_EVENT." &nbsp;  <input type='checkbox' name='allday_bits[]' value='2' {$allday_bit2}>"._PICAL_MB_LONG_SPECIALDAY." &nbsp;  <!-- <input type='checkbox' name='allday_bits[]' value='3' {$allday_bit3}>rsv3 &nbsp;  <input type='checkbox' name='allday_bits[]' value='4' {$allday_bit4}>rsv4 -->
			</fieldset>
		</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_LOCATION."</td>
		<td class='even'><input type='text' name='location' size='40' maxlength='250' value='$location'></td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_CONTACT."</td>
		<td class='even'><input type='text' name='contact' size='50' maxlength='250' value='$contact'></td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_DESCRIPTION."</td>
		<td class='even'>$description_textarea</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_CLASS."</td>
		<td class='even'><input type='radio' name='class' value='PUBLIC' $class_public onClick='document.MainForm.groupid.disabled=true'>"._PICAL_MB_PUBLIC." &nbsp;  &nbsp; <input type='radio' name='class' value='PRIVATE' $class_private onClick='document.MainForm.groupid.disabled=false'>"._PICAL_MB_PRIVATE.sprintf( _PICAL_MB_PRIVATETARGET , $select_private )."</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_RRULE."</td>
		<td class='even'>" . $this->rrule_to_form( $rrule , $end_date ) . "</td>
	</tr>
	<tr>
		<td class='head'>"._PICAL_TH_ADMISSIONSTATUS."</td>
		<td class='even'>$admission_status</td>
	</tr>\n" ;

	if( $editable ) {
	$ret .= "
	<tr>
		<td style='text-align:center' colspan='2'>
			<input name='reset' type='reset' value='"._PICAL_BTN_RESET."'>
			$update_button
			$insert_button
			$delete_button
		</td>
	</tr>\n" ;
	}

	$ret .= "
	<tr>
		<td><img src='$this->images_url/spacer.gif' alt='' width='150' height='4' /></td>		<td width='100%'></td>
	</tr>
	<!-- Copyright of piCal (Don't remove this) -->
	<tr>
		<td width='100%' align='right' colspan='2'>".PICAL_COPYRIGHT."</td>
	</tr>
	</table>
</form>\n" ;

	return $ret ;
}




// �������塼���ι��������ӿ�����Ͽ
function update_schedule( $set_sql_append = '' , $whr_sql_append = '' , $notify_callback = null )
{
	// debug�⡼�ɤ� Location �������ʤ��ʤ��Τ��ɤ�
//	error_reporting( 0 ) ;

	// $_SERVER �ѿ��μ���
	$PHP_SELF = $_SERVER[ 'PHP_SELF' ] ;
	$HTTP_HOST = $_SERVER[ 'HTTP_HOST' ] ;

	// Timezone �ν����ʤ����Τߡ��桼�����֤��饵���л��֤ؤ��Ѵ���
	$tzoffset_u2s = ( $this->server_TZ - $this->user_TZ ) * 3600 ;

	// summary�Υ����å���̤�����ʤ餽�λݤ��ɲá�
	if( $_POST[ 'summary' ] == "" ) $_POST[ 'summary' ] = _PICAL_MB_NOSUBJECT ;

	// ���դ���������̵�������դʤ�caldate�˥��åȡ�
	$start_date = $this->mb_convert_kana( $_POST[ 'StartDate' ] , "a" ) ;
	if( ! ereg( "^([0-9][0-9]+)[-./]?([0-1]?[0-9])[-./]?([0-3]?[0-9])" , $start_date , $regs ) ) $start = $this->unixtime ;
	else if( ! checkdate( $regs[2] , $regs[3] , $regs[1] ) ) $start = $this->unixtime ;
	else $start = mktime( 0 , 0 , 0 , $regs[2] , $regs[3] , $regs[1] ) ;

	$end_date = $this->mb_convert_kana( $_POST[ 'EndDate' ] , "a" ) ;
	if( ! ereg( "^([0-9][0-9]+)[-./]?([0-1]?[0-9])[-./]?([0-3]?[0-9])" , $end_date , $regs ) ) $end = $this->unixtime ;
	else if( ! checkdate( $regs[2] , $regs[3] , $regs[1] ) ) $end = $this->unixtime ;
	else $end = mktime( 0 , 0 , 0 , $regs[2] , $regs[3] , $regs[1] ) ;

	if( isset( $_POST[ 'allday' ] ) && $_POST[ 'allday' ] ) {
		// �������٥��ȡʻ����׻��ʤ���
		if( $start > $end ) list( $start , $end ) = array( $end , $start ) ;
		$end += 86400 ;		// ��λ���֤ϡ���λ��������0:00���ؤ�
		$allday = 1 ;
		if( isset( $_POST[ 'allday_bits' ] ) ) {
			$bits = $_POST[ 'allday_bits' ] ;
			if( is_array( $bits ) ) foreach( $bits as $bit ) {
				if( $bit > 0 && $bit < 8 ) {
					$allday += pow( 2 , intval( $bit ) ) ;
				}
			}
		}
		$set_sql_date = "start='$start', end='$end', allday='$allday'" ;
		$allday_flag = true ;
	} else {
		// �̾磻�٥��ȡʻ����׻�������
		$start += $_POST[ 'StartHour' ] * 3600 + $_POST[ 'StartMin' ] * 60 + $tzoffset_u2s ;
		$end += $_POST[ 'EndHour' ] * 3600 + $_POST[ 'EndMin' ] * 60 + $tzoffset_u2s ;
		if( $start > $end ) list( $start , $end ) = array( $end , $start ) ;
		$set_sql_date = "start='$start', end='$end', allday=0" ;
		$allday_flag = false ;
	}

	// ��Ͽ���λ�������Ͽ���Ƥ��� (GIJ TODO)
	$set_sql_date .= ",tzid='$this->user_TZ'" ;

	// description ��XOOPS�������� (Ϫ���ʥĥ��ϥ��ǡ����ޤ��ʹ��ɤ��ʤ����ɡġ�)
	if( ! isset( $_POST[ 'description' ] ) && isset( $_POST[ 'description_text' ] ) ) {
		$_POST[ 'description' ] = $_POST[ 'description_text' ] ;
	}

	// RRULE�μ���
	$rrule = $this->rrule_from_post( $start , $allday_flag ) ;

	// �����оݥ���������
	$cols = array( "summary" => "255:J:1" , "location" => "255:J:0" , "contact" => "255:J:0" , "description" => "A:J:0" , "class" => "255:E:0" , "groupid" => "I:N:0" ) ;

	$set_str = $this->get_sql_set( $cols ) . ", $set_sql_date $set_sql_append" ;

	// event_id��POST�����������ơ�ͭ�������ʤ�UPDATE��̵���ʤ�INSERT�����ߤ�
	$event_id = $_POST[ 'event_id' ] ;
	if( $event_id > 0 ) {
		// ��������

		// �ޤ��ϡ�rrule_pid����������ͭ����id�ʤ顢Ʊ��rrule_pid������
		// ¾�쥳���ɤ�����
		$rs = mysqli_query( $this->conn ,  "SELECT rrule_pid FROM $this->table WHERE id='$event_id' $whr_sql_append" ) ;
		if( ! ( $event = mysqli_fetch_object( $rs ) ) ) die( "Record Not Exists." ) ;
		if( $event->rrule_pid > 0 ) {
			if( ! mysqli_query( $this->conn ,  "DELETE FROM $this->table WHERE rrule_pid='$event->rrule_pid' AND id<>'$event_id'" ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		}

		// �оݥ쥳���ɤ�UPDATE����
		if( $rrule != '' ) $set_str .= ", rrule_pid=id" ;
		$sql = "UPDATE $this->table SET $set_str , rrule='$rrule' , sequence=sequence+1 WHERE id='$event_id' $whr_sql_append" ;
		if( ! mysqli_query( $this->conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;

		// RRULE���顢�ҥ쥳���ɤ�Ÿ��
		if( $rrule != '' ) {
			$this->rrule_extract( $event_id ) ;
		}

		// ���٤Ƥ򹹿��塢�������դΥ�������������������
		Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?smode={$_POST['last_smode']}&amp;caldate={$_POST['last_caldate']}" ) ;
		exit ;

	} else {
		// ������Ͽ����

		// ����(��)�쥳���ɤ�INSERT����
		$sql = "INSERT INTO $this->table SET $set_str , rrule='$rrule' , sequence=0" ;
		if( ! mysqli_query( $this->conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		// �ƥ쥳���ɤ� unique_id,rrule_pid �η׻�����Ͽ
		$event_id = ((is_null($___mysqli_res = mysqli_insert_id( $this->conn ))) ? false : $___mysqli_res) ;
		$unique_id = 'pical040-' . md5( "$HTTP_HOST$PHP_SELF$event_id") ;
		$rrule_pid = $rrule ? $event_id : 0 ;
		mysqli_query( $this->conn ,  "UPDATE $this->table SET unique_id='$unique_id',rrule_pid='$rrule_pid' WHERE id='$event_id'" ) ;

		// RRULE���顢�ҥ쥳���ɤ�Ÿ��
		if( $rrule != '' ) {
			$this->rrule_extract( $event_id ) ;
		}

		if( isset( $notify_callback ) ) $this->$notify_callback( $event_id ) ;

		// ���٤Ƥ���Ͽ�塢start_date �Υ�������������������
		Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?smode={$_POST['last_smode']}&amp;caldate=$start_date" ) ;
		exit ;

	}
}



// �������塼���κ���
function delete_schedule( $whr_sql_append = '' , $eval_after = null )
{
	// debug�⡼�ɤ� Location �������ʤ��ʤ��Τ��ɤ�
	// error_reporting( 0 ) ;

	if( isset( $_POST[ 'event_id' ] ) ) {

		$event_id = $_POST[ 'event_id' ] ;

		// �ޤ��ϡ�rrule_pid����������ͭ����id�ʤ顢Ʊ��rrule_pid������
		// ���쥳���ɤ�����
		$rs = mysqli_query( $this->conn ,  "SELECT rrule_pid FROM $this->table WHERE id='$event_id' $whr_sql_append" ) ;
		if( ! ( $event = mysqli_fetch_object( $rs ) ) ) die( "Record Not Exists." ) ;
		if( $event->rrule_pid > 0 ) {
			if( ! mysqli_query( $this->conn ,  "DELETE FROM $this->table WHERE rrule_pid='$event->rrule_pid' $whr_sql_append" ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
			// �������������ɲý�����eval�Ǽ����� (XOOPS�Ǥϡ��������Ȥκ�����
			if( mysqli_affected_rows($GLOBALS["___mysqli_ston"]) > 0 && isset( $eval_after ) ) {
				$id = $event->rrule_pid ;
				eval( $eval_after ) ;
			}
		} else {
			if( ! mysqli_query( $this->conn ,  "DELETE FROM $this->table WHERE id='$event_id' $whr_sql_append" ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
			// �������������ɲý�����eval�Ǽ����� (XOOPS�Ǥϡ��������Ȥκ�����
			if( mysqli_affected_rows($GLOBALS["___mysqli_ston"]) == 1 && isset( $eval_after ) ) {
				$id = $event_id ;
				eval( $eval_after ) ;
			}
		}

	}
	Header( "Location: $this->connection://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}?smode={$_POST['last_smode']}&amp;caldate={$_POST['last_caldate']}" ) ;
	exit ;
}



/*******************************************************************/
/*        ��ʪ�ؿ�                                                 */
/*******************************************************************/

// Ϣ�������������˼��ꡢ$_POST����INSERT,UPDATE�Ѥ�SETʸ���������륯�饹�ؿ�
function get_sql_set( $cols )
{
	$ret = "" ;

	foreach( $cols as $col => $types ) {

		list( $field , $lang , $essential ) = explode( ':' , $types ) ;

		// ̤�����ʤ�''�ȸ��ʤ�
		if( empty( $_POST[ $col ] ) ) $data = '' ;
		else if( get_magic_quotes_gpc() ) $data = $_POST[ $col ] ;
		else $data = addslashes( $_POST[ $col ] ) ;

		// ɬ�ܥե������ɤΥ����å�
		if( $essential && ! $data ) {
			die( sprintf( _PICAL_ERR_LACKINDISPITEM , $col ) ) ;
		}

		// ���졦�����ʤɤ��̤ˤ�������
		switch( $lang ) {
			case 'N' :	// ���� (�������� , ������)
				$data = str_replace( "," , "" , $data ) ;
				break ;
			case 'J' :	// ���ܸ��ƥ����� (Ⱦ�ѥ��ʢ����Ѥ���)
				$data = $this->mb_convert_kana( $data , "KV" ) ;
				break ;
			case 'E' :	// Ⱦ�ѱѿ����Τ�
				$data = $this->mb_convert_kana( $data , "as" ) ;
				break ;
		}

		// �ե������ɤη��ˤ�������
		switch( $field ) {
			case 'A' :	// textarea
				$data = $this->textarea_sanitizer_for_sql( $data ) ;
				$ret .= "$col='$data'," ;
				break ;
			case 'I' :	// integer
				$data = intval( $data ) ;
				$ret .= "$col='$data'," ;
				break ;
			default :	// varchar(�ǥե�����)�Ͽ��ͤˤ���ʸ��������
				$data = $this->text_sanitizer_for_sql( $data ) ;
				if( $field < 1 ) $field = 255 ;
				$data = mb_strcut( $data , 0 , $field ) ;
				$ret .= "$col='$data'," ;
		}
	}

	// �Ǹ��� , ������
	$ret = substr( $ret , 0 , -1 ) ;

	return $ret ;
}



// unixtimestamp���顢��������(timestamp����)�ʹߤ�ͽ��������ʸ����������
function get_coming_time_description( $start , $now , $admission = true )
{
	// ��ǧ��̵ͭ�ˤ��äƥɥå�GIF���ؤ���
	if( $admission ) $dot = "" ;
	else $dot = "<img border='0' src='$this->images_url/dot_notadmit.gif' />" ;

	if( $start >= $now && $start - $now < 86400 ) {
		// 24���ְ����Υ��٥���
		if( ! $dot ) $dot = "<img border='0' src='$this->images_url/dot_today.gif' />" ;
		$ret = "$dot <b>" . date( "H:i" , $start ) . "</b>"._PICAL_MB_TIMESEPARATOR ;
	} else if( $start < $now ) {
		// ���Ǥ˳��Ϥ��줿���٥���
		if( ! $dot ) $dot = "<img border='0' src='$this->images_url/dot_started.gif' />" ;
		$ret = "$dot "._PICAL_MB_CONTINUING ;
	} else {
		// �����ʹߤ˳��Ϥˤʤ륤�٥���
		if( ! $dot ) $dot = "<img border='0' src='$this->images_url/dot_future.gif' />" ;
//		$ret = "$dot " . date( "n/j H:i" , $start ) . _PICAL_MB_TIMESEPARATOR ;
		$ret = "$dot " . sprintf( _PICAL_FMT_MD , $this->month_middle_names[ date( 'n' , $start ) ] , $this->date_long_names[ date( 'j' , $start ) ] ) . " " . date( _PICAL_DTFMT_TIME , $start ) . _PICAL_MB_TIMESEPARATOR ;
	}

	return $ret ;
}



// ���Ĥ�unixtimestamp���顢������(Y-n-j����)��ͽ�����֤�ʸ����������
function get_todays_time_description( $start , $end , $ynj , $justify = true , $admission = true )
{
	$start_desc = date( "H:i" , $start ) ;
	$end_desc = date( "H:i" , $end ) ;
	$is_start_date = ( date( "Y-n-j" , $start ) == $ynj ) ;
	$is_end_date = ( date( "Y-n-j" , $end ) == $ynj ) ;

	// $this->day_start �ޤǤʤ顢�����Υǡ����Ǥ⽪λ���֤�ɽ������
	if( ! $is_end_date && date( "Y-n-j" , $end - $this->day_start - 1 ) == $ynj ) {
		$is_end_date = true ;
		$end_desc = ( date( 'H' , $end ) + 24 ) . ':' . date( 'i' , $end ) ;
	}

	$stuffing = $justify ? '     ' : '' ;

	// ͽ�����ֻ�����̵ͭ����ǧ��̵ͭ�ˤ��äƥɥå�GIF���ؤ���
	if( $admission ) {
		if( $is_start_date ) $dot = "<img border='0' src='$this->images_url/dot_startday.gif' />" ;
		else if( $is_end_date ) $dot = "<img border='0' src='$this->images_url/dot_endday.gif' />" ;
		else $dot = "<img border='0' src='$this->images_url/dot_interimday.gif' />" ;
	} else $dot = "<img border='0' src='$this->images_url/dot_notadmit.gif' />" ;

	if( $is_start_date ) {
		if( $is_end_date ) $ret = "$dot {$start_desc}"._PICAL_MB_TIMESEPARATOR."{$end_desc}" ;
		else $ret = "$dot {$start_desc}"._PICAL_MB_TIMESEPARATOR."{$stuffing}" ;
	} else {
		if( $is_end_date ) $ret = "$dot {$stuffing}"._PICAL_MB_TIMESEPARATOR."{$end_desc}" ;
		else $ret = "$dot "._PICAL_MB_CONTINUING ;
	}

	return $ret ;
}


// �������ϥܥå����δؿ� (JavaScript�����Ϥ����ݤ�Override�о�)

function get_formtextdateselect( $name , $value )
{
	return "<input type='text' name='$name' size='12' value='$value' style='ime-mode:disabled'>" ;
}



// $this->images_url���ˤ���style.css���ɤ߹��ߡ����˥��������ư����Ϥ�
function get_embed_css( )
{
	$css_filename = "$this->images_path/style.css" ;
	if( ! is_readable( $css_filename ) ) return "" ;
	else return strip_tags( join( "" , file( $css_filename ) ) ) ;
}



// ���ƼԤ�ɽ��ʸ�������֤� (Override�о�)
function get_submitter_info( $uid )
{
	return '' ;
}



// CLASS(������������)�ط���WHERE�Ѿ���������
function get_where_about_class()
{
	if( $this->isadmin ) {
		// �����Ԥ������Ԥʤ�����True
		return "1" ;
	} else if( $this->user_id <= 0 ) {
		// �����Ԥ������Ȥʤ�����(PUBLIC)�쥳���ɤΤ�
		return "class='PUBLIC'" ;
	} else {
		// �̾��桼���ʤ顢PUBLIC�쥳���ɤ����桼��ID�����פ����쥳���ɡ��ޤ��ϡ���°���Ƥ��륰�롼��ID�Τ����ΰ��Ĥ��쥳���ɤΥ��롼��ID�Ȱ��פ����쥳����
		$ids = ' ' ;
		foreach( $this->groups as $id => $name ) {
			$ids .= "$id," ;
		}
		$ids = substr( $ids , 0 , -1 ) ;
		if( intval( $ids ) == 0 ) $group_section = '' ;
		else $group_section = "OR groupid IN ($ids)" ;
		return "(class='PUBLIC' OR uid=$this->user_id $group_section)" ;
	}
}



// mb_convert_kana�ν���
function mb_convert_kana( $str , $option )
{
	// convert_kana �ν����ϡ����ܸ��ǤΤ߹Ԥ�
	if( $this->language != 'japanese' || ! function_exists( 'mb_convert_kana' ) ) {
		return $str ;
	} else {
		return mb_convert_kana( $str , $option ) ;
	}
}



/*******************************************************************/
/*   ���˥�������Ϣ�δؿ� (���֥��饹��������������Override�о�)   */
/*******************************************************************/

function textarea_sanitizer_for_sql( $data )
{
	// ñ������������̵���Ȥ���sanitize
	$data = strip_tags( $data ) ;
	/* ���ε�ǽ��piCal�ǤϻȤ��ʤ�
	if( isset( $_POST[ 'flg_nl2br' ] ) && $_POST[ 'flg_nl2br' ] ) {
		$data = nl2br( $data ) ;
		$data = str_replace( '<br /><br />' , '<br>' , $data ) ;
		$data = str_replace( '<br />' , '<br>' , $data ) ;
	} */
	return $data ;
}

function text_sanitizer_for_sql( $data )
{
	// ñ������������̵���Ȥ���sanitize
	return strip_tags( $data ) ;
}

function textarea_sanitizer_for_show( $data )
{
	return nl2br( htmlspecialchars( $data , ENT_QUOTES ) ) ;
}

function text_sanitizer_for_show( $data )
{
	return htmlspecialchars( $data , ENT_QUOTES ) ;
}

function textarea_sanitizer_for_edit( $data )
{
	return htmlspecialchars( $data , ENT_QUOTES ) ;
}

function text_sanitizer_for_edit( $data )
{
	return htmlspecialchars( $data , ENT_QUOTES ) ;
}

function textarea_sanitizer_for_export_ics( $data )
{
	return $data ;
}


/*******************************************************************/
/*        iCalendar �����ؿ�                                       */
/*******************************************************************/

// iCalendar�����Ǥν��� (mbstringɬ��)
// ���Ϥ������Τߤξ�����$_GET['event_id']�������ξ�����$_POST['event_ids']
function output_ics( )
{
	$PHP_SELF = $_SERVER[ "PHP_SELF" ] ;
	$HTTP_HOST = $_SERVER[ "HTTP_HOST" ] ;

	// $event_id �����ꤵ���Ƥ��ʤ����н�λ
	if( empty( $_GET[ 'event_id' ] ) AND empty( $_POST[ 'event_ids' ] ) ) die( _PICAL_ERR_INVALID_EVENT_ID ) ;

	// iCalendar���ϵ��Ĥ��ʤ����н�λ
	if( ! $this->can_output_ics ) die( _PICAL_ERR_NOPERM_TO_OUTPUTICS ) ;
	if( isset( $_GET[ 'event_id' ] ) ) {
		// $_GET[ 'event_id' ] �ˤ������������λ����ξ���
		$event_ids = array( intval( $_GET[ 'event_id' ] ) ) ;
		$rs = mysqli_query( $this->conn ,  "SELECT summary AS udtstmp FROM $this->table WHERE id='{$_GET['event_id']}'" ) ;
		if( mysqli_num_rows( $rs ) < 1 ) die( _PICAL_ERR_INVALID_EVENT_ID ) ;
		$summary = mysqli_result( $rs ,  0 ,  0 ) ;
		// ��̾ �� X-WR-CALNAME �Ȥ���
		$x_wr_calname = $summary ;
		// �ե�����̾�˻Ȥ��ʤ�������ʸ���Ϻ���
		if( function_exists( "mb_ereg_replace" ) ) {
			$summary = mb_ereg_replace( '[<>|"?*,:;\\/]' , '' , $summary ) ;
		} else {
			$summary = ereg_replace( '[<>|"?*,:;\\/]' , '' , $summary ) ;
		}
		// �ػ�ʸ�������ä���̾.ics ���ե�����̾�Ȥ��� (��SJIS�Ѵ�)
		$output_filename = mb_convert_encoding( $summary , "SJIS" ) . '.ics' ;
	} else if( is_array( $_POST[ 'event_ids' ] ) ) {
		// $_POST[ 'event_ids' ] �ˤ��������ˤ��������ξ���
		$event_ids = array_unique( $_POST[ 'event_ids' ] ) ;
		// events-��������(GMT) �� X-WR-CALNAME �Ȥ���
		$x_wr_calname = 'events-' . gmdate( 'Ymd\THis\Z' ) . '.ics' ;
		// events-��������.ics ���ե�����̾�Ȥ���
		$output_filename = $x_wr_calname . '.ics' ;
	} else die( _PICAL_ERR_INVALID_EVENT_ID ) ;

	// HTTP�إå�����
	header("Content-type: text/calendar");
	header("Content-Disposition: attachment; filename=$output_filename" );
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
	header("Pragma: public");

	// ���Ϥ���ü�Хåե����󥰤���
	ob_start() ;

	// iCalendar�إå�������Timezone����
	echo "BEGIN:VCALENDAR\r
CALSCALE:GREGORIAN\r
X-WR-TIMEZONE;VALUE=TEXT:Japan\r
PRODID:PEAK Corporation - piCal -\r
X-WR-CALNAME;VALUE=TEXT:$x_wr_calname\r
VERSION:2.0\r
METHOD:PUBLISH\r
BEGIN:VTIMEZONE\r
TZID:Japan\r
BEGIN:STANDARD\r
DTSTART:19390101T000000\r
TZOFFSETFROM:+0900\r
TZOFFSETTO:+0900\r
TZNAME:JST\r
END:STANDARD\r
END:VTIMEZONE\r
" ;

	// CLASS��Ϣ��WHERE��������
	$whr_class = $this->get_where_about_class() ;

	// ���٥������Υ롼��
	foreach( $event_ids as $event_id ) {

		$sql = "SELECT *,UNIX_TIMESTAMP(dtstamp) AS udtstmp FROM $this->table WHERE id='$event_id' AND ($whr_class)" ;
		if( ! $rs = mysqli_query( $this->conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		$event = mysqli_fetch_object( $rs ) ;
		if( ! $event ) continue ;

		if( $event->allday ) {
			// �������٥��ȡʻ��������ʤ���
			$dtstart = date( 'Ymd' , $event->start ) ;
			$dtend = date( 'Ymd' , $event->end ) ;
			// ���ϤȽ�λ��Ʊ���ξ����ϡ���λ�����������ˤ��餹
			if( $dtstart == $dtend ) $dtend = date( 'Ymd' , $event->end + 86400 ) ;
			$dtstart_opt = $dtend_opt = ";VALUE=DATE" ;
		} else {
			// �̾磻�٥��Ȥλ��������ʾ���GMTɽ���Ȥ�����
			$dtstart = date( 'Ymd\THis\Z' , $event->start - $this->server_TZ * 3600 ) ;
			$dtend = date( 'Ymd\THis\Z' , $event->end - $this->server_TZ * 3600 ) ;
			$dtstart_opt = $dtend_opt = "" ;
		}

		// DTSTAMP�Ͼ���GMT
		$dtstamp = date( 'Ymd\THis\Z' , $event->udtstmp - $this->server_TZ * 3600 ) ;

		// DESCRIPTION�� folding , \r���� ������ \n -> \\n �Ѵ�, ���˥�����
		// (folding ̤����)
		$description = str_replace( "\r" , '' , $event->description ) ;
		$description = str_replace( "\n" , '\n' , $description ) ;
		$description = $this->textarea_sanitizer_for_export_ics( $description ) ;

		// RRULE�Ԥϡ�RRULE�����Ȥ���������������
		$rrule_line = $event->rrule ? "RRULE:{$event->rrule}\r\n" : "" ;

		// ���٥��ȥǡ����ν���
		echo "BEGIN:VEVENT\r
DTSTART{$dtstart_opt}:{$dtstart}\r
DTEND{$dtend_opt}:{$dtend}\r
LOCATION:{$event->location}\r
TRANSP:OPAQUE\r
SEQUENCE:{$event->sequence}\r
UID:{$event->unique_id}\r
DTSTAMP:{$dtstamp}\r
CATEGORIES:{$event->categories}\r
DESCRIPTION:{$description}\r
SUMMARY:{$event->summary}\r
{$rrule_line}PRIORITY:{$event->priority}\r
CLASS:{$event->class}\r
END:VEVENT\r\n" ;

	}

	// iCalendar�եå�
	echo "END:VCALENDAR\r\n" ;

	// ���ϥХåե��μ���
	$ical_data = ob_get_contents() ;
	ob_end_clean() ;

	// mbstring �����������Τߡ�UTF-8 �ؤ��Ѵ�
	if( extension_loaded( 'mbstring' ) ) {
		mb_http_output( "pass" ) ;
		$ical_data = mb_convert_encoding( $ical_data , "UTF-8" ) ;
	}

	echo $ical_data ;

	exit ;
}



function import_ics_via_http( $uri )
{
	$PHP_SELF = $_SERVER[ "PHP_SELF" ] ;
	$HTTP_HOST = $_SERVER[ "HTTP_HOST" ] ;

	// debug�⡼�ɤ� Location �������ʤ��ʤ��Τ��ɤ�
	// error_reporting( 0 ) ;

	// webcal://* �� connection̤�����⡢���٤� http://* ������
	$user_uri = $uri ;
	$uri = str_replace( "webcal://" , "http://" , $uri ) ;
	if( substr( $uri , 0 , 7 ) != 'http://' ) $uri = "http://" . $uri ;

	// iCal parser �ˤ�������
	include_once "$this->mydir/iCal_parser.php" ;
	$ical = new iCal_parser() ;
	$ical->language = $this->language ;
	$ical->timezone = ( $this->server_TZ >= 0 ? "+" : "-" ) . sprintf( "%02d%02d" , abs( $this->server_TZ ) , abs( $this->server_TZ ) * 60 % 60 ) ;
	list( $ret_code , $message ) = explode( ":" , $ical->parse( $uri , $user_uri ) , 2 ) ;
	if( $ret_code != 0 ) {
		// �ѡ������Ԥʤ饨�顼���å�������ON�ˤ������Ф�
		Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?mes=".urlencode( $message ) ) ;
		exit ;
	}
	$setsqls = $ical->output_setsqls() ;

	foreach( $setsqls as $setsql ) {
		$sql = "INSERT INTO $this->table SET $setsql,admission=1,uid=$this->user_id" ;
		if( ! mysqli_query( $this->conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		else $mes = urlencode( "$message �򥤥��ݡ��Ȥ��ޤ���" ) ;

		// �ƥ쥳���ɤ� rrule_pid ����Ͽ
		$event_id = ((is_null($___mysqli_res = mysqli_insert_id( $this->conn ))) ? false : $___mysqli_res) ;
		$rs = mysqli_query( $this->conn ,  "SELECT rrule FROM $this->table WHERE id='$event_id'" ) ;
		$event = mysqli_fetch_object( $rs ) ;
		$rrule_pid = $event->rrule ? $event_id : 0 ;
		mysqli_query( $this->conn ,  "UPDATE $this->table SET rrule_pid='$rrule_pid' WHERE id='$event_id'" ) ;

		// RRULE���顢�ҥ쥳���ɤ�Ÿ��
		if( $event->rrule != '' ) {
			$this->rrule_extract( $event_id ) ;
		}
	}
	Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?mes=$mes" ) ;
	exit ;
}



function import_ics_from_local( $userfile )
{
	$PHP_SELF = $_SERVER[ "PHP_SELF" ] ;
	$HTTP_HOST = $_SERVER[ "HTTP_HOST" ] ;

	// debug�⡼�ɤ� Location �������ʤ��ʤ��Τ��ɤ�
	// error_reporting( 0 ) ;

	// iCal parser �ˤ�������
	include_once "$this->mydir/iCal_parser.php" ;
	$ical = new iCal_parser() ;
	$ical->language = $this->language ;
	$ical->timezone = ( $this->server_TZ >= 0 ? "+" : "-" ) . sprintf( "%02d%02d" , abs( $this->server_TZ ) , abs( $this->server_TZ ) * 60 % 60 ) ;
	list( $ret_code , $message ) = explode( ":" , $ical->parse( $_FILES[ $userfile ][ 'tmp_name' ] , $_FILES[ $userfile ][ 'name' ] ) , 2 ) ;
	if( $ret_code != 0 ) {
		// �ѡ������Ԥʤ饨�顼���å�������ON�ˤ������Ф�
		Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?mes=".urlencode( $message ) ) ;
		exit ;
	}
	$setsqls = $ical->output_setsqls() ;

	foreach( $setsqls as $setsql ) {
		$sql = "INSERT INTO $this->table SET $setsql,admission=1,uid=$this->user_id" ;
		if( ! mysqli_query( $this->conn ,  $sql ) ) echo mysqli_error($GLOBALS["___mysqli_ston"]) ;
		else $mes = urlencode( "$message �򥤥��ݡ��Ȥ��ޤ���" ) ;

		// �ƥ쥳���ɤ� rrule_pid ����Ͽ
		$event_id = ((is_null($___mysqli_res = mysqli_insert_id( $this->conn ))) ? false : $___mysqli_res) ;
		$rs = mysqli_query( $this->conn ,  "SELECT rrule FROM $this->table WHERE id='$event_id'" ) ;
		$event = mysqli_fetch_object( $rs ) ;
		$rrule_pid = $event->rrule ? $event_id : 0 ;
		mysqli_query( $this->conn ,  "UPDATE $this->table SET rrule_pid='$rrule_pid' WHERE id='$event_id'" ) ;

		// RRULE���顢�ҥ쥳���ɤ�Ÿ��
		if( $event->rrule != '' ) {
			$this->rrule_extract( $event_id ) ;
		}
	}
	Header( "Location: $this->connection://$HTTP_HOST$PHP_SELF?mes=$mes" ) ;
	exit ;
}





/*******************************************************************/
/*        RRULE �����ؿ�                                           */
/*******************************************************************/

// rrule�������������������륯�饹�ؿ�
function rrule_to_human_language( $rrule )
{
	$rrule = trim( $rrule ) ;
	if( $rrule == '' ) return '' ;

	// rrule �γ����Ǥ��ѿ���Ÿ��
	$rrule = strtoupper( $rrule ) ;
	$rules = split( ';' , $rrule ) ;
	foreach( $rules as $rule ) {
		list( $key , $val ) = explode( '=' , $rule , 2 ) ;
		$key = trim( $key ) ;
		$$key = trim( $val ) ;
	}

	if( empty( $FREQ ) ) $FREQ = 'DAILY' ;
	if( empty( $INTERVAL ) || $INTERVAL <= 0 ) $INTERVAL = 1 ;

	// ���پ�������
	$ret_freq = '' ;
	$ret_day = '' ;
	switch( $FREQ ) {
		case 'DAILY' :
			if( $INTERVAL == 1 ) $ret_freq = _PICAL_RR_EVERYDAY ;
			else $ret_freq = sprintf( _PICAL_RR_PERDAY , $INTERVAL ) ;
			break ;
		case 'WEEKLY' :
			if( empty( $BYDAY ) ) break ;	// BYDAY ɬ��
			$ret_day = strtr( $BYDAY , $this->byday2langday_w ) ;
			if( $INTERVAL == 1 ) $ret_freq = _PICAL_RR_EVERYWEEK ;
			else $ret_freq = sprintf( _PICAL_RR_PERWEEK , $INTERVAL ) ;
			break ;
		case 'MONTHLY' :
			if( isset( $BYMONTHDAY ) ) {
				$ret_day = "" ;
				$monthdays = explode( ',' , $BYMONTHDAY ) ;
				foreach( $monthdays as $monthday ) {
					$ret_day .= $this->date_long_names[ $monthday ] . "," ;
				}
				$ret_day = substr( $ret_day , 0 , -1 ) ;
			} else if( isset( $BYDAY ) ) {
				$ret_day = strtr( $BYDAY , $this->byday2langday_m ) ;
			} else {
				break ;		// BYDAY �ޤ��� BYMONTHDAY ɬ��
			}
			if( $INTERVAL == 1 ) $ret_freq = _PICAL_RR_EVERYMONTH ;
			else $ret_freq = sprintf( _PICAL_RR_PERMONTH , $INTERVAL ) ;
			break ;
		case 'YEARLY' :
			if( empty( $BYMONTH ) ) break ;		// BYMONTH ɬ��
			$ret_day = "" ;
			$months = explode( ',' , $BYMONTH ) ;
			foreach( $months as $month ) {
				$ret_day .= $this->month_long_names[ $month ] . "," ;
			}
			$ret_day = substr( $ret_day , 0 , -1 ) ;
			if( isset( $BYDAY ) ) {
				$ret_day .= strtr( $BYDAY , $this->byday2langday_m ) ;
			}
			if( $INTERVAL == 1 ) $ret_freq = _PICAL_RR_EVERYYEAR ;
			else $ret_freq = sprintf( _PICAL_RR_PERYEAR , $INTERVAL ) ;
			break ;
	}

	// ��λ��������
	$ret_terminator = '' ;
	// UNTIL �� COUNT ��ξ������������ COUNT ͥ��
	if( isset( $COUNT ) && $COUNT > 0 ) {
		$ret_terminator = sprintf( _PICAL_RR_COUNT , $COUNT ) ;
	} else if( isset( $UNTIL ) ) {
		// UNTIL �ϡ����������Ǥ�����̵�����Ǹ��ʤ�
		$year = substr( $UNTIL , 0 , 4 ) ;
		$month = substr( $UNTIL , 4 , 2 ) ;
		$date = substr( $UNTIL , 6 , 2 ) ;
		$ret_terminator = sprintf( _PICAL_RR_UNTIL , "$year-$month-$date" ) ;
	}

	return "$ret_freq $ret_day $ret_terminator" ;
}



// rrule���Խ��ѥե�������Ÿ�����륯�饹�ؿ�
function rrule_to_form( $rrule , $until_init )
{
	// �ƽ����ͤ�����
	$norrule_checked = '' ;
	$daily_checked = '' ;
	$weekly_checked = '' ;
	$monthly_checked = '' ;
	$yearly_checked = '' ;
	$norrule_checked = '' ;
	$noterm_checked = '' ;
	$count_checked = '' ;
	$until_checked = '' ;
	$daily_interval_init = 1 ;
	$weekly_interval_init = 1 ;
	$monthly_interval_init = 1 ;
	$yearly_interval_init = 1 ;
	$count_init = 1 ;
	$wdays_checked = array( 'SU'=>'' , 'MO'=>'' , 'TU'=>'' , 'WE'=>'' , 'TH'=>'' , 'FR'=>'' , 'SA'=>'' ) ;
	$byday_m_init = '' ;
	$bymonthday_init = '' ;
	$bymonths_checked = array( 1=>'' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' , '' ) ;

	if( trim( $rrule ) == '' ) {
		$norrule_checked = 'checked' ;
	} else {

		// rrule �γ����Ǥ��ѿ���Ÿ��
		$rrule = strtoupper( $rrule ) ;
		$rules = split( ';' , $rrule ) ;
		foreach( $rules as $rule ) {
			list( $key , $val ) = explode( '=' , $rule , 2 ) ;
			$key = trim( $key ) ;
			$$key = trim( $val ) ;
		}

		if( empty( $FREQ ) ) $FREQ = 'DAILY' ;
		if( empty( $INTERVAL ) || $INTERVAL <= 0 ) $INTERVAL = 1 ;

		// ���پ�������
		switch( $FREQ ) {
			case 'DAILY' :
				$daily_interval_init = $INTERVAL ;
				$daily_checked = 'checked' ;
				break ;
			case 'WEEKLY' :
				if( empty( $BYDAY ) ) break ;	// BYDAY ɬ��
				$weekly_interval_init = $INTERVAL ;
				$weekly_checked = 'checked' ;
				$wdays = explode( ',' , $BYDAY , 7 ) ;
				foreach( $wdays as $wday ) {
					if( isset( $wdays_checked[ $wday ] ) ) $wdays_checked[ $wday ] = 'checked' ;
				}
				break ;
			case 'MONTHLY' :
				if( isset( $BYDAY ) ) {
					$byday_m_init = $BYDAY ;
				} else if( isset( $BYMONTHDAY ) ) {
					$bymonthday_init = $BYMONTHDAY ;
				} else {
					break ;	// BYDAY �ޤ��� BYMONTHDAY ɬ��
				}
				$monthly_interval_init = $INTERVAL ;
				$monthly_checked = 'checked' ;
				break ;
			case 'YEARLY' :
				if( empty( $BYMONTH ) ) break ;		// BYMONTH ɬ��
				if( isset( $BYDAY ) ) $byday_m_init = $BYDAY ;
				$yearly_interval_init = $INTERVAL ;
				$yearly_checked = 'checked' ;
				$months = explode( ',' , $BYMONTH , 12 ) ;
				foreach( $months as $month ) {
					$month = intval( $month ) ;
					if( $month > 0 && $month <= 12 ) $bymonths_checked[ $month ] = 'checked' ;
				}
				break ;
		}

		// ��λ��������
		// UNTIL �� COUNT ��ξ������������ COUNT ͥ��
		if( isset( $COUNT ) && $COUNT > 0 ) {
			$count_init = $COUNT ;
			$count_checked = 'checked' ;
		} else if( isset( $UNTIL ) ) {
			// UNTIL �ϡ������ǡ����Ǥ�����̵�����Ǹ��ʤ�
			$year = substr( $UNTIL , 0 , 4 ) ;
			$month = substr( $UNTIL , 4 , 2 ) ;
			$date = substr( $UNTIL , 6 , 2 ) ;
			$until_init = "$year-$month-$date" ;
			$until_checked = 'checked' ;
		} else {
			// ξ�ԤȤ����꤬�ʤ����С���λ�����ʤ�
			$noterm_checked = 'checked' ;
		}

	}

	// UNTIL �����ꤹ�뤿���Υܥå���
	$textbox_until = $this->get_formtextdateselect( 'rrule_until' , $until_init ) ;

	// �������������å��ܥå�����Ÿ��
	$wdays_checkbox = '' ;
	foreach( $this->byday2langday_w as $key => $val ) {
		$wdays_checkbox .= "<input type='checkbox' name='rrule_weekly_bydays[]' value='$key' {$wdays_checked[$key]}>$val &nbsp; \n" ;
	}

	// �����������å��ܥå�����Ÿ��
	$bymonth_checkbox = "<table border='0' cellpadding='2'><tr>\n" ;
	foreach( $bymonths_checked as $key => $val ) {
		$bymonth_checkbox .= "<td><input type='checkbox' name='rrule_bymonths[]' value='$key' $val>{$this->month_short_names[$key]}</td>\n" ;
		if( $key == 6 ) $bymonth_checkbox .= "</tr>\n<tr>\n" ;
	}
	$bymonth_checkbox .= "</tr></table>\n" ;

	// ��N��������OPTION��Ÿ��
	$byday_m_options = '' ;
	foreach( $this->byday2langday_m as $key => $val ) {
		if( $byday_m_init == $key ) {
			$byday_m_options .= "<option value='$key' selected='selected'>$val</option>\n" ;
		} else {
			$byday_m_options .= "<option value='$key'>$val</option>\n" ;
		}
	}

	return "
			<input type='radio' name='rrule_freq' value='none' $norrule_checked>"._PICAL_RR_R_NORRULE."<br />
			<br />
			<fieldset>
				<legend class='blockTitle'>"._PICAL_RR_R_YESRRULE."</legend>
				<fieldset>
					<legend class='blockTitle'><input type='radio' name='rrule_freq' value='daily' $daily_checked>"._PICAL_RR_FREQDAILY."</legend>
					"._PICAL_RR_FREQDAILY_PRE." <input type='text' size='2' name='rrule_daily_interval' value='$daily_interval_init'> "._PICAL_RR_FREQDAILY_SUF."
				</fieldset>
				<br />
				<fieldset>
					<legend class='blockTitle'><input type='radio' name='rrule_freq' value='weekly' $weekly_checked>"._PICAL_RR_FREQWEEKLY."</legend>
					"._PICAL_RR_FREQWEEKLY_PRE."<input type='text' size='2' name='rrule_weekly_interval' value='$weekly_interval_init'> "._PICAL_RR_FREQWEEKLY_SUF." <br />
					$wdays_checkbox
				</fieldset>
				<br />
				<fieldset>
					<legend class='blockTitle'><input type='radio' name='rrule_freq' value='monthly' $monthly_checked>"._PICAL_RR_FREQMONTHLY."</legend>
					"._PICAL_RR_FREQMONTHLY_PRE."<input type='text' size='2' name='rrule_monthly_interval' value='$monthly_interval_init'> "._PICAL_RR_FREQMONTHLY_SUF." &nbsp;
					<select name='rrule_monthly_byday'>
						<option value=''>"._PICAL_RR_S_NOTSELECTED."</option>
						$byday_m_options
					</select> &nbsp; "._PICAL_RR_OR." &nbsp;
					<input type='text' size='10' name='rrule_bymonthday' value='$bymonthday_init'>"._PICAL_NTC_MONTHLYBYMONTHDAY."
				</fieldset>
				<br />
				<fieldset>
					<legend class='blockTitle'><input type='radio' name='rrule_freq' value='yearly' $yearly_checked>"._PICAL_RR_FREQYEARLY."</legend>
					"._PICAL_RR_FREQYEARLY_PRE."<input type='text' size='2' name='rrule_yearly_interval' value='$yearly_interval_init'> "._PICAL_RR_FREQYEARLY_SUF." <br />
					$bymonth_checkbox <br />
					<select name='rrule_yearly_byday'>
						<option value=''>"._PICAL_RR_S_SAMEASBDATE."</option>
						$byday_m_options
					</select>
				</fieldset>
				<br />
				<input type='radio' name='rrule_terminator' value='noterm' $noterm_checked onClick='document.MainForm.rrule_until.disabled=true;document.MainForm.rrule_count.disabled=true;'>"._PICAL_RR_R_NOCOUNTUNTIL." &nbsp; ".sprintf( _PICAL_NTC_EXTRACTLIMIT , $this->max_rrule_extract )."  <br />
				<input type='radio' name='rrule_terminator' value='count' $count_checked onClick='document.MainForm.rrule_until.disabled=true;document.MainForm.rrule_count.disabled=false;'>"._PICAL_RR_R_USECOUNT_PRE." <input type='text' size='3' name='rrule_count' value='$count_init'> "._PICAL_RR_R_USECOUNT_SUF."<br />
				<input type='radio' name='rrule_terminator' value='until' $until_checked onClick='document.MainForm.rrule_until.disabled=false;document.MainForm.rrule_count.disabled=true;'>"._PICAL_RR_R_USEUNTIL." $textbox_until
			</fieldset>
  \n" ;
}



// POST���줿rrule��Ϣ�������ͤ���RRULEʸ�������Ȥ߾夲�륯�饹�ؿ�
function rrule_from_post( $start , $allday_flag )
{
	// �����֤�̵���ʤ顢̵�����Ƕ�ʸ�������֤�
	if( $_POST['rrule_freq'] == 'none' ) return '' ;

	// ���پ���
	switch( strtoupper( $_POST['rrule_freq'] ) ) {
		case 'DAILY' :
			$ret_freq = "FREQ=DAILY;INTERVAL=" . abs( intval( $_POST['rrule_daily_interval'] ) ) ;
			break ;
		case 'WEEKLY' :
			$ret_freq = "FREQ=WEEKLY;INTERVAL=" . abs( intval( $_POST['rrule_weekly_interval'] ) ) ;
			if( empty( $_POST['rrule_weekly_bydays'] ) ) {
				// �����λ��꤬���Ĥ��ʤ����С���������Ʊ�������ˤ���
				$bydays = array_keys( $this->byday2langday_w ) ;
				$byday = $bydays[ date( 'w' , $start ) ] ;
			} else {
				$byday = '' ;
				foreach( $_POST['rrule_weekly_bydays'] as $wday ) {
					$byday .= substr( $wday , 0 , 2 ) . ',' ;
				}
				$byday = substr( $byday , 0 , -1 ) ;
			}
			$ret_freq .= ";BYDAY=$byday" ;
			break ;
		case 'MONTHLY' :
			$ret_freq = "FREQ=MONTHLY;INTERVAL=" . abs( intval( $_POST['rrule_monthly_interval'] ) ) ;
			if( $_POST['rrule_monthly_byday'] != '' ) {
				// ��N�����ˤ�������
				$byday = substr( trim( $_POST['rrule_monthly_byday'] ) , 0 , 4 ) ;
				$ret_freq .= ";BYDAY=$byday" ;
			} else if( $_POST['rrule_bymonthday'] != '' ) {
				// ���դˤ�������
				$bymonthday = preg_replace( '/[^0-9,]+/' , '' , $_POST['rrule_bymonthday'] ) ;
				$ret_freq .= ";BYMONTHDAY=$bymonthday" ;
			} else {
				// ��N���������դλ��꤬�ʤ����С���������Ʊ�����դȤ���
				$ret_freq .= ";BYMONTHDAY=" . date( 'j' , $start ) ;
			}
			break ;
		case 'YEARLY' :
			$ret_freq = "FREQ=YEARLY;INTERVAL=" . abs( intval( $_POST['rrule_yearly_interval'] ) ) ;
			if( empty( $_POST['rrule_bymonths'] ) ) {
				// ���λ��꤬���Ĥ��ʤ����С���������Ʊ�����ˤ���
				$bymonth = date( 'n' , $start ) ;
			} else {
				$bymonth = '' ;
				foreach( $_POST['rrule_bymonths'] as $month ) {
					$bymonth .= intval( $month ) . ',' ;
				}
				$bymonth = substr( $bymonth , 0 , -1 ) ;
			}
			if( $_POST['rrule_yearly_byday'] != '' ) {
				// ��N�����ˤ�������
				$byday = substr( trim( $_POST['rrule_yearly_byday'] ) , 0 , 4 ) ;
				$ret_freq .= ";BYDAY=$byday" ;
			}
			$ret_freq .= ";BYMONTH=$bymonth" ;
			break ;
		default :
			return '' ;
	}

	// ��λ����
	if( empty( $_POST['rrule_terminator'] ) ) $_POST['rrule_terminator'] = '' ;
	switch( strtoupper( $_POST['rrule_terminator'] ) ) {
		case 'COUNT' :
			$ret_term = ';COUNT=' . abs( intval( $_POST['rrule_count'] ) ) ;
			break ;
		case 'UNTIL' :
			// ���դ���������̵�������դʤ�caldate�˥��åȡ�
			$until = $this->mb_convert_kana( $_POST[ 'rrule_until' ] , "a" ) ;
			if( ! ereg( "^([0-9][0-9]+)[-./]?([0-1]?[0-9])[-./]?([0-3]?[0-9])" , $until , $regs ) ) $until = $this->unixtime ;
			else if( ! checkdate( $regs[2] , $regs[3] , $regs[1] ) ) $until = $this->unixtime ;
			else $until = mktime( 0 , 0 , 0 , $regs[2] , $regs[3] , $regs[1] ) ;
			if( ! $allday_flag ) {
				// �������٥��ȤǤʤ����� UTC �ػ����׻�
				// $tzoffset_u2g = ( 0 - $this->user_TZ ) * 3600 ;
				// $until += $tzoffset_u2g ;
				// (GIJ TODO)���λ����׻������⤽��ɬ�פʤΤ��ɤ���Ƚ���ʤ�����
				// �Ȥꤢ�����������ȥ�����
			}
			$ret_term = ';UNTIL=' . date( 'Ymd\THis\Z' , $until ) ;
			break ;
		case 'NOTERM' :
		default :
			$ret_term = '' ;
			break ;
	}

	// WKST�ϡ���ư��������
	$ret_wkst = $this->week_start ? ';WKST=MO' : ';WKST=SU' ;

	return $ret_freq . $ret_term . $ret_wkst ;
}


// �Ϥ��줿event_id������(��)�Ȥ��ơ�RRULE��Ÿ�����ƥǡ����١�����ȿ��
function rrule_extract( $event_id )
{
	$yrs = mysqli_query( $this->conn ,  "SELECT * FROM $this->table WHERE id='$event_id'" ) ;
	if( mysqli_num_rows( $yrs ) < 1 ) return ;
	$event = mysqli_fetch_object( $yrs ) ;

	if( $event->rrule == '' ) return ;

	// rrule �γ����Ǥ��ѿ���Ÿ��
	$rrule = strtoupper( $event->rrule ) ;
	$rules = split( ';' , $rrule ) ;
	foreach( $rules as $rule ) {
		list( $key , $val ) = explode( '=' , $rule , 2 ) ;
		$key = trim( $key ) ;
		$$key = trim( $val ) ;
	}

	// �����ˤ��äơ�RRULE�����ջ��꤬�ɤ��֤������뤫�η׻�
	if( $event->allday ) {
		$tzoffset_date = 0 ;
	} else {
		// GIJ TODO ����$user_TZ ����0.6�ʹߤǤϡ�event_TZ ���֤�������
		$user_TZ = is_numeric( $event->tzid ) ? doubleval( $event->tzid ) : $this->user_TZ ;
		$tzoffset = ( $user_TZ - $this->server_TZ ) * 3600 ;
		$tzoffset_date = date( 'z' , $event->start + $tzoffset ) - date( 'z' , $event->start ) ;
		if( $tzoffset_date > 1 ) $tzoffset_date = -1 ;
		else if( $tzoffset_date < -1 ) $tzoffset_date = 1 ;
	}

	if( empty( $FREQ ) ) $FREQ = 'DAILY' ;
	if( empty( $INTERVAL ) || $INTERVAL <= 0 ) $INTERVAL = 1 ;

	// �١����Ȥʤ�SQLʸ
	$base_sql = "INSERT INTO $this->table SET uid='$event->uid',groupid='$event->groupid',summary='".addslashes($event->summary)."',location='".addslashes($event->location)."',organizer='".addslashes($event->organizer)."',sequence='$event->sequence',contact='".addslashes($event->contact)."',tzid='$event->tzid',description='".addslashes($event->description)."',dtstamp='$event->dtstamp',categories='".addslashes($event->categories)."',transp='$event->transp',priority='$event->priority',admission='$event->admission',class='$event->class',rrule='".addslashes($event->rrule)."',unique_id='$event->unique_id',allday='$event->allday',extkey0='$event->extkey0',extkey1='$event->extkey1',rrule_pid='$event_id'" ;

	// ��λ��������
	// ��������
	$count = $this->max_rrule_extract ;
	if( isset( $COUNT ) && $COUNT > 0 && $COUNT < $count ) {
		$count = $COUNT ;
	}
	// Ÿ����λ��
	if( isset( $UNTIL ) ) {
		// UNTIL �ϡ����������Ǥ�����̵�����Ǹ��ʤ�
		$year = substr( $UNTIL , 0 , 4 ) ;
		$month = substr( $UNTIL , 4 , 2 ) ;
		$date = substr( $UNTIL , 6 , 2 ) ;
		if( ! checkdate( $month , $date , $year ) ) $until = 0x7FFFFFFF ;
		else {
			$until = mktime( 23 , 59 , 59 , $month , $date , $year ) ;
			if( ! $event->allday ) {
				// GMT -> user_TZ �λ����׻�
				// $until += $this->user_TZ * 3600 ;
				// (GIJ TODO) �����ˤĤ��Ƥ⥳�����ȥ�����
			}
		}
	} else $until = 0x7FFFFFFF ;

	// WKST
	if( empty( $WKST ) ) $WKST = 'MO' ;

	// ���پ�������
	$sqls = array() ;
	switch( $FREQ ) {
		case 'DAILY' :
			$start = $event->start ;
			$end = $event->end ;
			for( $c = 1 ; $c < $count ; $c ++ ) {
				$start += $INTERVAL * 86400 ;
				$end += $INTERVAL * 86400 ;
				if( $start > $until ) break ;
				$sqls[] = $base_sql . ",start='$start',end='$end'" ;
			}
			break ;

		case 'WEEKLY' :
			$start = $event->start ;
			$duration = $event->end - $event->start ;
			$wtop_date = date( 'j' , $start ) - date( 'w' , $start ) ;
			if( $WKST != 'SU' ) $wtop_date = $wtop_date == 7 ? 1 : $wtop_date + 1 ;
			$secondofday = ( date( 'Z' ) + $start ) % 86400 ;
			$month = date( 'm' , $start ) ;
			$year = date( 'Y' , $start ) ;
			$week_top = mktime( 0 , 0 , 0 , $month , $wtop_date , $year ) ;
			$c = 1 ;
			// ���Ͳ����������κ���
			$temp_dates = explode( ',' , $BYDAY ) ;
			$wdays = array_keys( $this->byday2langday_w ) ;
			if( $WKST != 'SU' ) {
				// rotate wdays for creating array stating with Monday
				$sun_date = array_shift( $wdays ) ;
				array_push( $wdays , $sun_date ) ;
			}
			$dates = array() ;
			foreach( $temp_dates as $date ) {
				// measure for bug of PHP<4.2.0
				if( in_array( $date , $wdays ) ) {
					$dates[] = array_search( $date , $wdays ) ;
				}
			}
			sort( $dates ) ;
			$dates = array_unique( $dates ) ;
			if( ! count( $dates ) ) return ;
			while( 1 ) {
				foreach( $dates as $date ) {
					// �����л��֤ȥ桼�����֤��������ۤʤ������ν����ɲ�
					$start = $week_top + ( $date - $tzoffset_date ) * 86400 + $secondofday ;
					if( $start <= $event->start ) continue ;
					$end = $start + $duration ;
					if( $start > $until ) break 2 ;
					if( ++ $c > $count ) break 2 ;
					$sqls[] = $base_sql . ",start='$start',end='$end'" ;
				}
				$week_top += $INTERVAL * 86400 * 7 ;
			}
			break ;

		case 'MONTHLY' :
			$start = $event->start ;
			$secondofday = ( date( 'Z' ) + $start ) % 86400 ;
			$duration = $event->end - $event->start ;
			$month = date( 'm' , $start ) ;
			$year = date( 'Y' , $start ) ;
			$c = 1 ;
			if( isset( $BYDAY ) && ereg( '^(-1|[1-4])(SU|MO|TU|WE|TH|FR|SA)' , $BYDAY , $regs ) ) {
				// ��N��������(BYDAY)�ξ�����ʣ���Բġ�
				// ��Ū�������ֹ�������
				$wdays = array_keys( $this->byday2langday_w ) ;
				$wday = array_search( $regs[2] , $wdays ) ;
				$first_ymw = date( 'Ym' , $start ) . intval( ( date( 'j' , $start ) - 1 ) / 7 ) ;
				if( $regs[1] == -1 ) {
					// �ǽ��������ξ����Υ롼��
					$monthday_bottom = mktime( 0 , 0 , 0 , $month + 1 , 0 , $year ) ;
					while( 1 ) {
						for( $i = 0 ; $i < $INTERVAL ; $i ++ ) {
							$monthday_bottom += date( 't' , $monthday_bottom + 86400 ) * 86400 ;
						}
						// �ǽ�����������Ĵ�٤�
						$last_monthdays_wday = date( 'w' , $monthday_bottom ) ;
						$date_back = $wday - $last_monthdays_wday ;
						if( $date_back > 0 ) $date_back -= 7 ;
						// �����л��֤ȥ桼�����֤��������ۤʤ������ν����ɲ�
						$start = $monthday_bottom + ( $date_back - $tzoffset_date ) * 86400 + $secondofday ;
						$end = $start + $duration ;
						if( $start > $until ) break ;
						if( ++ $c > $count ) break ;
						// echo date( "Y-m-d" , $start ) . "<br />" ;
						$sqls[] = $base_sql . ",start='$start',end='$end'" ;
					}
				} else {
					// ��N�������ξ����Υ롼��
					$monthday_top = mktime( 0 , 0 , 0 , $month , 1 , $year ) ;
					$week_number_offset = ( $regs[1] - 1 ) * 7 * 86400 ;
					while( 1 ) {
						for( $i = 0 ; $i < $INTERVAL ; $i ++ ) {
							$monthday_top += date( 't' , $monthday_top ) * 86400 ;
						}
						// ��N��������������Ĵ�٤�
						$week_numbers_top_wday = date( 'w' , $monthday_top + $week_number_offset ) ;
						$date_ahead = $wday - $week_numbers_top_wday ;
						if( $date_ahead < 0 ) $date_ahead += 7 ;
						// �����л��֤ȥ桼�����֤��������ۤʤ������ν����ɲ�
						$start = $monthday_top + $week_number_offset + ( $date_ahead - $tzoffset_date ) * 86400 + $secondofday ;
						$end = $start + $duration ;
						if( $start > $until ) break ;
						if( ++ $c > $count ) break ;
						// echo date( "Y-m-d" , $start ) . "<br />" ;
						$sqls[] = $base_sql . ",start='$start',end='$end'" ;
					}
				}
			} else if( isset( $BYMONTHDAY ) ) {
				// ���ջ���(BYMONTHDAY)�ξ�����ʣ���ġ�
				$monthday_top = mktime( 0 , 0 , 0 , $month , 1 , $year ) ;
				// BYMONTHDAY �����������ơ�$dates�����ˤ���
				$temp_dates = explode( ',' , $BYMONTHDAY ) ;
				$dates = array() ;
				foreach( $temp_dates as $date ) {
					if( $date > 0 && $date <= 31 ) $dates[] = intval( $date ) ;
				}
				sort( $dates ) ;
				$dates = array_unique( $dates ) ;
				if( ! count( $dates ) ) return ;
				while( 1 ) {
					$months_day = date( 't' , $monthday_top ) ;
					foreach( $dates as $date ) {
						// ���κǽ����ե��������å�
						if( $date > $months_day ) $date = $months_day ;
						// �����л��֤ȥ桼�����֤��������ۤʤ������ν����ɲ�
						$start = $monthday_top + ( $date - 1 - $tzoffset_date ) * 86400 + $secondofday ;
						if( $start <= $event->start ) continue ;
						$end = $start + $duration ;
						if( $start > $until ) break 2 ;
						if( ++ $c > $count ) break 2 ;
						// echo date( "Y-m-d" , $start ) . "<br />" ;
						$sqls[] = $base_sql . ",start='$start',end='$end'" ;
					}
					for( $i = 0 ; $i < $INTERVAL ; $i ++ ) {
						$monthday_top += date( 't' , $monthday_top ) * 86400 ;
					}
				}
			} else {
				// ͭ����$BYDAY��$BYMONTHDAY��̵�����С������֤��������ʤ�
				return ;
			}
			break ;

		case 'YEARLY' :
			$start = $event->start ;
			$secondofday = ( date( 'Z' ) + $start ) % 86400 ;
			$duration = $event->end - $event->start ;

			// BYMONTH �����������ơ�$months�����ˤ�����BYMONTH��ʣ���ġ�
			$temp_months = explode( ',' , $BYMONTH ) ;
			$months = array() ;
			foreach( $temp_months as $month ) {
				if( $month > 0 && $month <= 12 ) $months[] = intval( $month ) ;
			}
			sort( $months ) ;
			$months = array_unique( $months ) ;
			if( ! count( $months ) ) return ;

			if( isset( $BYDAY ) && ereg( '^(-1|[1-4])(SU|MO|TU|WE|TH|FR|SA)' , $BYDAY , $regs ) ) {
				// ��N���������ξ�����ʣ���Բġ�
				// ��Ū�������ֹ�������
				$wdays = array_keys( $this->byday2langday_w ) ;
				$wday = array_search( $regs[2] , $wdays ) ;
				$first_ym = date( 'Ym' , $start ) ;
				$year = date( 'Y' , $start ) ;
				$c = 1 ;
				if( $regs[1] == -1 ) {
					// �ǽ��������ξ����Υ롼��
					while( 1 ) {
						foreach( $months as $month ) {
							// �ǽ�����������Ĵ�٤�
							$last_monthdays_wday = date( 'w' , mktime( 0 , 0 , 0 , $month + 1 , 0 , $year ) ) ;
							$date_back = $wday - $last_monthdays_wday ;
							if( $date_back > 0 ) $date_back -= 7 ;
							$start = mktime( 0 , 0 , 0 , $month + 1 , $date_back - $tzoffset_date , $year ) + $secondofday ;
							// ������Ʊ�����������ɤ��������å�
							if( date( 'Ym' , $start ) <= $first_ym ) continue ;
							$end = $start + $duration ;
							if( $start > $until ) break 2 ;
							if( ++ $c > $count ) break 2 ;
							// echo date( "Y-m-d" , $start ) . "<br />" ;
							$sqls[] = $base_sql . ",start='$start',end='$end'" ;
						}
						$year += $INTERVAL ;
						if( $year >= 2038 ) break ;
					}
				} else {
					// ��N�������ξ����Υ롼��
					$week_numbers_top_date = 1 + ( $regs[1] - 1 ) * 7 ;
					while( 1 ) {
						foreach( $months as $month ) {
							// ��N��������������Ĵ�٤�
							$week_numbers_top_wday = date( 'w' , mktime( 0 , 0 , 0 , $month , $week_numbers_top_date , $year ) ) ;
							$date_ahead = $wday - $week_numbers_top_wday ;
							if( $date_ahead < 0 ) $date_ahead += 7 ;
							$start = mktime( 0 , 0 , 0 , $month , $week_numbers_top_date + $date_ahead - $tzoffset_date , $year ) + $secondofday ;
							// ������Ʊ�����������ɤ��������å�
							if( date( 'Ym' , $start ) <= $first_ym ) continue ;
							$end = $start + $duration ;
							if( $start > $until ) break 2 ;
							if( ++ $c > $count ) break 2 ;
							// echo date( "Y-m-d" , $start ) . "<br />" ;
							$sqls[] = $base_sql . ",start='$start',end='$end'" ;
						}
						$year += $INTERVAL ;
						if( $year >= 2038 ) break ;
					}
				}
			} else {
				// ���ջ����ξ����Υ롼�ס�ʣ���Բġ�
				$first_date = date( 'j' , $start ) ;
				$year = date( 'Y' , $start ) ;
				$c = 1 ;
				while( 1 ) {
					foreach( $months as $month ) {
						$date = $first_date ;
						// ���κǽ����ե��������å�
						while( ! checkdate( $month , $date , $year ) && $date > 0 ) $date -- ;
						$start = mktime( 0 , 0 , 0 , $month , $date , $year ) + $secondofday ;
						if( $start <= $event->start ) continue ;
						$end = $start + $duration ;
						if( $start > $until ) break 2 ;
						if( ++ $c > $count ) break 2 ;
						// echo date( "Y-m-d" , $start ) . "<br />" ;
						$sqls[] = $base_sql . ",start='$start',end='$end'" ;
					}
					$year += $INTERVAL ;
					if( $year >= 2038 ) break ;
				}
			}
			break ;

		default :
			return ;
	}

	// echo "<pre>" ; print_r( $sqls ) ; echo "</pre>" ; exit ;
	foreach( $sqls as $sql ) {
		mysqli_query( $this->conn ,  $sql ) ;
	}
}


// The End of Class
}

?>
