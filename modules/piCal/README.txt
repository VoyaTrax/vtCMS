        RFC2445 Class for PHP and Calendar Module for XOOPS2
                           "piCal"

                                GIJ=CHECKMATE <gij@peak.ne.jp>
                                PEAK Corp.   http://www.peak.ne.jp/
                                (in XOOPS site, my uname is GIJOE)


(1) What about piCal?

piCal is an independent calendar class of php.

piCal is also a powerful calendar module for Xoops2.
This module can generate iCalendar data dynamically,
and can import via http or from a local file. 

And piCal also has a little functions of group-ware.
Of course, this module has enough calendar feature,
eg) 4 type of view -Daily,Weekly,Monthly,Yearly- .

This archive contains English, Japanese, Germany,Spanish,French,Dutch,Russian,Tchinese language files.

The initial version of piCal was developped as a module only for 
Japanese in 2003-4-23.

I made internationalization in version 0.50 half a year later 0.10 
released.


(2) How to Install

Same as another modules for XOOPS2.
No changes to permissions of files or directories are necessary.


(3) How to Upgrade

- Overwrite all files in the archive.
- Update module by modules admin in your system's admin.

(- Check piCal's status by module maintenance in piCal's admin.(only after 0.60))


(4) FAQ

Q) The Displayed time is different from the time input time
A) This is caused the wrong setting of Time Zones in your XOOPS.
Check Time Zones of your account, default account, or server.


(5) Copyrights

The license of piCal conforms to GPL.
see COPYING for detail.








----------------------------------------------------------------
(Japanese)


        XOOPS2用 スケジューラ付カレンダーモジュール 「piCal」


                                GIJ=CHECKMATE <gij@peak.ne.jp>
                                PEAK Corp.   http://www.peak.ne.jp/
                             （XOOPS関連サイトでは、GIJOEというハンドルです）

●piCalとは

PHP+MySQL用のクラスです。かなり以前に仕事で作ったカレンダー機能をベー
スとしています。あくまで汎用の「クラス」として作りましたが、かなり
XOOPSに特化した機能も充実してきたと自負しています。

名前の由来は、うちの社名 PEAK の頭文字と、iCalendar を合わせただけで
す。読み方は「ピーカル」ではなく「パイカル」です。
でも、後で調べてみたら、Python用のiCalendarライブラリも、picalという
名前のようです。こちらこそ「パイカル」でしょうから、読み方だけでも変
えようか、などと考えたりしてます。（ヨタ話にして失礼）



●開発の背景

XOOPSそのものはかなり前から興味を持っていたのですが、2003年の4月頃、
たまたまいくつかの案件があって、ようやく直接いじる機会にめぐまれまし
た。

ところが、それらの案件で必要になる、カレンダーもしくはスケジューラ機
能が公式モジュールにはありません。当然、3rd Partyモジュールを探すこと
になるのですが、当時唯一みつかったeCalは、私の利用方法にはそぐいませ
んでした。

最初はeCalをベースに改変しようかと思ったのですが、元がフランス語であ
ることもあって、あまりにもソースが読みづらく、これくらいなら0から作っ
てしまえ、と一念発起して、「車輪の再発明」することになりました。 (^^;;;

実際、カレンダー関係のPHPクラスなど、おそらくゴロゴロしていると思うの
ですが、暦というのは、意外とローカルな仕様が多くて、ある国の暦をベー
スとしたカレンダークラスを別の国向けにローカライズするのは大変です。

そんなわけで、piCalクラスも、XOOPS2用インターフェースの部分も、当初は
日本環境のためだけに作り、バージョン0.1〜0.4まで、日本人だけを対象と
してきたのですが、Horacio Salazarさん達の熱心なXOOPS伝道者にすすめら
れたこともあり、0.50でついに国際化しました。

元々が日本専用仕様で作っていただけに、国際化は非常に困難を極めました
が、今はそれなりのものが出来たと思っています。

今後も、「XOOPSにはpiCalがある」と言わせるだけのモジュールに育ててい
こうと思っています。



●利用方法（piCal新規導入の方）

XOOPS2で使うのは簡単なはずです。

他のモジュールと同様に、modules/ 以下に展開して下さい。特にパーミッショ
ンを変更するようなフォルダはありません。

その後、モジュール管理でインストールすればOKです。ブロックやモジュー
ルのアクセス権限は、従来通り、システムのグループ管理でもできますが、
piCalにはmyblocksadminがありますので、piCal内で行う方が便利なはずです。

「一般設定」のオプションもかなり多めですが、それほど難しいものはない
と思います。以下に、比較的、理解しづらい「権限」について書いておきます。

「一般ユーザの権限」

   一般ユーザが登録したスケジュールを全体にすぐ公開する場合は、「登録
   可・承認不要」を選びます。

   登録はできるけれども、管理者権限を持ったメンバーによって「承認」さ
   れるまでは自分にしか見えなくする場合は、「登録可・要承認」を
   そもそも登録できなくするためには、「登録不可」を選びます。

   グループごとに細かく設定したいのでしたら「グループ毎に設定する」を選び、
   「各グループの権限設定」ページで個別に指定して下さい。

   なお、登録されたスケジュールは、承認済・未承認にかかわらず、登録者
   本人および管理者が編集できます。管理者が編集しても、登録者はそのま
   まですが、未承認スケジュールは自動的に承認済となります。


「ゲストの権限」

   基本的には、「一般ユーザの権限」と同じです。ただし、ゲストが登録し
   た未承認状態のスケジュールは、管理者にしか見えません。



●アップグレード方法

piCal 0.40以降で運用なさっている方は、他のモジュールと同様です

(1) 本パッケージをダウンロードし、解凍後、全てのファイルをmodules/以下
    に上書きアップロードします。
(2) システム管理->モジュール管理 からpiCalのアップデートを実行します


なお、0.4x または 0.5x から 0.6 以降にアップグレードする際には、さらに、


(3) piCalのモジュールメンテナンス画面にアクセスし、指示に従う


という手順が必要です。


なお、0.36以前のpiCalをお使いの方で、0.4以降にアップグレードするため
には、piCal 0.50 などを落として、テーブルを0.40形式にしてから、再度、
最新のpiCalへのアップグレード手順を踏んで下さい。



●FAQ

Q) なんだか表示される時間がずれているのですが…
A) piCalはその性格上、タイムゾーンの設定を正確にしないといけません。
   ありがちなのは、９時間ずれているというパターンで、この場合、あなた
   のアカウントのタイムゾーンがグリニッジ標準時のまま、という可能性を
   まず疑ってください。（管理者のタイムゾーンは、インストール後に特に
   設定しない限り、GMTのままです）
   また、デフォルトのタイムゾーンや、サーバのタイムゾーンが狂っている
   ことも考えられます。
   特に海外サーバをお使いの場合、そのサーバのタイムゾーンをきちんと設
   定する必要があります。



●著作権などの表記

piCalクラスおよびこのパッケージ内の各ファイルは、GPLに準拠します。
"see COPYING" という奴です。
当然、利用も配布も自由です。商用利用も構いませんし、改変も自由ですが、
GPLである以上、ソースの公開は必須条件となりますので、ご注意下さい。



●最後に

XOOPSって、すごいですね。PHPの貧弱な仕様でもこのレベルのものが作れる
のかと同じPHPプログラマーとして驚嘆しました。原開発者のOnokazuさんや、
XOOPSコミュニティの皆様（特にRyujiさん）には感謝します。

私自身、それなりに、XOOPSプログラミングにも慣れてきたとは思いますが、
まだまだおかしなことをやっている可能性もあります。もしお気づきの点が
ございましたら、ぜひご指摘下さい。

もちろん、バグの報告・改善案も大歓迎です。

