Hallo und guten Tag.

Sie haben gerade eine Partneranfrage auf {SITENAME} bekommen.
Die Anfrage wurde vom Mitglied {USER} mit der IP {IP} gesendet.
Folgende Informationen wurden hierbei eingegeben:  

Títle: {TITLE}
URL:    {URL}
Image: {IMAGE}
Description:
{DESCRIPTION}

Sie können diese Anfrage editieren, indem Sie auf den Link klicken:
{SITEURL}modules/xoopspartners/admin/index.php?op=partners_manage_add

-----------
{SITENAME} ({SITEURL}) 
