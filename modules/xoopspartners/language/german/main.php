<?php
// $Id: main.php,v 1.6 2003/03/20 12:39:41 w4z004 Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MD_EDIT', 'Ändern');
define('_MD_DELETE', 'Löschen');
define('_MD_JOIN', 'Partner werden:');
define('_MD_PARTNERS', 'Partner');
define('_MD_PARTNER', 'Partner:');
define('_MD_DESCRIPTION', 'Beschreibung:');
define('_MD_HITS', 'Aufrufe:');
define('_MD_NOPART', 'Keinen Partner gefunden.');
//file join.php
define('_MD_IMAGE', 'Bild:');
define('_MD_TITLE', 'Titel:');
define('_MD_URL', 'URL:');
define('_MD_SEND', 'Email abschicken');
define('_MD_ERROR1', '<center>FEHLER: Es ist ein Fehler aufgetreten. <a href="javascript:history.back(1)">Bitte versuchen Sie es erneut !</a></center>');
define('_MD_ERROR2', '<center>Die Bildgrösse ist grösser als 110x50. <a href="javascript:history.back(1)">Bitte versuchen Sie es mit einem anderen Bild !</a></center>');
define('_MD_ERROR3', '<center>Die Bilddatei existiert nicht. <a href="javascript:history.back(1)">Bitte versuchen Sie es mit einem anderen Bild !</a></center>');
define('_MD_GOBACK', '<a href="javascript:history.back(1)">Zurück!</a>');
define('_MD_NEWPARTNER', '%s Partner Anfragen');
define('_MD_SENDMAIL', 'Ihre Anfrage wurde dem Administrator zugestellt.<br /><a href="index.php">Zurück zur Hauptseite</a>');
?>
