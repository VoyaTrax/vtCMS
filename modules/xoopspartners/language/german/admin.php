<?php
// $Id: admin.php,v 1.6 2003/03/20 12:39:41 w4z004 Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MD_PARTNERADMIN', 'Administration XOOPS-Partner');
define('_MD_URL', 'URL:');
define('_MD_HITS', 'Aufrufe:');
define('_MD_IMAGE', 'Bild:');
define('_MD_TITLE', 'Titel:');
define('_MD_WEIGHT', 'Position:');
define('_MD_DESCRIPTION', 'Beschreibung:');
define('_MD_STATUS', 'Status:');
define('_MD_ACTIVE', 'Aktiv:');
define('_MD_INACTIVE', 'Inaktiv:');
define('_MD_REORDER', 'Sortieren');
define('_MD_UPDATED', 'Einstellungen Aktualisiert!');
define('_MD_NOTUPDATED', 'Konnte Einstellungen nicht aktualisieren!');
define('_MD_BESURE', 'Geben Sie bitte mindestens einen Titel, eine URL und eine Beschreibung an.');
define('_MD_NOEXITS', 'Bilddatei existiert nicht!');
define('_MD_ADDPARTNER', 'Hinzufügen');
define('_MD_EDITPARTNER', 'Ändern');
define('_MD_EDIT', 'Ändern');
define('_MD_DELETE', 'Löschen');
define('_MD_SUREDELETE', 'Sind Sie sicher, dass Sie diese Seite löschen wollen?');
define('_MD_YES', 'Ja');
define('_MD_NO', 'Nein');
define('_MD_IMAGE_ERROR', 'Bilddatei ist grösser als 110x50 pixel!');
define('_MD_PARTNERS_ADD','Partner hinzufügen');
define('_MD_AUTOMATIC_SORT', 'Automatisch sortieren');
?>
