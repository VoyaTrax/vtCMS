<?php
// $Id: blocks.php,v 1.5 2003/02/22 19:39:21 w4z004 Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MB_PARTNERS_PSPACE', 'Zwischen den Partnern Leerzeichen einfügen ?');
define('_MB_BRAND', 'Partner im Block anzeigen?');
define('_MB_BLIMIT', 'Maximale Einträge im Block: (0 = endlos)');
define('_MB_BSHOW', 'Im Block anzeigen:');
define('_MB_BORDER', 'Im Block sortieren nach:');
define('_MB_ID', 'ID:');
define('_MB_HITS', 'Aufrufe:');
define('_MB_TITLE', 'Titel:');
define('_MB_WEIGHT', 'Position:');
define('_MB_ASC', 'Aufsteigende Reihenfolge');
define('_MB_DESC', 'Absteigende Reihenfolge');
define('_MB_IMAGES', 'Bilder');
define('_MB_TEXT', 'Text Links');
define('_MB_BOTH', 'Beides');
define('_MB_FADE', 'Bild einblenden');
?>
