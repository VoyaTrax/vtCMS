<?php
// $Id: modinfo.php,v 1.5 2003/02/22 19:39:21 w4z004 Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

define('_MI_PARTNERS_NAME', 'Partner');
define('_MI_PARTNERS_DESC', 'Zeigt Parnerseiten in einem Block oder einem Modul');
define('_MI_PARTNERS_ADMIN', 'Partner bearbeiten');
define('_MI_PARTNERS_ADMIN_ADD', 'Partner hinzufügen');
define('_MI_ID', 'ID:');
define('_MI_HITS', 'Aufrufe:');
define('_MI_TITLE', 'Titel:');
define('_MI_WEIGHT', 'Position:');
define('_MI_RECLICK', 'Zeit bis Wiederholung:');
define('_MI_IMAGES', 'Bilder');
define('_MI_TEXT', 'Text Links');
define('_MI_BOTH', 'Beides');
define('_MI_MLIMIT', 'Maximale Einträge auf Hauptseite: (0 = unendlich)');
define('_MI_MSHOW', 'Auf Hauptseite anzeigen:');
define('_MI_MORDER', 'Auf Hauptseite sortiert nach:');
define('_MI_HOUR', '1 Stunde');
define('_MI_3HOURS', '3 Stunden');
define('_MI_5HOURS', '5 Stunden');
define('_MI_10HOURS', '10 Stunden');
define('_MI_DAY', '1 Tag');
?>
