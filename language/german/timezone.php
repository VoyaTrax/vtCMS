<?php
// $Id: timezone.php,v 1.7 2003/03/21 13:12:21 w4z004 Exp $
//%%%%%		Time Zone	%%%%
define('_TZ_GMTM12', '(GMT-12:00) Eniwetok, Kwajalein');
define('_TZ_GMTM11', '(GMT-11:00) Midway Insel, Samoa');
define('_TZ_GMTM10', '(GMT-10:00) Hawaii');
define('_TZ_GMTM9', '(GMT-9:00) Alaska');
define('_TZ_GMTM8', '(GMT-8:00) Pacific Time (USA &amp; Kanada)');
define('_TZ_GMTM7', '(GMT-7:00) Mountain Time (USA &amp; Kanada)');
define('_TZ_GMTM6', '(GMT-6:00) Central Time (USA &amp; Kanada), Mexico City');
define('_TZ_GMTM5', '(GMT-5:00) Eastern Time (USA &amp; Kanada), Bogota, Lima, Quito');
define('_TZ_GMTM4', '(GMT-4:00) Atlantic Time (Kanada), Caracas, La Paz');
define('_TZ_GMTM35', '(GMT-3:30) Neufundland');
define('_TZ_GMTM3', '(GMT-3:00) Brasilien, Buenos Aires, Georgetown');
define('_TZ_GMTM2', '(GMT-2:00) Mid-Atlantic');
define('_TZ_GMTM1', '(GMT-1:00) Azoren, Cape Verdische Inseln');
define('_TZ_GMT0', '(GMT) Greenwich Mean Time, London, Dublin, Lissabon, Casablanca, Monrovia');
define('_TZ_GMTP1', '(GMT+1:00) Amsterdam, Berlin, Rom, Kopenhagen, Brüssel, Madrid, Paris');
define('_TZ_GMTP2', '(GMT+2:00) Athen, Istanbul, Minsk, Helsinki, Jerusalem, Süd Afrika');
define('_TZ_GMTP3', '(GMT+3:00) Baghdad, Kuwait, Riyadh, Moskau, St. Petersburg');
define('_TZ_GMTP35', '(GMT+3:30) Teheran');
define('_TZ_GMTP4', '(GMT+4:00) Abu Dhabi, Muscat, Baku, Tbilisi');
define('_TZ_GMTP45', '(GMT+4:30) Kabul');
define('_TZ_GMTP5', '(GMT+5:00) Ekaterinburg, Islamabad, Karachi, Tashkent');
define('_TZ_GMTP55', '(GMT+5:30) Bombay, Kalkutta, Madras, Neu Delhi');
define('_TZ_GMTP6', '(GMT+6:00) Almaty, Dhaka, Colombo');
define('_TZ_GMTP7', '(GMT+7:00) Bangkok, Hanoi, Jakarta');
define('_TZ_GMTP8', '(GMT+8:00) Beijing, Perth, Singapur, Hong Kong, Urumqi, Taipei');
define('_TZ_GMTP9', '(GMT+9:00) Tokio, Seoul, Osaka, Sapporo, Yakutsk');
define('_TZ_GMTP95', '(GMT+9:30) Adelaide, Darwin');
define('_TZ_GMTP10', '(GMT+10:00) Brisbane, Canberra, Melbourne, Sydney, Guam, Vlasdiostok');
define('_TZ_GMTP11', '(GMT+11:00) Magadan, Solomon Inseln, Neu Caledonien');
define('_TZ_GMTP12', '(GMT+12:00) Auckland, Wellington, Fiji, Kamchatka, Marshall Insel');
?>
