<?php
// $Id: comment.php,v 1.7 2003/03/21 13:12:20 w4z004 Exp $
define('_CM_TITLE','Titel');
define('_CM_MESSAGE','Nachricht');
define('_CM_DOSMILEY','Smilies erlauben');
define('_CM_DOHTML','HTML Tags erlauben');
define('_CM_DOAUTOWRAP','Automatischer Zeilenumbruch');
define('_CM_DOXCODE','XOOPS Codes erlauben');
define('_CM_REFRESH','Aktualisieren');
define('_CM_PENDING','Anstehend');
define('_CM_HIDDEN','Versteckt');
define('_CM_ACTIVE','Aktiv');
define('_CM_STATUS','Status');
define('_CM_POSTCOMMENT','Kommentar posten');
define('_CM_REPLIES','Antworten');
define('_CM_PARENT','Parent');
define('_CM_TOP','Top');
define('_CM_BOTTOM','Bottom');
define('_CM_ONLINE','Online!');
define('_CM_POSTED','verfasst am'); // Posted date
define('_CM_UPDATED', 'Aktualisiert');
define('_CM_THREAD','Thema');
define('_CM_POSTER','Autor');
define('_CM_JOINED','Joined');
define('_CM_POSTS','Posts');
define('_CM_FROM','von');
define('_CM_COMDELETED', 'Kommentar(e) gelöscht.');
define('_CM_COMDELETENG', 'Kommentar konnte nicht gelöscht werden.');
define('_CM_DELETESELECT' , 'Alle Kommentare dazu löschen?');
define('_CM_DELETEONE' , 'Nein, nur diesen Kommetar löschen');
define('_CM_DELETEALL', 'Ja, alle löschen');
define('_CM_THANKSPOST', 'Danke für Ihren Beitrag!');
define('_CM_NOTICE', 'Wir sind nicht für die Inhalte der Kommentare verantwortlich. Dies obliegt dem Autor des jeweiligen Kommentars!');
define('_CM_COMRULES','Kommentar Regeln');
define('_CM_COMAPPROVEALL','Kommentare werden immer akzeptiert');
define('_CM_COMAPPROVEUSER','Kommentare von registrierten Usern werden immer akzeptiert');
define('_CM_COMAPPROVEADMIN','Alle Kommentare müssen vom Administrator freigegeben werden');
define('_CM_COMANONPOST','Unregistrierten Besuchern erlauben, Kommentare zu verfassen?');
define('_CM_COMNOCOM','Kommentare abschalten');
?>
