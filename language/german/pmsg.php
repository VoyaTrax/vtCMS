<?php
// $Id: pmsg.php,v 1.7 2003/03/21 13:12:20 w4z004 Exp $
//%%%%%%	File Name readpmsg.php 	%%%%%
define('_PM_DELETED','Nachricht(en) gelöscht');
define('_PM_PRIVATEMESSAGE','Private Nachricht');
define('_PM_INBOX','Posteingang');
define('_PM_FROM','Von');
define('_PM_YOUDONTHAVE','Sie haben keine privaten Nachrichten');
define('_PM_FROMC','Von: ');
define('_PM_SENTC','Gesendet: '); // The date of message sent
define('_PM_PROFILE','Profil');

// %s is a username
define('_PM_PREVIOUS','Vorherige Nachricht');
define('_PM_NEXT','Nächste Nachricht');

//%%%%%%	File Name pmlite.php 	%%%%%
define('_PM_SORRY','Sorry! Sie sind kein registrierter User.');
define('_PM_REGISTERNOW','Registrieren Sie sich jetzt!');
define('_PM_GOBACK','Zurück');
define('_PM_USERNOEXIST','Der gewählte User existiert nicht in unserer Datenbank.');
define('_PM_PLZTRYAGAIN','Bitte überprüfen Sie den Namen und versuchen Sie es erneut.');
define('_PM_MESSAGEPOSTED','Ihre Nachricht wurde abgesendet.');
define('_PM_CLICKHERE','Sie können hier klicken, um Ihre privaten Nachrichten zu lesen');
define('_PM_ORCLOSEWINDOW','Oder klicken Sie hier, um dieses Fenster zu schliessen.');
define('_PM_USERWROTE','%s schrieb:');
define('_PM_TO','An: ');
define('_PM_SUBJECTC','Betreff: ');
define('_PM_MESSAGEC','Nachricht: ');
define('_PM_CLEAR','Löschen');
define('_PM_CANCELSEND','Senden abbrechen');
define('_PM_SUBMIT','Abschicken');

//%%%%%%	File Name viewpmsg.php 	%%%%%
define('_PM_SUBJECT','Betreff');
define('_PM_DATE','Datum');
define('_PM_NOTREAD','Nicht gelesen');
define('_PM_SEND','Gesendet');
define('_PM_DELETE','Löschen');
define('_PM_REPLY', 'Antworten');
define('_PM_PLZREG','Bitte registrieren Sie sich, um private Nachrichten senden zu können!');

define('_PM_ONLINE', 'Online');
?>
