<?php
// $Id: misc.php,v 1.7 2003/03/21 13:12:20 w4z004 Exp $
define('_MSC_YOURNAMEC','Ihr Name: ');
define('_MSC_YOUREMAILC','Ihre eMail: ');
define('_MSC_FRIENDNAMEC','Name des Freundes: ');
define('_MSC_FRIENDEMAILC','eMail des Freundes: ');
define('_MSC_RECOMMENDSITE','Empfehlen Sie diese Site einem Freund');
// %s is your site name
define('_MSC_INTSITE','Interessante Site: %s');
define('_MSC_REFERENCESENT','Die Empfehlung für unsere Site wurde an Ihren Freund gesendet. Danke!');
define('_MSC_ENTERYNAME','Bitte geben Sie Ihren Namen ein');
define('_MSC_ENTERFNAME','Bitte geben Sie den Namen Ihres Freundes ein');
define('_MSC_ENTERFMAIL','Bitte geben Sie die eMail-Adresse Ihres Freundes ein');
define('_MSC_NEEDINFO','Sie müssen die erforderlichen Informationen eingeben!');
define('_MSC_INVALIDEMAIL1','Die von Ihnen eingetragene eMail-Adresse ist nicht gültig.');
define('_MSC_INVALIDEMAIL2','Bitte überprüfen Sie die Adresse und versuchen Sie es erneut.');

define('_MSC_AVAVATARS','Verfügbare Avatare');

define('_MSC_SMILIES','Smilies');
define('_MSC_CLICKASMILIE','Klicken Sie auf ein Smily, um es in Ihre Nachricht einzufügen.');
define('_MSC_CODE','Code');
define('_MSC_EMOTION','Emotion');
?>
