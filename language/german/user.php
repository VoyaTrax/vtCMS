<?php
// $Id: user.php,v 1.7 2003/03/21 13:12:21 w4z004 Exp $
//%%%%%%		File Name user.php 		%%%%%
define('_US_NOTREGISTERED','Nicht registriert? <a href=register.php>Bitte hier klicken</a>.');
define('_US_LOSTPASSWORD','Passwort vergessen?');
define('_US_NOPROBLEM','Kein Problem. Gweben Sie einfach die eMail-Adresse ein, mit der Sie sich registriert haben.');
define('_US_YOUREMAIL','Ihre eMail-Adresse: ');
define('_US_SENDPASSWORD','Passwort senden');
define('_US_LOGGEDOUT','Sie sind nun ausgeloggt');
define('_US_THANKYOUFORVISIT','Danke für Ihren Besuch!');
define('_US_INCORRECTLOGIN','Ungültiger Login!');
define('_US_LOGGINGU','Schön, dass Sie wieder da sind, %s.');

// 2001-11-17 ADD
define('_US_NOACTTPADM','Der gewählte User wurde deaktiviert oder nocht nicht aktiviert.<br />Bitte wenden Sie sich für nähere Infos an den Webmaster.');
define('_US_ACTKEYNOT','Activierungs-Schlüssel nicht korrekt!');
define('_US_ACONTACT','Der gewählte Account ist bereits aktiviert!');
define('_US_ACTLOGIN','Ihr Account wurde aktiviert. Bitte melden Sie sich nun mit Ihrem gewählten Passwort an.');
define('_US_NOPERMISS','Sorry, Sie haben keine Berechtigung, diese Aktion auszuführen!');
define('_US_SURETODEL','Sind Sie sicher, dass Sie Ihren Account löschen wollen??');
define('_US_REMOVEINFO','Dies wird alle Ihre Informationen aus unserer Datenbank entfernen.');
define('_US_BEENDELED','Ihr Account wurde gelöscht.');
//

//%%%%%%		File Name register.php 		%%%%%
define('_US_USERREG','User-Registrierung');
define('_US_NICKNAME','Username');
define('_US_EMAIL','eMail');
define('_US_ALLOWVIEWEMAIL','Anderen Usern meine eMail-Adresse sichtbar machen');
define('_US_WEBSITE','Website');
define('_US_TIMEZONE','Zeitzone');
define('_US_AVATAR','Avatar');
define('_US_VERIFYPASS','Passwort überprüfen');
define('_US_SUBMIT','Absenden');
define('_US_USERNAME','Username');
define('_US_FINISH','Abschliessen');
define('_US_REGISTERNG','Neuer User konnte nicht registriert werden.');
define('_US_MAILOK','Möchten Sie Administratoren/Moderatoren<br />gestatten,Ihnen eMails zu senden?');
define('_US_DISCLAIMER','Disclaimer');
define('_US_IAGREE','Ich stimme dem Disclaimer zu.');
define('_US_UNEEDAGREE', 'Sorry, Sie müssen unserem disclaimer zustimmen, um registriert zu werden.');
define('_US_NOREGISTER','Sorry, neue User-Registrierungen werden derzeit nicht angenommen.');


// %s is username. This is a subject for email
define('_US_USERKEYFOR','User Aktivierungs-Schlüssel für %s');

define('_US_YOURREGISTERED','Sie sind nun registriert. Eine eMail mit Ihrem Aktivierungs-Schlüssel wurde an die von Ihnen registrierte eMail-Adresse gesendet. Bitte folgen Sie den Anweisungen in dieser Mail, um Ihren Account zu aktivieren. ');
define('_US_YOURREGMAILNG','Sie sind nun registriert. Leider konnten wir Ihnen aufgrund von technischen Problemen auf unserem Server nicht die Aktivierungs-Mail senden. Wir bedauern diese Unannehmlichkeit. Bitte informieren Sie den Webmaster per eMail über diesen Umstand.');
define('_US_YOURREGISTERED2','Sie sind nun registriert. Bitte warten Sie auf die Aktivierung Ihres Accounts durch die Administratoren. Sie erhalten eine eMail, wenn Ihr Account aktiviert wurde. Dies kann eine Weile dauern, daher bitten wir Sie um Geduld.');

// %s is your site name
define('_US_NEWUSERREGAT','Neue User-Registrierung bei %s');
// %s is a username
define('_US_HASJUSTREG','%s hat sich gerade registriert!');

define('_US_INVALIDMAIL','FEHLER: Ungültige eMail-Adresse');
define('_US_EMAILNOSPACES','FEHLER: eMail-Adressen enthalten keine Leerzeichen.');
define('_US_INVALIDNICKNAME','FEHLER: Ungültiger Username');
define('_US_NICKNAMETOOLONG','Username ist zu lang. Er muss weniger als %s Zeichen lang sein.');
define('_US_NICKNAMETOOSHORT','Username ist zu kurz. Er muss mehr als  %s Zeichen lang sein.');
define('_US_NAMERESERVED','FEHLER: Name ist reserviert.');
define('_US_NICKNAMENOSPACES','Usernamen dürfen keine Leerzeichen enthalten.');
define('_US_NICKNAMETAKEN','FEHLER: Username bereits vergeben.');
define('_US_EMAILTAKEN','FEHLER: eMail-Adresse wurde bereits registriert.');
define('_US_ENTERPWD','FEHLER: Sie müssen ein Passwort angeben.');
define('_US_SORRYNOTFOUND','Sorry, keine passende User-Information gefunden.');




// %s is your site name
define('_US_NEWPWDREQ','Passwort-Anforderung bei %s');
define('_US_YOURACCOUNT', 'Ihr account bei %s');

define('_US_MAILPWDNG','mail_password: User-Eintrag konnte nicht aktualisiert werden. Bitte wenden Sie sich an den Administrator.');

// %s is a username
define('_US_PWDMAILED','Passwort für %s gesendet.');
define('_US_CONFMAIL','Bestätigungs-Mail für %s gesendet.');
define('_US_ACTVMAILNG', 'Benachrichtigungs-Mail konnte nicht an %s gesendet werden.');
define('_US_ACTVMAILOK', 'Benachrichtigungs-Mail an %s gesendet.');

//%%%%%%		File Name userinfo.php 		%%%%%
define('_US_SELECTNG','Kein User ausgewählt! Bitte gehen Sie zurück und versuchen Sie es erneut.');
define('_US_PM','PM');
define('_US_ICQ','ICQ');
define('_US_AIM','AIM');
define('_US_YIM','YIM');
define('_US_MSNM','MSNM');
define('_US_LOCATION','Wohnort');
define('_US_OCCUPATION','Beruf');
define('_US_INTEREST','Interessen');
define('_US_SIGNATURE','Signatur');
define('_US_EXTRAINFO','zusätzliche Infos');
define('_US_EDITPROFILE','Profil editieren');
define('_US_LOGOUT','Logout');
define('_US_INBOX','Posteingang');
define('_US_MEMBERSINCE','Mitglied seit');
define('_US_RANK','Rang');
define('_US_POSTS','Kommentare/Beiträge');
define('_US_LASTLOGIN','Letzter Login');
define('_US_ALLABOUT','Alles über %s');
define('_US_STATISTICS','Statistiken');
define('_US_MYINFO','Meine Info');
define('_US_BASICINFO','Grundinformation');
define('_US_MOREABOUT','Mehr über mich');
define('_US_SHOWALL','Zeige alle(s)');

//%%%%%%		File Name edituser.php 		%%%%%
define('_US_PROFILE','Profil');
define('_US_REALNAME','Real-Name');
define('_US_SHOWSIG','Meine Signatur immer anhängen');
define('_US_CDISPLAYMODE','Kommentar Darstellungs-Modus');
define('_US_CSORTORDER','Kommentar Sortierung');
define('_US_PASSWORD','Passwort');
define('_US_TYPEPASSTWICE','(geben Sie das neue Passwort ZWEImal ein, um es zu ändern)');
define('_US_SAVECHANGES','Änderungen speichern');
define('_US_NOEDITRIGHT','Sorry, Sie haben keine Berechtigung, die Info dieses Users zu ändern..');
define('_US_PASSNOTSAME','Passworte sind unterschiedlich. Sie müssen identisch sein!');
define('_US_PWDTOOSHORT','Sorry, Ihr Passwort muss mindestens <b>%s</b> Zeichen lang sein.');
define('_US_PROFUPDATED','Ihr Profil wurde aktualisiert!');
define('_US_USECOOKIE','Usernamen für 1 Jahr in einem Cookie speichern');
define('_US_NO','Nein');
define('_US_DELACCOUNT','Account löschen');
define('_US_MYAVATAR', 'Mein Avatar');
define('_US_UPLOADMYAVATAR', 'Avatar hochladen');
define('_US_MAXPIXEL','Max. Pixel');
define('_US_MAXIMGSZ','Max. Bild Grösse (Bytes)');
define('_US_SELFILE','Datei auswählen');
define('_US_OLDDELETED','Ihr alter Avatar wird dann gelöscht!');
define('_US_CHOOSEAVT', 'Avatar aus einer Liste auswählen');

define('_US_PRESSLOGIN', 'Button zum Einloggen anklicken');
?>
