<?php
// $Id: notification.php,v 1.2 2003/03/21 13:12:20 w4z004 Exp $

// RMV-NOTIFY

// Text for various templates...

define ('_NOT_NOTIFICATIONOPTIONS', 'Benachrichtigungs-Optionen');
define ('_NOT_UPDATENOW', 'Jetzt aktualisieren');
define ('_NOT_UPDATEOPTIONS', 'Benachrichtigungs-Optionen aktualisieren');

define ('_NOT_CANCEL', 'Abbruch');
define ('_NOT_CLEAR', 'Zurücksetzen (Clear)');
define ('_NOT_DELETE', 'Löschen');
define ('_NOT_CHECKALL', 'Alle auswählen');
define ('_NOT_MODULE', 'Modul');
define ('_NOT_CATEGORY', 'Kategorie');
define ('_NOT_ITEMID', 'ID');
define ('_NOT_ITEMNAME', 'Name');
define ('_NOT_EVENT', 'Ereignis');
define ('_NOT_EVENTS', 'Ereignisse');
define ('_NOT_ACTIVENOTIFICATIONS', 'Aktive Benachrichtigungen');
define ('_NOT_NAMENOTAVAILABLE', 'Name nicht verfügbar');
define ('_NOT_DELETINGNOTIFICATIONS', 'Benachrichtigungen werden gelöscht');
define ('_NOT_DELETESUCCESS', 'Benachrichtigung(en) erfolgreich gelöscht.');
define ('_NOT_UPDATEOK', 'Benachrichtigungs-Optionen aktualisiert');
define ('_NOT_NOTIFICATIONMETHODIS', 'Benachrichtigungs-Methode ist');
define ('_NOT_EMAIL', 'eMail');
define ('_NOT_PM', 'private Nachricht');
define ('_NOT_CHANGE', 'Ändern');

// Text for module config options

define ('_NOT_ENABLE', 'Einschalten');
define ('_NOT_NOTIFICATION', 'Benachrichtigung');

define ('_NOT_CONFIG_ENABLED', 'Benachrichtigung einschalten');
define ('_NOT_CONFIG_ENABLEDDSC', 'Dieses Modul ermöglicht es Usern, sich über verschiedene Ereignisse benachrichtigen zu lassen.  Wählen Sie "ja", um diese Funktion zu aktivieren.');

define ('_NOT_CONFIG_EVENTS', 'Spezielle Ereignisse aktivieren');
define ('_NOT_CONFIG_EVENTSDSC', 'Wählen Sie, welche Benachrichtigungs-Ereignisse Ihre User auswählen können.');

define ('_NOT_CONFIG_ENABLE', 'Benachrichtigung aktivieren');
define ('_NOT_CONFIG_ENABLEDSC', 'Dieses Modul ermöglicht es Usern, über verschiedene Ereignisse benachrichtigt zu werden. Wählen Sie, ob den Usern die Benachrichtigungs-Optionen in einem Block (Block-style), innerhalb des Moduls (Inline-style), oder auf beide Arten angezeigt werden sollen.  Für die block-style Benachrichtigung muss der Benachrichtigungs-Block für dieses Modul aktiviert sein.');
define ('_NOT_CONFIG_DISABLE', 'Benachrichtigung abschalten');
define ('_NOT_CONFIG_ENABLEBLOCK', 'Nur Block-style ermöglichen');
define ('_NOT_CONFIG_ENABLEINLINE', 'Nur Inline-style ermöglichen');
define ('_NOT_CONFIG_ENABLEBOTH', 'Benachrichtigung aktivieren (beide styles)');

// For notification about comment events

define ('_NOT_COMMENT_NOTIFY', 'Kommentar hinzugefügt');
define ('_NOT_COMMENT_NOTIFYCAP', 'Benachrichtigen sie mich, wenn ein hierzu neuer Kommentar verfasst wurde.');
define ('_NOT_COMMENT_NOTIFYDSC', 'Sie erhalten eine Benachrichtigung, wenn hierzu ein neuer Kommentar verfasst (oder freigegeben) wurde.');
define ('_NOT_COMMENT_NOTIFYSBJ', '[{X_NOTIFY_MODULE}] Kommentar zu {X_ITEM_TYPE} "{X_ITEM_TITLE}" [AUTO-NOTIFY] hinzugefügt');

define ('_NOT_COMMENTSUBMIT_NOTIFY', 'Kommentar eingereicht');
define ('_NOT_COMMENTSUBMIT_NOTIFYCAP', 'Benachrichtigen sie mich, wenn ein neuer Kommentar hierzu verfasst wurde (auf Freigabe wartend).');
define ('_NOT_COMMENTSUBMIT_NOTIFYDSC', 'Sie erhalten eine Benachrichtigung, wenn ein neuer Kommentar hierzu verfasst wurde (auf Freigabe wartend).');
define ('_NOT_COMMENTSUBMIT_NOTIFYSBJ', '[{X_NOTIFY_MODULE}] Kommentar zu {X_ITEM_TYPE} "{X_ITEM_TITLE}" [AUTO-NOTIFY] hinzugefügt');

// For notification bookmark feature
// (Not really notification, but easy to do with this module)

define ('_NOT_BOOKMARK_NOTIFY', 'Bookmark');
define ('_NOT_BOOKMARK_NOTIFYCAP', 'Bookmark setzen (keine Benachrichtigung).');
define ('_NOT_BOOKMARK_NOTIFYDSC', 'Dies beobachten, ohne Benachrichtigungen zu erhalten.');

// For user profile
// FIXME: These should be reworded a little...

define ('_NOT_NOTIFYMETHOD', 'Benachrichtungs-Methode: Wenn Sie z.B. ein Forum beobachten, wie möchten Sie über Aktualisierungen benachrichtigt werden?');
define ('_NOT_METHOD_EMAIL', 'eMail (eMail-Adresse in meinem Profil benutzen)');
define ('_NOT_METHOD_PM', 'Private Nachricht');
define ('_NOT_METHOD_DISABLE', 'Vorübergehend deaktivieren');

define ('_NOT_NOTIFYMODE', 'Voreingesteller Benachrichtigungs-Modus');
define ('_NOT_MODE_SENDALWAYS', 'Benachrichtigen Sie mich über alle gewählten Aktualisierungen');
define ('_NOT_MODE_SENDONCE', 'Benachrichtigen Sie mich nur EINmalig');
define ('_NOT_MODE_SENDONCEPERLOGIN', 'Benachrichtigen Sie mich nur EINmalig, danach bis zu meinem nächsten Log-In nicht mehr.');

?>
