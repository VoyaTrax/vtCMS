<?php
// $Id: admin.php,v 1.7 2003/03/21 13:12:19 w4z004 Exp $
//%%%%%%	File Name  admin.php 	%%%%%
define('_AD_NORIGHT','Sie haben nicht die erforderlichen Zugriffsrechte für diesen Bereich');
define('_AD_ACTION','Aktion');
define('_AD_EDIT','Editieren');
define('_AD_DELETE','Löschen');
define('_AD_LASTTENUSERS','Die letzten 10 registrierten User');
define('_AD_NICKNAME','Nickname');
define('_AD_EMAIL','eMail');
define('_AD_AVATAR','Avatar');
define('_AD_REGISTERED','Registriert am'); //Registered Date
define('_AD_PRESSGEN', 'Dies ist das erste Mal, dass Sie auf den Admin-Bereich zugreifen. Bitte Button zum Fortsetzen anklicken.');
define('_AD_LOGINADMIN', 'Sie werden eingeloggt...');
?>
