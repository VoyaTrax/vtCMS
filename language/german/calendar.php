<?php
// $Id: calendar.php,v 1.2 2003/03/21 13:12:20 w4z004 Exp $
//%%%%%		Time Zone	%%%%
define('_CAL_SUNDAY', 'Sonntag');
define('_CAL_MONDAY', 'Montag');
define('_CAL_TUESDAY', 'Dienstag');
define('_CAL_WEDNESDAY', 'Mittwoch');
define('_CAL_THURSDAY', 'Donnerstag');
define('_CAL_FRIDAY', 'Freitag');
define('_CAL_SATURDAY', 'Samstag');
define('_CAL_JANUARY', 'Januar');
define('_CAL_FEBRUARY', 'Februar');
define('_CAL_MARCH', 'März');
define('_CAL_APRIL', 'April');
define('_CAL_MAY', 'Mai');
define('_CAL_JUNE', 'Juni');
define('_CAL_JULY', 'Juli');
define('_CAL_AUGUST', 'August');
define('_CAL_SEPTEMBER', 'September');
define('_CAL_OCTOBER', 'Oktober');
define('_CAL_NOVEMBER', 'November');
define('_CAL_DECEMBER', 'Dezember');
define('_CAL_TGL1STD', 'Ersten Tag der Woche einstellen');
define('_CAL_PREVYR', 'Letztes Jahr (hold for menu)');
define('_CAL_PREVMNTH', 'Letzter Monat (hold for menu)');
define('_CAL_GOTODAY', 'Zeige HEUTE');
define('_CAL_NXTMNTH', 'Nächster Monat (hold for menu)');
define('_CAL_NEXTYR', 'Nächstes Jahr (hold for menu)');
define('_CAL_SELDATE', 'Datum auswählen');
define('_CAL_DRAGMOVE', 'Ziehen zum Bewegen');
define('_CAL_TODAY', 'Heute');
define('_CAL_DISPM1ST', 'Zeige Montag zuerst');
define('_CAL_DISPS1ST', 'Display Sonntag zuerst');
?>
