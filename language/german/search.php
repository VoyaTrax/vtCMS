<?php
// $Id: search.php,v 1.7 2003/03/21 13:12:21 w4z004 Exp $
//%%%%%%	File Name search.php 	%%%%%
define('_SR_SEARCH','Suche');
define('_SR_PLZENTER','Bitte geben Sie alle erforderlichen Daten ein!');
define('_SR_SEARCHRESULTS','Such-Ergebnisse');
define('_SR_NOMATCH','Keine Treffer für Ihre Anfrage gefunden');
define('_SR_FOUND','<b>%s</b> Treffer gefunden');
define('_SR_SHOWING','(Zeige %d - %d)');
define('_SR_ANY','Irgendein (ODER)');
define('_SR_ALL','Alle (UND)');
define('_SR_EXACT','Exakte Übereinstimmung');
define('_SR_SHOWALLR','Alle Resultate anzeigen');
define('_SR_NEXT','Nächste >>');
define('_SR_PREVIOUS','<< Vorherige');
define('_SR_KEYWORDS','Schlüsselwort(e)');
define('_SR_TYPE','Typ');
define('_SR_SEARCHIN','Suche in');
define('_SR_KEYTOOSHORT', 'Suchbegriffe müssen mindestens <b>%s</b> Zeichen lang sein');
define('_SR_KEYIGNORE', 'Suchbegriffe kürzer als <b>%s</b> Zeichen werden ignoriert');
define('_SR_SEARCHRULE', 'Such Regel');
?>
