Hello {X_UNAME},

Vielen Dank f�r die Anmeldung auf {SITENAME}. Als registriertes Mitglied k�nnen Sie:
- Private Nachrichten an andere registrierte Mitglieder schicken,
- am Forum teilnehmen
- die neuesten Nachrichten studieren
- und, vieles, vieles mehr....

Ihre E-Mail Adresse ({X_UEMAIL}) wurde zur Registrierung verwendet.

Um ein Mitglied auf {SITENAME} zu werden, brauchen Sie nur noch den unten aufgef�hrten
Link klicken.

{X_UACTLINK}

-----------
Best Regards
{SITENAME}
({SITEURL}) 
{ADMINMAIL}