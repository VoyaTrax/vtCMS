Hallo {X_UNAME},

ein Besucher von {IP} hat gerade ein neues Passwort angefordert.
Hier sind Ihre Login-Daten von {SITENAME}.

Username: {X_UNAME}
Neues Passwort: {NEWPWD}

Nach dem Login k�nnen Sie Ihr Passwort auf {SITEURL}user.php �ndern.

Sollten Sie kein neues Passwort angefordert haben, ist die sicherlich ein
Versehen. Nur Sie erhalten diese Nachricht.

Wenn dies ein Fehler sein sollte, bitten wir, diesen zu entschuldigen, aber 
bitte nutzten Sie die neuen Login-Daten um sich anzumelden (!).

-----------
{SITENAME} ({SITEURL}) 
webmaster
{ADMINMAIL}