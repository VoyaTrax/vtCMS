RMV-NOTIFY

Hallo {X_UNAME},

Ein Ereignis {X_NOTIFY_EVENT} passierte in {X_ITEM_TYPE} '{X_ITEM_TILE}' des Moduls {X_NOTIFY_MODULE} welches Sie gerade auf unserer Seite beobachten.

Um das Ereignis zu sehen klicken Sie hier:
{X_COMMENT_URL}

-----------

Sie haben derzeit dieses Thema abonniert.

Wenn dies ein Fehler ist, und Sie keine neuen Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{UNSUBSCRIBE_URL}

Bitte nicht auf diese Mail antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Webmaster
{X_ADMINMAIL}