RMV-NOTIFY

Hallo {X_UNAME},

Gerade wurde ein noch nicht gepr�fter Kommentar im {X_NOTIFY_MODULE}-Modul unserer Site hinzugef�gt.

Um den Kommentar zu sehen klicken Sie hier:
{X_COMMENT_URL}

-----------

Sie haben derzeit dieses Thema abonniert.

Wenn dies ein Fehler ist, und Sie keine neuen Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{UNSUBSCRIBE_URL}

Bitte nicht auf diese Mail antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Webmaster
{X_ADMINMAIL}
