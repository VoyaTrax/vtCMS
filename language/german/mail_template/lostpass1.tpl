Hallo {X_UNAME},


ein Benutzer von {IP} hat gerade ein neues Passwort f�r Ihren Account auf {SITENAME}
angefordert.

Sie erhalten Ihr neues Passwort, wenn Sie auf den unten aufgef�hrten Link klicken:

{NEWPWD_LINK}

Sollten Sie kein neues Passwort angefordert haben, ignorieren Sie einfach diese Mail.


-----------
{SITENAME} ({SITEURL}) 
webmaster
{ADMINMAIL}