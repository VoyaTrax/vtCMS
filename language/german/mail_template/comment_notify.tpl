RMV-NOTIFY

Hallo {X_UNAME},

Gerade wurde ein Kommentar in {X_ITEM_TYPE} '{X_ITEM_TITLE}' welches du im {X_NOTIFY_MODULE}-Modul unserer Site beobachtest, hinzugefügt.

Um den Kommentar zu sehen klicken Sie hier:
{X_COMMENT_URL}

-----------

Sie haben derzeit dieses Thema abonniert.

Wenn dies ein Fehler ist, und Sie keine neuen Nachrichten dieser Art mehr empfangen wollen, folgen Sie diesem Link:
{UNSUBSCRIBE_URL}

Bitte nicht auf diese Mail antworten.

-----------
{X_SITENAME} ({X_SITEURL}) 
Webmaster
{X_ADMINMAIL}
