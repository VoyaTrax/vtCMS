Hallo {X_UNAME},

Das neue Mitglied {USERNAME} ({USEREMAIL}) hat sich gerade auf {SITENAME} 
registriert.

Wenn Sie unten auf den Link klicken, wird dieser Account aktiviert.

{USERACTLINK}

-----------
{SITENAME} ({SITEURL}) 
webmaster
{ADMINMAIL}