<?php
// $Id: global.php,v 1.9 2003/03/21 13:12:20 w4z004 Exp $
//%%%%%%	File Name mainfile.php 	%%%%%
define('_PLEASEWAIT','Bite warten...');
define('_FETCHING','Lade...');
define('_TAKINGBACK','Wir bringen Sie dorthin zurück, wo Sie waren....');
define('_LOGOUT','Logout');
define('_SUBJECT','Betreff');
define('_MESSAGEICON','Message Icon');
define('_COMMENTS','Kommentare');
define('_POSTANON','Anonym schreiben');
define('_DISABLESMILEY','Smilies deaktivieren');
define('_DISABLEHTML','HTML deaktivieren');
define('_PREVIEW','Vorschau');

define('_GO','Los!');
define('_NESTED','Nested');
define('_NOCOMMENTS','Keine Kommentare');
define('_FLAT','Flach');
define('_THREADED','Threaded');
define('_OLDESTFIRST','Älteste zuerst');
define('_NEWESTFIRST','Neueste zuerst');
define('_MORE','mehr...');
define('_MULTIPAGE','Um Ihren Artikel auf mehrere Seiten zu verteilen, fügen Sie das Wort <font color=red>[pagebreak]</font> (mit Klammern!) in den Artikel ein.');
define('_IFNOTRELOAD','Wenn die Seite nicht automatisch neu lädt, so klicken Sie bitte <a href=\'%s\'>HIER</a>');
define('_WARNINSTALL2','WARNUNG: Verzeichnis %s existiert auf Ihrem Server. <br />Bitte entfernen Sie dieses Verzeichnis aus Sicherheitsgründen.');
define('_WARNINWRITEABLE','WARNUNG: Datei %s ist vom Server beschreibbar. <br />Bitte ändern Sie die Zugriffsrechte (Permissions) dieser Datei aus Sicherheitsgründen.<br /> in Unix (444), in Win32 (read-only)');

//%%%%%%	File Name themeuserpost.php 	%%%%%
define('_PROFILE','Profil');
define('_POSTEDBY','Verfasst von');
define('_VISITWEBSITE','Website besuchen');
define('_SENDPMTO','Private Nachricht an %s senden');
define('_SENDEMAILTO','eMail an %s senden');
define('_ADD','Hinzufügen');
define('_REPLY','Antworten');
define('_DATE','Datum');   // Posted date

//%%%%%%	File Name admin_functions.php 	%%%%%
define('_MAIN','Main');
define('_MANUAL','Manual');
define('_INFO','Info');
define('_CPHOME','Control Panel Home');
define('_YOURHOME','Home Page');

//%%%%%%	File Name misc.php (who's-online popup)	%%%%%
define('_WHOSONLINE','Wer ist online');
define('_GUESTS', 'Gäste');
define('_MEMBERS', 'Mitglieder');
define('_ONLINEPHRASE','<b>%s</b> User online');
define('_ONLINEPHRASEX','<b>%s</b> User im Bereich <b>%s</b>');
define('_CLOSE','Schliessen');  // Close window

//%%%%%%	File Name module.textsanitizer.php 	%%%%%
define('_QUOTEC','Zitat:');

//%%%%%%	File Name admin.php 	%%%%%
define('_NOPERM','Sorry, Sie haben keine Zugriffsberechtigung für diesen Bereich.');

//%%%%%		Common Phrases		%%%%%
define('_NO','Nein');
define('_YES','Ja');
define('_EDIT','Editieren');
define('_DELETE','Löschen');
define('_SUBMIT','Absenden');
define('_MODULENOEXIST','Gewähltes Modul existiert nicht!');
define('_ALIGN','Ausrichten');
define('_LEFT','Links');
define('_CENTER','Zentriert');
define('_RIGHT','Rechts');
define('_FORM_ENTER', 'Bitte eingeben %s');
// %s represents file name
define('_MUSTWABLE','Datei %s muss vom Server beschreibbar sein!');
// Module info
define('_PREFERENCES', 'Einstellungen');
define('_VERSION', 'Version');
define('_DESCRIPTION', 'Beschreibung');
define('_ERRORS', 'Fehler');
define('_NONE', 'Keine');
define('_ON','an');
define('_READS','reads');
define('_WELCOMETO','Willkommen bei %s');
define('_SEARCH','Suche');
define('_ALL', 'Alle');
define('_TITLE', 'Titel');
define('_OPTIONS', 'Optionen');
define('_QUOTE', 'Zitat');
define('_LIST', 'Liste');
define('_LOGIN','User Login');
define('_USERNAME','Username: ');
define('_PASSWORD','Passwort: ');
define('_SELECT','Auswählen');
define('_IMAGE','Bild');
define('_SEND','Senden');
define('_CANCEL','Abbruch');
define('_ASCENDING','Aufsteigende Sortierung');
define('_DESCENDING','Absteigende Sortierung');
define('_BACK', 'Zurück');

/* Image manager */
define('_IMGMANAGER','Bilder-Manager');
define('_NUMIMAGES', '%s Bilder');
define('_ADDIMAGE','Bilddatei hinzufügen');
define('_IMAGENAME','Name:');
define('_IMGMAXSIZE','Max. erlaubte Grösse (kb):');
define('_IMGMAXWIDTH','Max. erlaubte Breite (Pixel):');
define('_IMGMAXHEIGHT','Max. erlaubte Höhe (Pixel):');
define('_IMAGECAT','Kategorie:');
define('_IMAGEFILE','Bilddatei:');
define('_IMGWEIGHT','Sortierung im Bilder-Manager:');
define('_IMGDISPLAY','Dieses Bild anzeigen?');
define('_IMAGEMIME','MIME Typ:');
define('_FAILFETCHIMG', 'Fehler beim Upload der Datei %s');
define('_FAILSAVEIMG', 'Bild %s konnte nicht in der Datenbank gespeichert werden');
define('_NOCACHE', 'Kein Cache');
define('_CLONE', 'Clone');

//%%%%%	File Name class/xoopsform/formmatchoption.php 	%%%%%
define('_STARTSWITH', 'Startet mit');
define('_ENDSWITH', 'Endet mit');
define('_MATCHES', 'Treffer');
define('_CONTAINS', 'Enthält');

//%%%%%%	File Name commentform.php 	%%%%%
define('_REGISTER','Registrieren');

//%%%%%%	File Name xoopscodes.php 	%%%%%
define('_SIZE','GRÖSSE');  // font size
define('_FONT','SCHRIFT');  // font family
define('_COLOR','FARBE');  // font color
define('_EXAMPLE','BEISPIEL');
define('_ENTERURL','Geben Sie die URL für den Link ein, den Sie einfügen wollen:');
define('_ENTERWEBTITLE','Geben Sie den Titel der Website an:');
define('_ENTERIMGURL','Geben Sie die URL des Bildes an, das Sie hinzufügen wollen.');
define('_ENTERIMGPOS','Nun geben Sie die Ausrichtung des Bildes an.');
define('_IMGPOSRORL',"'R' oder 'r' für rechts, 'L' oder 'l' für links, oder lassen Sie es leer.");
define('_ERRORIMGPOS','FEHLER! Geben Sie die Position des Bildes ein.');
define('_ENTEREMAIL','Geben Sie die eMail-Adresse ein, die Sie hinzufügen wollen.');
define('_ENTERCODE','Geben Sie den Code ein, den Sie hinzufügen wollen.');
define('_ENTERQUOTE','Geben Sie den Text ein, den Sie zitieren wollen.');
define('_ENTERTEXTBOX','Bitte geben Sie Text in die Textbox ein.');
define('_ALLOWEDCHAR','Maximal erlaubte Zeichen-Länge: ');
define('_CURRCHAR','Aktuelle Zeichen-Länge: ');
define('_PLZCOMPLETE','Bitte füllen Sie Betreff- UND Nachrichten-Feld aus.');
define('_MESSAGETOOLONG','Ihre Naxchricht ist zu lang.');

//%%%%%		TIME FORMAT SETTINGS   %%%%%
define('_SECOND', '1 Sekunde');
define('_SECONDS', '%s Sekunden');
define('_MINUTE', '1 Minute');
define('_MINUTES', '%s Minuten');
define('_HOUR', '1 Stunde');
define('_HOURS', '%s Stunden');
define('_DAY', '1 Tag');
define('_DAYS', '%s Tage');
define('_WEEK', '1 Woche');
define('_MONTH', '1 Monat');

define('_DATESTRING','Y/n/j G:i:s');
define('_MEDIUMDATESTRING','Y/n/j G:i');
define('_SHORTDATESTRING','Y/n/j');
/*
The following characters are recognized in the format string:
a - 'am' or 'pm'
A - 'AM' or 'PM'
d - day of the month, 2 digits with leading zeros; i.e. '01' to '31'
D - day of the week, textual, 3 letters; i.e. 'Fri'
F - month, textual, long; i.e. 'January'
h - hour, 12-hour format; i.e. '01' to '12'
H - hour, 24-hour format; i.e. '00' to '23'
g - hour, 12-hour format without leading zeros; i.e. '1' to '12'
G - hour, 24-hour format without leading zeros; i.e. '0' to '23'
i - minutes; i.e. '00' to '59'
j - day of the month without leading zeros; i.e. '1' to '31'
l (lowercase 'L') - day of the week, textual, long; i.e. 'Friday'
L - boolean for whether it is a leap year; i.e. '0' or '1'
m - month; i.e. '01' to '12'
n - month without leading zeros; i.e. '1' to '12'
M - month, textual, 3 letters; i.e. 'Jan'
s - seconds; i.e. '00' to '59'
S - English ordinal suffix, textual, 2 characters; i.e. 'th', 'nd'
t - number of days in the given month; i.e. '28' to '31'
T - Timezone setting of this machine; i.e. 'MDT'
U - seconds since the epoch
w - day of the week, numeric, i.e. '0' (Sunday) to '6' (Saturday)
Y - year, 4 digits; i.e. '1999'
y - year, 2 digits; i.e. '99'
z - day of the year; i.e. '0' to '365'
Z - timezone offset in seconds (i.e. '-43200' to '43200')
*/


//%%%%%		LANGUAGE SPECIFIC SETTINGS   %%%%%
// ====| Getting Ready For Unicode (UTF-8) conversion - hyperclock |==== \\
#define('_CHARSET', 'ISO-8859-1');
##define('_CHARSET', 'UTF-8');
##define('_LANGCODE', 'de');
define('_CHARSET', empty($xlanguage["charset"])?'UTF-8':$xlanguage["charset"]);
define('_LANGCODE', empty($xlanguage["code"])?'de':$xlanguage["code"]);
$xlanguage['charset_base'] = "UTF-8";

// change 0 to 1 if this language is a multi-bytes language
define('XOOPS_USE_MULTIBYTES', '0');

// Begin TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock
define('_TERMS','Nutzungsbedingungen');
define('_PRIVACY','Datenschutzerklärung');
define('_IMPRINT','Impressum');
define('_ABOUT','Über Uns');
define('_DISCLAIMER','Haftungsausschluss');
// End TERMS,PRIVACY,IMPRINT,DISCLAIMER & ABOUT US - hyperclock

// Add By PinMaster
define('_US_VALID_CAPTCHA', 'Verifizierung CAPTCHA gültig.');
define('_US_ERROR_CAPTCHA', 'Sie haben einen Fehler bei der Eingabe der Codes gemacht!');
define('_US_CAPTCHA', 'Bitte geben Sie den Code im Bild unten ein');
define('_US_CAPTCHA_GD2', 'GD2 Erweiterung für PHP muss installiert werden');
define('_US_CAPTCHA_GD2STP', 'Das SICHERHEIT CAPTCHA wird ignoriert');

?>
