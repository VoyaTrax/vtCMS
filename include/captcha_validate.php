<?php
/***************************************************************/
/* Adaptation system and fusion by Kern Christian (PinMaster)  */
/* Last Updated: 18-03-06									   */
/***************************************************************/
if(! is_object($member_handler)) {
	$member_handler =& xoops_gethandler('member');
	$xoopsUser =& $member_handler->getUser($_SESSION['xoopsUserId']);
}

if(! is_object($config_handler)) {
	$config_handler =& xoops_gethandler('config');
}

$xoopsConfigCaptcha =& $config_handler->getConfigsByCat(XOOPS_CONF_CAPTCHA);

if ($xoopsConfigCaptcha['captcha_active'])
{
    $allowed = false;
    if (is_object($xoopsUser)) {
        foreach ($xoopsUser->getGroups() as $group) {
            if (in_array($group, $xoopsConfigCaptcha['captcha_active_okgrp']) || XOOPS_GROUP_ANONYMOUS == $group) {
                $allowed = true;
                break;
            }
        }
    } else {
        $allowed = true;
    }
    if ($allowed) {
		$sUserCode = $_POST['user_captcha'];
		if ($xoopsConfigCaptcha['captcha_insensitive']) {
			$sUserCode = strtoupper($sUserCode);
		}
	
		if (md5($sUserCode) == $_SESSION[$ConfigCaptcha['captcha_session']]) {
			// clear to prevent re-use
			$_SESSION[$ConfigCaptcha['captcha_session']] = '';
		} else {
			redirect_header(XOOPS_URL, 3, _US_ERROR_CAPTCHA);
			exit();
		}
	}
    unset($allowed, $group);
}

?>