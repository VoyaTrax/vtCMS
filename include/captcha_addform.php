<?php
/***************************************************************/
/* Adaptation system and fusion by Kern Christian (PinMaster)  */
/* Last Updated: 23-03-06									   */
/***************************************************************/
function addCaptcha(& $add_form)
{
	$member_handler =& xoops_gethandler('member');
	$xoopsUser =& $member_handler->getUser($_SESSION['xoopsUserId']);

	if(! is_object($config_handler))
	{
		$config_handler =& xoops_gethandler('config');
	}
	$xoopsConfigCaptcha =& $config_handler->getConfigsByCat(XOOPS_CONF_CAPTCHA);
	
	if ($xoopsConfigCaptcha['captcha_active'])
	{
	    $allowed = false;
	    if (is_object($xoopsUser)) {
	        foreach ($xoopsUser->getGroups() as $group) {
	            if (in_array($group, $xoopsConfigCaptcha['captcha_active_okgrp']) || XOOPS_GROUP_ANONYMOUS == $group) {
	                $allowed = true;
	                break;
	            }
	        }
	    } else {
	        $allowed = true;
	    }
	    if ($allowed) {
				$add_form->addElement(new PhpCaptcha(_US_CAPTCHA, $xoopsConfigCaptcha));
		}
	    unset($allowed, $group);
	}
}

?>